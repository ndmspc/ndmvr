// create th2 demo histogram
import { createHistogram } from '@ndmspc/react-ndmspc-core';

const createDemoTH3Histogram = () => {
  const histo = createHistogram('TH3I', 20, 20, 20)
  let cnt = 0;

  for (let iz=1; iz<=20; iz++)
    for (let iy=1; iy<=20; iy++)
      for (let ix=1; ix<=20; ix++) {
        const bin = histo.getBin(ix, iy, iz)
        let val = 0
        val=cnt;
        histo.setBinContent(bin, val);
        cnt+=0.0005;
      }
  histo.fXaxis.fTitle = 'x Axis'
  histo.fYaxis.fTitle = 'y Axis'
  histo.fZaxis.fTitle = 'z Axis'
  histo.fName = "You don't have a valid file path"
  histo.fTitle = "This is a TH3 histogram demo"
  histo.fMaximum = cnt

  return histo
}

// create th2 demo histogram
const createDemoTH2Histogram = () => {
  const histo = createHistogram('TH2I', 20, 20)
  let cnt = 0;
  for (let iy=1; iy<=20; iy++)
    for (let ix=1; ix<=20; ix++) {
      let bin = histo.getBin(ix, iy), val = 0
      val=cnt;
      histo.setBinContent(bin, val);
      cnt+=0.1;
    }
  histo.fXaxis.fTitle = 'x Axis'
  histo.fYaxis.fTitle = 'y Axis'
  histo.fName = "You don't have a valid file path"
  histo.fTitle = "This is a TH2 histogram demo"
  histo.fMaximum = cnt

  return histo
}

const createDemoTH3Example = () => {
  const histo = createHistogram('TH3I', 30, 30, 30)

  for (let iz=1; iz<=30; iz++)
    for (let iy=1; iy<=30; iy++)
      for (let ix=1; ix<=30; ix++) {
        const bin = histo.getBin(ix, iy, iz)
        let val=getRandomArbitrary(0,20);
        histo.setBinContent(bin, val);
      }
  histo.fXaxis.fTitle = 'x Axis'
  histo.fYaxis.fTitle = 'y Axis'
  histo.fZaxis.fTitle = 'z Axis'
  histo.fName = "Example"
  histo.fTitle = "TH3 histogram"
  histo.fMaximum = 50

  return histo
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

export { createDemoTH3Example }
export { createDemoTH2Histogram }
export { createDemoTH3Histogram }
