import './patternfly.css'
import './patternfly-addons.css'

import React, { useEffect } from 'react'
import { HashRouter as Router, Navigate, Route, Routes } from 'react-router-dom'

import {
  localStore,
  NdmSpcContext,
  redirectStore,
  WebSocketStreamBroker
} from '@ndmspc/react-ndmspc-core'

import Navbar from './components/Navbar/Navbar.jsx'
import AframeReactTH3 from './pages/AframeReactTH3'
import THProjection from './pages/THProjection'
import THProjectionAna from './pages/THProjectionAna'
import THAdaptation from './pages/THAdaptation'
import AframeReactShared from './pages/AframeReactShared'

const App = () => {
  // ndmvrContext initialize
  // const contextValue = {
  //     ndmvrCameraPosRot: localStore('ndmvrCameraPosRot'),
  //     ndmvrInputDevice: redirectStore('ndmvrInputDevice'),
  //     ndmvrSchema: redirectStore('ndmvrSchema'),
  //     ndmvrFullscreen: redirectStore('ndmvrFullscreen')
  // }

  //At initialization custom a-frame components are registered
  // useEffect(() => {
  //     registerVRModeDetector(contextValue.ndmvrInputDevice.setData)
  //     registerScreenControlsComponent()
  //     RegisterVerticalMoveComponent()
  // }, [])

  return (
    <>
      <Router>
        {/*<NdmVrContext.Provider value={contextValue}>*/}
        <NdmSpcContext.Provider
          value={{
            shared: WebSocketStreamBroker()
          }}
        >
          <>
            <Navbar />
            <Routes>
              <Route
                exact
                path="/aframe/th3demo"
                element={<AframeReactTH3 />}
              />
              <Route
                exact
                path="/aframe/gitlabProjection"
                element={
                  <THProjection
                    filename={
                      'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root'
                    }
                  />
                }
              />
              <Route
                exact
                path="/aframe/physicsProjectionAna"
                element={
                  <THProjectionAna
                    filename={
                      // 'https://eos.ndmspc.io/eos/ndmspc/scratch/out.root'
                      'https://eos.ndmspc.io/eos/ndmspc/scratch/ndmspc/demo/rsn'
                    }
                  />
                }
              />
              <Route
                exact
                path="/aframe/physicsProjection"
                element={
                  <THProjection
                    filename={
                      'https://eos.ndmspc.io/eos/ndmspc/scratch/out.root'
                    }
                  />
                }
              />
              <Route
                exact
                path="/aframe/thAdaptation"
                element={<THAdaptation />}
              />
              <Route
                exat
                path="/aframe/sharedProjection"
                element={<AframeReactShared />}
              />
              <Route
                path="/"
                element={<Navigate to="/aframe/physicsProjectionAna" />}
              />
            </Routes>
          </>
        </NdmSpcContext.Provider>
        {/*</NdmVrContext.Provider>*/}
      </Router>
    </>
  )
}

export default App
