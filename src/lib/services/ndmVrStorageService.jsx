/* object for manipulating local storage */

/** @module NdmVrStorageService */

/**
 * Servis pre ovládanie lokálneho úložiska.
 * Ukladanie a načítavanie dát o ofsetoch a histograme.
 * @class
 */
export class NdmVrStorageService {
  #storage
  #camera
  #selectedBins = []

  /**
   * Nastavenie referencií a potrebných identifikátorov.
   * Načítanie poľa označených binov z lokalneho úložiska, ak nejaké sú.
   * @return {void}
   */
  constructor() {
    this.#storage = window.localStorage
    this.#camera = document.getElementById('cameraRig')
    this.#selectedBins = this.getBinsFromLocalStorage()
  }

  /*
  initLocalStorage() {
    if (!this.loadProperty('TH2')) {
      this.storeProperty('TH2', {})
    }
    if (!this.loadProperty('TH3')) {
      this.storeProperty('TH3', {})
    }
    if (!this.#storage.getItem('rootFile')) {
      this.storeFilePath(
        'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root'
      )
    }
    this.storeTH2Offsets(1, 1, 6)
    this.storeTH3Offsets(1, 1, 1, 6)
  }
   */

  /**
   * Funkcia pre uloženie binu do zoznamu označených binov.
   * @param {Object} binData - Objekt obsahujúci všetky dáta o zvolenom bine
   * @return {void}
   */
  storeCurrentBinToLocalStorage(binData) {
    this.#storage.setItem('currentBin', JSON.stringify(binData))
  }

  getCurrentBinFromLocalStorage() {
    const currentBin = JSON.parse(this.#storage.getItem('currentBin'))
    if (currentBin === undefined || currentBin === '') {
      return null
    }
    return currentBin
  }

  /**
   * Funkcia pre načítanie aktuálnych informácií o histograme.
   * @param {string} property - Objekt označujúci typ histogramu, pre ktorý sa majú získať ofsety
   * @return {Object} - Objekt obsahujúci informácie o sekciách určitého histogramu
   */
  loadProperty(property) {
    return JSON.parse(this.#storage.getItem(property))
  }

  /**
   * Funkcia pre uloženie aktuálnych informácií o histograme.
   * @param {string} property - Objekt označujúci typ histogramu, pre ktorý sa majú získať ofsety
   * @param {Object} object - Objekt obsahujúci informácie o sekciách určitého histogramu
   * @return {void}
   */
  storeProperty(property, object) {
    this.#storage.setItem(property, JSON.stringify(object))
  }

  /*
  storeTH2HistogramLimits(histogram) {
    const object = this.loadProperty('TH2')
    object.binScales = this.#getBinTH2Scales(histogram)
    object.limits = {
      xOffset: histogram.fXaxis.fXmax / object.binScales.xWidth,
      yOffset: histogram.fYaxis.fXmax / object.binScales.yWidth
    }
    this.storeProperty('TH2', object)
  }

  storeTH3HistogramLimits(histogram) {
    const object = this.loadProperty('TH3')
    object.binScales = this.#getBinTH3Scales(histogram)
    object.limits = {
      xOffset: histogram.fXaxis.fXmax / object.binScales.xWidth,
      yOffset: histogram.fYaxis.fXmax / object.binScales.yWidth,
      zOffset: histogram.fZaxis.fXmax / object.binScales.zWidth
    }
    this.storeProperty('TH3', object)
  }

  storeHistogramLimits(type, histogram) {
    if (type.includes('TH3') || type.includes('th3')) {
      this.storeTH3HistogramLimits(histogram)
    } else if (type.includes('TH2') || type.includes('th2')) {
      this.storeTH2HistogramLimits(histogram)
    }
  }

  loadTH3Limits() {
    const object = this.loadProperty('TH3')
    return object.limits
  }

  loadTH2Limits() {
    const object = this.loadProperty('TH2')
    return object.limits
  }

  loadLimits(type) {
    if (type.includes('TH3') || type.includes('th3')) {
      return this.loadTH3Limits()
    } else if (type.includes('TH2') || type.includes('th2')) {
      return this.loadTH2Limits()
    }
  }
   */
  /*
  #getBinTH2Scales = (histogram) => {
    const xWidth = histogram.fXaxis.fXmax / histogram.fXaxis.fNbins
    const yWidth = histogram.fYaxis.fXmax / histogram.fYaxis.fNbins
    return {
      xWidth: xWidth,
      yWidth: yWidth
    }
  }

  #getBinTH3Scales = (histogram) => {
    const xcenter = histogram.fXaxis.GetBinCenter(1)
    const xmin = histogram.fXaxis.GetBinLowEdge(1)
    const xmax = xcenter + (xcenter - xmin)
    const xWidth = xmax - xmin
    const ycenter = histogram.fYaxis.GetBinCenter(1)
    const ymin = histogram.fYaxis.GetBinLowEdge(1)
    const ymax = ycenter + (ycenter - ymin)
    const yWidth = ymax - ymin
    const zcenter = histogram.fZaxis.GetBinCenter(1)
    const zmin = histogram.fZaxis.GetBinLowEdge(1)
    const zmax = zcenter + (zcenter - zmin)
    const zWidth = zmax - zmin
    return {
      xWidth: xWidth,
      yWidth: yWidth,
      zWidth: zWidth
    }
  }
   */

  /*
  loadBinScale(type) {
    if (type.includes('TH3') || type.includes('th3')) {
      const object = this.loadProperty('TH3')
      return object.binScales
    } else if (type.includes('TH2') || type.includes('th2')) {
      const object = this.loadProperty('TH2')
      return object.binScales
    }
  }
   */

  initHistogramData(histogram) {
    if (histogram._typename.includes('TH3')) {
      this.storeTH3HistogramLimits(histogram)
    } else if (histogram._typename.includes('TH2')) {
      this.storeTH2HistogramLimits(histogram)
    }
  }

  /**
   * Funkcia pre uloženie aktuálnych informácií o histograme TH2.
   * @param {number} xOffset - Hodnota reprezentujúca ofset na osi X
   * @param {number} yOffset - Hodnota reprezentujúca ofset na osi Y
   * @param {number} range - Hodnota reprezentujúca rozsah binov na osiach
   * @return {void}
   */
  storeTH2Offsets(xOffset, yOffset, range) {
    let object = this.loadProperty('TH2')
    let section
    if (object === null) {
      object = {}
      section = {
        name: 'TH2',
        xOffset: 1,
        yOffset: 1,
        range: 8
      }
    } else {
      section = {
        name: 'TH2',
        xOffset: xOffset,
        yOffset: yOffset,
        range: range
      }
    }
    object.section = section
    this.storeProperty('TH2', object)
  }

  /**
   * Funkcia pre uloženie aktuálnych informácií o histograme TH3.
   * @param {number} xOffset - Hodnota reprezentujúca ofset na osi X
   * @param {number} yOffset - Hodnota reprezentujúca ofset na osi Y
   * @param {number} zOffset - Hodnota reprezentujúca ofset na osi Z
   * @param {number} range - Hodnota reprezentujúca rozsah binov na osiach
   * @return {void}
   */
  storeTH3Offsets(xOffset, yOffset, zOffset, range) {
    let object = this.loadProperty('TH3')
    let section
    if (object === null) {
      object = {}
      section = {
        name: 'TH3',
        xOffset: 1,
        yOffset: 1,
        zOffset: 1,
        range: 4
      }
    } else {
      section = {
        name: 'TH3',
        xOffset: xOffset,
        yOffset: yOffset,
        zOffset: zOffset,
        range: range
      }
    }
    object.section = section
    this.storeProperty('TH3', object)
  }

  /**
   * Funkcia pre uloženie aktuálnych informácií o histograme.
   * @param {Object} section - Objekt obsahujúci ofsety a rozsah pre histogramy. Objekt obsahuje aj typ histogramu podľa ktorého sa uloží správna sekcia do lokálneho úložiska
   * @return {void}
   */
  storeOffsets(section) {
    if (section.name.includes('TH3') || section.name.includes('th3')) {
      this.storeTH3Offsets(
        section.xOffset,
        section.yOffset,
        section.zOffset,
        section.range
      )
    } else if (section.name.includes('TH2') || section.name.includes('th2')) {
      this.storeTH2Offsets(section.xOffset, section.yOffset, section.range)
    }
  }

  /**
   * Uloženie názvu root súboru.
   * @param {string} filePath - URL cesta k súboru
   * @return {void}
   */
  storeFilePath(filePath) {
    this.#storage.setItem('rootFile', filePath)
  }

  /**
   * Načítanie názvu root súboru.
   * @return {string} - URL root súboru
   */
  loadFilePath() {
    const filename = this.#storage.getItem('rootFile')
    if (filename === null || filename === undefined) {
      // throw new Error('The filename is empty')
      return 'defaultHistogram'
    }
    return filename
  }

  /**
   * Načítanie informácií o TH2 histograme
   * @return {Object} - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom uložisku vracia null
   */
  loadTH2Offsets() {
    const object = this.loadProperty('TH2')
    if (object !== null) {
      return object.section
    } else {
      return null
    }
  }

  /**
   * Načítanie informácií o TH3 histograme.
   * @return {Object} - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom uložisku vracia null
   */
  loadTH3Offsets() {
    const object = this.loadProperty('TH3')
    if (object !== null) {
      return object.section
    } else {
      return null
    }
  }

  /**
   * Načítanie informácií o histograme.
   * @param {string} type - Typ histogramu podľa, ktorého sa získa správny objekt obsahujúci informácie o histograme
   * @return {Object} - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom uložisku vracia null
   */
  loadOffsets(type) {
    if (type.includes('TH3') || type.includes('th3')) {
      return this.loadTH3Offsets()
    } else if (type.includes('TH2') || type.includes('th2')) {
      return this.loadTH2Offsets()
    }
  }

  /*
  #getValidOffset = (
    histogramType,
    limits,
    offsets,
    increment,
    axisOffset,
    defaultRange
  ) => {
    if (increment) {
      if (defaultRange) {
        offsets[axisOffset] += 1
      } else {
        offsets[axisOffset] += offsets.range + 1
      }
      if (limits[axisOffset] <= offsets.range) {
        offsets[axisOffset] = 1
        return offsets
      }
      if (offsets[axisOffset] > limits[axisOffset] - offsets.range - 1) {
        offsets[axisOffset] = limits[axisOffset] - offsets.range
        return offsets
      }
    } else {
      if (offsets[axisOffset] - offsets.range + 1 <= 1) {
        offsets[axisOffset] = 1
        return offsets
      }
      if (defaultRange) {
        offsets[axisOffset] -= 1
      } else {
        offsets[axisOffset] -= offsets.range + 1
      }
    }
    return offsets
  }
   */

  /*
  editTH2OffsetByOffset(axisOffset, increment, defaultRange) {
    const limits = this.loadTH2Limits()
    const offsets = this.loadTH2Offsets()
    const result = this.#getValidOffset(
      'TH2',
      limits,
      offsets,
      increment,
      axisOffset,
      defaultRange
    )
    // this.storeTH2Offsets(result.xOffset, result.yOffset, result.range)
    return result
  }
   */

  /*
  editTH3OffsetByOffset(axisOffset, increment, defaultRange) {
    const limits = this.loadTH3Limits()
    const offsets = this.loadTH3Offsets()
    const result = this.#getValidOffset(
      'TH3',
      limits,
      offsets,
      increment,
      axisOffset,
      defaultRange
    )
    // this.storeTH3Offsets(result.xOffset, result.yOffset, result.zOffset, result.range)
    return result
  }
   */

  // set offset value
  /*
  setOffsetValue = (typeName, range) => {
    if (typeName.includes('TH2')) {
      if (range > 16) {
        return 16
      } else if (range < 2) {
        return 2
      } else {
        return range
      }
    } else if (typeName.includes('TH3')) {
      if (range > 10) {
        return 10
      } else if (range < 2) {
        return 2
      } else {
        return range
      }
    }
  }
   */

  #deleteCurrentBin = (binData) => {
    return this.#selectedBins.filter((item) => item.id !== binData.id)
  }

  // check containing bin
  /**
   * Overenie, či je bin v poli označených binov.
   * @param {Object} binData - Objekt obsahujúci informácie o bine
   * @return {boolean} - Ak je True bin je v poli označených binov, ak False bin nie je v poli označených binov
   */
  containThisBin = (binData) => {
    const updatedArray = this.#selectedBins.filter(
      (item) => item.id === binData.id
    )
    return updatedArray.length !== 0
  }

  // store bin to local storage
  /**
   * Uloženie binu, uloženie do poľa označených binov.
   * @param {Object} binData - Objekt obsahujúci informácie o bine
   * @return {void}
   */
  storeBinToLocalStorage = (binData) => {
    // let bins = JSON.parse(this.#storage.getItem('currentBin'))
    if (!this.containThisBin(binData)) {
      this.#selectedBins.push(binData)
      this.#storage.setItem('selectedBins', JSON.stringify(this.#selectedBins))
      // window.dispatchEvent(
      //   new CustomEvent('localStorageAdd', { detail: { data: binData } })
      // )
    }
  }

  /**
   * Odstránenie binu, vymzanie binu z poľa označených binov.
   * @param {Object} binData - Objekt obsahujúci informácie o bine
   * @return {void}
   */
  deleteBinFromLocalStorage = (binData) => {
    // let bins = JSON.parse(this.#storage.getItem('currentBin'))
    if (this.containThisBin(binData)) {
      this.#selectedBins = this.#deleteCurrentBin(binData)
      this.#storage.setItem('selectedBins', JSON.stringify(this.#selectedBins))
      // window.dispatchEvent(
      //   new CustomEvent('localStorageRemove', { detail: { data: binData } })
      // )
    }
  }

  // get bin from local storage
  /**
   * Načítanie všetkých binov z poľa označených binov.
   * @return {Array} - Pole s označenými binmi
   */
  getBinsFromLocalStorage = () => {
    const selectedBins = JSON.parse(this.#storage.getItem('selectedBins'))
    if (selectedBins === null) {
      return []
    } else {
      return selectedBins
    }
  }
}

const localStorageService = new NdmVrStorageService()
export { localStorageService }
