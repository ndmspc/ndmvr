// service to manage camera
/** @module CameraService */

import {isObjectEmpty, useNdmVrRedirect} from "@ndmspc/react-ndmspc-core";

/**
 * Servis pre ovládanie a manipuláciu pozície kamery.
 * @class
 */
export class CameraService {
  #cameraRig
  #camera

  constructor() {
    this.#cameraRig = document.getElementById('cameraRig')
    this.#camera = document.getElementById('camera')
  }


  /**
   * Zmena vertikálnej polohy kamery.
   * @param {boolean} moveUp - Ak je True, kamera vyššie ak je False kamera nižšie
   * @param {number} speed - Rýchlosť zmeny pozície kamery
   * @return {void}
   */
  verticalMoveCamera = (moveUp, speed) => {
    // if we have not a cameraRig
    if (this.#cameraRig === null)
      this.#cameraRig = document.getElementById('cameraRig')
    if (this.#cameraRig !== null) {
      const targetPosition = new THREE.Vector3()
      const currentPosition = this.#cameraRig.object3D.position.clone() // naklonuj aktuálnu pozíciu

      if (moveUp) {
        targetPosition.copy(currentPosition).add(new THREE.Vector3(0, +speed, 0)) // nastav cieľovú pozíciu o `speed` jednotiek vyššie
        this.#cameraRig.object3D.position.lerp(targetPosition, 0.5) // 0.5 - koeficient interpolácie
      } else {
        if (this.#cameraRig.object3D.position.y > 3){
          targetPosition.copy(currentPosition).add(new THREE.Vector3(0, -speed, 0)) // nastav cieľovú pozíciu o `speed` jednotiek nižšie
          this.#cameraRig.object3D.position.lerp(targetPosition, 0.5) // 0.5 - koeficient interpolácie
        }
      }
    }
  }

  /**
   * Zmena horizontálnej polohy kamery.
   * @param {string} axis - Os po ktorej je ralizovaný pohyb kamery
   * @param {number} increment - Hodnota posunu kamery
   * @param {number} speed - Rýchlosť zmeny pozície kamery
   * @return {void}
   */
  horizontalMoveCamera = (axis, increment, speed) => {
    // if we have not a cameraRig
    if (this.#cameraRig === null)
      this.#cameraRig = document.getElementById('cameraRig')
    if (this.#cameraRig !== null) {
      if (increment) {
        this.#cameraRig.object3D.position[axis] += speed
      } else {
        this.#cameraRig.object3D.position[axis] -= speed
      }
    }
  }

  horizontalMoveCameraLocal = (joystickX, joystickY, movementSpeed) => {
    if (this.#cameraRig !== null) {
      let joystickVector = new THREE.Vector2(joystickX, joystickY)
      let elementRotation = this.#camera.object3D.rotation.y
      joystickVector.rotateAround(new THREE.Vector3(0,0), -elementRotation)
      joystickVector.normalize()
      joystickVector.multiplyScalar(movementSpeed)
      this.#cameraRig.object3D.position['x'] += joystickVector.x
      this.#cameraRig.object3D.position['z'] += joystickVector.y
    }
  }

  /**
   * Zmena horizontálnej polohy kamery.
   * @param {string} axis1 - Os x po ktorej je ralizovaný pohyb kamery
   * @param {string} axis2 - Os z po ktorej je ralizovaný pohyb kamery
   * @param {number} increment - Hodnota posunu kamery
   * @param {number} speed - Rýchlosť zmeny pozície kamery
   * @return {void}
   */

  horizontalDiagonalMoveCamera = (axis1, axis2, increment, speed) => {
    // if we have not a cameraRig
    if (this.#cameraRig === null)
      this.#cameraRig = document.getElementById('cameraRig')
    if (this.#cameraRig !== null) {
      // Calculate diagonal direction vector
      const diagonalDirection = new THREE.Vector3();
      diagonalDirection[axis1] = Math.cos(Math.PI/4);
      diagonalDirection[axis2] = Math.sin(Math.PI/4);
      diagonalDirection.normalize();
      // Move camera position diagonally
      if (increment) {
        this.#cameraRig.object3D.position.addScaledVector(diagonalDirection, speed);
      } else {
        this.#cameraRig.object3D.position.addScaledVector(diagonalDirection, -speed);
      }
    }
  }

  /**
   * Zmena vertikálnej polohy kamery.
   * @param {HTMLElement} elm - Cieľový element
   * @param {Object} offset - Ofsety zobrazenej sekcie
   * @param {number} rotation - Rotácia kamery
   * @return {void}
   */
  #setVerticalOffset = (elm, offset, rotation) => {
    if (this.#cameraRig !== null && elm !== null) {
      // this.#cameraRig.object3D.rotation.y = rotation
      const newXPos = elm.object3D.position.x + 1 * elm.object3D.position.x
      const newZPos = elm.object3D.position.z + 1 * elm.object3D.position.z
      const newYPos = offset
      // this.#cameraRig.object3D.position.x = newXPos + 1
      // this.#cameraRig.object3D.position.z = newZPos + 1
      // this.#cameraRig.object3D.position.y = newYPos
      this.#cameraRig.setAttribute(
        'animation',
        `property: position; to: ${newXPos + 1} ${newYPos} ${
          newZPos + 1
        }; dur: 100;`
      )
      this.#cameraRig.setAttribute(
        'animation__1',
        `property: rotation; to: ${
          this.#cameraRig.object3D.rotation.x
        } ${rotation} ${this.#cameraRig.object3D.rotation.z}; dur: 100;`
      )
    }
  }

  /**
   * Zmena vertikálnej polohy kamery podľa ofsetov.
   * @param {Object} offsets - Ofsety zobrazenej sekcie
   * @return {void}
   */
  setCameraPosition = (offsets) => {
    if (this.#cameraRig !== null) {
      let newZPos = this.#cameraRig.object3D.position.y
      if (offsets.zOffset !== undefined) {
        // this.#cameraRig.object3D.position.y = offsets.zOffset + 2
        newZPos = offsets.zOffset * 2
      }
      const newXPos = offsets.xOffset + offsets.xOffset
      const newYPos = offsets.yOffset + offsets.yOffset
      // this.#cameraRig.object3D.position.x = newXPos - 3
      // this.#cameraRig.object3D.position.z = newYPos - 3
      setTimeout(() => {
        this.#cameraRig.setAttribute(
          'animation',
          `property: position; to: ${newXPos - 3} ${newZPos} ${
            newYPos - 3
          }; dur: 40;`
        )
      }, 1800)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície.
   * @return {void}
   */
  setPredefinedDownPosition = () => {
    // get target element
    const labelElement = document.getElementById('downLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 0, 180)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedUpPosition = () => {
    // get target element
    const labelElement = document.getElementById('upLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 0, 0)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedRightPosition = () => {
    // get target element
    const labelElement = document.getElementById('rightLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 0, 90)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedLeftPosition = () => {
    // get target element
    const labelElement = document.getElementById('leftLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 0, 270)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedDownPositionWithOffset = () => {
    // get target element
    const labelElement = document.getElementById('downLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 10, 180)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedUpPositionWithOffset = () => {
    // get target element
    const labelElement = document.getElementById('upLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 10, 0)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedRightPositionWithOffset = () => {
    // get target element
    const labelElement = document.getElementById('rightLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 10, 90)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedLeftPositionWithOffset = () => {
    // get target element
    const labelElement = document.getElementById('leftLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 10, 270)
    }
  }

  /**
   * Retrieves current posRot (position, rotation).
   * Mind that rotation is in radians.*/
  getCurrentPosRot() {
    const camera = document.getElementById('camera')
    const cameraRig = document.getElementById('cameraRig')
    return {
      position: {
        x: camera.object3D.position.x + cameraRig.object3D.position.x,
        y: camera.object3D.position.y + cameraRig.object3D.position.y,
        z: camera.object3D.position.z + cameraRig.object3D.position.z
      },
      rotation: {
        x: camera.object3D.rotation.x + cameraRig.object3D.rotation.x,
        y: camera.object3D.rotation.y + cameraRig.object3D.rotation.y,
        z: camera.object3D.rotation.z + cameraRig.object3D.rotation.z
      }
    }
  }

  /**
   * Calculates default posRot(rotation, position) of camera,
   * based on currentValue of context,
   * mind that posRot is calculated only if passed posRot IS NULL!
   * @return posRot:
   *    position:
   *      x: number
   *      y: number
   *      z: number
   *    rotation:
   *      x: number
   *      y: number
   *      z: number
   */
  calculateDefaultPosRot(posRot, context) {
    if(isObjectEmpty(posRot)){
      const binWidth = 1
      const binScaleCoef = 2
      const range = context.states.range || 5;
      const xNBins = (context.data.histogram.fXaxis.fNbins) > range ? range : (context.data.histogram.fXaxis.fNbins)
      const yNBins = (context.data.histogram.fYaxis.fNbins) > range ? range : (context.data.histogram.fYaxis.fNbins)
      const zNBins = (context.data.histogram.fZaxis.fNbins) > range ? range : (context.data.histogram.fZaxis.fNbins)

      const xCenter = (((xNBins) * binWidth - 0.5) * (-2 * binWidth) - (binWidth * 3) / 4) / 2

      return {
        position: {
          x: xCenter,
          y: 8 + (yNBins / 2),
          z: -7
        },
        rotation: {
          x: -0.5,
          y: 3,
          z: 0
        }
      }    }
    if (posRot ) {
      return posRot
    }
  }

  /**
   * parses posRot map into string values
   * @param posRot
   * @return parsedPosRot:
   *            position: "x y z",
   *            rotation: "x y z"
   */
  parsePosition(posRot) {
    if(!posRot || posRot.position?.x === undefined){
      return
    }
    posRot.position = posRot.position.x + ' ' + posRot.position.y + ' ' + posRot.position.z
    return posRot
  }

  setCameraRotation(rotation){
    if (!this.#camera) return

    const rot = rotation ? rotation : {x: -0.4, y: 3, z: 0}
    this.#camera.setAttribute('look-controls', 'magicWindowTrackingEnabled', 'false');
    this.#camera.components['look-controls'].pitchObject.rotation.set(-0.9, 0, 0);
    this.#camera.components['look-controls'].yawObject.rotation.set(0, 3.14159265, 0);
    if (rot?.x) {
      this.#camera.components['look-controls'].pitchObject.rotation.set(rot.x, 0, 0);
    }
    if (rot?.y) {
      this.#camera.components['look-controls'].yawObject.rotation.set(0, rot.y, 0);
    }
  }

  getCamera(){
    return this.#camera;
  }



}
