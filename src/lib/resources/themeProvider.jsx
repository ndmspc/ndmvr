/** @module ThemeProvider */
/* object provides all necessary color palettes and schemes */
import palette from './theme'

/**
 * Provider pre zabezpečenie všetkých potrebných farieb a štýlov.
 * @class
 */
export default class ThemeProvider {
  /**
   * Konštruktor pre nastavenie všetkých potrebných atribútov objektu.
   * @param {Object} theme - Objekt obsahujúci všetky potrebné palety a údaje o témach a fontoch
   * @return {void}
   */
  constructor(theme) {
    if (palette[theme]) {
      // set selected theme
      this.xAxis = palette[theme].axis.x
      this.yAxis = palette[theme].axis.y
      this.zAxis = palette[theme].axis.z
      this.palette = palette[theme].colorPalette
      this.primaryAccent = palette[theme].accent.primary
      this.secondaryAccent = palette[theme].accent.secondary
      this.primaryFont = palette[theme].font.primary
      this.secondaryFont = palette[theme].font.secondary
    } else {
      // set default theme
      this.xAxis = palette.def.axis.x
      this.yAxis = palette.def.axis.y
      this.zAxis = palette.def.axis.z
      this.palette = palette.def.colorPalette
      this.primaryAccent = palette.def.accent.primary
      this.secondaryAccent = palette.def.accent.secondary
      this.primaryFont = palette.def.font.primary
      this.secondaryFont = palette.def.font.secondary
    }
  }

  /**
   * Získanie kompetentnej farby pre entitu osi.
   * @param {string} axis - Označenie osi
   * @return {string} - Vracia null alebo aktuálnu farbu pre entitu
   */
  getAxisColor(axis) {
    if (axis === 'x' || axis === 'X') return this.xAxis
    if (axis === 'y' || axis === 'Y') return this.yAxis
    if (axis === 'z' || axis === 'Z') return this.zAxis
    return null
  }

  /**
   * Získanie hlavnej farby pre zvýraznenie a označenie.
   * @return {string} - Vracia null alebo aktuálnu farbu pre entitu
   */
  getPrimaryAccentColor() {
    return this.primaryAccent
  }

  /**
   * Získanie sekundárnej farby pre zvýraznenie a označenie.
   * @return {string} - Vracia null alebo aktuálnu farbu pre entitu
   */
  getSecondaryAccentColor() {
    return this.secondaryAccent
  }

  /**
   * Získanie hlavnej farby písma.
   * @return {string} - Vracia null alebo aktuálnu farbu pre entitu
   */
  getPrimaryFontColor() {
    return this.primaryFont
  }

  /**
   * Získanie sekundárnej farby písma.
   * @return {string} - Vracia null alebo aktuálnu farbu pre entitu
   */
  getSecondaryFontColor() {
    return this.secondaryFont
  }

  // set bin colors get color
  /**
   * Získanie kompetentnej farby pre bin podľa obsahu binu.
   * @param {number} content - Hodnota definujúca obsah binu
   * @return {string} - Vracia null alebo aktuálnu farbu pre bin, ktorá náleží binu podľa obsahu
   */
  getBinColor(content) {
    const fullContent = 1

    let color = this.palette[0]

    if (content >= fullContent) {
      return this.palette[9]
    }

    const difference = 0.12
    let sum = difference

    // find right color
    for (let i = 0; i <= 9; i++) {
      if (content <= sum) {
        color = this.palette[i]
        break
      }
      sum += difference
    }
    // return right color from palette
    return color
  }
}
