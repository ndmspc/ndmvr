import NdmVrScene from './components/NdmVrScene.jsx'
import NdmVr from './components/NdmVr.jsx'
import HistogramTH2 from './components/AframeComponent'
import { NdmVrStorageService } from './services/ndmVrStorageService'
import Banner from './aframe/infoBanner'
import LeftController from './aframe/leftOculusController'
import RightController from './aframe/rightOculusController'
import defaultScheme from './resources/builtinShemas.json'
import { TabletContext } from './components/Tablet/TabletProvider'
import sourceSettingSubject from './observables/sourceSettingSubject'
import ThumbsticController from './aframe/thumbstickOculusController'
import registerVRModeDetector from './utils/VrModeDetector.jsx'
import registerScreenControlsComponent from "./utils/ScreenControls.jsx"
import RegisterVerticalMoveComponent from "./controllers/verticalMove.jsx"
import wasdControlsCustom from './aframe/wasdControlsCustom.js'
import Menu from '../lib/components/Menu/Menu.jsx'



// create services
const ndmVrStorage = new NdmVrStorageService()

// export
export {
  // react components
  NdmVrScene,
  NdmVr,
  // export aframe components
  Banner,
  LeftController,
  RightController,
  ThumbsticController,
  wasdControlsCustom,
  Menu,
  // context for used in custom tablet components
  TabletContext,
  HistogramTH2,
  // export necessary services
  ndmVrStorage,
  // rxjs bin data distributor
  sourceSettingSubject,
  defaultScheme,
  registerVRModeDetector,
  registerScreenControlsComponent,
  RegisterVerticalMoveComponent
}
