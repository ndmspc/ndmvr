import './mobileController.css'
import React, {useEffect} from "react";
import {CameraService} from "../services/cameraService.jsx";
import arrow from '../assets/ndmvr/mobileControls/arrow.png'
import joystick_base from '../assets/ndmvr/mobileControls/joystick-base.png';
import joystick_blue from '../assets/ndmvr/mobileControls/joystick-blue.png';

/**
 * Holds elements responsible for controlling user (camera)
 * */
const MobileController = () => {
   let upClickInterval;
   let downClickInterval;
   const cameraService = new CameraService();

   function upClickFunction(){
      cameraService.verticalMoveCamera(false, 0.15)
   }
   function downClickFunction(){
      cameraService.verticalMoveCamera(true, 0.15)
   }

   function startUpClick() {
      upClickFunction();
      upClickInterval = setInterval(upClickFunction, 10);
   }
   function endUpClick() {
      clearInterval(upClickInterval);
   }
   function startDownClick() {
      downClickFunction();
      downClickInterval = setInterval(downClickFunction, 10);
   }
   function endDownClick() {
      clearInterval(downClickInterval);
   }

   /**
    * Sets event listener for move up button.
    * When user clicks/holds this button, verticalMoveCamera
    * from cameraService with frequency 100ms is called to move camera up
    * */
   const setUpArrowListener = () => {
      const arrowElement = document.getElementById("up-arrow");
      if(arrowElement){
         arrowElement.addEventListener('touchstart', startUpClick)
         arrowElement.addEventListener('touchend', endUpClick)
      } else {
         setTimeout(setUpArrowListener, 100);
      }
   }

   /**
    * Sets event listener for move down button.
    * When user clicks/holds this button, verticalMoveCamera
    * from cameraService with frequency 100ms is called to move camera down
    * */
   const setDownArrowListener = () => {
      const arrowElement = document.getElementById("down-arrow");
      if(arrowElement){
         arrowElement.addEventListener('touchstart', startDownClick)
         arrowElement.addEventListener('touchend', endDownClick)
      } else {
         setTimeout(setUpArrowListener, 100);
      }
   }

   const clearUpArrowListener = () => {
      const arrowElement = document.getElementById("up-arrow");
      if(arrowElement){
         arrowElement.removeEventListener('touchstart', startUpClick)
         arrowElement.removeEventListener('touchend', endUpClick)
      }
   }
   const clearDownArrowListener = () => {
      const arrowElement = document.getElementById("down-arrow");
      if(arrowElement){
         arrowElement.removeEventListener('touchstart', startDownClick)
         arrowElement.removeEventListener('touchend', endDownClick)
      }
   }

   /**
    * At initialization sets event listeners for vertical move button,
    * at end event listeners are removed
    * */
   useEffect(() => {
      setUpArrowListener();
      setDownArrowListener();
      return () => {
         clearUpArrowListener();
         clearDownArrowListener();
      }
   }, [])

   return (
      <div className="mainUI" id="uiDiv" onContextMenu={(e) => e.preventDefault()}>
         {/* top-left */}
         <div className="regionUI skyColor" style={{ top: '10px', left: '10px' }} onContextMenu={(e) => e.preventDefault()}>
         </div>

         {/* top-right */}
         <div className="regionUI" style={{ top: '10px', right: '10px' }}>
         </div>

         {/* bottom-left */}
         <div className="regionUI" style={{ bottom: '50px', left: '50px', flexDirection: 'column' }}>
            <div style={{flexDirection: 'row', display: 'flex', marginLeft: '-32px'}}>
               <div id="up-arrow" className="buttonUI" style={{width: '64px', height: '64px'}}>
                  <img style={{rotate: '90deg'}} src={arrow} alt="Up Arrow"/>
               </div>
               <div id="down-arrow" className="buttonUI" style={{width: '64px', height: '64px'}}>
                  <img style={{rotate: '270deg'}} src={arrow} alt="Down Arrow"/>
               </div>
            </div>
            <div className="buttonUI" style={{ width: '128px', opacity: 0.80 }}>
               <img src={joystick_base} alt="Joystick Base"/>
               <div id="stick1" style={{ position: 'absolute' }}>
                  <img src={joystick_blue} alt="Joystick Red"/>
               </div>
            </div>
         </div>

         {/* bottom-right */}
         <div className="regionUI baseColor" style={{ bottom: '10px', right: '10px' }}>
         </div>
      </div>
   )
}
export default MobileController