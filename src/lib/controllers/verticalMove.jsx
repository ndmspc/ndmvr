/**
 * Overrides onTouchStart/Move*/
export default function RegisterVerticalMoveComponent() {
   let originalLookControls = AFRAME.components['look-controls'];

   /**
    * Overrides the onTouchStart function.
    * Rotating vies works only if touch started:
    * portrait mode: right 2/3, upper 2/3
    * landscape mode: right 3/4, upper 1/2
    * */
   originalLookControls.Component.prototype.onTouchStart = function (evt) {
      if (!this.data.touchEnabled ||
         this.el.sceneEl.is('vr-mode') ||
         this.el.sceneEl.is('ar-mode')) {
         return;
      }
      evt.preventDefault();

      this.touchStart = {
         x: evt.changedTouches[(evt.changedTouches.length)-1].pageX,
         y: evt.changedTouches[(evt.changedTouches.length)-1].pageY,
         touchId: evt.changedTouches[(evt.changedTouches.length)-1].identifier
      };

      const screenWidth = this.el.sceneEl.canvas.clientWidth;
      const screenHeight = this.el.sceneEl.canvas.clientHeight;

      //portrait mode(vertical)
      if(screenHeight > screenWidth){
         if(this.touchStart.x > (screenWidth * (1/3)) ||
            this.touchStart.y < screenHeight * (2/3)){
            this.touchStarted = true;
         }
      } else { //landscape mode(horizontal)
         if(this.touchStart.x > (screenWidth * (1/4)) ||
            this.touchStart.y < screenHeight * (1/2)){
            this.touchStarted = true;
         }
      }
   }

   originalLookControls.Component.prototype.onTouchMove = function (evt) {
      if (this.touchStarted && this.data.touchEnabled) {
         evt.preventDefault();
         let index;
         for (let i = 0; i < evt.touches.length; i++) {
            if(evt.touches[i].identifier === this.touchStart.touchId){
               index = evt.touches[i].identifier;
               break;
            }
         }

         this.pitchObject.rotation.x -= 0.6 * Math.PI * (evt.touches[index].pageY - this.touchStart.y) / this.el.sceneEl.canvas.clientHeight;
         this.yawObject.rotation.y -= /*  */ Math.PI * (evt.touches[index].pageX - this.touchStart.x) / this.el.sceneEl.canvas.clientWidth;
         this.pitchObject.rotation.x = Math.max(Math.PI / -2, Math.min(Math.PI / 2, this.pitchObject.rotation.x));
         this.touchStart = {
            x: evt.touches[index].pageX,
            y: evt.touches[index].pageY,
            touchId: evt.touches[index].identifier
         };
      }
   };

   originalLookControls.Component.prototype.onTouchEnd = function (evt) {
      for (let i = 0; i < evt.touches.length; i++) {
         if(evt.touches[i].identifier === this.touchStart.touchId){
            return;
         }
      }
      this.touchStarted = false;
   }

}
