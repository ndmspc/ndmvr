/** @module HistogramReactFactory */
import 'aframe'
import ThemeProvider from '../resources/themeProvider'
/**
 * @typedef {object} Histogram
 * @property {string} _typename
 * @property {number} fUniqueID
 * @property {number} fBits
 * @property {string} fName
 * @property {string} fTitle
 * @property {number} fLineColor
 * @property {number} fLineStyle
 * @property {number} fLineWidth
 * @property {number} fFillColor
 * @property {number} fFillStyle
 * @property {number} fMarkerColor
 * @property {number} fMarkerStyle
 * @property {number} fMarkerSize
 * @property {number} fNcells
 * @property {number} fBarOffset
 * @property {number} fBarWidth
 * @property {number} fEntries
 * @property {number} fTsumw
 * @property {number} fTsumw2
 * @property {number} fTsumwx
 * @property {number} fTsumwx2
 * @property {number} fMaximum
 * @property {number} fMinimum
 * @property {number} fNormFactor
 * @property {string} fOption
 * @property {number} fBufferSize
 * @property {} fBuffer
 * @property {number} fBinStatErrOpt
 * @property {number} fStatOverflows
 * @property {number} fScalefactor
 * @property {number} fTsumwy
 * @property {number} fTsumwy2
 * @property {number} fTsumwxy
 */
/**
 * Vytvorenie objektu pre generovanie entít histogramu.
 * @class HistogramReactFactory - Histogram generator
 */
export default class HistogramReactFactory {
  //#region PRESETS
  #id = ''
  #selectedBins = []
  #histogram = null
  #type = ''
  #themeProvider = null
  // current offsets
  #xOffset = 1
  #yOffset = 1
  #zOffset = 1
  // current histogram limits
  #xLimit = 0
  #yLimit = 0
  #zLimit = 0
  // bin scales
  #xWidth = 1
  #yWidth = 1
  #zWidth = 1
  // range
  #range = 8
  // shift scene (shift to initial position)
  #labels = {
    xInitial: 0,
    yInitial: 0,
    zInitial: 0
  }
  // functions
  #viewFunction
  #progressFunction

  // constants
  #maximum = 1
  #contentScale = 1
  #minDisplayedContent = 0
  #binScaleCoef = 2

  #binHeightScale = 4

  // option for animation
  #shift = {
    xOffset: 0,
    yOffset: 0,
    zOffset: 0
  }

  // older elements
  #previousBins = []
  //#endregion PRESSETS ------------------------------------------

  /**
   * Inicializácia objektu pre vytváranie elementov histogramu.
   * @param {string} uniqueName - Identifikátor histogramu
   * @param {Histogram} histogram - Objekt histogramu určený na vizualizáciu
   * @param {Object} section - Objekt definujúci ofsety a rozsah histogramu
   * @param {number} range - Rozsah, ak sa nedefinujú ofsety ale len rozsah
   * @param {string} theme - string Definujúci názov témy, ktorá má byť použitá pri vizualizovaní
   * @param selectedBins
   * @param viewFunction
   * @param progressFunction
   * @return {void}
   */
  constructor(
    uniqueName,
    histogram,
    section,
    range,
    theme,
    selectedBins,
    viewFunction,
    progressFunction
  ) {
    this.initializeFactory(
      uniqueName,
      histogram,
      section,
      range,
      theme,
      selectedBins,
      viewFunction,
      progressFunction
    )
  }

  /**
   * Inicializácia objektu pre vytváranie elementov histogramu.
   * @param {string} uniqueName - Identifikátor histogramu
   * @param {Histogram} histogram - Objekt histogramu určený na vizualizáciu
   * @param {Object} section - Objekt definujúci ofsety a rozsah histogramu
   * @param {number} range - Rozsah, ak sa nedefinujú ofsety ale len rozsah
   * @param {string} theme - String definujúci názov témy, ktorá má byť použitá pri vizualizovaní
   * @param {Array} selectedBins - pole obsahujuce oznacene biny - identifikatory su zlozene z nazvu binov
   * @param {Function} viewFunction -
   * @param {Function} progressFunction -
   * @return {void}
   */
  initializeFactory(
    uniqueName,
    histogram,
    section,
    range,
    theme,
    selectedBins,
    viewFunction,
    progressFunction
  ) {
    this.setUniqueName(uniqueName)
    this.setTheme(theme)
    this.setHistogram(histogram)
    this.setHistogramContentMaximum(histogram.fMaximum)
    this.setSection(section)
    this.setRange(range)
    this.setSelectedBin(selectedBins)
    this.setViewFunction(viewFunction)
    this.setProgressFunction(progressFunction)
  }

  //#region UPDATE FUNCTIONS
  updateContentScale(contentScale) {
    this.#contentScale = contentScale
  }

  updateMinDisplayedContent(minValue) {
    if (minValue >= 0) this.#minDisplayedContent = minValue
  }
  //#endregion UPDATE FUNCTIONS ------------------------------------------

  //#region SETTERS
  /**
   * Nastaví nove unikátne meno pre histogram
   */
  setUniqueName(uniqueName) {
    this.#id = uniqueName
  }
  setHistogramContentMaximum(histogramMaximum) {
    this.#maximum = histogramMaximum > 0 ? histogramMaximum : 1
  }

  setSelectedBin(selectedBins) {
    this.#selectedBins = selectedBins ? selectedBins : []
  }

  setViewFunction(viewFunction) {
    this.#viewFunction = viewFunction
  }

  setProgressFunction(progressFunction) {
    this.#progressFunction = progressFunction
  }

  /**
   * Nastaví ofsety pre zobrazenie sekcie histogramu
   */
  setSection(section) {
    // set current section offsets
    if (section) {
      if (section.xOffset)
        this.#xOffset = this.#checkOffsetValue(section?.xOffset, this.#xLimit)

      if (section.yOffset && !histogram._typename.includes('TH1'))
        this.#yOffset = this.#checkOffsetValue(section?.yOffset, this.#yLimit)

      if (section.zOffset && histogram._typename.includes('TH3'))
        this.#zOffset = this.#checkOffsetValue(section?.zOffset, this.#zLimit)
    }
  }

  /**
   * Nastaví histogram a všetky potrebné atribúty
   * @param {Histogram} histogram
   */
  setHistogram(histogram) {
    this.#histogram = histogram
    this.#type = histogram?._typename

    // reset all properties
    this.#xOffset = 1
    this.#yOffset = 1
    this.#zOffset = 1
    // current histogram limits
    this.#xLimit = 0
    this.#yLimit = 0
    this.#zLimit = 0
    // bin scales
    this.#xWidth = 1
    this.#yWidth = 1
    this.#zWidth = 1

    // set current limits
    this.#xLimit = histogram?.fXaxis?.fNbins
    // bin scales
    this.#xWidth =
      (histogram?.fXaxis?.fXmax - histogram?.fXaxis?.fXmin) /
      histogram?.fXaxis?.fNbins
    // bin shift in the scene
    this.#labels.xInitial = histogram?.fXaxis?.fXmin

    this.#xOffset = this.#checkOffsetValue(this.#xOffset, this.#xLimit)

    // append axis for TH1
    if (!histogram?._typename?.includes('TH1')) {
      // set current limits
      this.#yLimit = histogram?.fYaxis?.fNbins
      // bin scales
      this.#yWidth =
        (histogram?.fYaxis?.fXmax - histogram?.fYaxis?.fXmin) /
        histogram?.fYaxis?.fNbins
      // bin shift in the scene
      this.#labels.yInitial = histogram?.fYaxis?.fXmin
      this.#yOffset = this.#checkOffsetValue(this.#yOffset, this.#yLimit)
    }

    // append axis for TH3
    if (histogram?._typename?.includes('TH3')) {
      // set current limits
      this.#zLimit = histogram?.fZaxis?.fNbins
      // bin scales
      this.#zWidth = histogram?.fZaxis?.fXmax / histogram?.fZaxis?.fNbins
      // bin shift in the scene
      this.#labels.zInitial = histogram?.fZaxis?.fXmin

      this.#zOffset = this.#checkOffsetValue(this.#zOffset, this.#zLimit)
    }
    this.setHistogramContentMaximum(histogram?.fMaximum)
  }

  /**
   * Nastaví range pre požet zobrazených binoch v danom rozmere
   */
  setRange(range) {
    if (range) {
      if (range < 4) this.#range = 4
      this.#range = range
    }
  }

  /**
   * Nastaví tému pre vizualizáciu
   */
  setTheme(theme) {
    this.#themeProvider = new ThemeProvider(theme)
  }

  //#endregion SETTERS ------------------------------------------

  //#region GETTERS
  /**
   * Vracia atribút objektu offset x.
   * @return {number} offsetX - Offset na osi x
   */
  getXOffset = () => {
    return this.#xOffset
  }

  /**
   * Vracia atribút objektu offset y.
   * @return {number} offsetX - Offset na osi y
   */
  getYOffset = () => {
    return this.#yOffset
  }

  /**
   * Vracia atribút objektu offset y.
   * @return {number} offsetX - Offset na osi y
   */
  getZOffset = () => {
    return this.#zOffset
  }

  /**
   * Vracia atribút objektu rozsah skaly obsahu.
   * @return {number} contentScale - koeficient pre skalu rosahu obsahu
   */
  getContentScale = () => {
    return this.#contentScale
  }

  /**
   * Vracia atribút objektu range.
   * @return {number} range - Vracia rozsah sekcie
   */
  getRange = () => {
    return this.#range
  }

  /**
   * Funkcia mapuje hodnotu obsahu do určitého rozsahu.
   * @return {Elements} binContent - Vracia optimalizovaný obsah v potrebnom rozsahu
   */
  getElements() {
    // generate all necessary element
    let elements = {}
    if (this.#type.includes('TH2')) {
      elements = this.#createTH2Histogram()
    } else if (this.#type.includes('TH3')) {
      elements = this.#createTH3Histogram()
    } else if (this.#type.includes('TH1')) {
      elements = this.#createTH1Histogram()
    }

    return elements
    //return this.#createTH3Histogram()
  }
  //#endregion GETTERS ------------------------------------------

  //#region PRIVATE FUNCTIONS
  /**
   * Funkcia overí korektnosť hodnoty offsetu.
   * Porovná aktuálny upravený offset s limitom na danej osi.
   * @param {number} offsetValue - Hodnota offsetu, ktorú je potrebné overiť
   * @param {number} limit - Hodnota offsetu, ktorá predstavuje limit pre danú os.
   * @return {number} currOffset - Vracia upravený offset
   */
  #checkOffsetValue = (offsetValue, limit) => {
    let currOffset =
      offsetValue > limit - this.#range ? limit - this.#range : offsetValue

    if (currOffset < 1) {
      currOffset = 1
    }
    return currOffset
  }

  /**
   * Funkcia overí stav binu.
   * @param {string} binId - Identifikátor binu, ktorý je potrebné overiť
   * @return {boolean} state - Vracia True ak je bin v poli označených binov, True ak nie je v poli označených binov
   */
  #checkBinState = (binId) => {
    const updatedArray = this.#selectedBins.filter((item) => item.id === binId)
    return updatedArray.length !== 0
  }

  /**
   * Funkcia vytvorí entitu reprezentujúcu označenie na osi.
   * @param {string} id - Identifikátor, ktorým bude disponovať vytvorená entita
   * @param {string} axis - Identifikátor osi, ktorým bude disponovať vytvorená entita
   * @param {number} positionX - X pozícia entity
   * @param {number} positionY - Y pozícia entity
   * @param {number} positionZ - Z pozícia entity
   * @param {number} rotationX - X rotácia entity
   * @param {number} rotationY - Y rotácia entity
   * @param {number} rotationZ - Z rotácia entity
   * @param {number} width - Šírka entity
   * @param {number} height - Výška entity
   * @param {number} value - Hodnota definujúca časové omeškanie pre zobrazenie označenia v entite
   * @param {string} color - HEX hodnota farby entity
   * @param {string} fontColor - HEX hodnota farby fontu entity
   * @return {HTMLElement} entita - Vytvorená entita
   */
  #createAxisEntity = (
    id,
    axis,
    positionX,
    positionY,
    positionZ,
    rotationX,
    rotationY,
    rotationZ,
    width,
    height,
    value,
    color,
    fontColor
  ) => {
    return {
      key: id + this.#id,
      axis: axis,
      geometry: {
        primitive: 'plane',
        width: width,
        height: height
      },
      text: {
        color: fontColor,
        width: 4,
        height: 'auto'
      },
      labelHandler: {
        value: value,
        delay: 500
      },
      rotation: {
        x: rotationX,
        y: rotationY,
        z: rotationZ
      },
      material: {
        color: color
      },
      animation: {
        fromX: positionX - this.#shift.xOffset,
        fromY: positionY + this.#shift.zOffset,
        fromZ: positionZ + this.#shift.yOffset,
        toX: positionX,
        toY: positionY,
        toZ: positionZ
      }
    }
  }

  /**
   * Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH3.
   * @return {Elements} elements - Vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR
   */
  #createTH3Histogram = () => {
    // react elements to draw the histograms
    const elements = {
      type: 'TH3',
      bins: [],
      labels: [],
      banners: [],
      ground: {},
      titles: [],
      axisLabels: []
    }
    // variables for rendering the histogram
    let xcenter, xmin, xmax, xwidth
    let ycenter, ymin, ymax, ywidth
    let zcenter, zmin, zmax, zwidth
    const binName = ''
    let c
    let count = 0

    // load th3 limits from localstorage
    // const binScale = ndmVrStorage.loadBinScale('TH3')
    // console.log(binScale)

    const maxXLength =
      this.#xOffset + this.#range > this.#xLimit
        ? this.#xLimit
        : this.#xOffset + this.#range
    const maxYLength =
      this.#yOffset + this.#range > this.#yLimit
        ? this.#yLimit
        : this.#yOffset + this.#range
    const maxZLength =
      this.#zOffset + this.#range > this.#zLimit
        ? this.#zLimit
        : this.#zOffset + this.#range

    const xTitle = this.#histogram.fXaxis.fTitle
    const yTitle = this.#histogram.fYaxis.fTitle
    const zTitle = this.#histogram.fZaxis.fTitle
    const xLabels = this.#histogram.fXaxis.fLabels
      ? this.#histogram.fXaxis.fLabels.arr
      : []
    const yLabels = this.#histogram.fYaxis.fLabels
      ? this.#histogram.fYaxis.fLabels.arr
      : []
    const zLabels = this.#histogram.fZaxis.fLabels
      ? this.#histogram.fZaxis.fLabels.arr
      : []

    const currentBinScales = []

    // generate react elements of the bins
    for (let iz = this.#zOffset; iz <= maxZLength; iz++)
      for (let iy = this.#yOffset; iy <= maxYLength; iy++)
        for (let ix = this.#xOffset; ix <= maxXLength; ix++) {
          xcenter = this.#histogram.fXaxis.GetBinCenter(ix)
          xmin = this.#histogram.fXaxis.GetBinLowEdge(ix)
          xmax = xcenter + (xcenter - xmin)
          xwidth = xmax - xmin
          ycenter = this.#histogram.fYaxis.GetBinCenter(iy)
          ymin = this.#histogram.fYaxis.GetBinLowEdge(iy)
          ymax = ycenter + (ycenter - ymin)
          ywidth = ymax - ymin
          zcenter = this.#histogram.fZaxis.GetBinCenter(iz)
          zmin = this.#histogram.fZaxis.GetBinLowEdge(iz)
          zmax = zcenter + (zcenter - zmin)
          zwidth = zmax - zmin
          const binContent = this.#optimizeBinContent(
            this.#histogram.getBinContent(ix, iy, iz)
          )
          if (binContent <= this.#minDisplayedContent) {
            continue
          }
          c = this.#progressFunction
            ? this.#validateProgress(
                this.#progressFunction({
                  histogram: this.#histogram,
                  histogramId: this.#id,
                  x: ix,
                  y: iy,
                  z: iz
                })
              )
            : binContent

          // set a-frame entity box
          // set children component
          let size = c
          if (c < 0.15) size = 0.15

          let color = this.#viewFunction
            ? this.#viewFunction({
                histogram: this.#histogram,
                histogramId: this.#id,
                x: ix,
                y: iy,
                z: iz,
                content: c
              })
            : null
          color = color ? color : this.#themeProvider.getBinColor(c)
          // check bin status
          const id =
            this.#id +
            xmin +
            'th3' +
            xmax +
            'th3' +
            zmin +
            'th3' +
            zmax +
            'th3' +
            ymin +
            'th3' +
            ymax
          let markedColor
          if (this.#checkBinState(id)) {
            markedColor = this.#themeProvider.getSecondaryAccentColor()
          } else {
            markedColor = color
          }
          // fill bin data
          const binData = `id: ${id};
             histogramName: ${this.#id};
             typeName: 'TH3';
             content: ${c};
             color: ${color};
             yTitle: ${yTitle};
             xTitle: ${xTitle};
             zTitle: ${zTitle};
             absoluteContent: ${binContent};
             binName: ${binName};
             xMin: ${xmin};
             yMin: ${ymin};
             zMin: ${zmin};
             xMax: ${xmax};
             yMax: ${ymax};
             zMax: ${zmax};
             xPos: ${ix};
             yPos: ${iy};
             zPos: ${iz};
             xCenter: ${xcenter};
             yCenter: ${ycenter};
             zCenter: ${zcenter};
             xWidth: ${xwidth};
             yWidth: ${ywidth};
             zWidth: ${zwidth};
             markedColor: ${markedColor};
             selectColor: ${this.#themeProvider.getSecondaryAccentColor()};
             axisX: ${this.#themeProvider.getAxisColor('x')};
             axisY: ${this.#themeProvider.getAxisColor('y')};
             axisZ: ${this.#themeProvider.getAxisColor('z')}`

          const width = size * 0.8
          // TODO const posX = (ix - 1) / this.#xWidth
          // TODO const posY = (iz - 1) / this.#zWidth + 0.2
          // TODO const posZ = (iy - 1) / this.#yWidth
          const posX = -(ix - 1)
          const posY = iz - 1
          const posZ = iy - 1
          elements.bins[count] = {
            id: id,
            color: color,
            binData: binData,
            content: binContent,
            animation: {
              fromWidth: this.#getPreviousBinScales(id),
              fromHeight: this.#getPreviousBinScales(id),
              fromDepth: this.#getPreviousBinScales(id),
              toWidth: width,
              toHeight: width,
              toDepth: width
            },
            animation2: {
              // fromX: posX + this.#shift.xOffset,
              // fromY: posY + this.#shift.zOffset,
              // fromZ: posZ + this.#shift.yOffset,
              fromX: posX,
              fromY: posY,
              fromZ: posZ,
              toX: posX,
              toY: posY,
              toZ: posZ
            },
            animation3: {
              fromColor: this.#getPreviousBinColor(id),
              toColor: markedColor
            }
          }
          currentBinScales[count] = {
            key: id,
            scale: width,
            color: markedColor
          }
          count++
        }

    this.#previousBins = currentBinScales
    // create arrays for labels
    const normalAxis = []
    const reversedAxis = []

    // generate y axis
    count = this.#createYLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxYLength,
      maxXLength,
      yLabels,
      yTitle,
      0
    )

    // generate x axis
    count = this.#createXLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      xLabels,
      xTitle,
      count
    )

    // y axis
    count = this.#createZLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      maxZLength,
      zLabels,
      zTitle,
      count
    )

    // add histogram environment
    this.#createEnvironment(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      count
    )

    // add normal and reversed labels into elements
    elements.labels = [...normalAxis, ...reversedAxis]

    return elements
  }

  /**
   * @typedef {Object} Elements objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR
   * @prop {string} type
   * @prop {never[]} bins
   * @prop {never[]} labels
   * @prop {never[]} banners
   * @prop {Object} ground
   * @prop {never[]} titles
   * @prop {never[]} axisLabels
   */

  /**
   * Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH2.
   * @return {Elements} elements - vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR
   */
  #createTH2Histogram = () => {
    // react elements to draw the histogram

    /**
     * {@link Elements}
     * @const
     * @type {Elements}
     * @name elemntes
     */
    const elements = {
      type: 'TH2',
      bins: [],
      labels: [],
      banners: [],
      ground: {},
      titles: [],
      axisLabels: []
    }
    // variables for rendering the histogram
    let centeredYPosition
    let xcenter, xmin, xmax, xwidth
    let ycenter, ymin, ymax, ywidth
    let c
    const binName = ''
    let count = 0

    // bin attributes
    // const binScale = ndmVrStorage.loadBinScale('TH2')
    // max length
    const maxXLength =
      this.#xOffset + this.#range > this.#histogram.fXaxis.fNbins
        ? this.#histogram.fXaxis.fNbins
        : this.#xOffset + this.#range
    const maxYLength =
      this.#yOffset + this.#range > this.#histogram.fYaxis.fNbins
        ? this.#histogram.fYaxis.fNbins
        : this.#yOffset + this.#range
    const xTitle =
      this.#histogram.fXaxis.fTitle !== '' ? this.#histogram.fXaxis.fTitle : 'x'
    const yTitle =
      this.#histogram.fYaxis.fTitle !== '' ? this.#histogram.fYaxis.fTitle : 'y'
    const xLabels = this.#histogram.fXaxis.fLabels
      ? this.#histogram.fXaxis.fLabels.arr
      : []
    const yLabels = this.#histogram.fYaxis.fLabels
      ? this.#histogram.fYaxis.fLabels.arr
      : []
    // reset previous bins
    const currentBinScales = []

    // generate react elements of the bins
    for (let iy = this.#yOffset; iy <= maxYLength; iy++) {
      for (let ix = this.#xOffset; ix <= maxXLength; ix++) {
        xcenter = this.#histogram.fXaxis.GetBinCenter(ix)
        xmin = this.#histogram.fXaxis.GetBinLowEdge(ix)
        xmax = xcenter + (xcenter - xmin)
        xwidth = xmax - xmin
        ycenter = this.#histogram.fYaxis.GetBinCenter(iy)
        ymin = this.#histogram.fYaxis.GetBinLowEdge(iy)
        ymax = ycenter + (ycenter - ymin)
        ywidth = ymax - ymin
        const binContent = this.#optimizeBinContent(
          this.#histogram.getBinContent(ix, iy)
        )
        if (binContent <= this.#minDisplayedContent) {
          continue
        }
        c = this.#progressFunction
          ? this.#validateProgress(
              this.#progressFunction({
                histogram: this.#histogram,
                histogramId: this.#id,
                x: ix,
                y: iy
              })
            )
          : binContent
        centeredYPosition =
          (c * this.#contentScale) / (this.#binHeightScale * 2)

        // set a-frame entity box
        // set children component
        let color = this.#viewFunction
          ? this.#viewFunction({
              histogram: this.#histogram,
              histogramId: this.#id,
              x: ix,
              y: iy,
              content: c
            })
          : null
        color = color ? color : this.#themeProvider.getBinColor(c)

        // check bin status
        const id = this.#id + xmin + 'th2' + xmax + 'th2' + ymin + 'th2' + ymax
        let markedColor
        if (this.#checkBinState(id)) {
          markedColor = this.#themeProvider.getSecondaryAccentColor()
        } else {
          markedColor = color
        }
        // fill bin data
        // console.log(count, ix,xmin,xmax,xcenter)
        const binData = `id: ${id};
           histogramName: ${this.#id};
           typeName: 'TH2';
           content: ${c};
           color: ${color};
           yTitle: ${yTitle};
           xTitle: ${xTitle};
           zTitle: 'none';
           absoluteContent: ${binContent};
           binName: ${binName};
           xMin: ${xmin};
           yMin: ${ymin};
           zMin: ${0};
           xMax: ${xmax};
           yMax: ${ymax};
           zMax: ${0};
           xPos: ${ix};
           yPos: ${iy};
           zPos: ${0};
           xCenter: ${xcenter};
           yCenter: ${ycenter};
           yCenter: ${0};
           xWidth: ${xwidth};
           yWidth: ${ywidth};
           zWidth: ${0};
           markedColor: ${markedColor};
           selectColor: ${this.#themeProvider.getSecondaryAccentColor()};
           axisX: ${this.#themeProvider.getAxisColor('x')};
           axisY: ${this.#themeProvider.getAxisColor('y')};
           axisZ: ${this.#themeProvider.getAxisColor('z')}`

        const width = xwidth / this.#xWidth / this.#binScaleCoef
        const depth = ywidth / this.#yWidth / this.#binScaleCoef
        const posX = -(ix - 1)
        const posY = iy - 1
        elements.bins[count] = {
          id: id,
          color: color,
          binData: binData,
          content: binContent,
          animation: {
            fromWidth: width,
            fromHeight: this.#getPreviousBinScales(id),
            fromDepth: depth,
            toWidth: width,
            toHeight: (c * this.#contentScale) / this.#binHeightScale,
            toDepth: depth
          },
          animation2: {
            // fromX: posX - this.#shift.xOffset,
            // fromY: centeredYPosition,
            // fromZ: posY - this.#shift.yOffset,
            fromX: posX,
            fromY: centeredYPosition,
            fromZ: posY,
            toX: posX,
            toY: centeredYPosition,
            toZ: posY
          },
          animation3: {
            fromColor: this.#getPreviousBinColor(id),
            toColor: markedColor
          }
        }
        currentBinScales[count] = {
          key: id,
          scale: c,
          color: markedColor
        }
        count++
      }
    }
    this.#previousBins = currentBinScales
    // create arrays for labels
    const normalAxis = []
    const reversedAxis = []

    // set axis
    count = this.#createYLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxYLength,
      maxXLength,
      yLabels,
      yTitle,
      0
    )

    // generate x axis
    // add x label
    count = this.#createXLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      xLabels,
      xTitle,
      count
    )

    // generate y axis
    count = this.#createZLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      undefined,
      undefined,
      undefined,
      count
    )

    // add histogram environment
    this.#createEnvironment(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      count
    )

    // add normal and reversed labels into elements
    elements.labels = [...normalAxis, ...reversedAxis]

    return elements
  }

  /**
   * Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH1.
   * @return {Elements} elements - vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR
   */
  #createTH1Histogram = () => {
    // react elements to draw the histogram
    const elements = {
      type: 'TH1',
      bins: [],
      labels: [],
      banners: [],
      ground: {},
      titles: [],
      axisLabels: []
    }
    // variables for rendering the histogram
    let centeredYPosition
    let xcenter, xmin, xmax, xwidth
    let c
    const binName = ''
    let count = 0
    // bin attributes
    // const binScale = ndmVrStorage.loadBinScale('TH1')
    // max length
    const maxXLength =
      this.#xOffset + this.#range > this.#histogram.fXaxis.fNbins
        ? this.#histogram.fXaxis.fNbins
        : this.#xOffset + this.#range
    const maxYLength = 1
    const xTitle =
      this.#histogram.fXaxis.fTitle !== '' ? this.#histogram.fXaxis.fTitle : 'x'
    const xLabels = this.#histogram.fXaxis.fLabels
      ? this.#histogram.fXaxis.fLabels.arr
      : []
    // reset previous bins
    const currentBinScales = []

    // generate react elements of the bins
    for (let ix = this.#xOffset; ix <= maxXLength; ix++) {
      xcenter = this.#histogram.fXaxis.GetBinCenter(ix)
      xmin = this.#histogram.fXaxis.GetBinLowEdge(ix)
      xmax = xcenter + (xcenter - xmin)
      xwidth = xmax - xmin
      const binContent = this.#optimizeBinContent(
        this.#histogram.getBinContent(ix)
      )
      if (binContent <= this.#minDisplayedContent) {
        continue
      }
      c = this.#progressFunction
        ? this.#validateProgress(
            this.#progressFunction({
              histogram: this.#histogram,
              histogramId: this.#id,
              x: ix
            })
          )
        : binContent
      centeredYPosition = (c * this.#contentScale) / (this.#binHeightScale * 2)

      // set a-frame entity box
      // set children component
      let color = this.#viewFunction
        ? this.#viewFunction({
            histogram: this.#histogram,
            histogramId: this.#id,
            x: ix,
            content: c
          })
        : null
      color = color ? color : this.#themeProvider.getBinColor(c)

      // check bin status
      const id = this.#id + xmin + 'th1' + xmax
      let markedColor
      if (this.#checkBinState(id)) {
        markedColor = this.#themeProvider.getSecondaryAccentColor()
      } else {
        markedColor = color
      }
      // fill bin data
      // console.log(ix, xmin, xmax, xcenter)
      const binData = `id: ${id};
          histogramName: ${this.#id};
          typeName: 'TH1';
          content: ${c};
          xTitle: ${xTitle};
          zTitle: 'none';
          absoluteContent: ${binContent};
          binName: ${binName};
          xMin: ${xmin};
          zMin: ${0};
          xMax: ${xmax};
          zMax: ${0};
          xPos: ${ix};
          zPos: ${0};
          xCenter: ${xcenter};
          yCenter: ${0};
          xWidth: ${xwidth};
          zWidth: ${0};
          markedColor: ${markedColor};
          selectColor: ${this.#themeProvider.getSecondaryAccentColor()};
          axisX: ${this.#themeProvider.getAxisColor('x')};
          axisY: ${this.#themeProvider.getAxisColor('y')};
          axisZ: ${this.#themeProvider.getAxisColor('z')}`

      const width = xwidth / this.#xWidth / this.#binScaleCoef
      const posX = -(ix - 1)
      const posY = 0
      elements.bins[count] = {
        id: id,
        color: color,
        binData: binData,
        content: binContent,
        animation: {
          fromWidth: width,
          fromHeight: this.#getPreviousBinScales(id),
          fromDepth: width,
          toWidth: width,
          toHeight: (c * this.#contentScale) / this.#binHeightScale,
          toDepth: width
        },
        animation2: {
          fromX: posX + this.#shift.xOffset,
          fromY: centeredYPosition,
          fromZ: posY,
          toX: posX,
          toY: centeredYPosition,
          toZ: posY
        },
        animation3: {
          fromColor: this.#getPreviousBinColor(id),
          toColor: markedColor
        }
      }
      currentBinScales[count] = {
        key: id,
        scale: c,
        color: markedColor
      }
      count++
    }
    this.#previousBins = currentBinScales
    // create arrays for labels
    const normalAxis = []
    const reversedAxis = []
    // generate y axis
    count = this.#createYLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxYLength,
      maxXLength,
      undefined,
      undefined,
      0
    )

    // generate x axis
    count = this.#createXLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      xLabels,
      xTitle,
      count
    )

    // generate y axis
    count = this.#createZLabels(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      undefined,
      undefined,
      undefined,
      count
    )

    // add histogram environment
    this.#createEnvironment(
      elements,
      normalAxis,
      reversedAxis,
      maxXLength,
      maxYLength,
      count
    )

    // add normal and reversed labels into elements
    elements.labels = [...normalAxis, ...reversedAxis]

    return elements
  }

  /**
   * Funkcia mapuje hodnotu obsahu do určitého rozsahu.
   * @param {number} binContent - Aktuálny obsah binu, ktorý obsahuje objekt histogramu
   * @return {number} binContent - Vracia optimalizovaný obsah v potrebnom rozsahu
   */
  #optimizeBinContent = (binContent) => {
    if (binContent <= 0) return 0
    if (this.#maximum > 1) {
      // round to 2 decimal numbers
      binContent =
        Math.round((binContent / this.#maximum + Number.EPSILON) * 100) / 100
    }
    return binContent
  }

  updateSection(axisOffset, increment, defaultRange) {
    if (this.#type.includes('TH3'))
      return this.#editTH3OffsetByOffset(axisOffset, increment, defaultRange)
    else if (this.#type.includes('TH2'))
      return this.#editTH2OffsetByOffset(axisOffset, increment, defaultRange)
    else if (this.#type.includes('TH1'))
      return this.#editTH1OffsetByOffset(axisOffset, increment, defaultRange)
  }

  /**
   * Funkcia upraví ofset, určí všetky podmienky a na základe nich vráti modifikované ofsety.
   * @param {Object} limits - Objekt, obsahujúci najvyššie povolené ofsety pre histogram
   * @param {Object} offsets - Objekt, obsahujúci všetky aktuálne ofsety pre histogram
   * @param {number} increment - Hodnota obsahujúca hodnotu, o ktoru sa modifikuje ofset
   * @param {string} axisOffset - Názov ofsetu, ktorý je potrebné modifikovať
   * @param {boolean} defaultRange - Ak je True, inkrement je predvolený (1), ak False použije sa hodnota parametra increment
   * @return {Object} result - Vracia objekt obsahujúci všetky aktualizované ofsety
   */
  #getValidOffset = (limits, offsets, increment, axisOffset, defaultRange) => {
    this.#shift = {
      xOffset: 0,
      yOffset: 0,
      zOffset: 0
    }
    if (increment) {
      if (defaultRange) {
        offsets[axisOffset] += 1
        this.#shift[axisOffset] = -1
      } else {
        offsets[axisOffset] += offsets.range
        this.#shift[axisOffset] = -offsets.range
      }
      if (limits[axisOffset] <= offsets.range) {
        offsets[axisOffset] = 1
        this.#shift[axisOffset] = 0
        return offsets
      }
      if (offsets[axisOffset] > limits[axisOffset] - offsets.range) {
        offsets[axisOffset] = limits[axisOffset] - offsets.range
        this.#shift[axisOffset] = 0
        return offsets
      }
    } else {
      if (defaultRange) {
        offsets[axisOffset] -= 1
        this.#shift[axisOffset] = 1
      } else {
        offsets[axisOffset] -= offsets.range
        this.#shift[axisOffset] = offsets.range
      }
      if (offsets[axisOffset] < 1) {
        offsets[axisOffset] = 1
        this.#shift[axisOffset] = 0
        return offsets
      }
    }
    return offsets
  }

  #createZLabels = (
    elements,
    normalAxis,
    reversedAxis,
    maxXLength,
    maxYLength,
    maxZLength,
    zLabels,
    zTitle,
    count
  ) => {
    normalAxis[count] = this.#createAxisEntity(
      90000 + count,
      'z',
      -(maxXLength + 0.5),
      this.#zOffset - 1,
      maxYLength + 0.5,
      270,
      360,
      0,
      2,
      2,
      `${
        zLabels
          ? zLabels[this.#zOffset - 1]
            ? zLabels[this.#zOffset - 1].fString
            : Math.round(
                (this.#labels.zInitial + this.#zOffset - 1 + Number.EPSILON) *
                  100
              ) / 100
          : '0'
      }`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    reversedAxis[count] = this.#createAxisEntity(
      91000 + count,
      'z',
      -(maxXLength + 0.5),
      this.#zOffset - 1,
      this.#yOffset - 2.5,
      270,
      180,
      0,
      2,
      2,
      `${
        zLabels
          ? zLabels[this.#zOffset - 1]
            ? zLabels[this.#zOffset - 1].fString
            : Math.round(
                (this.#labels.zInitial + this.#zOffset - 1 + Number.EPSILON) *
                  100
              ) / 100
          : '0'
      }`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    count++
    normalAxis[count] = this.#createAxisEntity(
      92000 + count,
      'z',
      -(this.#xOffset - 2.5),
      this.#zOffset - 1,
      maxYLength + 0.5,
      270,
      180,
      0,
      2,
      2,
      `${
        zLabels
          ? zLabels[this.#zOffset - 1]
            ? zLabels[this.#zOffset - 1].fString
            : Math.round(
                (this.#labels.zInitial + this.#zOffset - 1 + Number.EPSILON) *
                  100
              ) / 100
          : '0'
      }`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    reversedAxis[count] = this.#createAxisEntity(
      93000 + count,
      'z',
      -(this.#xOffset - 2.5),
      this.#zOffset - 1,
      this.#yOffset - 2.5,
      270,
      180,
      0,
      2,
      2,
      `${
        zLabels
          ? zLabels[this.#zOffset - 1]
            ? zLabels[this.#zOffset - 1].fString
            : Math.round(
                (this.#labels.zInitial + this.#zOffset - 1 + Number.EPSILON) *
                  100
              ) / 100
          : '0'
      }`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    count++
    // add y label
    if (zTitle) {
      elements.axisLabels[4] = {
        id: 'upLabel1',
        key: 95004 + this.#id,
        text: {
          value: zTitle,
          color: this.#themeProvider.getAxisColor('y')
        },
        rotation: {
          x: 0,
          y: 270,
          z: 90
        },
        animation: {
          fromX: this.#xOffset - 1.5 + this.#shift.xOffset,
          fromY: maxZLength - 1 + this.#shift.zOffset,
          fromZ: this.#yOffset - 2 + this.#shift.yOffset,
          toX: this.#xOffset - 1.5,
          toY: maxZLength - 1,
          toZ: this.#yOffset - 2
        }
      }

      elements.axisLabels[5] = {
        id: 'upLabel2',
        key: 100005 + this.#id,
        text: {
          value: zTitle,
          color: this.#themeProvider.getAxisColor('y')
        },
        rotation: {
          x: 0,
          y: 90,
          z: 90
        },
        animation: {
          fromX: maxXLength - 0.5 + this.#shift.xOffset,
          fromY: maxZLength - 1 + this.#shift.zOffset,
          fromZ: maxYLength + this.#shift.yOffset,
          toX: maxXLength - 0.5,
          toY: maxZLength - 1,
          toZ: maxYLength
        }
      }

      elements.axisLabels[6] = {
        id: 'upLabel3',
        key: 105006 + this.#id,
        text: {
          value: zTitle,
          color: this.#themeProvider.getAxisColor('y')
        },
        rotation: {
          x: 0,
          y: 360,
          z: 90
        },
        animation: {
          fromX: this.#xOffset - 2 + this.#shift.xOffset,
          fromY: maxZLength - 1 + this.#shift.zOffset,
          fromZ: maxYLength - 0.5 + this.#shift.yOffset,
          toX: this.#xOffset - 2,
          toY: maxZLength - 1,
          toZ: maxYLength - 0.5
        }
      }

      elements.axisLabels[7] = {
        id: 'upLabel4',
        key: 105007 + this.#id,
        text: {
          value: zTitle,
          color: this.#themeProvider.getAxisColor('y')
        },
        rotation: {
          x: 0,
          y: 180,
          z: 90
        },
        animation: {
          fromX: maxXLength - 0.5 + this.#shift.xOffset,
          fromY: maxZLength - 1 + this.#shift.zOffset,
          fromZ: this.#yOffset - 1.5 + this.#shift.yOffset,
          toX: maxXLength - 0.5,
          toY: maxZLength - 1,
          toZ: this.#yOffset - 1.5
        }
      }
    }
    return count
  }

  #createYLabels = (
    elements,
    normalAxis,
    reversedAxis,
    maxYLength,
    maxXLength,
    yLabels,
    yTitle,
    count
  ) => {
    for (let iy = this.#yOffset; iy <= maxYLength; iy++) {
      normalAxis[count] = this.#createAxisEntity(
        1500 + count,
        'y',
        -(this.#xOffset - 2.5),
        this.#zOffset ? this.#zOffset - 1 : 0,
        iy - 1,
        270,
        180,
        0,
        2,
        1,
        `${
          yLabels
            ? yLabels[iy - 1]
              ? yLabels[iy - 1].fString
              : Math.round(
                  (this.#labels.yInitial +
                    (iy - 1) * this.#yWidth +
                    Number.EPSILON) *
                    100
                ) / 100
            : ''
        }`,
        this.#themeProvider.getAxisColor('z'),
        this.#themeProvider.getSecondaryFontColor()
      )
      reversedAxis[count] = this.#createAxisEntity(
        3000 + count,
        'y',
        -(maxXLength + 0.5),
        this.#zOffset ? this.#zOffset - 1 : 0,
        iy - 1,
        270,
        360,
        0,
        2,
        1,
        `${
          yLabels
            ? yLabels[iy - 1]
              ? yLabels[iy - 1].fString
              : Math.round(
                  (this.#labels.yInitial +
                    (iy - 1) * this.#yWidth +
                    Number.EPSILON) *
                    100
                ) / 100
            : ''
        }`,
        this.#themeProvider.getAxisColor('z'),
        this.#themeProvider.getSecondaryFontColor()
      )
      // axis title
      count++
    }
    // add y label
    elements.axisLabels[0] = {
      id: 'leftLabel',
      key: 4500 + this.#id,
      text: {
        value: yTitle ? yTitle : '',
        color: this.#themeProvider.getAxisColor('z')
      },
      rotation: {
        x: 270,
        y: 270,
        z: 0
      },
      animation: {
        fromX: -(this.#xOffset - 4 + this.#shift.xOffset),
        fromY: (this.#zOffset ? this.#zOffset - 1 : 0) + this.#shift.zOffset,
        fromZ: this.#yOffset + 0.5 + this.#shift.yOffset,
        toX: -(this.#xOffset - 4),
        toY: this.#zOffset ? this.#zOffset - 1 : 0,
        toZ: this.#yOffset + 0.5
      }
    }

    elements.axisLabels[1] = {
      id: 'rightLabel',
      key: 6001 + this.#id,
      text: {
        value: yTitle ? yTitle : '',
        color: this.#themeProvider.getAxisColor('z')
      },
      rotation: {
        x: 270,
        y: 90,
        z: 0
      },
      animation: {
        fromX: -(maxXLength + 2 + this.#shift.xOffset),
        fromY: (this.#zOffset ? this.#zOffset - 1 : 0) + this.#shift.zOffset,
        fromZ: maxYLength - 2.5 + this.#shift.yOffset,
        toX: -(maxXLength + 2),
        toY: this.#zOffset ? this.#zOffset - 1 : 0,
        toZ: maxYLength - 2.5
      }
    }
    return count
  }

  #createXLabels = (
    elements,
    normalAxis,
    reversedAxis,
    maxXLength,
    maxYLength,
    xLabels,
    xTitle,
    count
  ) => {
    for (let ix = this.#xOffset; ix <= maxXLength; ix++) {
      normalAxis[count] = this.#createAxisEntity(
        7500 + count,
        'x',
        -(ix - 1),
        this.#zOffset ? this.#zOffset - 1 : 0,
        this.#yOffset - 2.5,
        270,
        180,
        0,
        1,
        2,
        `${
          xLabels[ix - 1]
            ? xLabels[ix - 1].fString
            : Math.round(
                (this.#labels.xInitial +
                  (ix - 1) * this.#xWidth +
                  Number.EPSILON) *
                  100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('x'),
        this.#themeProvider.getSecondaryFontColor()
      )
      reversedAxis[count] = this.#createAxisEntity(
        9000 + count,
        'x',
        -(ix - 1),
        this.#zOffset ? this.#zOffset - 1 : 0,
        maxYLength + 0.5,
        270,
        180,
        0,
        1,
        2,
        `${
          xLabels[ix - 1]
            ? xLabels[ix - 1].fString
            : Math.round(
                (this.#labels.xInitial +
                  (ix - 1) * this.#xWidth +
                  Number.EPSILON) *
                  100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('x'),
        this.#themeProvider.getSecondaryFontColor()
      )
      count++
    }

    elements.axisLabels[2] = {
      id: 'downLabel',
      key: 10502 + this.#id,
      text: {
        value: xTitle,
        color: this.#themeProvider.getAxisColor('x')
      },
      rotation: {
        x: 270,
        y: 180,
        z: 0
      },
      animation: {
        fromX: -(this.#xOffset + this.#shift.xOffset),
        fromY: (this.#zOffset ? this.#zOffset - 1 : 0) + this.#shift.zOffset,
        fromZ: this.#yOffset - 4 + this.#shift.yOffset,
        toX: -this.#xOffset,
        toY: this.#zOffset ? this.#zOffset - 1 : 0,
        toZ: this.#yOffset - 4
      }
    }

    elements.axisLabels[3] = {
      id: 'upLabel',
      key: 12003 + this.#id,
      text: {
        value: xTitle,
        color: this.#themeProvider.getAxisColor('x')
      },
      rotation: {
        x: 270,
        y: 0,
        z: 0
      },
      animation: {
        fromX: -(maxXLength - 2.5 - this.#shift.xOffset),
        fromY: (this.#zOffset ? this.#zOffset - 1 : 0) + this.#shift.zOffset,
        fromZ: maxYLength + 2 + this.#shift.yOffset,
        toX: -(maxXLength - 2.5),
        toY: this.#zOffset ? this.#zOffset - 1 : 0,
        toZ: maxYLength + 2
      }
    }
    return count
  }

  #createEnvironment(
    elements,
    normalAxis,
    reversedAxis,
    maxXLength,
    maxYLength,
    xLabels,
    xTitle,
    count
  ) {
    count++
    const rotation = 135
    const x = -(maxXLength + 5.5)
    const y = (this.#zOffset ? this.#zOffset + 7 : 7) + this.#shift.zOffset
    const z = this.#yOffset + this.#range / 2 + 10
    elements.titles[0] = {
      key: 195000 + this.#id,
      text: {
        value: this.#histogram.fTitle,
        color: this.#themeProvider.getPrimaryFontColor()
      },
      rotation: {
        x: 0,
        y: rotation,
        z: 0
      },
      position: {
        x: x,
        y: y + 9,
        z: z
      },
      innerText: {
        value: 'Press V to show projections or controls',
        color: this.#themeProvider.getPrimaryFontColor()
      }
    }
    // add banner
    count++
    const bannerWidthX =
      this.#range > this.#xLimit ? this.#xLimit + 1 : this.#range
    const bannerWidthY =
      this.#range > this.#yLimit ? this.#yLimit + 1 : this.#range
    elements.ground = {
      key: 22500 + count + this.#id,
      scale: {
        width: bannerWidthX + 8,
        height: 0.2,
        depth: bannerWidthY + 8
      },
      animation: {
        fromX: this.#xOffset + bannerWidthX * 0.5 - 1 + this.#shift.xOffset,
        fromY: -0.5,
        fromZ: this.#yOffset + bannerWidthY * 0.5 - 0.5 + this.#shift.yOffset,
        toX: this.#xOffset + bannerWidthX * 0.5 - 1,
        toY: -0.5,
        toZ: this.#yOffset + bannerWidthY * 0.5 - 0.5
      }
    }

    // add banner with mappings
    count++
    elements.banners[1] = {
      key: 240000 + count + this.#id,
      geometry: {
        width: 0.1,
        height: 15,
        depth: 22
      },
      position: {
        x: x,
        y: y,
        z: z
      },
      rotation: {
        x: 0,
        y: -rotation,
        z: 0
      },
      material: {
        color: '#ffffff',
        transparent: true
      }
    }
  }

  /**
   * Funkcia upravuje ofset sekcie pre TH3 histogram.
   * @param {string} axisOffset - Označenie ofsetu, ktorý je potrebné modifikovať
   * @param {number} increment - Hodnota, o ktorú je potrebné upraviť ofset
   * @param {boolean} defaultRange - Ak je True, inkrement je predvolený (1), ak False použije sa hodnota parametra increment
   * @return {Object} result - Vracia objekt obsahujúci všetky aktualizované ofsety
   */
  #editTH3OffsetByOffset = (axisOffset, increment, defaultRange) => {
    // set object with limits
    const limits = {
      xOffset: this.#xLimit,
      yOffset: this.#yLimit,
      zOffset: this.#zLimit
    }
    // set object with current offsets
    const offsets = {
      name: 'TH3',
      xOffset: this.#xOffset,
      yOffset: this.#yOffset,
      zOffset: this.#zOffset,
      range: this.#range
    }
    const result = this.#getValidOffset(
      limits,
      offsets,
      increment,
      axisOffset,
      defaultRange
    )
    this.#xOffset = result.xOffset
    this.#yOffset = result.yOffset
    this.#zOffset = result.zOffset
    // return result as object
    return result
  }

  /**
   * Funkcia upravuje ofset sekcie pre TH2 histogram.
   * @param {string} axisOffset - Označenie ofsetu, ktorý je potrebné modifikovať
   * @param {number} increment - Hodnota, o ktorú je potrebné upraviť ofset
   * @param {boolean} defaultRange - Ak je True, inkrement je predvolený (1), ak False použije sa hodnota parametra increment
   * @return {Object} result - Vracia objekt obsahujúci všetky aktualizované ofsety
   */
  #editTH2OffsetByOffset = (axisOffset, increment, defaultRange) => {
    // set object with limits
    const limits = {
      xOffset: this.#xLimit,
      yOffset: this.#yLimit
    }
    // set object with current offsets
    const offsets = {
      name: 'TH2',
      xOffset: this.#xOffset,
      yOffset: this.#yOffset,
      range: this.#range
    }
    const result = this.#getValidOffset(
      limits,
      offsets,
      increment,
      axisOffset,
      defaultRange
    )
    this.#xOffset = result.xOffset
    this.#yOffset = result.yOffset
    // return result as object
    return result
  }

  /**
   * Funkcia upravuje ofset sekcie pre TH1 histogram.
   * @param {string} axisOffset - Označenie ofsetu, ktorý je potrebné modifikovať
   * @param {number} increment - Hodnota, o ktorú je potrebné upraviť ofset
   * @param {boolean} defaultRange - Ak je True, inkrement je predvolený (1), ak False použije sa hodnota parametra increment
   * @return {Object} result - Vracia objekt obsahujúci všetky aktualizované ofsety
   */
  #editTH1OffsetByOffset = (axisOffset, increment, defaultRange) => {
    // set object with limits
    const limits = {
      xOffset: this.#xLimit
    }
    // set object with current offsets
    const offsets = {
      name: 'TH1',
      xOffset: this.#xOffset,
      range: this.#range
    }
    const result = this.#getValidOffset(
      limits,
      offsets,
      increment,
      axisOffset,
      defaultRange
    )
    this.#xOffset = result.xOffset
    // return result as object
    return result
  }

  /**
   * Funkcia určuje predošlú veľkosť binu, Pri zmene veľkosti binu sa bin upráví z predošlej hodnoty na novú hodnotu.
   * @param {string} id - Identifikátor binu
   * @return {number} result - Vracia predošlú veľkosť binu
   */
  #getPreviousBinScales = (id) => {
    const previousBin = this.#previousBins.filter(
      (prevBin) => prevBin.key === id
    )
    if (previousBin[0] && previousBin[0].scale) {
      return previousBin[0].scale
    }
    return 0
  }

  /**
   * Funkcia určuje predošlú farbu binu, Pri zmene farby binu sa bin zmení z predošlej farby na novú farbu.
   * @param {string} id - Identifikátor binu
   * @return {string} result - Vracia HEX hodnotu predošlej farby binu
   */
  #getPreviousBinColor = (id) => {
    const previousBin = this.#previousBins.filter(
      (prevBin) => prevBin.key === id
    )
    if (previousBin[0] && previousBin[0].scale) {
      return previousBin[0].color
    }
    return '#ffffff'
  }

  #validateProgress = (progress) => {
    if (progress > this.#maximum) return this.#maximum
    if (progress < 0) return 0
    return progress
  }
  //#endregion PRIVATE FUNCTIONS ------------------------------------------
}
