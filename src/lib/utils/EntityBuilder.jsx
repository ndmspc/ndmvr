/* object for creating the entity builder from templates */

export default class EntityBuilder {
  model = null
  constructor(model) {
    this.model = model
  }

  updateElement(count, histogramElm, element, entityType) {
    if (this.model[element].elements[count]) {
      if (element === 'titles') {
        this.createTitle(
          this.model[element].elements[count],
          this.model[element].attributes[count]
        )
      } else {
        this.createElement(
          this.model[element].elements[count],
          this.model[element].attributes[count],
          entityType
        )
      }
    } else {
      let newElement

      if (element === 'titles') {
        newElement = this.createTitle(
          undefined,
          this.model[element].attributes[count]
        )
      } else {
        newElement = this.createElement(
          undefined,
          this.model[element].attributes[count],
          entityType
        )
      }
      this.model[element].elements[count] = newElement
      histogramElm.appendChild(newElement)
    }
  }

  deleteUnnecessaryElements(count, element) {
    const newLength = count
    // overbins
    if (element.length === 1) {
      while (this.model[element[0]].elements[count]) {
        this.model[element[0]].elements[count].remove()
        count++
      }
      this.model[element[0]].elements.length = newLength
      this.model[element[0]].attributes.length = newLength
    } else {
      while (this.model[element[0]].elements[count]) {
        this.model[element[0]].elements[count].remove()
        this.model[element[1]].elements[count].remove()
        count++
      }
      this.model[element[0]].elements.length = newLength
      this.model[element[0]].attributes.length = newLength
      this.model[element[1]].elements.length = newLength
      this.model[element[1]].attributes.length = newLength
    }
  }

  createElement(Element, data, type) {
    let element = Element
    if (element === undefined || element === null) {
      const entityType = this.checkEntityType(type)
      element = document.createElement(entityType)
    }
    for (const feature in data) {
      element.setAttribute(feature, data[feature])
    }
    return element
  }

  createBannerElement(Element, linesCount, data) {
    let parent = Element
    if (parent === undefined || parent === null) {
      parent = document.createElement('a-entity')
      const array = this.createBannerLines(linesCount, data)
      array.map((element) => parent.append(element))
    }
    parent.setAttribute('position', data.position)
    parent.setAttribute('id', data.id)
    return parent
  }

  createBannerLines(linesCount, data) {
    const array = []
    for (let i = linesCount - 1; i > -1; i--) {
      data.line.position.y = 2 * i
      const line = document.createElement('a-entity')
      line.setAttribute('rotation', data.line.rotation)
      line.setAttribute('position', data.line.position)
      line.setAttribute('text', data.line.text)
      array.push(line)
    }
    const background = document.createElement('a-entity')
    background.setAttribute('geometry', data.background.geometry)
    background.setAttribute('position', data.background.position)
    background.setAttribute('material', data.background.material)
    array.push(background)
    return array
  }

  createTitle(Element, data) {
    let parent = Element
    if (parent === undefined || parent === null) {
      parent = document.createElement('a-entity')
      const child = this.createElement(undefined, data.background)
      parent.appendChild(child)
    }
    delete data.background
    return this.createElement(parent, data)
  }

  checkEntityType(type) {
    if (
      type === 'a-box' ||
      type === 'a-sphere' ||
      type === 'a-text' ||
      type === 'a-circle' ||
      type === 'a-text'
    ) {
      return type
    } else {
      return 'a-entity'
    }
  }
}
