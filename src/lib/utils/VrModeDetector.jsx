/**
 * Contains logic for setting input device.
 * Takes setter function as argument.
 * */
export default function registerVRModeDetector(setInputDevice) {
    if (AFRAME.components['vr-mode-detector']){
        setInputDevice.setData('keyboard')
        if (AFRAME.utils.device.isMobile()) {
            setInputDevice.setData('mobile')
        } else if(window.AFRAME.utils.device.checkHeadsetConnected()){
            setInputDevice.setData('oculus')
        }
        return
    }

    AFRAME.registerComponent('vr-mode-detector', {
        init: function () {
            setInputDevice.setData('keyboard')
            if (AFRAME.utils.device.isMobile()) {
                setInputDevice.setData('mobile')
            }
            this.el.sceneEl.addEventListener('enter-vr', () => {
                if (window.AFRAME.utils.device.checkHeadsetConnected()) {
                    if (AFRAME.utils.device.isMobile()) {
                        setInputDevice.setData('mobile')
                    } else {
                        setInputDevice.setData('oculus')
                    }
                } else {
                    setInputDevice.setData('keyboard')
                }
            });
            this.el.sceneEl.addEventListener('exit-vr', () => {
                if (AFRAME.utils.device.isMobile()) {
                    setInputDevice.setData('mobile')
                } else {
                    setInputDevice.setData('keyboard')
                }
            });
        }
    });

}