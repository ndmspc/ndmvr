// import React from 'react'
import { localStorageService } from '../services/ndmVrStorageService'
import 'aframe'
import EntityBuilder from './EntityBuilder'
import EntityTemplate from './EntityTemplate'
import ThemeProvider from '../resources/themeProvider'

let selectedBins = []

function checkBinState(binId) {
  const updatedArray = selectedBins.filter((item) => item.id === binId)
  return updatedArray.length !== 0
}
// create axis entity
/*
function createAxisEntity(
  id,
  positionX,
  positionY,
  positionZ,
  rotationX,
  rotationY,
  rotationZ,
  width,
  height,
  value,
  color
) {
  const element = document.createElement('a-entity')
  element.setAttribute('position', {
    x: positionX,
    y: positionY,
    z: positionZ
  })
  element.setAttribute('rotation', {
    x: rotationX,
    y: rotationY,
    z: rotationZ
  })
  element.setAttribute('geometry', {
    primitive: 'plane',
    width: width,
    height: height
  })
  element.setAttribute('text', {
    value: value,
    color: 'black',
    width: 14,
    height: 'auto',
    align: 'center'
  })
  element.setAttribute('material', {
    color: color,
    opacity: 0.8
  })
  return element
}
*/
// create th3 histogram
/*
  function createTH3Histogram(histogram, xOffset, yOffset, zOffset, range) {
    // get selected bins from local storage
    selectedBins = localStorageService.getBinsFromLocalStorage()
    // react elements to draw the histograms
    const elements = {
      bins: [],
      normalAxis: [],
      reversedAxis: [],
      banners: []
    }
    // variables for rendering the histogram
    let xcenter, xmin, xmax, xwidth
    let ycenter, ymin, ymax, ywidth
    let zcenter, zmin, zmax, zwidth
    const binName = ''
    let absoluteContent
    let c
    let count = 0

    const maxXLength =
      xOffset + range > histogram.fXaxis.fXmax
        ? histogram.fXaxis.fXmax
        : xOffset + range
    const maxYLength =
      yOffset + range > histogram.fYaxis.fXmax
        ? histogram.fYaxis.fXmax
        : yOffset + range
    const maxZLength =
      zOffset + range > histogram.fZaxis.fXmax
        ? histogram.fZaxis.fXmax
        : zOffset + range

    const xTitle = histogram.fXaxis.fTitle
    const yTitle = histogram.fYaxis.fTitle
    const zTitle = histogram.fZaxis.fTitle

    // generate react elements of the bins
    for (let iz = zOffset; iz <= maxZLength; iz++)
      for (let iy = yOffset; iy <= maxYLength; iy++)
        for (let ix = xOffset; ix <= maxXLength; ix++) {
          xcenter = histogram.fXaxis.GetBinCenter(ix)
          xmin = histogram.fXaxis.GetBinLowEdge(ix)
          xmax = xcenter + (xcenter - xmin)
          xwidth = xmax - xmin
          ycenter = histogram.fYaxis.GetBinCenter(iy)
          ymin = histogram.fYaxis.GetBinLowEdge(iy)
          ymax = ycenter + (ycenter - ymin)
          ywidth = ymax - ymin
          zcenter = histogram.fZaxis.GetBinCenter(iz)
          zmin = histogram.fZaxis.GetBinLowEdge(iz)
          zmax = zcenter + (zcenter - zmin)
          zwidth = zmax - zmin
          absoluteContent = histogram.getBinContent(ix, iy, iz)
          c = optimizeBinContent(absoluteContent, histogram.fMaximum)

          // set a-frame entity box
          // set children component
          if (c > 0) {
            let size = c
            if (c < 0.15) size = 0.15

            const color = setBinColor(c)
            // check bin status
            const id =
              xmin +
              'th3' +
              xmax +
              'th3' +
              zmin +
              'th3' +
              zmax +
              'th3' +
              ymin +
              'th3' +
              ymax
            let markedColor
            if (checkBinState(id)) {
              markedColor = '#FF3700'
            } else {
              markedColor = color
            }
            // fill bin data
            const binData = `id: ${id};
           content: ${c};
           color: ${color};
           yTitle: ${yTitle};
           xTitle: ${xTitle};
           zTitle: ${zTitle};
           absoluteContent: ${absoluteContent};
           binName: ${binName};
           xMin: ${xmin};
           yMin: ${ymin};
           zMin: ${zmin};
           xMax: ${xmax};
           yMax: ${ymax};
           zMax: ${zmax};
           xCenter: ${xcenter};
           yCenter: ${ycenter};
           zCenter: ${zcenter};
           xWidth: ${xwidth};
           yWidth: ${ywidth};
           zWidth: ${zwidth};
           markedColor: ${markedColor}`
            elements.bins[count] = `
              <a-box
                key=${count}
                class='clickable'
                material=color: ${markedColor}; opacity: 0.75;
                bin-th3=${binData}
                position=${xmin} ${zmin + 0.2} ${ymin}
                height=${size * 0.9}
                width=${size * 0.9}
                depth=${size * 0.9}
                mouseEvent
              />
            `
          }
          count++
        }

    // set axis
    count = 0
    for (let iz = yOffset; iz <= maxYLength; iz++) {
      const normalAxis = createAxisEntity(
        1500 + count,
        -2.5 + xOffset,
        zOffset - 1,
        iz - 1,
        270,
        180,
        0,
        2,
        1,
        `__${iz - 1}`,
        'green'
      )
      const reversedAxis = createAxisEntity(
        4500 + count,
        maxXLength + 0.5,
        zOffset - 1,
        iz - 1,
        270,
        360,
        0,
        2,
        1,
        `____\n${iz - 1}`,
        'green'
      )
      elements.normalAxis[count] = normalAxis
      elements.reversedAxis[count] = reversedAxis
      count++
    }
    // add z label
    elements.normalAxis[count] = (
      <a-entity
        id='leftLabel'
        key={1500 + count}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${yTitle}; color: green; width: 14; height:auto; align:center;`}
        rotation={`${270} ${270} ${0}`}
        position={`${xOffset - 4} ${zOffset - 1} ${yOffset + 0.5}`}
        material='opacity: 0;'
      />
    )
    elements.reversedAxis[count] = (
      <a-entity
        id='rightLabel'
        key={4500 + count}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${yTitle}; color: green; width: 14; height:auto; align:center;`}
        rotation={`${270} ${90} ${0}`}
        position={`${maxXLength + 2} ${zOffset - 1} ${maxYLength - 2.5}`}
        material='opacity: 0;'
      />
    )
    count++
    // generate x and y axis
    for (let ix = xOffset; ix <= maxXLength; ix++) {
      const normalAxis = createAxisEntity(
        1500 + count,
        ix - 1,
        zOffset - 1,
        yOffset - 2.5,
        270,
        180,
        0,
        1,
        2,
        `''''|\n${ix - 1}`,
        'darkred'
      )
      const reversedAxis = createAxisEntity(
        4500 + count,
        ix - 1,
        zOffset - 1,
        maxYLength + 0.5,
        270,
        360,
        0,
        1,
        2,
        `|''''\n${ix - 1}`,
        'darkred'
      )
      elements.normalAxis[count] = normalAxis
      elements.reversedAxis[count] = reversedAxis
      count++
    }
    // add x label
    elements.normalAxis[count] = (
      <a-entity
        id='downLabel'
        key={1500 + count}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${xTitle}; color: red; width: 14; height:auto; align:center;`}
        rotation={`${270} ${180} ${0}`}
        position={`${xOffset} ${zOffset - 1} ${yOffset - 4}`}
        material='opacity: 0;'
      />
    )
    elements.reversedAxis[count] = (
      <a-entity
        id='upLabel'
        key={4500 + count}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${xTitle}; color: red; width: 14; height:auto; align:center;`}
        rotation={`${270} ${0} ${0}`}
        position={`${maxXLength - 2.5} ${zOffset - 1} ${maxYLength + 2}`}
        material='opacity: 0;'
      />
    )
    count++
    // y axis
    elements.normalAxis[count] = createAxisEntity(
      4500 + count,
      maxXLength + 0.5,
      zOffset - 1,
      maxYLength + 0.5,
      270,
      360,
      0,
      2,
      2,
      zOffset - 1,
      'blue'
    )
    elements.reversedAxis[count] = createAxisEntity(
      1500 + count,
      maxXLength + 0.5,
      zOffset - 1,
      yOffset - 2.5,
      270,
      180,
      0,
      2,
      2,
      zOffset - 1,
      'blue'
    )
    count++
    elements.normalAxis[count] = createAxisEntity(
      1500 + count,
      xOffset - 2.5,
      zOffset - 1,
      maxYLength + 0.5,
      270,
      180,
      0,
      2,
      2,
      zOffset - 1,
      'blue'
    )
    elements.reversedAxis[count] = createAxisEntity(
      1500 + count,
      xOffset - 2.5,
      zOffset - 1,
      yOffset - 2.5,
      270,
      180,
      0,
      2,
      2,
      zOffset - 1,
      'blue'
    )
    count++
    // add y label
    /*
    elements.normalAxis[count] = (
      <a-entity
        key={1500 + count}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${zTitle}; color: blue; width: 14; height:auto; align:center;`}
        rotation={`${0} ${270} ${90}`}
        position={`${xOffset - 1.5} ${maxZLength - 1.5} ${yOffset - 2}`}
        material='opacity: 0;'
      />
    )
    elements.reversedAxis[count] = (
      <a-entity
        key={4500 + count}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${zTitle}; color: blue; width: 14; height:auto; align:center;`}
        rotation={`${0} ${90} ${90}`}
        position={`${maxXLength - 0.5} ${maxZLength - 1.5} ${maxYLength}`}
        material='opacity: 0;'
      />
    )
    count++
    elements.normalAxis[count] = (
      <a-entity
        key={1500 + count}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${zTitle}; color: blue; width: 14; height:auto; align:center;`}
        rotation={`${0} ${360} ${90}`}
        position={`${xOffset - 2} ${maxZLength - 1.5} ${maxYLength - 0.5}`}
        material='opacity: 0;'
      />
    )
    elements.reversedAxis[count] = (
      <a-entity
        key={4500 + count}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${zTitle}; color: blue; width: 14; height:auto; align:center;`}
        rotation={`${0} ${180} ${90}`}
        position={`${maxXLength - 0.5} ${maxZLength - 1.5} ${yOffset - 1.5}`}
        material='opacity: 0;'
      />
    )
    // add histogram title
    count++
    elements.normalAxis[count] = (
      <a-entity
        key={1500 + count}
        // geometry='primitive: plane; width:170; height:30;'
        text={`value: ${histogram.fTitle}; color: black; width: 230; height:auto; align:center;`}
        rotation={`${0} ${123} ${0}`}
        position={`${xOffset - 90} ${maxZLength - 6.5} ${yOffset + 90}`}
        // material='opacity: 0.5; color: white;'
      >
        <a-entity
          geometry='primitive: plane; width:170; height:30;'
          position='0 0 -1'
          material='opacity: 0.6; color: white;'
        />
      </a-entity>
    )
    // add file title
    elements.reversedAxis[count] = (
      <a-entity
        key={4500 + count}
        // geometry='primitive: plane; width:170; height:30;'
        text={`value: ${histogram.fName}; color: black; width: 230; height:auto; align:center;`}
        rotation={`${0} ${330} ${0}`}
        position={`${xOffset + 120} ${-6.5} ${yOffset - 70}`}
        // material='opacity: 0.5; color: white;'
      >
        <a-entity
          geometry='primitive: plane; width:170; height:30;'
          position='0 0 -1'
          material='opacity: 0.6; color: white;'
        />
      </a-entity>
    )
    // add banner
    elements.banners[0] = (
      <a-entity
        key={4500 + count + 2}
        id='th3-banner'
        text={`value: ""; color: white; width: 230; height:auto; align:center;`}
        // rotation={`${0} ${330} ${0}`}
        position={`${xOffset + range / 2 + 2.5} ${zOffset + range / 2 + 1.5} ${
          maxYLength + 8
        }`}
      >
        <a-entity rotation='0 180 0' position='0 6 0' text={`value: "";`} />
        <a-entity rotation='0 180 0' position='0 4 0' text={`value: "";`} />
        <a-entity rotation='0 180 0' position='0 2 0' text={`value: "";`} />
        <a-entity rotation='0 180 0' position='0 0 0' text={`value: "";`} />
        <a-entity
          geometry='primitive: box; width:15; height:15; depth:0.1;'
          position='0 0 0.5'
          material='color: black;'
        />
      </a-entity>
    )
     */
// add banner with mappings
/*
    elements.banners[1] = (
      <a-entity
        key={4500 + count + 3}
        id='th3-mapping'
        geometry='primitive: box; width:1; height:15; depth:15;'
        position={`${maxXLength + 8} ${zOffset + range / 2 + 0.5}
         ${yOffset + range / 2 + 2.5}`}
        material='opacity: 1; color: black;'
      />
    )

    return elements
  }
   */

// create th2 histogram
function createTH2Histogram(
  Element,
  model,
  histogram,
  xOffset,
  yOffset,
  range,
  theme
) {
  // get selected bins from local storage
  selectedBins = localStorageService.getBinsFromLocalStorage()
  // react elements to draw the histogram

  const entityTemplate = new EntityTemplate()
  const entityBuilder = new EntityBuilder(model)
  const themeProvider = new ThemeProvider(theme)

  const data = {}

  const histogramElm = Element
  // variables for rendering the histogram
  let centeredYPosition
  let xcenter, xmin, xmax, xwidth
  let ycenter, ymin, ymax, ywidth
  let c
  const binName = ''
  let absoluteContent
  let count = 0
  const maxXLength =
    xOffset + range > histogram.fXaxis.fXmax
      ? histogram.fXaxis.fXmax
      : xOffset + range
  const maxYLength =
    yOffset + range > histogram.fYaxis.fXmax
      ? histogram.fYaxis.fXmax
      : yOffset + range
  const xTitle = histogram.fXaxis.fTitle
  const yTitle = histogram.fYaxis.fTitle
  const xLabels = histogram.fXaxis.fLabels ? histogram.fXaxis.fLabels.arr : []
  const yLabels = histogram.fYaxis.fLabels ? histogram.fYaxis.fLabels.arr : []

  // generate react elements of the bins
  for (let iy = yOffset; iy <= maxYLength; iy++) {
    for (let ix = xOffset; ix <= maxXLength; ix++) {
      xcenter = histogram.fXaxis.GetBinCenter(ix)
      xmin = histogram.fXaxis.GetBinLowEdge(ix)
      xmax = xcenter + (xcenter - xmin)
      xwidth = xmax - xmin
      ycenter = histogram.fYaxis.GetBinCenter(iy)
      ymin = histogram.fYaxis.GetBinLowEdge(iy)
      ymax = ycenter + (ycenter - ymin)
      ywidth = ymax - ymin
      absoluteContent = histogram.getBinContent(ix, iy)
      c = optimizeBinContent(absoluteContent, histogram.fMaximum)
      centeredYPosition = c / 2

      // set a-frame entity box
      // set children component
      if (c > 0) {
        const color = themeProvider.getBinColor(c)
        // check bin status
        const id = xmin + 'th2' + xmax + 'th2' + ymin + 'th2' + ymax
        let markedColor
        if (checkBinState(id)) {
          markedColor = themeProvider.getSecondaryAccentColor()
        } else {
          markedColor = color
        }
        // fill bin data
        const binData = `id: ${id};
         content: ${c};
         color: ${color};
         yTitle: ${yTitle};
         xTitle: ${xTitle};
         absoluteContent: ${absoluteContent};
         binName: ${binName};
         xMin: ${xmin};
         yMin: ${ymin};
         xMax: ${xmax};
         yMax: ${ymax};
         xCenter: ${xcenter};
         yCenter: ${ycenter};
         xWidth: ${xwidth};
         yWidth: ${ywidth};
         markedColor: ${markedColor};
         selectColor: ${themeProvider.getSecondaryAccentColor()};
         axisX: ${themeProvider.getAxisColor('x')};
         axisY: ${themeProvider.getAxisColor('y')};
         axisZ: ${themeProvider.getAxisColor('z')}`

        data.id = id
        data.positionX = xmin
        data.positionY = centeredYPosition
        data.positionZ = ymin
        data.scaleX = xwidth / 2
        data.scaleY = c
        data.scaleZ = ywidth / 2
        data.color = markedColor
        data.class = 'clickable'
        data.data = binData

        model.bins.attributes[count] = entityTemplate.binTemplate(data)
        entityBuilder.updateElement(count, histogramElm, 'bins', 'a-box')
        count++
      }
    }
  }

  entityBuilder.deleteUnnecessaryElements(count, ['bins'])

  // generate YYYYYYYYYYYYYYYYYYYYYYYYYYYY axis
  // left up
  data.id = 1500 + count
  data.positionX = maxXLength + 0.5
  data.positionY = 0
  data.positionZ = maxYLength + 0.5
  data.rotationX = 270
  data.rotationY = 360
  data.rotationZ = 0
  data.width = 2
  data.height = 2
  data.value = `0`
  data.color = themeProvider.getAxisColor('y')
  model.normalXAxis.attributes[0] = entityTemplate.axisTemplate(data)
  entityBuilder.updateElement(0, histogramElm, 'normalXAxis', 'a-entity')

  // left down
  data.positionZ = yOffset - 2.5
  data.rotationX = 270
  data.rotationY = 180
  model.reversedYAxis.attributes[0] = entityTemplate.axisTemplate(data)
  entityBuilder.updateElement(0, histogramElm, 'reversedYAxis', 'a-entity')

  // right up
  data.positionX = xOffset - 2.5
  data.positionZ = maxYLength + 0.5

  model.normalYAxis.attributes[0] = entityTemplate.axisTemplate(data)
  entityBuilder.updateElement(0, histogramElm, 'normalYAxis', 'a-entity')

  // right down
  data.positionZ = yOffset - 2.5
  data.rotationY = 180
  model.reversedXAxis.attributes[0] = entityTemplate.axisTemplate(data)
  entityBuilder.updateElement(0, histogramElm, 'reversedXAxis', 'a-entity')

  // generate ZZZZZZZZZZZZZZZZZZ axis
  count = 1
  for (let iy = yOffset; iy <= maxYLength; iy++) {
    data.id = 1500 + count
    data.positionX = xOffset - 2.5
    data.positionY = 0
    data.positionZ = iy - 1
    data.rotationX = 270
    data.rotationY = 180
    data.rotationZ = 0
    data.width = 2
    data.height = 1
    data.value = `${yLabels[iy - 1] ? yLabels[iy - 1].fString : iy - 1}`
    data.color = themeProvider.getAxisColor('z')

    model.normalYAxis.attributes[count] = entityTemplate.axisTemplate(data)
    entityBuilder.updateElement(count, histogramElm, 'normalYAxis', 'a-entity')

    data.id = 4500 + count
    data.positionX = maxXLength + 0.5
    data.rotationY = 360
    data.value = `${yLabels[iy - 1] ? yLabels[iy - 1].fString : iy - 1}`

    // axis title
    model.reversedYAxis.attributes[count] = entityTemplate.axisTemplate(data)
    entityBuilder.updateElement(
      count,
      histogramElm,
      'reversedYAxis',
      'a-entity'
    )
    count++
  }

  entityBuilder.deleteUnnecessaryElements(count, [
    'normalYAxis',
    'reversedYAxis'
  ])

  count = 1
  // generate XXXXXXXXXXXXXXXX axis
  for (let ix = xOffset; ix <= maxXLength; ix++) {
    data.id = 1500 + count
    data.positionX = ix - 1
    data.positionY = 0
    data.positionZ = yOffset - 2.5
    data.rotationX = 270
    data.rotationY = 270
    data.rotationZ = 0
    data.width = 2
    data.height = 1
    data.value = `${xLabels[ix - 1] ? xLabels[ix - 1].fString : ix - 1}`
    data.color = themeProvider.getAxisColor('x')

    model.normalXAxis.attributes[count] = entityTemplate.axisTemplate(data)
    entityBuilder.updateElement(count, histogramElm, 'normalXAxis', 'a-entity')

    data.id = 4500 + count
    data.positionZ = maxYLength + 0.5
    data.rotationX = 270
    data.rotationY = 90
    data.value = `${xLabels[ix - 1] ? xLabels[ix - 1].fString : ix - 1}`

    model.reversedXAxis.attributes[count] = entityTemplate.axisTemplate(data)
    entityBuilder.updateElement(
      count,
      histogramElm,
      'reversedXAxis',
      'a-entity'
    )
    count++
  }

  entityBuilder.deleteUnnecessaryElements(count, [
    'normalXAxis',
    'reversedXAxis'
  ])
  // add x label
  data.positionX = xOffset
  data.positionY = 0
  data.positionZ = yOffset - 4
  data.rotationX = 270
  data.rotationY = 180
  data.rotationZ = 0
  data.width = 4
  data.height = 1
  data.value = xTitle
  data.id = 'downLabel'

  model.labels.attributes[0] = entityTemplate.labelTemplate(data)
  entityBuilder.updateElement(0, histogramElm, 'labels', 'a-entity')

  data.positionX = maxXLength - 2.5
  data.positionZ = maxYLength + 2
  data.rotationY = 0
  data.id = 'upLabel'

  model.labels.attributes[1] = entityTemplate.labelTemplate(data)
  entityBuilder.updateElement(1, histogramElm, 'labels', 'a-entity')

  // add y label
  data.positionX = xOffset - 4
  data.positionY = 0
  data.positionZ = yOffset + 0.5
  data.rotationX = 270
  data.rotationY = 270
  data.rotationZ = 0
  data.width = 4
  data.height = 1
  data.value = yTitle
  data.color = themeProvider.getAxisColor('z')
  data.id = 'leftLabel'

  model.labels.attributes[2] = entityTemplate.labelTemplate(data)
  entityBuilder.updateElement(2, histogramElm, 'labels', 'a-entity')

  data.positionX = maxXLength + 2
  data.positionZ = maxYLength - 2.5
  data.rotationX = 270
  data.rotationY = 90
  data.id = 'rightLabel'

  model.labels.attributes[3] = entityTemplate.labelTemplate(data)
  entityBuilder.updateElement(3, histogramElm, 'labels', 'a-entity')

  data.positionX = xOffset - 90
  data.positionY = -6.5
  data.positionZ = yOffset + 90
  data.rotationX = 0
  data.rotationY = 123
  data.rotationZ = 0
  data.title = histogram.fTitle
  data.color = themeProvider.getPrimaryFontColor()

  const back = {}
  back.positionX = 0
  back.positionY = 0
  back.positionZ = -1
  back.width = 170
  back.height = 30
  back.color = 'white'

  model.titles.attributes[0] = entityTemplate.titleTemplate(data, back)
  entityBuilder.updateElement(0, histogramElm, 'titles', 'a-entity')

  // add file title
  data.positionX = xOffset + 120
  data.positionZ = yOffset - 70
  data.rotationY = 330
  data.title = histogram.fName

  model.titles.attributes[1] = entityTemplate.titleTemplate(data, back)
  entityBuilder.updateElement(1, histogramElm, 'titles', 'a-entity')
}

// edit bin content to range 0 - 1
const optimizeBinContent = (binContent, maxContent) => {
  if (binContent <= 0) return 0
  if (maxContent > 1) {
    // round to 2 decimal numbers
    binContent =
      Math.round((binContent / maxContent + Number.EPSILON) * 100) / 100
  }

  return binContent
}

export { createTH2Histogram }
// export { createTH3Histogram }
