/* object for creating the histogram demos */
import { createHistogram } from 'jsroot';
export default class HistogramDemoFactory {
  #jsroot

  constructor(jsroot) {
    this.#jsroot = jsroot
  }

  createTH2DemoHistogram(xLength, yLength, initialContent, contentIncrement) {
    // const histo = this.#jsroot.createHistogram('TH2I', xLength, yLength)
    const histo = createHistogram('TH2I', xLength, yLength)
    let cnt = initialContent
    for (let iy = 1; iy <= 20; iy++)
      for (let ix = 1; ix <= 20; ix++) {
        const bin = histo.getBin(ix, iy)
        let val = 0
        val = cnt
        histo.setBinContent(bin, val)
        // cnt += 0.1
        cnt += contentIncrement
      }

    histo.fXaxis.fTitle = 'x Axis'
    histo.fYaxis.fTitle = 'y Axis'
    histo.fName = 'You don\'t have a valid file path'
    histo.fTitle = 'This is a TH2 histogram demo'
    histo.fMaximum = cnt

    return histo
  }

  createTH3DemoHistogram(
    xLength,
    yLength,
    zLength,
    initialContent,
    contentIncrement
  ) {
    // const histo = this.#jsroot.createHistogram(
    const histo = createHistogram(
        'TH3I',
      xLength,
      yLength,
      zLength
    )
    let cnt = initialContent

    for (let iz = 1; iz <= 20; iz++)
      for (let iy = 1; iy <= 20; iy++)
        for (let ix = 1; ix <= 20; ix++) {
          const bin = histo.getBin(ix, iy, iz)
          let val = 0
          val = cnt
          histo.setBinContent(bin, val)
          // cnt += 0.0005
          cnt += contentIncrement
        }

    histo.fXaxis.fTitle = 'x Axis'
    histo.fYaxis.fTitle = 'y Axis'
    histo.fZaxis.fTitle = 'z Axis'
    histo.fName = 'You don\'t have a valid file path'
    histo.fTitle = 'This is a TH3 histogram demo'
    histo.fMaximum = cnt

    return histo
  }
}
