/* object for creating the entity templates for entities */

export default class EntityTemplate {
  bannerTemplate(data, lineData, backgroundData) {
    const position = {
      x: data.positionX,
      y: data.positionY,
      z: data.positionZ
    }
    const id = data.id
    const background = this.bannerBackgroundTemplate(backgroundData)
    const line = this.lineTemplate(lineData)
    return {
      position: position,
      id: id,
      background: background,
      line: line
    }
  }

  binTemplate(data) {
    const position = {
      x: data.positionX,
      y: data.positionY,
      z: data.positionZ
    }
    const scale = {
      x: data.scaleX,
      y: data.scaleY,
      z: data.scaleZ
    }
    const material = {
      color: data.color,
      opacity: 0.75
    }
    const clas = data.class
    const binTh2 = data.data
    const id = data.id
    return {
      position: position,
      scale: scale,
      material: material,
      id: id,
      class: clas,
      binth2: binTh2,
      mouseevent: ''
    }
  }

  labelTemplate(data) {
    const position = {
      x: data.positionX,
      y: data.positionY,
      z: data.positionZ
    }
    const rotation = {
      x: data.rotationX,
      y: data.rotationY,
      z: data.rotationZ
    }
    const geometry = {
      primitive: 'plane',
      width: 4,
      height: 1
    }
    const text = {
      value: data.value,
      color: data.color,
      width: 10,
      height: 'auto',
      align: 'center'
    }
    const material = {
      opacity: 0
    }
    const id = data.id
    return {
      position: position,
      rotation: rotation,
      geometry: geometry,
      text: text,
      material: material,
      id: id
    }
  }

  axisTemplate(data) {
    const position = {
      x: data.positionX,
      y: data.positionY,
      z: data.positionZ
    }
    const rotation = {
      x: data.rotationX,
      y: data.rotationY,
      z: data.rotationZ
    }
    const geometry = {
      primitive: 'plane',
      width: data.width,
      height: data.height
    }
    const text = {
      value: data.value,
      color: 'black',
      width: 4,
      height: 'auto',
      align: 'center'
    }
    const material = {
      color: data.color,
      opacity: 0.8
    }
    return {
      position: position,
      rotation: rotation,
      geometry: geometry,
      text: text,
      material: material
    }
  }

  titleTemplate(data, textData) {
    const position = {
      x: data.positionX,
      y: data.positionY,
      z: data.positionZ
    }
    const rotation = {
      x: data.rotationX,
      y: data.rotationY,
      z: data.rotationZ
    }
    const material = {
      opacity: 0
    }
    const text = {
      value: data.title,
      color: data.color,
      width: 230,
      height: 'auto',
      align: 'center'
    }
    const back = this.titleTextTemplate(textData)
    return {
      position: position,
      rotation: rotation,
      material: material,
      text: text,
      background: back
    }
  }

  titleTextTemplate(data) {
    const geometry = {
      primitive: 'plane',
      width: data.width,
      height: data.height
    }
    const position = {
      x: data.positionX,
      y: data.positionY,
      z: data.positionZ
    }
    const material = {
      color: data.color,
      opacity: 0.6
    }
    return {
      position: position,
      geometry: geometry,
      material: material
    }
  }

  lineTemplate(data) {
    const position = {
      x: data.positionX,
      y: data.positionY,
      z: data.positionZ
    }
    const rotation = {
      x: data.rotationX,
      y: data.rotationY,
      z: data.rotationZ
    }
    const text = {
      value: '',
      width: 14,
      height: 'auto',
      align: 'center'
    }
    return {
      position: position,
      rotation: rotation,
      text: text
    }
  }

  bannerBackgroundTemplate(data) {
    const position = {
      x: data.positionX,
      y: data.positionY,
      z: data.positionZ
    }
    const geometry = {
      primitive: 'box',
      width: data.width,
      height: data.height,
      depth: data.depth
    }
    const material = {
      color: data.color
    }
    return {
      position: position,
      geometry: geometry,
      material: material
    }
  }
}
