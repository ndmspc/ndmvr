import {Joystick} from '../controllers/joystick.jsx'
import {CameraService} from "../services/cameraService.jsx";


/**
 * Registers custom component that handles movement on mobile device via virtual joystick
 * Has tick function that moves camera based on state of virtual joystick.
 * */
export default function registerScreenControlsComponent() {
   if (AFRAME.components['screen-controls']) return
   AFRAME.registerComponent('screen-controls',
      {
         init: function () {
            this.cameraService = new CameraService();
            this.component = document.getElementById("camera").components["wasd-controls-custom"];
            //stickId is selected as Document.findElementById in joystick class
            this.joystick1 = new Joystick("stick1", 64, 8);
         },

         tick: function(time, deltaTime) {
            if(this.joystick1.value){
               this.cameraService.horizontalMoveCameraLocal(this.joystick1.value.x, this.joystick1.value.y, 0.3)
            }
         }
      });
}

function haltEvent(event)
{
   event.preventDefault && event.preventDefault();
   event.stopPropagation && event.stopPropagation();
   event.cancelBubble = true;
   event.returnValue = false;
   return false;
}

function addButtonEventListeners( buttonElementID, startFunction, endFunction )
{
   let element = document.getElementById( buttonElementID );
   element.addEventListener('touchstart', startFunction);
   element.addEventListener('mousedown',  startFunction);
   element.addEventListener('touchend',   endFunction);
   element.addEventListener('mouseup',    endFunction);
}

function associateMovementControls( buttonElementID, component, keyName )
{
   addButtonEventListeners(buttonElementID,
      function(event)
      {
         component.registerKeyDown( keyName );
         return haltEvent(event);
      },
      function(event)
      {
         component.registerKeyUp( keyName );
         return haltEvent(event);
      }
   );
}