import AFRAME from 'aframe'

// aframe component for positioning info banner in scene
export default AFRAME.registerComponent('banner-control', {
  schema: {
    position: {
      name: 'position',
      xValue: -1.28,
      yValue: 0.3,
      zValue: -1
    },
    scale: {
      name: 'scale',
      xValue: 0.5,
      yValue: 0.7,
      zValue: 0.1
    },
    rotation: {
      name: 'rotation',
      xValue: 0,
      yValue: 20,
      zValue: 0
    }
  },
  init: function() {
    // const el = this.el
    // setBannerPositioning(el, this.schema)
  }
})
