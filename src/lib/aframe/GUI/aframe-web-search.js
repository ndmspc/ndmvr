
import AFRAME from 'aframe'

if (typeof AFRAME === 'undefined') {
    throw new Error('AFRAME is not loaded. Please include AFRAME before this script.');
}

AFRAME.registerComponent('search-box', {
    init() {
        let searchButton = this.el.querySelector('#search-button');
        let searchKeyword = this.el.querySelector('a-xyinput');
        searchButton.addEventListener('click', (ev) => {
            this._search(searchKeyword.value);
        });

        searchKeyword.addEventListener('keydown', (ev) => {
            if (ev.code == "Enter" && searchKeyword.value != "") {
                this._search(searchKeyword.value);
            }
        });
    },
    _search(q) {
        document.querySelector('a-scene').exitVR();
        window.open("https://www.google.com/search?q=" + q);
    }
});