if (typeof AFRAME === 'undefined') {
  throw new Error(
    'AFRAME is not loaded. Please include AFRAME before this script.'
  )
}

//default colors
window.key_orange = '#ed5b21' // rgb(237, 91, 33) Light orange
window.key_orange_light = '#ef8c60' // rgb (239, 140, 96) Extra Light Orange
window.key_grey = '#22252a' // rgb(34, 37, 42) Standard grey
window.key_grey_dark = '#2c3037' // rgb(44, 48, 55) Medium grey
window.key_grey_light = '#606876' // rgb(96, 104, 118) Light grey
window.key_offwhite = '#d3d3d4' // rgb(211, 211, 212) Extra Light grey
window.key_white = '#fff'

AFRAME.registerComponent('gui-item', {
  schema: {
    type: { type: 'string' },
    width: { type: 'number', default: 1 },
    height: { type: 'number', default: 1 },
    baseDepth: { type: 'number', default: 0.01 },
    depth: { type: 'number', default: 0.02 },
    gap: { type: 'number', default: 0.025 },
    radius: { type: 'number', default: 0 },
    margin: { type: 'vec4', default: { x: 0, y: 0, z: 0, w: 0 } },

    bevel: { type: 'boolean', default: false },
    bevelSegments: { type: 'number', default: 5 },
    steps: { type: 'number', default: 2 },
    bevelSize: { type: 'number', default: 0.1 },
    bevelOffset: { type: 'number', default: 0 },
    bevelThickness: { type: 'number', default: 0.1 }
  }
  // init: function init() {},
  // update: function update() {},
  // tick: function tick() {},
  // remove: function remove() {},
  // pause: function pause() {},
  // play: function play() {}
})

AFRAME.registerComponent('bevelbox', {
  schema: {
    width: { type: 'number', default: 1 },
    height: { type: 'number', default: 1 },
    depth: { type: 'number', default: 1 },

    topLeftRadius: { type: 'number', default: 0.00001 },
    topRightRadius: { type: 'number', default: 0.00001 },
    bottomLeftRadius: { type: 'number', default: 0.00001 },
    bottomRightRadius: { type: 'number', default: 0.00001 },

    bevelEnabled: { type: 'boolean', default: true },
    bevelSegments: { type: 'number', default: 2 },
    steps: { type: 'number', default: 1 },
    bevelSize: { type: 'number', default: 0.1 },
    bevelOffset: { type: 'number', default: 0 },
    bevelThickness: { type: 'number', default: 0.1 }
  },

  multiple: false,

  init: function init() {
    var el = this.el
    var data = this.data

    var _w = data.width
    var _h = data.height
    var _x = -data.width / 2
    var _y = -data.height / 2

    var shape = new THREE.Shape()
    shape.moveTo(_x, _y + data.topLeftRadius)
    shape.lineTo(_x, _y + _h - data.topLeftRadius)
    shape.quadraticCurveTo(_x, _y + _h, _x + data.topLeftRadius, _y + _h)
    shape.lineTo(_x + _w - data.topRightRadius, _y + _h)
    shape.quadraticCurveTo(
      _x + _w,
      _y + _h,
      _x + _w,
      _y + _h - data.topRightRadius
    )
    shape.lineTo(_x + _w, _y + data.bottomRightRadius)
    shape.quadraticCurveTo(_x + _w, _y, _x + _w - data.bottomRightRadius, _y)
    shape.lineTo(_x + data.bottomLeftRadius, _y)
    shape.quadraticCurveTo(_x, _y, _x, _y + data.bottomLeftRadius)

    var extrudedShape = this.extrude(shape)

    el.setObject3D('mesh', extrudedShape)
  },

  extrude: function extrude(roundedBase) {
    var el = this.el
    var data = this.data

    var extrudeSettings = {
      steps: data.steps,
      depth: data.depth,
      bevelEnabled: data.bevelEnabled,
      bevelThickness: data.bevelThickness,
      bevelSize: data.bevelSize,
      bevelOffset: data.bevelOffset,
      bevelSegments: data.bevelSegments
    }

    var extrudedGeometry = new THREE.ExtrudeGeometry(
      roundedBase,
      extrudeSettings
    )
    return new THREE.Mesh(
      extrudedGeometry,
      new THREE.MeshStandardMaterial({
        side: THREE.DoubleSide
      })
    )
  },

  /**
   * Called when component is attached and when component data changes.
   * Generally modifies the entity based on the data.
   */
  update: function update(oldData) {},

  /**
   * Called when a component is removed (e.g., via removeAttribute).
   * Generally undoes all modifications to the entity.
   */
  remove: function remove() {},

  /**
   * Called on each scene tick.
   */
  // tick: function (t) { },

  /**
   * Called when entity pauses.
   * Use to stop or remove any dynamic or background behavior such as events.
   */
  pause: function pause() {},

  /**
   * Called when entity resumes.
   * Use to continue or add any dynamic or background behavior such as events.
   */
  play: function play() {}
})

AFRAME.registerComponent('gui-interactable', {
  schema: {
    clickAction: { type: 'string' },
    hoverAction: { type: 'string' },
    keyCode: { type: 'number', default: -1 },
    key: { type: 'string' }
  },
  init: function init() {
    var _this = this
    var data = this.data
    var el = this.el

    if (data.keyCode > 0) {
      window.addEventListener(
        'keydown',
        function (event) {
          // console.log('in keydown handler, event key: ' + event.key);
          if (event.key == data.key) {
            //    console.log("key press by gui-interactable, key: " + data.key);
            el.emit('click')
          } else if (event.keyCode == data.keyCode) {
            //    console.log("key press by gui-interactable, keyCode: " + data.keyCode);
            el.emit('click')
          }
          event.preventDefault()
        },
        true
      )
    }
  },
  update: function update() {},
  tick: function tick() {},
  remove: function remove() {},
  pause: function pause() {},
  play: function play() {},
  setClickAction: function setClickAction(action) {
    this.data.clickAction = action //change function dynamically
  }
})

AFRAME.registerComponent('gui-button', {
  schema: {
    on: { default: 'click' },
    value: { type: 'string', default: '' },
    fontSize: { type: 'number', default: 0.2 },
    fontFamily: { type: 'string', default: '' },
    fontColor: { type: 'string', default: key_offwhite },
    borderColor: { type: 'string', default: key_offwhite },
    focusColor: { type: 'string', default: key_orange_light },
    backgroundColor: { type: 'string', default: key_grey },
    hoverColor: { type: 'string', default: key_grey_dark },
    activeColor: { type: 'string', default: key_orange },
    toggle: { type: 'boolean', default: false },
    toggleState: { type: 'boolean', default: false }
  },

  dependencies: ['aframe-troika-text'],

  init: function init() {
    var data = this.data
    var el = this.el
    var guiItem = el.getAttribute('gui-item')
    this.guiItem = guiItem

    //fallback for old font-sizing
    if (data.fontSize > 20) {
      // 150/1000
      var newSize = data.fontSize / 750
      data.fontSize = newSize
    }

    var guiInteractable = el.getAttribute('gui-interactable')
    this.guiInteractable = guiInteractable

    el.setAttribute(
      'geometry',
      'primitive: plane; \n                                     height: ' +
        guiItem.height +
        '; \n                                     width: ' +
        guiItem.width +
        ';\n                                     '
    )
    el.setAttribute(
      'material',
      'shader: flat; \n                                     transparent: true; \n                                     opacity: 0.5; \n                                     side:double; \n                                     color:' +
        data.backgroundColor +
        ';\n                                     '
    )

    var buttonContainer = document.createElement('a-entity')

    if (guiItem.bevel) {
      var bevelsize_adjust = guiItem.bevelSize * 1
      var bevelthickness_adjust = guiItem.bevelThickness
      buttonContainer.setAttribute(
        'bevelbox',
        'width: ' +
          (guiItem.width - guiItem.width * bevelsize_adjust) +
          '; \n                                                      height: ' +
          (guiItem.height - guiItem.height * bevelsize_adjust) +
          '; \n                                                      depth: ' +
          (guiItem.baseDepth - guiItem.baseDepth * bevelthickness_adjust) +
          ';\n                                                      bevelThickness: 0;\n                                                      bevelSize: ' +
          guiItem.bevelSize +
          ';\n                                                      '
      )
      buttonContainer.setAttribute('position', '0 0 0')
    } else {
      buttonContainer.setAttribute(
        'geometry',
        'primitive: box; \n                                                      width: ' +
          guiItem.width +
          '; \n                                                      height: ' +
          guiItem.height +
          '; \n                                                      depth: ' +
          guiItem.baseDepth +
          ';\n                                                      '
      )
      buttonContainer.setAttribute('position', '0 0 ' + guiItem.baseDepth / 2)
    }
    buttonContainer.setAttribute('rotation', '0 0 0')
    buttonContainer.setAttribute(
      'material',
      'shader: flat; \n                                                  opacity: 1; \n                                                  side:double; \n                                                  color: ' +
        data.borderColor +
        '\n                                                  '
    )
    el.appendChild(buttonContainer)
    this.buttonContainer = buttonContainer

    var buttonEntity = document.createElement('a-entity')
    if (guiItem.bevel) {
      var bevelsize_adjust = guiItem.bevelSize * 1
      var bevelthickness_adjust = guiItem.bevelThickness
      buttonEntity.setAttribute(
        'bevelbox',
        'width: ' +
          (guiItem.width -
            guiItem.gap -
            (guiItem.width - guiItem.gap) * bevelsize_adjust) +
          '; \n                                                   height: ' +
          (guiItem.height -
            guiItem.gap -
            (guiItem.height - guiItem.gap) * bevelsize_adjust) +
          '; \n                                                   depth: ' +
          (guiItem.depth - guiItem.depth * bevelthickness_adjust) +
          ';\n                                                   bevelThickness: ' +
          guiItem.bevelThickness +
          ';\n                                                   bevelSize: ' +
          guiItem.bevelSize +
          ';\n                                                   '
      )
      buttonEntity.setAttribute('position', '0 0 0')
    } else {
      buttonEntity.setAttribute(
        'geometry',
        'primitive: box; \n                                               width: ' +
          (guiItem.width - guiItem.gap) +
          '; \n                                               height: ' +
          (guiItem.height - guiItem.gap) +
          '; \n                                               depth: ' +
          guiItem.depth +
          ';'
      )
      buttonEntity.setAttribute('position', '0 0 ' + guiItem.depth / 2)
    }
    buttonEntity.setAttribute(
      'material',
      'shader: flat; \n                                               opacity: 1; \n                                               side:double; \n                                               color: ' +
        (data.toggleState ? data.activeColor : data.backgroundColor) +
        '\n                                               '
    )
    buttonEntity.setAttribute('rotation', '0 0 0')
    el.appendChild(buttonEntity)
    this.buttonEntity = buttonEntity

    this.setText(data.value)

    el.addEventListener('mouseenter', function (event) {
      buttonEntity.removeAttribute('animation__leave')
      if (!data.toggle) {
        buttonEntity.setAttribute(
          'animation__enter',
          'property: material.color; from: ' +
            data.backgroundColor +
            '; to:' +
            data.hoverColor +
            '; dur:200;'
        )
      }
    })
    el.addEventListener('mouseleave', function (event) {
      if (!data.toggle) {
        buttonEntity.removeAttribute('animation__click')
        buttonEntity.setAttribute(
          'animation__leave',
          'property: material.color; from: ' +
            data.hoverColor +
            '; to:' +
            data.backgroundColor +
            '; dur:200; easing: easeOutQuad;'
        )
      }
      buttonEntity.removeAttribute('animation__enter')
    })

    el.addEventListener('focus', function (event) {
      buttonContainer.setAttribute('material', 'color', '' + data.focusColor)
    })

    el.addEventListener('blur', function (event) {
      buttonContainer.setAttribute('material', 'color', '' + data.borderColor)
      if (!data.toggle) {
        buttonEntity.removeAttribute('animation__click')
        buttonEntity.setAttribute(
          'animation__leave',
          'property: material.color; from: ' +
            data.hoverColor +
            '; to:' +
            data.backgroundColor +
            '; dur:200; easing: easeOutQuad;'
        )
      }
      buttonEntity.removeAttribute('animation__enter')
    })

    el.addEventListener(data.on, function (event) {
      if (!data.toggle) {
        // if not toggling flashing active state
        buttonEntity.setAttribute(
          'animation__click',
          'property: material.color; from: ' +
            data.activeColor +
            '; to:' +
            data.backgroundColor +
            '; dur:400; easing: easeOutQuad;'
        )
      } else {
        var guiButton = el.components['gui-button']
        // console.log("about to toggle, current state: " + guiButton.data.toggleState);
        guiButton.setActiveState(!guiButton.data.toggleState)
        //  buttonEntity.setAttribute('material', 'color', data.activeColor);
      }

      var clickActionFunctionName = guiInteractable.clickAction
      // console.log("in button, clickActionFunctionName: "+clickActionFunctionName);
      // find object
      var clickActionFunction = window[clickActionFunctionName]
      //console.log("clickActionFunction: "+clickActionFunction);
      // is object a function?
      if (typeof clickActionFunction === 'function') clickActionFunction(event)
    })

    el.addEventListener('keyup', function (event) {
      if (event.isComposing || event.keyCode === 229) {
        return
      }

      if (event.keyCode == 13 || event.keyCode == 32) {
        el.emit(data.on)
      }
      event.preventDefault()
    })

    ////WAI ARIA Support
    el.setAttribute('role', 'button')
    el.setAttribute('tabindex', '0')
    el.setAttribute('aria-label', data.value)
  },
  play: function play() {},
  update: function update(oldData) {
    var data = this.data
    var el = this.el
    var guiItem = el.getAttribute('gui-item')
    this.guiItem = guiItem

    el.setAttribute(
      'geometry',
      'primitive: plane; \n                                     height: ' +
        guiItem.height +
        '; \n                                     width: ' +
        guiItem.width +
        ';\n                                     '
    )
    el.setAttribute(
      'material',
      'shader: flat; \n                                     transparent: true; \n                                     opacity: 0.5; \n                                     side:double; \n                                     color:' +
        data.backgroundColor +
        ';\n                                     '
    )

    if (guiItem.bevel) {
      var bevelsize_adjust = guiItem.bevelSize * 1
      var bevelthickness_adjust = guiItem.bevelThickness
      //this.buttonContainer.setAttribute('bevelbox', 'width: ' + (guiItem.width - guiItem.width * bevelsize_adjust) + '; \n                                                           height: ' + (guiItem.height - guiItem.height * bevelsize_adjust) + '; \n                                                           depth: ' + (guiItem.baseDepth - guiItem.baseDepth * bevelthickness_adjust) + ';\n                                                           bevelThickness: 0;\n                                                           bevelSize: ' + guiItem.bevelSize + ';\n                                                           ');
      this.buttonContainer.setAttribute('position', '0 0 0')
    } else {
      this.buttonContainer.setAttribute(
        'geometry',
        'primitive: box; \n                                                       width: ' +
          guiItem.width +
          '; \n                                                       height: ' +
          guiItem.height +
          '; \n                                                       depth: ' +
          guiItem.baseDepth +
          ';\n                                                       '
      )
      this.buttonContainer.setAttribute(
        'position',
        '0 0 ' + guiItem.baseDepth / 2
      )
    }
    this.buttonContainer.setAttribute(
      'material',
      'shader: flat; \n                                                       opacity: 1; \n                                                       side:double; \n                                                       color: ' +
        data.borderColor +
        '\n                                                       '
    )

    if (guiItem.bevel) {
      var bevelsize_adjust = guiItem.bevelSize * 1
      var bevelthickness_adjust = guiItem.bevelThickness
      //this.buttonEntity.setAttribute('bevelbox', 'width: ' + (guiItem.width - guiItem.gap - (guiItem.width - guiItem.gap) * bevelsize_adjust) + '; \n                                                        height: ' + (guiItem.height - guiItem.gap - (guiItem.height - guiItem.gap) * bevelsize_adjust) + '; \n                                                        depth: ' + (guiItem.depth - guiItem.depth * bevelthickness_adjust) + ';\n                                                        bevelThickness: ' + guiItem.bevelThickness + ';\n                                                        bevelSize: ' + guiItem.bevelSize + ';\n                                                        ');
      this.buttonEntity.setAttribute('position', '0 0 0')
    } else {
      this.buttonEntity.setAttribute(
        'geometry',
        'primitive: box; \n                                               width: ' +
          (guiItem.width - guiItem.gap) +
          '; \n                                               height: ' +
          (guiItem.height - guiItem.gap) +
          '; \n                                               depth: ' +
          guiItem.depth +
          ';\n                                               '
      )
      this.buttonEntity.setAttribute('position', '0 0 ' + guiItem.depth / 2)
    }
    this.buttonEntity.setAttribute(
      'material',
      'shader: flat; \n                                                    opacity: 1; \n                                                    side:double; \n                                                    color: ' +
        (data.toggleState ? data.activeColor : data.backgroundColor) +
        '\n                                                    '
    )

    if (this.textEntity) {
      var oldEntity = this.textEntity
      oldEntity.parentNode.removeChild(oldEntity)

      this.setText(this.data.value)
    } else {
      console.warn('no textEntity!')
    }
  },
  setActiveState: function setActiveState(activeState) {
    // console.log("in setActiveState function, new state: " + activeState);
    this.data.toggleState = activeState
    if (!activeState) {
      console.log('not active, about to set background color')
      this.buttonEntity.setAttribute(
        'material',
        'color',
        this.data.backgroundColor
      )
    } else {
      console.log('active, about to set active color')
      this.buttonEntity.setAttribute('material', 'color', this.data.activeColor)
    }
  },
  setText: function setText(newText) {
    var data = this.data
    var el = this.el
    var guiItem = el.getAttribute('gui-item')

    var textEntity = document.createElement('a-entity')
    this.textEntity = textEntity
    textEntity.setAttribute(
      'position',
      '0 0 ' + guiItem.baseDepth + 0.5
      // + 'depthOffset: 1; ' +
      // 'maxWidth: ' + guiItem.width / 1.05 + ';')
    )
    textEntity.setAttribute(
      'text',
      'wrapCount: 40; ' +
        'lineHeight: 40; ' +
        'width: 10; ' +
        'align: center; ' +
        'baseline: center; ' +
        'value: ' +
        newText +
        '; ' +
        'color: ' +
        data.fontColor +
        '; ' +
        'font: ' +
        data.fontFamily +
        '; ' +
        'fontSize: ' +
        data.fontSize +
        '; '
      // + 'depthOffset: 1; ' +
      // 'maxWidth: ' + guiItem.width / 1.05 + ';')
    )
    // if (guiItem.bevel) {
    //     textEntity.setAttribute('position', '0 0 ' + (guiItem.depth + guiItem.bevelThickness / 2 + 0.05));
    // } else {
    //     textEntity.setAttribute('position', '0 0 ' + (guiItem.depth / 2 + 0.05));
    // }
    //        textEntity.setAttribute('troika-text-material', `shader: flat;`);
    this.buttonEntity.appendChild(textEntity)
  }
})

AFRAME.registerPrimitive('a-gui-button', {
  defaultComponents: {
    'gui-interactable': {},
    'gui-item': { type: 'button' },
    'gui-button': {}
  },
  mappings: {
    //gui interactable general
    onclick: 'gui-interactable.clickAction',
    onhover: 'gui-interactable.hoverAction',
    'key-code': 'gui-interactable.keyCode',
    //gui item general
    width: 'gui-item.width',
    height: 'gui-item.height',
    depth: 'gui-item.depth',
    'base-depth': 'gui-item.baseDepth',
    gap: 'gui-item.gap',
    radius: 'gui-item.radius',
    margin: 'gui-item.margin',
    //gui item bevelbox
    bevel: 'gui-item.bevel',
    'bevel-segments': 'gui-item.bevelSegments',
    steps: 'gui-item.steps',
    'bevel-size': 'gui-item.bevelSize',
    'bevel-offset': 'gui-item.bevelOffset',
    'bevel-thickness': 'gui-item.bevelThickness',
    //gui button specific
    on: 'gui-button.on',
    value: 'gui-button.value',
    'font-size': 'gui-button.fontSize',
    'font-family': 'gui-button.fontFamily',
    'font-color': 'gui-button.fontColor',
    'border-color': 'gui-button.borderColor',
    'focus-color': 'gui-button.focusColor',
    'background-color': 'gui-button.backgroundColor',
    'hover-color': 'gui-button.hoverColor',
    'active-color': 'gui-button.activeColor',
    toggle: 'gui-button.toggle',
    'toggle-state': 'gui-button.toggleState'
  }
})

AFRAME.registerComponent('rounded', {
  schema: {
    enabled: { default: true },
    width: { type: 'number', default: 1 },
    height: { type: 'number', default: 1 },
    radius: { type: 'number', default: 0.3 },
    topLeftRadius: { type: 'number', default: -1 },
    topRightRadius: { type: 'number', default: -1 },
    bottomLeftRadius: { type: 'number', default: -1 },
    bottomRightRadius: { type: 'number', default: -1 },
    depthWrite: { default: true },
    polygonOffset: { default: false },
    polygonOffsetFactor: { type: 'number', default: 0 },
    color: { type: 'color', default: '#F0F0F0' },
    opacity: { type: 'number', default: 1 }
  },
  init: function init() {
    this.rounded = new THREE.Mesh(
      this.draw(),
      new THREE.MeshStandardMaterial({
        color: new THREE.Color(this.data.color)
      })
    )
    this.updateOpacity()
    this.el.setObject3D('mesh', this.rounded)
  },
  update: function update() {
    if (this.data.enabled) {
      if (this.rounded) {
        this.rounded.visible = true
        this.rounded.geometry = this.draw()
        this.rounded.material.color = new THREE.Color(this.data.color)
        this.updateOpacity()
      }
    } else {
      this.rounded.visible = false
    }
  },
  updateOpacity: function updateOpacity() {
    if (this.data.opacity < 0) {
      this.data.opacity = 0
    }
    if (this.data.opacity > 1) {
      this.data.opacity = 1
    }
    if (this.data.opacity < 1) {
      this.rounded.material.transparent = true
      this.rounded.material.opacity = this.data.opacity
      this.rounded.material.alphaTest = 0
    } else {
      this.rounded.material.transparent = false
    }
  },
  tick: function tick() {},
  remove: function remove() {
    if (!this.rounded) {
      return
    }
    this.el.object3D.remove(this.rounded)
    this.rounded = null
  },
  draw: function draw() {
    var roundedRectShape = new THREE.Shape()
    function roundedRect(
      ctx,
      x,
      y,
      width,
      height,
      topLeftRadius,
      topRightRadius,
      bottomLeftRadius,
      bottomRightRadius
    ) {
      if (!topLeftRadius) {
        topLeftRadius = 0.00001
      }
      if (!topRightRadius) {
        topRightRadius = 0.00001
      }
      if (!bottomLeftRadius) {
        bottomLeftRadius = 0.00001
      }
      if (!bottomRightRadius) {
        bottomRightRadius = 0.00001
      }
      ctx.moveTo(x, y + topLeftRadius)
      ctx.lineTo(x, y + height - topLeftRadius)
      ctx.quadraticCurveTo(x, y + height, x + topLeftRadius, y + height)
      ctx.lineTo(x + width - topRightRadius, y + height)
      ctx.quadraticCurveTo(
        x + width,
        y + height,
        x + width,
        y + height - topRightRadius
      )
      ctx.lineTo(x + width, y + bottomRightRadius)
      ctx.quadraticCurveTo(x + width, y, x + width - bottomRightRadius, y)
      ctx.lineTo(x + bottomLeftRadius, y)
      ctx.quadraticCurveTo(x, y, x, y + bottomLeftRadius)
    }

    var corners = [
      this.data.radius,
      this.data.radius,
      this.data.radius,
      this.data.radius
    ]
    if (this.data.topLeftRadius != -1) {
      corners[0] = this.data.topLeftRadius
    }
    if (this.data.topRightRadius != -1) {
      corners[1] = this.data.topRightRadius
    }
    if (this.data.bottomLeftRadius != -1) {
      corners[2] = this.data.bottomLeftRadius
    }
    if (this.data.bottomRightRadius != -1) {
      corners[3] = this.data.bottomRightRadius
    }

    roundedRect(
      roundedRectShape,
      -this.data.width / 2,
      -this.data.height / 2,
      this.data.width,
      this.data.height,
      corners[0],
      corners[1],
      corners[2],
      corners[3]
    )
    return new THREE.ShapeGeometry(roundedRectShape)
  },
  pause: function pause() {},
  play: function play() {}
})

// Funkcia pre spracovanie pridaného dieťaťa
function handleChildAdded(containerElement, addedChild) {
  addedChild.addEventListener('loaded', function () {
    if (
      containerElement.components &&
      containerElement.components['gui-flex-container']
    ) {
      containerElement.components['gui-flex-container'].init()
    }
  })
}

// Pôvodná funkcia onAppendChildToContainer pre už existujúce deti
var onAppendChildToContainer = function onAppendChildToContainer(elem, f) {
  var observer = new MutationObserver(function (mutations, me) {
    mutations.forEach(function (m) {
      if (m.addedNodes.length) {
        f(m.target, m.addedNodes)
      }
    })
  })
  observer.observe(elem, { childList: true })
}

// Registrácia komponentu 'gui-flex-container'
AFRAME.registerComponent('gui-flex-container', {
  schema: {
    flexDirection: { type: 'string', default: 'row' },
    justifyContent: { type: 'string', default: 'flexStart' },
    alignItems: { type: 'string', default: 'flexStart' },
    itemPadding: { type: 'number', default: 0.0 },
    opacity: { type: 'number', default: 0.0 },
    isTopContainer: { type: 'boolean', default: false },
    panelColor: { type: 'string', default: 'grey' },
    panelRounded: { type: 'number', default: 0.05 },
    styles: {
      fontFamily: { type: 'string', default: 'Helvetica' },
      fontColor: { type: 'string', default: 'offwhite' },
      borderColor: { type: 'string', default: 'offwhite' },
      backgroundColor: { type: 'string', default: 'grey' },
      hoverColor: { type: 'string', default: 'darkgrey' },
      activeColor: { type: 'string', default: 'orange' },
      handleColor: { type: 'string', default: 'offwhite' }
    }
  },
  init: function init() {
    console.log(
      'in aframe-gui-component init for: ' + this.el.getAttribute('id')
    )
    var containerGuiItem = this.el.getAttribute('gui-item')

    if (this.data.isTopContainer) {
      this.setBackground()
    } else {
      //          this.el.setAttribute('material', `shader: flat; transparent: true; alphaTest: 0.5; side:front;`);
      this.el.setAttribute(
        'rounded',
        'height: ' +
          containerGuiItem.height +
          '; width: ' +
          containerGuiItem.width +
          '; opacity: ' +
          this.data.opacity +
          '; color: ' +
          this.data.panelColor +
          '; radius:' +
          this.data.panelRounded +
          '; depthWrite:false; polygonOffset:true; polygonOffsetFactor: 1;'
      )
    }

    this.children = this.el.getChildEntities()
    //console.log("childElements: "+this.children);
    //console.log("num child Elements: "+this.children.length);

    // coordinate system is 0, 0 in the top left
    var cursorX = 0
    var cursorY = 0
    if (this.data.flexDirection == 'row') {
      // first figure out cursor position on main X axis
      if (this.data.justifyContent == 'flexStart') {
        cursorX = 0
      } else if (
        this.data.justifyContent == 'center' ||
        this.data.justifyContent == 'flexEnd'
      ) {
        var rowWidth = 0
        for (var i = 0; i < this.children.length; i++) {
          var childElement = this.children[i]
          var childGuiItem = childElement.getAttribute('gui-item')
          rowWidth =
            rowWidth +
            childGuiItem.margin.w +
            childGuiItem.width +
            childGuiItem.margin.y
        }
        if (this.data.justifyContent == 'center') {
          cursorX = (containerGuiItem.width - rowWidth) * 0.5
        } else if (this.data.justifyContent == 'flexEnd') {
          cursorX = containerGuiItem.width - rowWidth
        }
      }
      // then figure out baseline / cursor position on cross Y axis
      if (this.data.alignItems == 'center') {
        cursorY = containerGuiItem.height // baseline is center
      } else if (this.data.alignItems == 'flexStart') {
        cursorY = 0 // baseline is top of container
      } else if (this.data.alignItems == 'flexEnd') {
        cursorY = containerGuiItem.height // baseline is bottom of container
      }
    } else if (this.data.flexDirection == 'column') {
      // first figure out cursor position on main Y axis
      if (this.data.justifyContent == 'flexStart') {
        cursorY = 0
      } else if (
        this.data.justifyContent == 'center' ||
        this.data.justifyContent == 'flexEnd'
      ) {
        var columnHeight = 0
        for (var i = 0; i < this.children.length; i++) {
          var childElement = this.children[i]
          //console.log("childElement: "+childElement);
          var childGuiItem = childElement.getAttribute('gui-item')
          //console.log("childGuiItem: "+childGuiItem);
          columnHeight = columnHeight + childGuiItem.height
        }
        if (this.data.justifyContent == 'center') {
          cursorY = (containerGuiItem.height - columnHeight) * 0.5
        } else if (this.data.justifyContent == 'flexEnd') {
          cursorY = containerGuiItem.height - columnHeight
        }
      }
      // then figure out baseline / cursor position on cross X axis
      if (this.data.alignItems == 'flexStart') {
        cursorX = 0 // baseline is left
      } else if (this.data.alignItems == 'center') {
        cursorX = containerGuiItem.width * 0.5 // baseline is center
      } else if (this.data.alignItems == 'flexEnd') {
        cursorX = 0 // baseline is right
      }
    }
    //console.log(`initial cursor position for ${this.el.getAttribute("id")}: ${cursorX} ${cursorY} 0.01`)

    // not that cursor positions are determined, loop through and lay out items
    var wrapOffsetX = 0 // not used yet since wrapping isn't supported
    var wrapOffsetY = 0 // not used yet since wrapping isn't supported
    for (var i = 0; i < this.children.length; i++) {
      var childElement = this.children[i]
      // TODO: change this to call gedWidth() and setWidth() of component
      var childPositionX = 0
      var childPositionY = 0
      var childPositionZ = 0.01
      var childGuiItem = childElement.getAttribute('gui-item')

      // now get object position in aframe container cordinates (0, 0 is center)
      if (childGuiItem) {
        if (this.data.flexDirection == 'row') {
          if (this.data.alignItems == 'center') {
            childPositionY = 0 // child position is always 0 for center vertical alignment
          } else if (this.data.alignItems == 'flexStart') {
            childPositionY =
              containerGuiItem.height * 0.5 -
              childGuiItem.margin.x -
              childGuiItem.height
          } else if (this.data.alignItems == 'flexEnd') {
            childPositionY =
              -containerGuiItem.height * 0.5 +
              childGuiItem.margin.z +
              childGuiItem.height
          }
          childPositionX =
            -containerGuiItem.width * 0.5 +
            cursorX +
            childGuiItem.margin.w +
            childGuiItem.width * 0.5
          cursorX =
            cursorX +
            childGuiItem.margin.w +
            childGuiItem.width +
            childGuiItem.margin.y
        } else if (this.data.flexDirection == 'column') {
          if (this.data.alignItems == 'center') {
            childPositionX = 0 // child position is always 0 to center
          } else if (this.data.alignItems == 'flexStart') {
            childPositionX =
              -containerGuiItem.width * 0.5 +
              childGuiItem.margin.w +
              childGuiItem.width * 0.5
          } else if (this.data.alignItems == 'flexEnd') {
            childPositionX =
              containerGuiItem.width * 0.5 -
              childGuiItem.margin.y -
              childGuiItem.width * 0.5
          }
          childPositionY =
            containerGuiItem.height * 0.5 -
            cursorY -
            -childGuiItem.margin.x -
            childGuiItem.height * 0.5
          cursorY =
            cursorY +
            childGuiItem.margin.x +
            childGuiItem.height +
            childGuiItem.margin.z
        }
        //console.log(`child element position for ${childElement.id}: ${childPositionX} ${childPositionY} ${childPositionZ}`)
        childElement.setAttribute(
          'position',
          childPositionX + ' ' + childPositionY + ' ' + childPositionZ
        )
        childElement.setAttribute(
          'geometry',
          'primitive: plane; height: ' +
            childGuiItem.height +
            '; width: ' +
            childGuiItem.width +
            ';'
        )

        // var childFlexContainer = childElement.components['gui-flex-container'];
        // if (childFlexContainer) {
        //     childFlexContainer.setBackground();
        // }
      }
    }

    // onAppendChildToContainer(this.el, function (containerElement, addedChildren) {
    //     //console.log('****** containerElement: ' + containerElement);
    //     //console.log('****** addedChildren: ' + addedChildren.length);
    //     // containerElement.components['gui-flex-container'].init();
    //     var addedChild = addedChildren[0];
    //     addedChildren[0].addEventListener("loaded", function (e) {
    //         //console.log('in appended element loaded handler: '+e);
    //         //console.log('addedChild: '+addedChild);
    //         //console.log('****** containerElement: ' + containerElement);
    //         containerElement.components['gui-flex-container'].init();
    //     });
    // });

    // Pridajte event listener na 'child-attached' event
    this.el.addEventListener(
      'child-attached',
      function (event) {
        var addedChild = event.detail.el
        handleChildAdded(this.el, addedChild)
      }.bind(this)
    )

    // Volanie pôvodnej funkcie na existujúce deti
    onAppendChildToContainer(
      this.el,
      function (containerElement, addedChildren) {
        var addedChild = addedChildren[0]
        handleChildAdded(containerElement, addedChild)
      }
    )
  },
  update: function () {},
  tick: function () {},
  remove: function () {},
  pause: function () {},
  play: function () {},
  getElementSize: function () {},
  setBackground: function () {
    if (this.data.opacity > 0) {
      var guiItem = this.el.getAttribute('gui-item')
      var panelBackground = document.createElement('a-entity')
      panelBackground.setAttribute(
        'rounded',
        'height: ' +
          guiItem.height +
          '; width: ' +
          guiItem.width +
          '; opacity: ' +
          this.data.opacity +
          '; color: ' +
          this.data.panelColor +
          '; radius:' +
          this.data.panelRounded +
          '; depthWrite:false; polygonOffset:true; polygonOffsetFactor: 2;'
      )
      panelBackground.setAttribute(
        'position',
        this.el.getAttribute('position').x +
          ' ' +
          this.el.getAttribute('position').y +
          ' ' +
          (this.el.getAttribute('position').z - 0.0125)
      )
      panelBackground.setAttribute(
        'rotation',
        this.el.getAttribute('rotation').x +
          ' ' +
          this.el.getAttribute('rotation').y +
          ' ' +
          this.el.getAttribute('rotation').z
      )
      this.el.parentNode.insertBefore(panelBackground, this.el)
    }
  }
})

// Registrácia primitívu 'a-gui-flex-container'
AFRAME.registerPrimitive('a-gui-flex-container', {
  defaultComponents: {
    'gui-item': { type: 'flex-container' },
    'gui-flex-container': {}
  },
  mappings: {
    width: 'gui-item.width',
    height: 'gui-item.height',
    margin: 'gui-item.margin',
    'flex-direction': 'gui-flex-container.flexDirection',
    'justify-content': 'gui-flex-container.justifyContent',
    'align-items': 'gui-flex-container.alignItems',
    'item-padding': 'gui-flex-container.itemPadding',
    opacity: 'gui-flex-container.opacity',
    'is-top-container': 'gui-flex-container.isTopContainer',
    'panel-color': 'gui-flex-container.panelColor',
    'panel-rounded': 'gui-flex-container.panelRounded',
    'font-family': 'gui-flex-container.styles.fontFamily',
    'font-color': 'gui-flex-container.styles.fontColor',
    'border-color': 'gui-flex-container.styles.borderColor',
    'background-color': 'gui-flex-container.styles.backgroundColor',
    'hover-color': 'gui-flex-container.styles.hoverColor',
    'active-color': 'gui-flex-container.styles.activeColor',
    'handle-color': 'gui-flex-container.styles.handleColor'
  }
})

/* global AFRAME */
AFRAME.registerComponent('highlight', {
  init: function () {
    var buttonEls = (this.buttonEls = this.el.querySelectorAll('.menu-button'))
    var backgroundEl = document.querySelector('#background')
    this.onClick = this.onClick.bind(this)
    this.onMouseEnter = this.onMouseEnter.bind(this)
    this.onMouseLeave = this.onMouseLeave.bind(this)
    this.reset = this.reset.bind(this)
    backgroundEl.addEventListener('click', this.reset)
    for (var i = 0; i < buttonEls.length; ++i) {
      buttonEls[i].addEventListener('mouseenter', this.onMouseEnter)
      buttonEls[i].addEventListener('mouseleave', this.onMouseLeave)
      buttonEls[i].addEventListener('click', this.onClick)
    }
  },

  onClick: function (evt) {
    evt.target.pause()
    evt.target.setAttribute('slice9', 'color', '#046de7')
    this.el.addState('clicked')
    evt.target.object3D.scale.set(1.2, 1.2, 1.2)
  },

  onMouseEnter: function (evt) {
    var buttonEls = this.buttonEls
    evt.target.setAttribute('slice9', 'color', '#a1bddd')
    for (var i = 0; i < buttonEls.length; ++i) {
      if (evt.target === buttonEls[i]) {
        continue
      }
      buttonEls[i].setAttribute('slice9', 'color', 'black')
    }
  },

  onMouseLeave: function (evt) {
    if (this.el.is('clicked')) {
      return
    }
    evt.target.setAttribute('slice9', 'color', 'black')
  },

  reset: function () {
    var buttonEls = this.buttonEls
    for (var i = 0; i < buttonEls.length; ++i) {
      this.el.removeState('clicked')
      buttonEls[i].play()
      buttonEls[i].emit('mouseleave')
    }
  }
})

AFRAME.registerComponent('gui-button-rounded', {
  schema: {
    on: { default: 'click' },
    value: { type: 'string', default: '' },
    fontSize: { type: 'number', default: 0.2 },
    fontFamily: { type: 'string', default: '' },
    fontColor: { type: 'string', default: key_offwhite },
    borderColor: { type: 'string', default: key_offwhite },
    focusColor: { type: 'string', default: key_orange_light },
    backgroundColor: { type: 'string', default: key_grey },
    hoverColor: { type: 'string', default: key_grey_dark },
    activeColor: { type: 'string', default: key_orange },
    toggle: { type: 'boolean', default: false },
    toggleState: { type: 'boolean', default: false },
    position: { type: 'string', default: '-1 2.5 0.08' }
  },

  dependencies: ['aframe-troika-text'],

  init: function init() {
    var data = this.data
    var el = this.el
    var guiItem = el.getAttribute('gui-item')
    this.guiItem = guiItem

    //fallback for old font-sizing
    if (data.fontSize > 20) {
      // 150/1000
      var newSize = data.fontSize / 750
      data.fontSize = newSize
    }

    var guiInteractable = el.getAttribute('gui-interactable')
    this.guiInteractable = guiInteractable

    var containerGuiItem = guiItem

    if (this.data.isTopContainer) {
      this.setBackground()
    } else {
      //          this.el.setAttribute('material', `shader: flat; transparent: true; alphaTest: 0.5; side:front;`);
      this.el.setAttribute(
        'rounded',
        'height: ' +
          containerGuiItem.height +
          '; width: ' +
          containerGuiItem.width +
          '; opacity: ' +
          1 +
          '; color: ' +
          'red' +
          '; radius:' +
          0.5 +
          '; depthWrite:false; polygonOffset:true; polygonOffsetFactor: 1;'
      )
      this.el.setAttribute(
        'material',
        'shader: flat; \n                                     transparent: true; \n                                     opacity: 1; \n                                     side:double; \n                                     color:' +
          data.backgroundColor +
          ';\n                                     '
      )
    }

    var buttonContainer = document.createElement('a-entity')
    buttonContainer.setAttribute('position', '0 0 ' + guiItem.baseDepth / 2)
    buttonContainer.setAttribute('rotation', '0 0 0')
    buttonContainer.setAttribute(
      'rounded',
      'height: ' +
        containerGuiItem.height +
        '; width: ' +
        containerGuiItem.width +
        '; opacity: ' +
        1 +
        '; color: ' +
        'red' +
        '; radius:' +
        0.5 +
        '; depthWrite:false; polygonOffset:true; polygonOffsetFactor: 1;'
    )
    buttonContainer.setAttribute(
      'material',
      'shader: flat; \n                                                  opacity: 1; \n                                                  side:double; \n                                                  color: ' +
        data.borderColor +
        '\n                                                  '
    )
    //el.appendChild(buttonContainer);
    this.buttonContainer = buttonContainer

    var buttonEntity = document.createElement('a-entity')
    buttonEntity.setAttribute('position', '0 0 ' + guiItem.baseDepth / 2)
    buttonEntity.setAttribute(
      'rounded',
      'height: ' +
        containerGuiItem.height +
        '; width: ' +
        containerGuiItem.width +
        '; opacity: ' +
        1 +
        '; color: ' +
        'red' +
        '; radius:' +
        0.35 +
        '; depthWrite:false; polygonOffset:true; polygonOffsetFactor: 1;'
    )
    buttonEntity.setAttribute(
      'material',
      'shader: flat; \n                                                    opacity: 1; \n                                                    side:double; \n                                                    color: ' +
        (data.toggleState ? data.activeColor : data.backgroundColor) +
        '\n                                                    '
    )
    buttonEntity.setAttribute('rotation', '0 0 0')
    el.appendChild(buttonEntity)
    this.buttonEntity = buttonEntity

    this.setText(data.value)

    el.addEventListener('mouseenter', function (event) {
      buttonEntity.removeAttribute('animation__leave')
      if (!data.toggle) {
        buttonEntity.setAttribute('animation__enter', 'property: rounded.color; from: ' + data.backgroundColor + '; to:' + data.hoverColor + '; dur:0;');
        el.object3D.scale.z += 1;
    }
    })
    el.addEventListener('mouseleave', function (event) {
      if (!data.toggle) {
        buttonEntity.removeAttribute('animation__click')
        buttonEntity.setAttribute(
          'animation__leave',
          'property: rounded.color; from: ' +
            data.hoverColor +
            '; to:' +
            data.backgroundColor +
            '; dur:100; easing: easeOutQuad;'
        )
        el.object3D.scale.z -= 1;
      }
      buttonEntity.removeAttribute('animation__enter')
    })

    el.addEventListener('focus', function (event) {
      buttonContainer.setAttribute('material', 'color', '' + data.focusColor)
    })

    el.addEventListener('blur', function (event) {
      buttonContainer.setAttribute('material', 'color', '' + data.borderColor)
      if (!data.toggle) {
        el.removeAttribute('animation__click')
        el.setAttribute(
          'animation__leave',
          'property: rounded.color; from: ' +
            data.hoverColor +
            '; to:' +
            data.backgroundColor +
            '; dur:200; easing: easeOutQuad;'
        )
      }
      buttonEntity.removeAttribute('animation__enter')
    })

    el.addEventListener(data.on, function (event) {
      if (!data.toggle) {
        // if not toggling flashing active state
        buttonEntity.setAttribute(
          'animation__click',
          'property: rounded.color; from: ' +
            data.activeColor +
            '; to:' +
            data.backgroundColor +
            '; dur:400; easing: easeOutQuad;'
        )
      } else {
        var guiButton = el.components['gui-button']
        // console.log("about to toggle, current state: " + guiButton.data.toggleState);
        guiButton.setActiveState(!guiButton.data.toggleState)
        //  buttonEntity.setAttribute('material', 'color', data.activeColor);
      }

      var clickActionFunctionName = guiInteractable.clickAction
      // console.log("in button, clickActionFunctionName: "+clickActionFunctionName);
      // find object
      var clickActionFunction = window[clickActionFunctionName]
      //console.log("clickActionFunction: "+clickActionFunction);
      // is object a function?
      if (typeof clickActionFunction === 'function') clickActionFunction(event)
    })

    el.addEventListener('keyup', function (event) {
      if (event.isComposing || event.keyCode === 229) {
        return
      }

      if (event.keyCode == 13 || event.keyCode == 32) {
        el.emit(data.on)
      }
      event.preventDefault()
    })

    ////WAI ARIA Support
    el.setAttribute('role', 'button')
    el.setAttribute('tabindex', '0')
    el.setAttribute('aria-label', data.value)
    el.setAttribute('position', data.position)
  },
  play: function play() {},
  update: function update(oldData) {},
  setActiveState: function setActiveState(activeState) {
    // console.log("in setActiveState function, new state: " + activeState);
    this.data.toggleState = activeState
    if (!activeState) {
      console.log('not active, about to set background color')
      this.buttonEntity.setAttribute(
        'material',
        'color',
        this.data.backgroundColor
      )
    } else {
      console.log('active, about to set active color')
      this.buttonEntity.setAttribute('material', 'color', this.data.activeColor)
    }
  },
  setText: function setText(newText) {
    var data = this.data
    var el = this.el

    var textEntity = document.createElement('a-text')
    this.textEntity = textEntity
    textEntity.setAttribute(
      'position',
      '0 0 0.1'
      // + 'depthOffset: 1; ' +
      // 'maxWidth: ' + guiItem.width / 1.05 + ';')
    )
    // textEntity.setAttribute('text', 'wrapCount: 40; ' +
    //     'lineHeight: 40; ' +
    //     'width: 10; ' +
    //     'align: center; ' +
    //     'baseline: center; ' +
    //     'value: ' + newText + '; ' +
    //     'color: ' + data.fontColor + '; ' +
    //     'font: ' + data.fontFamily + '; ' +
    //     'fontSize: ' + data.fontSize + '; '
    //     // + 'depthOffset: 1; ' +
    //     // 'maxWidth: ' + guiItem.width / 1.05 + ';')
    // );
    textEntity.setAttribute('wrapCount', 40);
    textEntity.setAttribute('lineHeight', 40);
    textEntity.setAttribute('width', 9);
    textEntity.setAttribute('align', 'center');
    textEntity.setAttribute('baseline', 'center');
    textEntity.setAttribute('value', newText);
    textEntity.setAttribute('color', data.fontColor);
    textEntity.setAttribute('fontSize', data.fontSize);
    this.buttonEntity.appendChild(textEntity);
  },
  setBackground: function () {
    if (this.data.opacity > 0) {
      var guiItem = this.el.getAttribute('gui-item')
      var panelBackground = document.createElement('a-entity')
      panelBackground.setAttribute(
        'rounded',
        'height: ' +
          guiItem.height +
          '; width: ' +
          guiItem.width +
          '; opacity: ' +
          this.data.opacity +
          '; color: ' +
          this.data.panelColor +
          '; radius:' +
          this.data.panelRounded +
          '; depthWrite:false; polygonOffset:true; polygonOffsetFactor: 2;'
      )
      panelBackground.setAttribute(
        'position',
        this.el.getAttribute('position').x +
          ' ' +
          this.el.getAttribute('position').y +
          ' ' +
          (this.el.getAttribute('position').z - 0.0125)
      )
      panelBackground.setAttribute(
        'rotation',
        this.el.getAttribute('rotation').x +
          ' ' +
          this.el.getAttribute('rotation').y +
          ' ' +
          this.el.getAttribute('rotation').z
      )
      this.el.parentNode.insertBefore(panelBackground, this.el)
    }
  }
})

AFRAME.registerPrimitive('a-gui-button-rounded', {
  defaultComponents: {
    'gui-interactable': {},
    'gui-item': { type: 'button' },
    'gui-button-rounded': {}
  },
  mappings: {
    //gui interactable general
    onclick: 'gui-interactable.clickAction',
    onhover: 'gui-interactable.hoverAction',
    'key-code': 'gui-interactable.keyCode',
    //gui item general
    width: 'gui-item.width',
    height: 'gui-item.height',
    depth: 'gui-item.depth',
    'base-depth': 'gui-item.baseDepth',
    gap: 'gui-item.gap',
    radius: 'gui-item.radius',
    margin: 'gui-item.margin',
    'position-xy': 'gui-item.position',
    //gui item bevelbox
    bevel: 'gui-item.bevel',
    'bevel-segments': 'gui-item.bevelSegments',
    steps: 'gui-item.steps',
    'bevel-size': 'gui-item.bevelSize',
    'bevel-offset': 'gui-item.bevelOffset',
    'bevel-thickness': 'gui-item.bevelThickness',
    //gui button specific
    on: 'gui-button-rounded.on',
    value: 'gui-button-rounded.value',
    'font-size': 'gui-button-rounded.fontSize',
    'font-family': 'gui-button-rounded.fontFamily',
    'font-color': 'gui-button-rounded.fontColor',
    'border-color': 'gui-button-rounded.borderColor',
    'focus-color': 'gui-button-rounded.focusColor',
    'background-color': 'gui-button-rounded.backgroundColor',
    'hover-color': 'gui-button-rounded.hoverColor',
    'active-color': 'gui-button-rounded.activeColor',
    toggle: 'gui-button-rounded.toggle',
    'toggle-state': 'gui-button-rounded.toggleState'
  }
})

AFRAME.registerComponent('fixed-position', {
  schema: {
    distance: { type: 'number', default: 0.1 } // Vzdialenosť od kamery
  },

  init: function () {
    // Získať počiatočnú polohu a rotáciu kamery
    const el = this.el
    let gripActive = false
    var camera = this.el.sceneEl.camera
    var cameraPosition = camera.el.getAttribute('position')
    var cameraRotation = camera.el.getAttribute('rotation')
    var fadeBackgroundEl = (this.fadeBackgroundEl =
      document.querySelector('#fadeBackground'))
    var windowEl = (this.windowEl = document.querySelector('#window'))
    var initialPosition = this.calculatePosition(cameraPosition, cameraRotation)
    //Desktop set up - EventListener
    var isVisible = false // Počiatočný stav viditeľnosti

    window.addEventListener('keydown', function (event) {
      if (event.key === 'x') {
        isVisible = !isVisible
        el.setAttribute('visible', isVisible)
        el.setAttribute('rotation', cameraRotation)
      }
    })

    // // Oculus set up - EventListener
    // el.addEventListener('bbuttondown', () => {
    //     // if (event.detail.index === 1) {  // Tlačidlo B má index 1
    //     toggleVisibility();
    //     // }
    // });

    // const toggleVisibility = () => {
    //     isVisible = !isVisible;
    //     console.log('Prepínanie viditeľnosti.');
    //     el.setAttribute('visible', isVisible);
    //     el.setAttribute('rotation', camera.getAttribute('rotation'));
    // };

    // //Oculus set up - EventListener
    // // Set gripActive to true on 'gripdown' event
    // el.addEventListener('gripdown', () => {
    //     gripActive = true;
    // });
    //
    // // Set gripActive to false on 'gripup' event
    // el.addEventListener('gripup', () => {
    //     gripActive = false;
    // });

    //gripActive ? this.el.visible = true : this.el.visible = false;

    this.el.setAttribute('position', initialPosition)
  },

  tick: function () {
    // Získať aktuálnu polohu a rotáciu kamery
    var camera = this.el.sceneEl.camera
    var cameraPosition = camera.el.getAttribute('position')
    var cameraRotation = camera.el.getAttribute('rotation')

    // Vypočítať novú polohu entity
    var newPosition = this.calculatePosition(cameraPosition, cameraRotation)

    // Nastaviť novú polohu entity
    // console.log(newPosition)
    this.el.setAttribute('position', newPosition)
    //this.el.setAttribute('rotation', cameraRotation)
  },

  calculatePosition: function (cameraPosition, cameraRotation) {
    var distance = this.data.distance

    // Previesť rotáciu kamery na radiány
    var theta = THREE.MathUtils.degToRad(cameraRotation.y)
    var phi = THREE.MathUtils.degToRad(cameraRotation.x)

    // Vypočítať novú polohu na základe vzdialenosti
    var x = cameraPosition.x - distance * Math.sin(theta) * Math.cos(phi)
    var y = cameraPosition.y + distance * Math.sin(phi)
    var z = cameraPosition.z - distance * Math.cos(theta) * Math.cos(phi)

    return { x: x, y: y, z: z }
  }
})

AFRAME.registerComponent('b-button-listener', {
  init: function () {
    const el = this.el
    el.addEventListener('bbuttondown', (evt) => {
      toggleVisibility()
    })

    const toggleVisibility = () => {
      const menuWindow = document.querySelector('#window')
      var camera = this.el.sceneEl.camera

      let isVisible = menuWindow.getAttribute('visible')
      isVisible = !isVisible
      menuWindow.setAttribute('visible', isVisible)
      menuWindow.setAttribute('position', camera.el.getAttribute('position'))
      let currentRotation = camera.el.getAttribute('rotation')
      menuWindow.setAttribute('rotation', {
        x: currentRotation.x,
        y: currentRotation.y,
        z: 0
      })
    }
  }
})

AFRAME.registerComponent('click-change-color', {
  schema: {
    id: { default: null },
    color: { default: '#639B4C' }
  },
  init: function () {
    var el = this.el
    var id = this.data.id
    var container = document.querySelector('#' + id)
    var colorFromSchema = el.components['click-change-color'].data.color

    el.addEventListener('click', function () {
      if (container) {
        var currentColor = container.getAttribute('rounded').color
        if (currentColor === colorFromSchema) {
          container.setAttribute('material', 'color', 'originalColor')
        } else {
          container.setAttribute('material', 'color', colorFromSchema)
        }
      } else {
        console.error('Element with ID ' + id + ' not found.')
      }
    })
  }
})

AFRAME.registerComponent('xyimage', {
  schema: {
    color: { type: 'string', default: '#ffffff' },
    fixed: { type: 'boolean', default: true },
    alphaTest: { type: 'number', default: 0.5 },
    src: { type: 'asset' },
    width: { type: 'number', default: 1 },
    height: { type: 'number', default: 1 },
    position: { type: 'string', default: '0 -2.5 0.1' },
    guiInteractable: { type: 'string', default: '' },
    clickChangeColor: { type: 'string', default: 'id: container-window-4' },
    on: { default: 'click' },
    value: { type: 'string', default: '' },
    fontSize: { type: 'number', default: 0.2 },
    fontFamily: { type: 'string', default: '' },
    fontColor: { type: 'string', default: key_offwhite },
    borderColor: { type: 'string', default: key_offwhite },
    focusColor: { type: 'string', default: key_orange_light },
    backgroundColor: { type: 'string', default: key_grey },
    hoverColor: { type: 'string', default: key_grey_dark },
    activeColor: { type: 'string', default: key_orange },
    toggle: { type: 'boolean', default: false },
    toggleState: { type: 'boolean', default: false }
  },

  init: function () {
    var data = this.data
    var el = this.el

    var guiInteractable = el.getAttribute('gui-interactable')
    this.guiInteractable = guiInteractable

    el.setAttribute('geometry', {
      primitive: 'plane',
      width: data.width,
      height: data.height
    })

    el.setAttribute('material', {
      shader: 'flat',
      side: 'front',
      src: data.src,
      transparent: true,
      alphaTest: data.alphaTest,
      color: data.color
    })

    el.setAttribute('position', data.position)

    // if (data.guiInteractable !== '') {
    //     el.setAttribute('gui-interactable', data.guiInteractable);
    // }

    // if (data.clickChangeColor !== '') {
    //     el.setAttribute('click-change-color', data.clickChangeColor);
    // }

    el.addEventListener('mouseenter', function (event) {
      el.removeAttribute('animation__leave')
      if (!data.toggle) {
        //el.setAttribute('animation__enter', 'property: material.color; from: ' + data.backgroundColor + '; to:' + data.hoverColor + '; dur:200;');
        el.object3D.scale.set(1.2, 1.2, 1.2)
      }
    })
    el.addEventListener('mouseleave', function (event) {
      if (!data.toggle) {
        el.removeAttribute('animation__click')
        //el.setAttribute('animation__leave', 'property: material.color; from: ' + data.hoverColor + '; to:' + data.backgroundColor + '; dur:200; easing: easeOutQuad;');
        el.object3D.scale.set(1, 1, 1)
      }
      el.removeAttribute('animation__enter')
    })

    // el.addEventListener('focus', function (event) {
    //     buttonContainer.setAttribute('rounded', 'color', '' + data.focusColor);
    // });

    // el.addEventListener('blur', function (event) {
    //     buttonContainer.setAttribute('rounded', 'color', '' + data.borderColor);
    //     if (!data.toggle) {
    //         buttonEntity.removeAttribute('animation__click');
    //         buttonEntity.setAttribute('animation__leave', 'property: rounded.color; from: ' + data.hoverColor + '; to:' + data.backgroundColor + '; dur:200; easing: easeOutQuad;');
    //     }
    //     buttonEntity.removeAttribute('animation__enter');
    // });

    el.addEventListener(data.on, function (event) {
      if (!data.toggle) {
        // if not toggling flashing active state
        el.setAttribute(
          'animation__click',
          'property: material.color; from: ' +
            data.activeColor +
            '; to:' +
            data.backgroundColor +
            '; dur:400; easing: easeOutQuad;'
        )
      } else {
        var guiButton = el.components['gui-button']
        // console.log("about to toggle, current state: " + guiButton.data.toggleState);
        guiButton.setActiveState(!guiButton.data.toggleState)
        //  buttonEntity.setAttribute('material', 'color', data.activeColor);
      }

      var clickActionFunctionName = guiInteractable.clickAction
      // console.log("in button, clickActionFunctionName: "+clickActionFunctionName);
      // find object
      var clickActionFunction = window[clickActionFunctionName]
      //console.log("clickActionFunction: "+clickActionFunction);
      // is object a function?
      if (typeof clickActionFunction === 'function') clickActionFunction(event)
    })

    // el.addEventListener("keyup", function (event) {
    //     if (event.isComposing || event.keyCode === 229) {
    //         return;
    //     }

    //     if (event.keyCode == 13 || event.keyCode == 32) {
    //         el.emit(data.on);
    //     }
    //     event.preventDefault();
    // });

    // ////WAI ARIA Support
    // el.setAttribute('role', 'button');
    // el.setAttribute('tabindex', '0');
    // el.setAttribute('aria-label', data.value);
  },
  setActiveState: function setActiveState(activeState) {
    // console.log("in setActiveState function, new state: " + activeState);
    this.data.toggleState = activeState
    if (!activeState) {
      console.log('not active, about to set background color')
      this.el.setAttribute('material', 'color', this.data.backgroundColor)
    } else {
      console.log('active, about to set active color')
      this.el.setAttribute('material', 'color', this.data.activeColor)
    }
  }
})

AFRAME.registerPrimitive('a-xyimage', {
  defaultComponents: {
    'gui-interactable': {},
    xyimage: {}
  },
  mappings: {
    onclick: 'gui-interactable.clickAction',
    onhover: 'gui-interactable.hoverAction',
    'key-code': 'gui-interactable.keyCode',

    color: 'xyimage.color',
    'alpha-test': 'xyimage.alphaTest',
    src: 'xyimage.src',
    width: 'xyimage.width',
    height: 'xyimage.height',
    'position-xy': 'xyimage.position',

    on: 'xyimage.on',
    value: 'xyimage.value',
    'font-size': 'xyimage.fontSize',
    'font-family': 'xyimage.fontFamily',
    'font-color': 'xyimage.fontColor',
    'border-color': 'xyimage.borderColor',
    'focus-color': 'xyimage.focusColor',
    'background-color': 'xyimage.backgroundColor',
    'hover-color': 'xyimage.hoverColor',
    'active-color': 'xyimage.activeColor',
    toggle: 'xyimage.toggle',
    'toggle-state': 'xyimage.toggleState'
  }
})

AFRAME.registerComponent('menu-icon', {
  schema: {
    color: { type: 'string', default: '#ffffff' },
    fixed: { type: 'boolean', default: true },
    alphaTest: { type: 'number', default: 0.5 },
    src: { type: 'asset' },
    width: { type: 'number', default: 1 },
    height: { type: 'number', default: 1 },
    position: { type: 'string', default: '0 -2.5 0.1' },
    radius: { type: 'number', default: 0.5 }, // veľkosť zaoblenia
    on: { default: 'click' },
    value: { type: 'string', default: '' },
    fontSize: { type: 'number', default: 0.2 },
    fontFamily: { type: 'string', default: '' },
    fontColor: { type: 'string', default: key_offwhite },
    borderColor: { type: 'string', default: key_offwhite },
    focusColor: { type: 'string', default: key_orange_light },
    backgroundColor: { type: 'string', default: key_grey },
    hoverColor: { type: 'string', default: key_grey_dark },
    activeColor: { type: 'string', default: key_orange },
    toggle: { type: 'boolean', default: false },
    toggleState: { type: 'boolean', default: false }
  },

  init: function () {
    var data = this.data
    var el = this.el
    var guiInteractable = el.getAttribute('gui-interactable')
    this.guiInteractable = guiInteractable

    el.setAttribute('position', data.position)

    // Vytvorenie pozadia s zaoblením
    //var background = document.createElement('a-entity');
    el.setAttribute(
      'rounded',
      'width: ' +
        data.width +
        ';' +
        'height: ' +
        data.height +
        ';' +
        'color: ' +
        data.color +
        ';' +
        'radius: ' +
        data.radius +
        ';'
    )

    // Pozícia pozadia
    //el.object3D.position.set(data.position.x, data.position.y, data.position.z);
    // this.background = background;
    // this.el.appendChild(background);

    // Vytvorenie ikony
    var icon = document.createElement('a-image')
    icon.setAttribute('src', data.src)
    icon.setAttribute('width', data.width - 0.2)
    icon.setAttribute('height', data.height - 0.2)
    icon.setAttribute('alphaTest', data.alphaTest)

    // Pozícia ikony
    icon.object3D.position.set(0, 0, 0.01) // Trochu dopredu aby bola nad pozadím
    el.appendChild(icon)
    this.icon = icon

    //el.setAttribute('position', data.position);
    //el.object3D.position.set(data.position.x, data.position.y, data.position.z);

    el.addEventListener('mouseenter', function (event) {
      el.removeAttribute('animation__leave')
      if (!data.toggle) {
        el.object3D.scale.set(1.2, 1.2, 1.2)
      }
    })

    el.addEventListener('mouseleave', function (event) {
      if (!data.toggle) {
        el.removeAttribute('animation__click')
        el.object3D.scale.set(1, 1, 1)
      }
      el.removeAttribute('animation__enter')
    })

    el.addEventListener(data.on, function (event) {
      if (!data.toggle) {
        var newPosition = data.position

        // Rozdelenie reťazca na jednotlivé zložky
        var positionArray = newPosition.split(' ')

        // Prevod zložiek na čísla
        var x = parseFloat(positionArray[0])
        var y = parseFloat(positionArray[1])
        var z = parseFloat(positionArray[2])

        // Príklad výpočtu: posun pozície o +1 v smere y
        y += 1

        // Vytvorenie nového reťazca s aktualizovanou pozíciou
        var updatedPosition = x + ' ' + y + ' ' + z
        // Nastavenie novej pozície pomocou el.setAttribute
        //el.removeAttribute('position')
        setTimeout(() => {
          el.setAttribute(
            'animation__click_1',
            'property: position; from: ' +
              newPosition +
              '; to:' +
              updatedPosition +
              '; dur:400; easing: easeOutQuad;'
          )
        }, 10)
        setTimeout(() => {
          el.setAttribute(
            'animation__click_2',
            'property: rotation; from: ' +
              '0 0 0' +
              '; to:' +
              '0 0 360' +
              '; dur:400; easing: easeOutQuad;'
          )
        }, 410)
        setTimeout(() => {
          el.setAttribute(
            'animation__click_3',
            'property: position; from: ' +
              updatedPosition +
              '; to:' +
              data.position +
              '; dur:400; easing: easeOutQuad;'
          )
        }, 810)

        setTimeout(() => {
          el.removeAttribute('animation__click_1')
        }, 1000)
        setTimeout(() => {
          el.removeAttribute('animation__click_2')
        }, 1400)
        setTimeout(() => {
          el.removeAttribute('animation__click_3')
        }, 1800)
        //el.setAttribute('animation__click', 'property: material.color; from: ' + data.activeColor + '; to:' + data.backgroundColor + '; dur:400; easing: easeOutQuad;');
      } else {
        var guiButton = el.components['menu-icon']
        guiButton.setActiveState(!guiButton.data.toggleState)
      }

      var clickActionFunctionName = guiInteractable.clickAction
      var clickActionFunction = window[clickActionFunctionName]
      if (typeof clickActionFunction === 'function') clickActionFunction(event)
    })
  },

  setActiveState: function setActiveState(activeState) {
    // console.log("in setActiveState function, new state: " + activeState);
    this.data.toggleState = activeState
    if (!activeState) {
      console.log('not active, about to set background color')
      this.el.setAttribute('material', 'color', this.data.backgroundColor)
    } else {
      console.log('active, about to set active color')
      this.el.setAttribute('material', 'color', this.data.activeColor)
    }
  }
})

AFRAME.registerPrimitive('a-menu-icon', {
  defaultComponents: {
    'gui-interactable': {},
    'menu-icon': {}
  },
  mappings: {
    onclick: 'gui-interactable.clickAction',
    onhover: 'gui-interactable.hoverAction',
    'key-code': 'gui-interactable.keyCode',
    color: 'menu-icon.color',
    'alpha-test': 'menu-icon.alphaTest',
    src: 'menu-icon.src',
    width: 'menu-icon.width',
    height: 'menu-icon.height',
    'position-xy': 'menu-icon.position',
    radius: 'menu-icon.rounded', // zaoblenie
    on: 'menu-icon.on',
    value: 'menu-icon.value',
    'font-size': 'menu-icon.fontSize',
    'font-family': 'menu-icon.fontFamily',
    'font-color': 'menu-icon.fontColor',
    'border-color': 'menu-icon.borderColor',
    'focus-color': 'menu-icon.focusColor',
    'background-color': 'menu-icon.backgroundColor',
    'hover-color': 'menu-icon.hoverColor',
    'active-color': 'menu-icon.activeColor',
    toggle: 'menu-icon.toggle',
    'toggle-state': 'menu-icon.toggleState'
  }
})
