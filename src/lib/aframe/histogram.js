// component for modification aframe entity of histogram
import AFRAME from 'aframe'
// import { setPositioning } from '../utils/histogramFactory'
import {
  initialKeyboardController,
  keyReleaseHandlerFunction,
  keyPressHandlerFunction,
  //  switchDisplayModeOnBin,
  keyboardUpdateCameraReference
} from '../controllers/keyboardController'
/**
 * A-Frame interakčný komponent pridávajúci funkcionalitu entite histogram, ktorá umožňuje zmenu atribútov entity.
 * @component histogram-control
 * @param {Object} histogram - Objekt histogramu, z ktorého je potrebné spraviť projekciu
 * @param {Array} projectionAxes - Pole obsahuje názvy osí, podľa ktorých sa má vytvoriť projekcia
 * @param {Object} projections - Objekt obsahujúci súborovú štruktúru s projekciami
 * @param {Number} projIndex - Index projekcie v poli, ktorá sa má zobraziť
 * @module histogramAframeComponent
 */

// aframe component for positioning histogram in scene
AFRAME.registerComponent('histogram-control', {
  schema: {
    position: {
      name: 'position',
      xValue: 0,
      yValue: 0.1,
      zValue: 0
    },
    scale: {
      name: 'scale',
      xValue: 2,
      yValue: 2,
      zValue: 2
    },
    rotation: {
      name: 'rotation',
      xValue: 0,
      yValue: 0,
      zValue: 0
    }
  },
  init: function() {
    const el = this.el

    // set controllers
    // setPositioning(el, this.schema)
    initialKeyboardController(this.schema, el)

    //add listener for mouseover event on A-frame element of the page
    document.querySelector('a-scene').addEventListener('mouseover', function() {
      //add keyboard listeners
      document.addEventListener('keydown', keyPressHandlerFunction);
      document.addEventListener('keyup', keyReleaseHandlerFunction);
    });
    //removes the keyboard event listeners from the mouseout event from the A-frame element of the page
    document.querySelector('a-scene').addEventListener('mouseout', function() {
      //remove keyboard listeners
      document.removeEventListener('keydown', keyPressHandlerFunction);
      document.removeEventListener('keyup', keyReleaseHandlerFunction);
    });

  },

  update: function() {
    keyboardUpdateCameraReference()
  }
})
