// component to change text value of the entity after delay
import AFRAME from 'aframe'
/**
 * A-Frame komponent zabezpečí oneskorené aktualizovanie označenia na entite.
 * @component label-handler
 * @param {string} value - Hodnota označenia, na ktorú sa má aktuálne označenie zmeniť
 * @param {number} delay - Hodnota oneskorenia aktualizácie označenia na entite
 * @module labelHandlerAframeComponent
 */

// aframe component for handle labels on entity
AFRAME.registerComponent('label-handler', {
  schema: {
    value: { type: 'string' },
    delay: { type: 'number' }
  },

  init: function() {
    const el = this.el
    this.value = this.data.value
    el.setAttribute('text', 'value', this.data.value)
  },

  update: function() {
    const el = this.el // Entity.
    if (this.value !== this.data.value) {
      setTimeout(() => {
        el.setAttribute('text', 'value', this.data.value)
      }, this.data.delay)
      this.value = this.data.value
    }
  }
})
