import AFRAME from 'aframe'
import { Observable } from 'rxjs'
import { rotationSubject } from '../components/NewTablet/Screen'

AFRAME.registerComponent('rotation-reader', {
  tick: function () {
    // `this.el` is the element.
    // `object3D` is the three.js object.

    const observable = new Observable((sub) => {
      sub.next({
        rotation: this.el.object3D.rotation,
        position: this.el.object3D.getWorldPosition(new THREE.Vector3())
      })
    })
    observable.subscribe(rotationSubject)
    // `rotation` is a three.js Euler using radians. `quaternion` also available.
    //console.log(this.el.object3D.rotation)

    // `position` is a three.js Vector3.
    //console.log(this.el.object3D.position)
  }
})
