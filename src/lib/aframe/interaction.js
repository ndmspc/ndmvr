import AFRAME from "aframe";

AFRAME.registerComponent("interaction", {
  init: function () {
    const el = this.el;
    el.addEventListener("click", (event) => {
      el.setAttribute("material", "color", "orange");
    });
    el.addEventListener("mouseenter", (event) => {
      el.object3D.position.z += 0.002;
    });
    el.addEventListener("mouseleave", (event) => {
      el.object3D.position.z -= 0.002;
    });
  },
});
