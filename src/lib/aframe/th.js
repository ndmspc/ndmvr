// component to insert data to th3i bin
import AFRAME from 'aframe'
/**
 * A-Frame interakčný komponent pridávajúci funkcionalitu entite binu TH3 histogram, ktorá umožňuje zmenu atribútov entity.
 * @component binth3
 * @param {string} id - Identifikátor binu
 * @param {number} content - Hodnota optimalizovaného obsahu binu (predvolená 0)
 * @param {number} absoluteContent - Hodnota neoptimalizovaného obsahu binu (predvolená 0)
 * @param {string} binName - Meno binu
 * @param {string} xTitle - Titulok na osi x
 * @param {string} yTitle - Titulok na osi y
 * @param {number} xMin - Súradnica min binu na osi x
 * @param {number} yMin - Súradnica min binu na osi y
 * @param {number} zMin - Súradnica min binu na osi z
 * @param {number} xMax - Súradnica max binu na osi x
 * @param {number} yMax - Súradnica max binu na osi y
 * @param {number} zMax - Súradnica max binu na osi z
 * @param {number} xPos - Realna poloha max binu na osi x
 * @param {number} yPos - Realna poloha max binu na osi y
 * @param {number} zPos - Realna poloha max binu na osi z
 * @param {number} xCenter - Súradnica stredu binu na osi x
 * @param {number} yCenter - Súradnica stredu binu na osi y
 * @param {number} zCenter - Súradnica stredu binu na osi z
 * @param {number} xWidth - Šírka binu na osi x
 * @param {number} yWidth - Šírka binu na osi y
 * @param {number} zWidth - Šírka binu na osi z
 * @param {string} color - Farba binu
 * @param {string} axisX - Farba označení na osi x
 * @param {string} axisY - Farba označení na osi y
 * @param {string} axisZ - Farba označení na osi z
 * @param {string} selectColor - Farba určená za zvolenie binu
 * @param {string} markedColor - Farba určená pre označenie binu (v poli označených binov)
 * @module th2AframeComponent
 */

AFRAME.registerComponent('binth', {
  schema: {
    id: { type: 'string' },
    histogramName: { type: 'string' },
    typeName: { type: 'string' },
    content: { default: 0 },
    absoluteContent: { default: 0 },
    binName: { default: '' },
    xTitle: { type: 'string' },
    yTitle: { type: 'string' },
    zTitle: { type: 'string' },
    xMin: { type: 'number' },
    yMin: { type: 'number' },
    zMin: { type: 'number' },
    xMax: { type: 'number' },
    yMax: { type: 'number' },
    zMax: { type: 'number' },
    xPos: { type: 'number' },
    yPos: { type: 'number' },
    zPos: { type: 'number' },
    xCenter: { type: 'number' },
    yCenter: { type: 'number' },
    zCenter: { type: 'number' },
    xWidth: { type: 'number' },
    yWidth: { type: 'number' },
    zWidth: { type: 'number' },
    color: { type: 'string' },
    axisX: { type: 'string' },
    axisY: { type: 'string' },
    axisZ: { type: 'string' },
    selectColor: { type: 'string' },
    markedColor: { type: 'string' }
  },

  init: function() {
    const el = this.el // Entity.
    el.userData = {
      typeName: this.data.typeName,
      id: this.data.id,
      histogramName: this.data.histogramName,
      content: this.data.content,
      absoluteContent: this.data.absoluteContent,
      binName: this.data.binName,
      xTitle: this.data.xTitle,
      yTitle: this.data.yTitle,
      zTitle: this.data.zTitle,
      xMin: this.data.xMin,
      yMin: this.data.yMin,
      zMin: this.data.zMin,
      xMax: this.data.xMax,
      yMax: this.data.yMax,
      zMax: this.data.zMax,
      xPos: this.data.xPos,
      yPos: this.data.yPos,
      zPos: this.data.zPos,
      xCenter: this.data.xCenter,
      yCenter: this.data.yCenter,
      zCenter: this.data.zCenter,
      xWidth: this.data.xWidth,
      yWidth: this.data.yWidth,
      zWidth: this.data.zWidth,
      color: this.data.color,
      axisX: this.data.axisX,
      axisY: this.data.axisY,
      axisZ: this.data.axisZ,
      selectColor: this.data.selectColor,
      markedColor: this.data.markedColor
    }
  },

  update: function() {
    const el = this.el // Entity.
    el.userData = {
      typeName: this.data.typeName,
      id: this.data.id,
      histogramName: this.data.histogramName,
      content: this.data.content,
      absoluteContent: this.data.absoluteContent,
      binName: this.data.binName,
      xTitle: this.data.xTitle,
      yTitle: this.data.yTitle,
      zTitle: this.data.zTitle,
      xMin: this.data.xMin,
      yMin: this.data.yMin,
      zMin: this.data.zMin,
      xMax: this.data.xMax,
      yMax: this.data.yMax,
      zMax: this.data.zMax,
      xPos: this.data.xPos,
      yPos: this.data.yPos,
      zPos: this.data.zPos,
      xCenter: this.data.xCenter,
      yCenter: this.data.yCenter,
      zCenter: this.data.zCenter,
      xWidth: this.data.xWidth,
      yWidth: this.data.yWidth,
      zWidth: this.data.zWidth,
      color: this.data.color,
      axisX: this.data.axisX,
      axisY: this.data.axisY,
      axisZ: this.data.axisZ,
      selectColor: this.data.selectColor,
      markedColor: this.data.markedColor
    }
  }
})
