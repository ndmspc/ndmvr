// component for modification aframe entity of histogram
// manage functionality of histogram
import AFRAME from 'aframe'
import {
  oculusThumbStickFunction,
  oculusUpdateCameraReference,
  oculusUpSection,
  oculusDownSection,
  oculusThumbStickMarkBin,
  oculusThumbStickWithGripFunction,
  oculusThumbStickUnmarkBin,
  oculusShowFunctionView,
  oculusShowDefaultView,
  oculusRedrawHistogramBanners,
  oculusShiftBanners
} from '../controllers/oculusController'

/**
 * A-Frame interakčný komponent zabezpečí možnosť používateľa interagovať s pravým ovládačom zariadenia Oculus.
 * @component right-controller-logging
 * @module rightOculusAframeComponent
 */

// aframe component for positioning histogram in scene
export default AFRAME.registerComponent('right-controller-logging', {
  init: function() {
    let gripActive = false
    const el = this.el
    // oculus listeners
    el.addEventListener('gripdown', () => {
      gripActive = true
    })
    el.addEventListener('gripup', () => {
      gripActive = false
    })
    el.addEventListener('thumbstickmoved', (event) => {
      if (gripActive) {
        oculusThumbStickFunction(event)
      } else {
        oculusThumbStickWithGripFunction(event)
      }
    })
    el.addEventListener('bbuttondown', () => {
      if (gripActive) {
        oculusUpSection(false)
      } else {
        oculusUpSection(true)
      }
    })
    el.addEventListener('abuttondown', () => {
      if (gripActive) {
        oculusDownSection(false)
      } else {
        oculusDownSection(true)
      }
    })
    el.addEventListener('thumbstickdown', () => {
      if (gripActive) {
        oculusShowFunctionView()
      } else {
        oculusShowDefaultView()
      }
    })
    el.addEventListener('triggerdown', () => {
      if (gripActive) {
        oculusThumbStickMarkBin()
        oculusRedrawHistogramBanners()
      } else {
        oculusThumbStickUnmarkBin()
        oculusShiftBanners()
      }
    })
  },

  update: function() {
    oculusUpdateCameraReference()
  }
})
