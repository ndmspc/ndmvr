import AFRAME from 'aframe'
import { localStorageService } from '../services/ndmVrStorageService'
import binSubject from '../observables/binSubject'
import jsrootSubject from '../observables/jsrootSubject'
import binDataDistributor from '../observables/distributorSubject'
import { tabletComunicator } from '../components/DesktopTablet'

function parseIntersection(intersection, bin, event) {
  if (intersection.face.normal.x !== 0) {
    bin.setAttribute('material', 'color', bin.userData.axisZ)
    binDataDistributor.sendDataOfTheSelectedBin({
      event: event,
      data: bin.userData,
      intersect: 'Z',
      id: bin.userData.histogramName
    })
  } else if (intersection.face.normal.y !== 0) {
    bin.setAttribute('material', 'color', bin.userData.axisY)
    binDataDistributor.sendDataOfTheSelectedBin({
      event: event,
      data: bin.userData,
      intersect: 'Y',
      id: bin.userData.histogramName
    })
  } else {
    bin.setAttribute('material', 'color', bin.userData.axisX)
    binDataDistributor.sendDataOfTheSelectedBin({
      event: event,
      data: bin.userData,
      intersect: 'X',
      id: bin.userData.histogramName
    })
  }
  bin.setAttribute('material', 'opacity', '0.75')
}

/**
 * A-Frame interakčný komponent pridávajúci funkcionalitu entite, ktorá spracuváva prieniky medzi entitou a raycastrom používateľa.
 * @component mouseevent
 * @module cursorEventAframeComponent
 */

// aframe component for set events on bin

AFRAME.registerComponent('mouseevent', {
  init: function () {
    let subscription

    const el = this.el

    let clickCount = 0
    let checked = false
    // add mouse listener on mouseenter
    el.addEventListener('mouseenter', function setHover(evt) {
      checked = false
      // subscribe for events
      const intersection = evt.detail.intersection
      subscription = binSubject.getKeyboardEvent().subscribe((save) => {
        if (save) {
          // localStorageService.storeBinToLocalStorage(this.userData)
          // localStorageService.storeCurrentBinToLocalStorage(this.userData)
          this.userData.markedColor = this.userData.selectColor
          this.setAttribute('material', 'opacity', '0.70')
          jsrootSubject.showBinProjection(this.userData)
        } else {
          localStorageService.deleteBinFromLocalStorage(this.userData)
          this.userData.markedColor = this.userData.color
          this.setAttribute('material', 'opacity', '0.50')
        }
      })
      parseIntersection(intersection, this, 'HOVER')
    })

    // add mouse listener on mouseleave
    el.addEventListener('mouseleave', function setPassive() {
      subscription.unsubscribe()
      this.setAttribute('material', 'color', this.userData.markedColor)
      this.setAttribute('material', 'opacity', '0.50')
      clickCount = 0
    })

    // add mouse listener on click
    el.addEventListener('click', function setActive(evt) {
      clickCount = clickCount + 1
      checked = false
      setTimeout(() => {
        clickCount = 0
      }, 600)
      // add mouse listener on double click
      // change position
      if (clickCount === 2) {
        clickCount = 0
        checked = true
        this.setAttribute('material', 'color', '#99ff00')
        this.setAttribute('material', 'opacity', '1')
        const intersection = evt.detail.intersection
        if (intersection) {
          parseIntersection(intersection, this, 'DB_CLICK')
        }
        // get camera
        // TODO fix camera movement after db click on the bin
        /*const camera = document.getElementById('cameraWrapper')
        // set pos of camera
        if (camera !== null) {
          camera.object3D.position.y = this.object3D.position.y + 0.4
          // camera.object3D.rotation.y = 180
          if (this.userData.content > 0.5) {
            // camera.object3D.position.x = -1.5 + this.object3D.position.x
            // camera.object3D.position.z = this.object3D.position.z - 1.2
            camera.setAttribute(
              'animation',
              `property: position; to: ${-0.5 + this.object3D.position.x} ${this.object3D.position.y
              } ${this.object3D.position.z}
              ; dur: 600; easing: linear;`
            )
          } else if (this.userData.content > 0.2) {
            // camera.object3D.position.x = -0.5 + this.object3D.position.x
            // camera.object3D.position.y = this.object3D.position.y + 0.1
            // camera.object3D.position.z = this.object3D.position.z + 4.5
            camera.setAttribute(
              'animation',
              `property: position; to: ${-0.5 + this.object3D.position.x} ${this.object3D.position.y + 0.1
              } ${this.object3D.position.z + 0.5}
              ; dur: 600; easing: linear;`
            )
          } else {
            // camera.object3D.position.x = this.object3D.position.x + 2
            // camera.object3D.position.y = this.object3D.position.y + 0.1
            // camera.object3D.position.z = this.object3D.position.z + 8.5
            camera.setAttribute(
              'animation',
              `property: position; to: ${this.object3D.position.x + 2} ${this.object3D.position.y + 0.1
              } ${this.object3D.position.z + 8.5}
              ; dur: 600; easing: linear;`
            )
          }
        }*/
        // change store info to localstorage
      } else if (clickCount === 1) {
        this.setAttribute('material', 'color', '#99ff00')
        this.setAttribute('material', 'opacity', '1')
        //  !!!!!!!!!!!!!!!!!!!
        jsrootSubject.showBinProjection(this.userData)
        //tabletComunicator.next({ binData: this.userData })
        const intersection = evt.detail.intersection
        if (intersection) {
          parseIntersection(intersection, this, 'CLICK')
        }
      }

      // get reference to info-banner element
      let infoBanner = document.getElementById('th2-banner')
      if (infoBanner === null) {
        infoBanner = document.getElementById('th3-banner')
      }

      // if we have any information banner
      if (infoBanner !== null) {
        const planes = infoBanner.childNodes
        // set message to display
        if (this.userData.typeName === 'TH3' && planes.length === 5) {
          const zCor = `[${this.userData.zTitle}] [${this.userData.zMin},${
            this.userData.zMax
          }] ${
            this.userData.binName !== '' ? this.userData.binName + '\n' : '\n'
          }`
          const xCor = `[${this.userData.xTitle}] [${this.userData.xMin},${
            this.userData.xMax
          }] ${
            this.userData.binName !== '' ? this.userData.binName + '\n' : '\n'
          }`
          const yCor = `[${this.userData.yTitle}] [${this.userData.yMin},${
            this.userData.yMax
          }] ${
            this.userData.binName !== '' ? this.userData.binName + '\n' : '\n'
          }`
          const content = `Content: ${this.userData.absoluteContent} [${this.userData.content}]\n`

          planes[0].setAttribute(
            'text',
            `value: ${content}; color: ${this.userData.color}; align: center; width: 30; height: auto;`
          )
          planes[1].setAttribute(
            'text',
            `value: ${xCor}; color: ${this.userData.axisX}; align: center; width: 30; height: auto;`
          )
          planes[2].setAttribute(
            'text',
            `value: ${yCor}; color: ${this.userData.axisY}; align: center; width: 30; height: auto;`
          )
          planes[3].setAttribute(
            'text',
            `value: ${zCor}; color: ${this.userData.axisZ}; align: center; width: 30; height: auto;`
          )
        } else {
          const content = `Content: ${this.userData.absoluteContent} [${this.userData.content}]\n`
          const xCor = `[${this.userData.xTitle}] [${this.userData.xMin},${
            this.userData.xMax
          }] ${
            this.userData.binName !== '' ? this.userData.binName + '\n' : '\n'
          }`
          const yCor = `[${this.userData.yTitle}] [${this.userData.yMin},${
            this.userData.yMax
          }] ${
            this.userData.binName !== '' ? this.userData.binName + '\n' : '\n'
          }`
          planes[0].setAttribute(
            'text',
            `value: ${content}; color: ${this.userData.color}; align: center; baseline: center; width: 30; height: auto;`
          )
          planes[1].setAttribute(
            'text',
            `value: ${xCor}; color: ${this.userData.axisX}; align: center; width: 30; height: auto;`
          )
          planes[2].setAttribute(
            'text',
            `value: ${yCor}; color: ${this.userData.axisY}; align: center; width: 30; height: auto;`
          )
        }
      }
    })
  },
  update: function () {
    const el = this.el
    el.setAttribute('material', 'color', el.userData.markedColor)
    el.setAttribute('material', 'opacity', '0.75')
  }
})
