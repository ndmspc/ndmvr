// service to manage histogram views
/** @module JsrootSubject */
import { Subject } from 'rxjs'

/**
 * Subjekt pre publikáciu signálov a dát pre JSROOT.
 * @class
 */
class JsrootSubject {
  #subject

  constructor() {
    this.#subject = new Subject()
  }

  /**
   * Zobrazenie projekcie pre zvolený bin histogramu.
   * @param {Object} binData - Objekt obsahuje dáta o zvolenom bine
   */
  showBinProjection(binData) {
    this.#subject.next(binData)
  }

  /**
   * Získanie objektu pre prístup k odberu signálov a dát.
   * @return {Subject} - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
   */
  getServiceEvent() {
    return this.#subject.asObservable()
  }
}

const jsrootSubject = new JsrootSubject()
export default jsrootSubject
