import {Subject} from "rxjs";

class SchemaSubject {

   #subject;

   constructor() {
      this.#subject = new Subject();
   }

   next(data) {
      this.#subject.next(data);
   }

   getSchemaSubject() {
      return this.#subject;
   }
}

const schemaSubject = new SchemaSubject();
export default schemaSubject