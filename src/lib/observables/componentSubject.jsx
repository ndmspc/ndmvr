import { Subject } from 'rxjs'

/**
 * Subjekt pre publikáciu signálov klikateľných komponentov
 * */

class ComponentSubject {
   #subject

   constructor() {
      this.#subject = new Subject();
   }

   getBinDistributor() {
      return this.#subject;
   }

   setResultPoints(resultPoints) {
      this.#subject.next({resultPoints: resultPoints});
   }

   setCinemaButtonClick(cinemaButtonClick){
      this.#subject.next({cinemaButtonClickFunction: cinemaButtonClick});
   }

   next(object) {
      this.#subject.next(object);
   }

}
const componentDistributor = new ComponentSubject()
export default componentDistributor