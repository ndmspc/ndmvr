// service to manage histogram views
/** @module BinSubject */
import { Subject } from 'rxjs'

/**
 * Subjekt pre publikáciu signálov a dát pre biny.
 * @class
 */
class BinSubject {
  #subject

  constructor() {
    this.#subject = new Subject()
  }

  /**
   * Publikácia signálnu s hodnotou True pre označenie daného binu.
   * @return {void}
   */
  saveSelectedBinToLocalStorage() {
    this.#subject.next(true)
  }

  /**
   * Publikácia signálnu s hodnotou False pre odznačenie daného binu.
   * @return {void}
   */
  deleteBinFromLocalStorage() {
    this.#subject.next(false)
  }

  /**
   * Získanie objektu pre prístup k odberu signálov a dát.
   * @return {Subject} - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
   */
  getKeyboardEvent() {
    return this.#subject.asObservable()
  }
}

const binSubject = new BinSubject()
export default binSubject
