// service to manage scene camera
/** @module CameraSubject */
import { Subject } from 'rxjs'

/**
 * Subjekt pre publikáciu signálov a dát pre kameru.
 * @class
 */
class CameraSubject {
  #subject

  constructor() {
    this.#subject = new Subject()
  }

  /**
   * Publikácia signálu s hodnotou špecifikujúcou vstupné zariadenie pre zobrazenie prišných panelov.
   * @return {void}
   */
  setVisibilityOfBanners(inputDevice) {
    this.#subject.next({
      device: inputDevice
    })
  }

  /**
   * Publikácia signálu s hodnotou "shift" pre posúvanie panelov.
   * @return {void}
   */
  shiftBanners() {
    this.#subject.next('shift')
  }

  /**
   * Publikácia signálu s hodnotou "show" pre zobrazenie prislúchajúcich panelov.
   * @return {void}
   */
  setUserState() {
    this.#subject.next('show')
  }

  /**
   * Získanie objektu pre prístup k odberu signálov a dát.
   * @return {Subject} - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
   */
  getCameraSubject() {
    return this.#subject.asObservable()
  }
}

const cameraSubject = new CameraSubject()
export default cameraSubject
