// bin distributor
/** @module BinDataDistributor */
import { Subject } from 'rxjs'

/**
 * Subjekt pre publikáciu signálov a dát pre biny.
 * @class
 */
class SourceSettingSubject {
  #subject

  constructor() {
    this.#subject = new Subject()
  }

  /**
   * Publikácia dat oznaceneho binu.
   * @return {void}
   */
  sendSourceSubject(source) {
    this.#subject.next(source)
  }

  /**
   * Získanie objektu pre prístup k odberu signálov a dát.
   * @return {Subject} - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
   */
  getSourceSettingSubject() {
    return this.#subject.asObservable()
  }
}

const sourceSettingSubject = new SourceSettingSubject()
export default sourceSettingSubject
