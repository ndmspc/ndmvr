// service to manage histogram views
/** @module HistogramSubject */
import { Subject } from 'rxjs'

/**
 * Subjekt pre publikáciu signálov a dát pre histogram.
 * Prezobrazovanie sekcií histogramu.
 * @class
 */
class HistogramSubject {
  #subject

  constructor() {
    this.#subject = new Subject()
  }

  /**
   * Publikácia signálu pre prezobrazenie sekcie histogramu.
   * @param {string} axis - Názov osi podľa, po ktorej sa budú posúvať ofsety
   * @param {string} histogramType - Typ histogramu (TH1, TH2 alebo TH3)
   * @param {boolean} increment - Ak je True tak sa hodnota prípočíta k ofsetu, ak False tak sa odpočíta
   * @param {boolean} defaultRange - Ak je True tak sa použije predvolená hodnota (1), ak False tak nastavený rozsah
   * @return {void}
   */
  changeHistogramSectionByOffset(axis, histogramType, increment, defaultRange) {
    this.#subject.next({
      name: histogramType,
      axis: axis,
      increment: increment,
      defaultRange: defaultRange
    })
  }

  /**
   * Publikácia signálu pre zmenu vizuálneho módu.
   * @param {string} typeFunction - Názov funkcie podľa, ktorej sa zmení vizualizačný mód
   * @param {string} histogramType - Typ histogramu (TH2 alebo TH3)
   * @return {void}
   */
  changeHistogramFunction(typeFunction, histogramType) {
    this.#subject.next({
      name: histogramType,
      fFunction: typeFunction
    })
  }

  changeHistogramScale(scale, histogramType) {
    this.#subject.next({
      name: histogramType,
      scale: scale
    })
  }

  changeMinRequestedBinValue(minValue, histogramType) {
    this.#subject.next({
      name: histogramType,
      minValue: minValue
    })
  }

  /**
   * Získanie objektu pre prístup k odberu signálov a dát.
   * @return {Subject} - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
   */
  getChangedSection() {
    return this.#subject.asObservable()
  }
}

const histogramTH1Service = new HistogramSubject()
const histogramTH2Service = new HistogramSubject()
const histogramTH3Service = new HistogramSubject()
export { histogramTH1Service, histogramTH2Service, histogramTH3Service }
