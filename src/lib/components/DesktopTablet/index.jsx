import React, { useState, useRef, useEffect, useContext } from 'react'
import { Subject } from 'rxjs'
import { StoreContext } from '../contexts/StoreContext'
import 'aframe'
import '../../aframe/interaction'
import {
  histogramTH2Service,
  histogramTH1Service
} from '../../observables/histogramSubject'
import { DesktopTabletUI } from './desktopTabletUI'
import { BoldBlueTextButton } from './tabletComponents'
import { useNdmVrRedirect } from '@ndmspc/react-ndmspc-core'

const AFRAME = window.AFRAME
const tabletComunicator = new Subject()
let activeTool
let subscription

function DesktopTablet() {
  const [ndmvrInputDevice] = useNdmVrRedirect('ndmvrInputDevice')
  const changeTabletDisplayRef = useRef()
  const [activeToolLocal, setActiveToolLocal] = useState(0)
  const [activeSource, setActiveSource] = useState(null)
  const [activeTheme, setActiveTheme] = useState(null)
  const [activeRange, setActiveRange] = useState(null)
  const [activeScene, setActiveScene] = useState(null)
  activeTool = activeToolLocal
  const [binInfo, setBinInfo] = useState({
    xPos: 0,
    yPos: 0,
    zPos: 0,
    absoluteContent: 0
  })
  const [scaleCoeficient, setScaleCoeficient] = useState(1)

  //  komunikacia ohladom aktivneho toolu a zobrazovanych dat na tablete, ako na desktope tak aj na oculuse
  const sc = useContext(StoreContext)
  const { source, theme, range, scene } = sc.stores
  const [dataSources, setDataSources] = useState([])
  const [dataThemes, setDataThemes] = useState([])
  const [dataRange, setDataRange] = useState([])
  const [dataScene, setDataScene] = useState([])

  useEffect(() => {
    const ds = source?.getOptions().length ? source?.getOptions() : []
    setDataSources(ds)
  }, [sc])

  useEffect(() => {
    const dt = theme?.getOptions().length ? theme?.getOptions() : []
    setDataThemes(dt)
  }, [sc])

  useEffect(() => {
    const dr = range?.getOptions().length ? range?.getOptions() : []
    setDataRange(dr)
  }, [sc])

  useEffect(() => {
    const dSc = scene?.getOptions().length ? scene?.getOptions() : []
    setDataScene(dSc)
  }, [sc])

  useEffect(() => {
    subscription = tabletComunicator.subscribe(handleComunication)
    return () => subscription.unsubscribe()
  }, [dataSources, dataThemes, dataRange, dataScene])

  useEffect(() => {
    if (activeToolLocal === 1 && ndmvrInputDevice === 'oculus') {
      // jsrootService.displayImageOfProjection(
      //   'projectionContainer',
      //   'tablet',
      //   '500px',
      //   '500px'
      // )
    }
  }, [activeToolLocal])

  useEffect(() => {
    if (source && activeSource) source.executeFunction(0, activeSource)
  }, [activeSource])

  useEffect(() => {
    if (theme && activeTheme) theme.executeFunction(0, activeTheme)
  }, [activeTheme])

  useEffect(() => {
    if (range && activeRange) range.executeFunction(0, activeRange)
  }, [activeRange])

  useEffect(() => {
    if (scene && activeScene) scene.executeFunction(0, activeScene)
  }, [activeScene])

  const handleComunication = (data) => {
    if (typeof data === 'number') {
      //  keyboard tool change handler
      if (data + 1 <= tools.length) {
        setActiveToolLocal(data)
        activeTool = activeToolLocal
      }
    } else if (data.changeTabletState) {
      //  keyboard closing/opening tablet
      changeTabletDisplayRef.current.changeTabletDisplayHandler('switch')
    } else if (data.binData) {
      if (binInfo !== data.binData) {
        setBinInfo(data.binData)
      }
    } else if (data.tool) {
      //  scrolling through tools input
      if (data.tool === 'next') {
        setActiveToolLocal((prevState) => {
          if (prevState === tools.length - 1) return 0
          return prevState + 1
        })
      }
      if (data.tool === 'prev') {
        setActiveToolLocal((prevState) => {
          if (prevState === 0) return tools.length - 1
          return prevState - 1
        })
      }
    } else if (data.source) {
      //  scrolling through sources input
      if (data.source === 'next') {
        setActiveSource((prevState) => {
          if (dataSources.indexOf(prevState) === dataSources.length - 1)
            return dataSources[0]
          return dataSources[dataSources.indexOf(prevState) + 1]
        })
      }
      if (data.source === 'prev') {
        setActiveSource((prevState) => {
          if (dataSources.indexOf(prevState) === 0)
            return dataSources[dataSources.length - 1]
          return dataSources[dataSources.indexOf(prevState) - 1]
        })
      }
    } else if (data.scale) {
      setScaleForOculus(data.scale)
    } else if (data.theme) {
      //scrolling through themes input
      if (data.theme === 'next') {
        setActiveTheme((prevState) => {
          if (dataThemes.indexOf(prevState) === dataThemes.length - 1)
            return dataThemes[0]
          return dataThemes[dataThemes.indexOf(prevState) + 1]
        })
      }
      if (data.theme === 'prev') {
        setActiveTheme((prevState) => {
          if (dataThemes.indexOf(prevState) === 0)
            return dataThemes[dataThemes.length - 1]
          return dataThemes[dataThemes.indexOf(prevState) - 1]
        })
      }
    } else if (data.range) {
      //scrolling through themes input
      if (data.range === 'next') {
        setActiveRange((prevState) => {
          if (dataRange.indexOf(prevState) === dataRange.length - 1)
            return dataRange[0]
          return dataRange[dataRange.indexOf(prevState) + 1]
        })
      }
      if (data.range === 'prev') {
        setActiveRange((prevState) => {
          if (dataRange.indexOf(prevState) === 0)
            return dataRange[dataRange.length - 1]
          return dataRange[dataRange.indexOf(prevState) - 1]
        })
      }
    } else if (data.scene) {
      //scrolling through themes input
      if (data.scene === 'next') {
        setActiveScene((prevState) => {
          if (dataScene.indexOf(prevState) === dataScene.length - 1)
            return dataScene[0]
          return dataScene[dataScene.indexOf(prevState) + 1]
        })
      }
      if (data.scene === 'prev') {
        setActiveScene((prevState) => {
          if (dataScene.indexOf(prevState) === 0)
            return dataScene[dataScene.length - 1]
          return dataScene[dataScene.indexOf(prevState) - 1]
        })
      }
    }
  }

  // const sourceDataSetter = useContext(contexts)
  // const dataSources = sourceDataSetter.getOptions()

  //  ===================TOOLS=================

  //  tools solution for oculus tablet
  const renderOculusTablet = () => {
    switch (activeToolLocal) {
      case 0:
        return (
          <React.Fragment>
            <a-text
              value={
                'Selected bin:\n\n' +
                'Value: ' +
                binInfo.absoluteContent +
                '\nX: ' +
                binInfo.xPos +
                '\nY: ' +
                binInfo.yPos +
                '\nZ: ' +
                binInfo.zPos
              }
              rotation="-60 0 0"
              position="0.1 0 0"
              wrap-count="400"
            ></a-text>
          </React.Fragment>
        )
      case 2:
        return (
          <React.Fragment>
            {/* <a-plane
            color='black'
            height='0.3'
            width='0.4'
            opacity='0.5'
            rotation='-30 0 0'
            position='0.25 0 0'
          ></a-plane> */}
            <a-text
              value={
                'Bin height changer:\n\nFor changing\nthe visual height of bins,\nuse grip + joystick on\nthe left controller.'
              }
              rotation="-60 0 0"
              position="0.1 0 0"
              wrap-count="400"
            ></a-text>
          </React.Fragment>
        )
      case 1:
        return (
          <a-box
            id="tablet"
            color="white"
            depth="0.001"
            height="0.3"
            lenght="0.01"
            width="0.4"
            rotation="-60 0 0"
            position="0.28 0 0"
          ></a-box>
        )

      case 3:
        return (
          <a-entity rotation="-60 0 0">
            <a-text
              value={'Change source:\n\n'}
              rotation="0 0 0"
              position="0.1 +0.030 0"
              wrap-count="450"
            ></a-text>
            {dataSources.map((source) => (
              <a-text
                value={source}
                color={source === activeSource ? 'white' : '#A6ACAF'}
                rotation="0 0 0"
                key={source}
                position={'0.1 ' + -0.025 * dataSources.indexOf(source) + ' 0'}
                wrap-count={source === activeSource ? '360' : '450'}
              ></a-text>
            ))}
          </a-entity>
        )

      case 4:
        return (
          <a-entity rotation="-60 0 0">
            <a-text
              value={'Change theme:\n\n'}
              rotation="0 0 0"
              position="0.1 +0.030 0"
              wrap-count="450"
            ></a-text>
            {dataThemes.map((theme) => (
              <a-plane
                position={'0.15 ' + -0.025 * dataThemes.indexOf(theme) + ' 0'}
                height="0.02"
                width="0.1"
                interaction
                class="clickable"
              >
                <a-text
                  value={theme}
                  position={'-0.05 0 0.002'}
                  color={theme === activeTheme ? 'white' : '#A6ACAF'}
                  rotation="0 0 0"
                  key={theme}
                  wrap-count={theme === activeTheme ? '360' : '450'}
                ></a-text>
              </a-plane>
            ))}
          </a-entity>
        )

      case 5:
        return (
          <a-entity rotation="-60 0 0">
            <a-text
              value={'Change range:\n\n'}
              rotation="0 0 0"
              position="0.1 +0.030 0"
              wrap-count="450"
            ></a-text>
            {dataRange.map((range) => (
              <a-text
                value={range}
                color={range === activeRange ? 'white' : '#A6ACAF'}
                rotation="0 0 0"
                key={range}
                position={'0.1 ' + -0.025 * dataRange.indexOf(range) + ' 0'}
                wrap-count={range === activeRange ? '360' : '450'}
              ></a-text>
            ))}
          </a-entity>
        )

      case 6:
        const scenesPerColumn = 15
        const numColumns = Math.ceil(dataScene.length / scenesPerColumn)
        const sceneColumns = []

        for (let i = 0; i < numColumns; i++) {
          const start = i * scenesPerColumn
          const end = start + scenesPerColumn
          const columnScenes = dataScene.slice(start, end)

          const column = (
            <a-entity key={`column-${i}`} position={`${i * 0.3} 0 0`}>
              {columnScenes.map((scene) => (
                <a-text
                  value={scene}
                  color={scene === activeScene ? 'white' : '#A6ACAF'}
                  rotation="0 0 0"
                  key={scene}
                  position={`0.1 ${-0.025 * columnScenes.indexOf(scene)} 0`}
                  wrap-count={scene === activeScene ? '360' : '450'}
                ></a-text>
              ))}
            </a-entity>
          )
          sceneColumns.push(column)
        }
        return (
          <a-entity rotation="-60 0 0">
            <a-text
              value={'Change scene:'}
              rotation="0 0 0"
              position="0.1 +0.035 0"
              wrap-count="450"
            ></a-text>
            {sceneColumns}
          </a-entity>
        )
      default:
        return (
          <a-text
            value={'This tool does not have\n an interface for Oculus Tablet'}
            rotation="-60 0 0"
            position="0.1 0 0"
            wrap-count="400"
          ></a-text>
        )
    }
  }

  function setScaleForOculus(scale) {
    setScaleCoeficient((prevValue) => {
      let newValue
      let jump = 0
      //decrease
      if (scale < 0) {
        //chcek for the min state
        if ((prevValue - 0.01).toFixed(2) >= 0.01) {
          if ((prevValue - 0.01).toFixed(2) > 0.7) {
            jump = -0.1
          } else if ((prevValue - 0.01).toFixed(2) > 0.1) {
            jump = -0.01
          } else {
            jump = -0.001
          }
        }
        //increase
      } else {
        if ((prevValue + 0.01).toFixed(2) > 0.7) {
          jump = 0.1
        } else if ((prevValue - 0.01).toFixed(2) > 0.1) {
          jump = 0.01
        } else {
          jump = 0.001
        }
      }
      //get the new scale
      newValue = prevValue + jump
      histogramTH2Service.changeHistogramScale(jump, 'TH2')
      histogramTH1Service.changeHistogramScale(jump, 'TH1')
      return newValue
    })
  }

  const tools = [
    { fun: getBinInfoTool(), name: 'Bin Info' },
    { fun: scaleZaxis(), name: 'Scale Z axis' },
    { fun: changeDataSource(), name: 'Change Data' },
    { fun: changeDataTheme(), name: 'Change Theme' },
    { fun: changeDataRange(), name: 'Change Range' },
    { fun: changeDataScene(), name: 'Change Scene' },
    { fun: null, name: 'Tool with a longer name' }
  ]

  // ==================== tools for a desktop tablet ==========================

  function getBinInfoTool() {
    if (ndmvrInputDevice !== 'oculus') {
      return (
        <React.Fragment>
          <table style={{ width: '100%' }}>
            <tbody>
              <tr style={{ display: 'flex', alignItems: 'flex-end' }}>
                <td
                  style={{
                    width: '50%',
                    fontSize: '1em',
                    textAlign: 'right',
                    marginBottom: '0.5em'
                  }}
                >
                  <i>value:</i>{' '}
                </td>
                <td style={{ width: '50%', fontSize: '2em', fontWeight: 600 }}>
                  {binInfo.absoluteContent}
                </td>
              </tr>
              <tr style={{ display: 'flex', alignItems: 'flex-end' }}>
                <td
                  style={{
                    width: '50%',
                    fontSize: '1em',
                    textAlign: 'right',
                    marginBottom: '0.5em'
                  }}
                >
                  <i>x:</i>{' '}
                </td>
                <td style={{ width: '50%', fontSize: '2em', fontWeight: 600 }}>
                  {binInfo.xPos}
                </td>
              </tr>
              <tr style={{ display: 'flex', alignItems: 'flex-end' }}>
                <td
                  style={{
                    width: '50%',
                    fontSize: '1em',
                    textAlign: 'right',
                    marginBottom: '0.5em'
                  }}
                >
                  <i>y:</i>{' '}
                </td>
                <td style={{ width: '50%', fontSize: '2em', fontWeight: 600 }}>
                  {binInfo.yPos}
                </td>
              </tr>
              <tr style={{ display: 'flex', alignItems: 'flex-end' }}>
                <td
                  style={{
                    width: '50%',
                    fontSize: '1em',
                    textAlign: 'right',
                    marginBottom: '0.5em'
                  }}
                >
                  <i>z:</i>{' '}
                </td>
                <td style={{ width: '50%', fontSize: '2em', fontWeight: 600 }}>
                  {binInfo.zPos}
                </td>
              </tr>
            </tbody>
          </table>
        </React.Fragment>
      )
    }
  }

  function scaleZaxis() {
    //default offset
    let jumpValue = 0.01
    let fixed = 2

    //smaller offset setting for the - operation
    if ((scaleCoeficient - 0.01).toFixed(2) > 0.1) {
      jumpValue = 0.1
      fixed = 1
    }

    return (
      <React.Fragment>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'
          }}
        >
          <BoldBlueTextButton
            onClick={() => {
              //smaller offset setting for the + operation
              if ((scaleCoeficient + 0.01).toFixed(2) > 0.1) {
                jumpValue = 0.1
                fixed = 1
              }
              setScaleCoeficient(scaleCoeficient + jumpValue)
              histogramTH2Service.changeHistogramScale(jumpValue, 'TH2')
              histogramTH1Service.changeHistogramScale(jumpValue, 'TH1')
            }}
          >
            +
          </BoldBlueTextButton>
          <h1 style={{ fontSize: '5em', fontWeight: 'bold' }}>
            {scaleCoeficient.toFixed(fixed)}
          </h1>
          <BoldBlueTextButton
            onClick={() => {
              //only if min. state is not reached
              if ((scaleCoeficient - 0.01).toFixed(2) >= 0.01) {
                setScaleCoeficient(scaleCoeficient - jumpValue)
                histogramTH2Service.changeHistogramScale(-jumpValue, 'TH2')
                histogramTH1Service.changeHistogramScale(-jumpValue, 'TH1')
              }
            }}
          >
            -
          </BoldBlueTextButton>
        </div>
      </React.Fragment>
    )
  }

  function changeDataSource() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        {dataSources.map((source) => {
          if (source === activeSource) {
            return (
              <a
                key={source}
                style={{
                  fontFamily: 'Open Sans',
                  fontSize: '17px',
                  fontWeight: '600',
                  color: 'black'
                }}
                onClick={() => setActiveSource(source)}
              >
                {source}
              </a>
            )
          } else {
            return (
              <a key={source} onClick={() => setActiveSource(source)}>
                {source}
              </a>
            )
          }
        })}
      </div>
    )
  }

  function changeDataTheme() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        {dataThemes.map((theme) => {
          if (theme === activeTheme) {
            return (
              <a
                key={theme}
                style={{
                  fontFamily: 'Open Sans',
                  fontSize: '17px',
                  fontWeight: '600',
                  color: 'black'
                }}
                onClick={() => setActiveTheme(theme)}
              >
                {theme}
              </a>
            )
          } else {
            return (
              <a key={theme} onClick={() => setActiveTheme(theme)}>
                {theme}
              </a>
            )
          }
        })}
      </div>
    )
  }

  function changeDataRange() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          marginTop: '7.5rem'
        }}
      >
        {dataRange.map((range) => {
          if (range === activeRange) {
            return (
              <a
                key={range}
                style={{
                  fontFamily: 'Open Sans',
                  fontSize: '17px',
                  fontWeight: '600',
                  color: 'black'
                }}
                onClick={() => setActiveRange(range)}
              >
                {range}
              </a>
            )
          } else {
            return (
              <a key={range} onClick={() => setActiveRange(range)}>
                {range}
              </a>
            )
          }
        })}
      </div>
    )
  }

  function changeDataScene() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          marginTop: '16.5rem'
        }}
      >
        {dataScene.map((scene) => {
          if (scene === activeScene) {
            return (
              <a
                key={scene}
                style={{
                  fontFamily: 'Open Sans',
                  fontSize: '17px',
                  fontWeight: '600',
                  color: 'black'
                }}
                onClick={() => setActiveScene(scene)}
              >
                {scene}
              </a>
            )
          } else {
            return (
              <a key={scene} onClick={() => setActiveScene(scene)}>
                {scene}
              </a>
            )
          }
        })}
      </div>
    )
  }
  //  =================== END OF TOOLS ===================

  if (document.getElementById('desktop--tablet--container'))
    dragElement(document.getElementById('desktop--tablet--container'))

  function dragElement(elmnt) {
    let pos1 = 0
    let pos2 = 0
    let pos3 = 0
    let pos4 = 0
    if (document.getElementById(elmnt.id + '--header')) {
      // if present, the header is where you move the DIV from:
      document.getElementById(elmnt.id + '--header').onmousedown = dragMouseDown
    } else {
      // otherwise, move the DIV from anywhere inside the DIV:
      elmnt.onmousedown = dragMouseDown
    }

    function dragMouseDown(e) {
      e.preventDefault()
      // get the mouse cursor position at startup:
      pos3 = e.clientX
      pos4 = e.clientY
      document.onmouseup = closeDragElement
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag
    }

    function elementDrag(e) {
      e.preventDefault()
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX
      pos2 = pos4 - e.clientY
      pos3 = e.clientX
      pos4 = e.clientY
      // set the element's new position:
      elmnt.style.top = elmnt.offsetTop - pos2 + 'px'
      elmnt.style.left = elmnt.offsetLeft - pos1 + 'px'
    }

    function closeDragElement() {
      // stop moving when mouse button is released:
      document.onmouseup = null
      document.onmousemove = null
    }
  }

  const changeActiveToolLocalHandler = (value) => {
    setActiveToolLocal(value)
  }

  if (ndmvrInputDevice !== 'oculus') {
    //  desktop tablet
    return (
      <DesktopTabletUI
        tools={tools}
        activeToolLocal={activeToolLocal}
        ref={changeTabletDisplayRef}
        changeActiveToolLocalHandler={changeActiveToolLocalHandler}
      />
    )
    //  oculus tablet
  } else {
    return <React.Fragment>{renderOculusTablet()}</React.Fragment>
  }
}

const getActiveTool = () => {
  return activeTool
}

export { tabletComunicator }
export { getActiveTool }
export { DesktopTablet }
