import styled from 'styled-components'

const DesktopTabletContainer = styled.div`
  font-family: 'IBM Plex Mono', 'Roboto', sans-serif;
  background-color: #fafafa;
  color: #26272;
  width: 500px;
  height: 350px;
  position: absolute;
  right: 1vw;
  bottom: 3vh;
  z-index: 10000;
  display: ${(props) => props.display};
  border-radius: 10px;
  box-shadow: 5px 5px 23px 5px rgba(0, 0, 0, 0.25);
  border-top: 1px solid rgba(255, 255, 255, 0.1);
  resize: both;
  overflow: hidden;

  & h1 span {
    font-size: 0.5em;
  }
`
const ToolbarWrapper = styled.div`
  display: flex;
  height: 100%;
  width: 170px;
  flex-direction: column;
  border-right: 1px solid #ddd;
  background-color: #ddd;
  border-radius: 10px 0 0 10px;
  white-space: nowrap;
  padding: 25px 0px 25px 15px;

  & p {
    color: #999;
    margin-left: -3px;
    font-size: 0.8em;
    font-weight: bold;
  }

  & h1 {
    font-size: 1.8em;
    font-weight: 700;
    width: auto;
    text-align: center;
    margin: -20px 20px 10px 0px;
  }

  &:active {
    cursor: grabbing;
  }

  & span {
    cursor: pointer;
  }
`

const DesktopTabletContainerHeader = styled.div`
  width: 100%;
  cursor: grab;
  background-color: #eee;
  border-bottom: 1px solid #0f52ba;
  border-radius: 0 10px 0 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 20px;

  & h1 {
    font-size: 1.5em;
    font-weight: 600;
  }
`

const CloseButton = styled.div`
  cursor: pointer;
  transform: rotate(90deg);
  color: gray;
  position: absolute;
  top: 12px;
  right: 0;

  &:hover {
    text-decoration: underline;
  }
`

const ToolActive = styled.span`
  font-family: 'Open Sans';
  font-size: 15px;
  font-weight: 700;
  cursor: default !important;
  margin: 5px 15px 5px 0px;
  margin-left: 0.5rem;
`

const ToolNonActive = styled.span`
  margin: 5px 15px 5px 0px;
  transition: all 500ms ease;

  &:hover {
    text-decoration: underline;
    margin-left: 1rem;
  }
`

const ToolsContainer = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  overflow-x: hidden;
  padding-bottom: 50px;
  font-size: 0.8em;
`

const BoldBlueTextButton = styled.div`
  font-size: 3em;
  color: #0f52ba;
  font-weight: bold;
  cursor: pointer;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
`

export {
  BoldBlueTextButton,
  ToolsContainer,
  ToolNonActive,
  ToolActive,
  CloseButton,
  DesktopTabletContainerHeader,
  ToolbarWrapper,
  DesktopTabletContainer
}
