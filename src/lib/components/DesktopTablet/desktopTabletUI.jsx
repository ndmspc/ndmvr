import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle
} from 'react'
import {
  ToolsContainer,
  ToolNonActive,
  ToolActive,
  CloseButton,
  DesktopTabletContainerHeader,
  ToolbarWrapper,
  DesktopTabletContainer
} from './tabletComponents'

const DesktopTabletUI = forwardRef(
  ({ activeToolLocal, tools, changeActiveToolLocalHandler }, ref) => {
    useImperativeHandle(ref, () => ({
      changeTabletDisplayHandler(value) {
        if (value === 'switch')
          setTabletDisplay((prevValue) =>
            prevValue === 'none' ? 'flex' : 'none'
          )
        else setTabletDisplay(value)
      }
    }))

    const [tabletDisplay, setTabletDisplay] = useState('none')

    return (
      <React.Fragment>
        <DesktopTabletContainer
          display={tabletDisplay}
          id='desktop--tablet--container'
        >
          {/* toolbar. Desired tool can be selected by clicking on its coresponding number on the NDMVR tablet or on your keyboard. */}
          <ToolbarWrapper>
            <h1>
              NDMVR
              <span>
                <i>tablet</i>
              </span>
            </h1>
            <p>Tools</p>
            <ToolsContainer>
              {tools.map((tool, number) => {
                if (number === activeToolLocal) {
                  return (
                    <ToolActive key={number}>
                      <span style={{ fontWeight: 700, color: '#0f52ba' }}>
                        {number < 10 ? (number + 1) % 10 : '>'}
                      </span>{' '}
                      {tool.name.length > 15
                        ? tool.name.slice(0, 12) + '...'
                        : tool.name}
                    </ToolActive>
                  )
                }
                return (
                  <ToolNonActive
                    onClick={() => changeActiveToolLocalHandler(number)}
                    key={number}
                  >
                    <span style={{ fontWeight: 700, color: '#0f52ba' }}>
                      {number < 10 ? (number + 1) % 10 : '>'}
                    </span>{' '}
                    {tool.name.length > 15
                      ? tool.name.slice(0, 12) + '...'
                      : tool.name}
                  </ToolNonActive>
                )
              })}
            </ToolsContainer>
          </ToolbarWrapper>
          <div style={{ width: 100 + '%', overflow: 'scroll' }}>
            <DesktopTabletContainerHeader id='desktop--tablet--container--header'>
              <h1>{tools[activeToolLocal].name}</h1>
              <CloseButton
                id='close--button'
                onClick={() => setTabletDisplay('none')}
                style={{
                  cursor: 'pointer',
                  maxWidth: 30 + 'px'
                }}
              >
                <i>Hide</i>
              </CloseButton>
            </DesktopTabletContainerHeader>
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                height: '83%',
                padding: '10px',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              {/* Selected tool interface */}
              {tools[activeToolLocal].fun}
            </div>
          </div>
        </DesktopTabletContainer>
      </React.Fragment>
    )
  }
)

export { DesktopTabletUI }
