import builtInSchema from "../resources/builtinShemas.json";
import NdmVrScene from "./NdmVrScene.jsx";
import React, {useEffect, useLayoutEffect, useState} from "react";
import {useNdmVrRedirect} from "@ndmspc/react-ndmspc-core";
import '../resources/NdmVr.css'
import registerVRModeDetector from "../utils/VrModeDetector.jsx";
import registerScreenControlsComponent from "../utils/ScreenControls.jsx";
import RegisterVerticalMoveComponent from "../controllers/verticalMove.jsx";
import registerCustomButton from "../../components/buttonComponent.jsx";
import componentDistributor from "../observables/componentSubject.jsx";

/**
 * Component that wraps (is parent) NdmVrScene.
 * Have logic for toggling fullscreen for mobile devices.*/
const NdmVrContainer = ({
                  data,
                  states,
                  stores,
                  config = builtInSchema,
                  customComponentProvider,
                  experimental,
                  selectedBins,
                  cinemaFunc,
                  onClick,
                  onDbClick,
                  onHover,
                  onView,
                  onProgress,
                  onExeCommand,
                  style,
                  useShared
               }) => {

   const [sceneElem, setSceneElem] = useState(null)
   const [fullScreen, setFullScreen] = useNdmVrRedirect('ndmvrFullscreen')
   const [ndmvrInputDevice, setNdmvrInputDevice] = useNdmVrRedirect('ndmvrInputDevice')

   useEffect(() => {
      registerVRModeDetector(setNdmvrInputDevice)
      registerScreenControlsComponent()
      RegisterVerticalMoveComponent()
      registerCustomButton();
   }, [])

   /**
    * Sets Event listeners for handling events when fullscreen changes.
    * In return statement they're cleared
    * */
   useEffect(() => {
      const handleBackNavigation = (event) => {
         setFullScreen.setData(false);
      };

      const handleFullscreenChange = () => {
         if (!document.fullscreenElement) {
            setFullScreen.setData(false);
         }
      };
      window.addEventListener('popstate', handleBackNavigation);
      document.addEventListener('fullscreenchange', handleFullscreenChange);

      return () => {
         window.removeEventListener('popstate', handleBackNavigation);
         document.removeEventListener('fullscreenchange', handleFullscreenChange);
      };
   }, [])

   /**
    * Sets Event listeners for handling events when fullscreen changes.
    * In return statement they're cleared
    * */
   useLayoutEffect(() => {
      setSceneElem(document.getElementById("scene"))
      const handleBackNavigation = (event) => {
         if (event.state && event.state.navigationType === 'back') {
            console.log('User pressed the native back button');
         }
      }
      window.addEventListener(document.exitFullscreen, handleBackNavigation)
      window.addEventListener('popstate', handleBackNavigation)
      return () => {
         window.removeEventListener('popstate', handleBackNavigation)
      }

   }, [])

   useEffect(() => {
      if (!ndmvrInputDevice) return
      setFullScreen.setData(false)
   }, [ndmvrInputDevice])

   /**
    * Toggles fullscreen based on fullscreen variable.
    * */
   useEffect(() => {
      if (!sceneElem) return
      fullScreen ? enterFullScreen() : disableFullscreen()
   }, [fullScreen])

   /**
    * Sets scene (div containing a-scene and mobile controls) element on fullscreen
    * and rotates screen to landscape mode
    * */
   function enterFullScreen() {
      sceneElem.requestFullscreen().catch(() => {})
      screen.orientation.lock('landscape').catch();
   }

   /**
    * Toggles scene (div containing a-scene and mobile controls) element element to minimized view
    * and unlocks the screen orientation.
    * */
   function disableFullscreen() {
      screen.orientation.unlock();
      document.exitFullscreen().catch(() => { });
   }

   /**
    * Sends cinema button functions to subject
    * */
   useEffect(() => {
      if (cinemaFunc) {
         componentDistributor.setCinemaButtonClick(cinemaFunc);
      }
   },[cinemaFunc])

   /**
    * Sends result points from states(bins clicked) to subject
    * */
   useEffect(() => {
      componentDistributor.setResultPoints(states.resultPoints);
   },[states.resultPoints])

   return (
      <NdmVrScene
         data={data}
         states={states}
         stores={stores}
         config={config}
         customComponentProvider={customComponentProvider}
         experimental={experimental}
         selectedBins={selectedBins}
         onClick={onClick}
         onDbClick={onDbClick}
         onHover={onHover}
         onView={onView}
         onProgress={onProgress}
         onExeCommand={onExeCommand}
         style={style}
         useShared={useShared}
      />
   )
}

export default NdmVrContainer