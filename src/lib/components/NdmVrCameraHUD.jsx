import React, { useEffect, useState } from 'react'
import 'aframe'
import { tabletComunicator } from './DesktopTablet'
import { useNdmVrRedirect } from '@ndmspc/react-ndmspc-core'


/**
 * Contains HUD (Head-up display) for camera,
 * for informative non interactable elements*/
const NdmVrCameraHUD = () => {
  let subscription
  const [showHint, setShowHint] = useState(true)
  const [ndmvrInputDevice] = useNdmVrRedirect('ndmvrInputDevice')

  const handleTabletSubscription = (data) => {
    if (data.changeTabletState) {
      setShowHint(false)
    }
  }

  useEffect(() => {
    subscription = tabletComunicator.subscribe(handleTabletSubscription)
    return () => {
      subscription.unsubscribe()
    }
  }, [])

  return (
    <a-entity>
      {ndmvrInputDevice === 'keyboard'&&
         showHint && (
        <a-entity
          text="value: Press T to open NDMVR Tablet;color:black;"
          position="0.2 0.8 -1"
        ></a-entity>
      )}

      {ndmvrInputDevice !== 'oculus' && (
        <a-entity
           id="cursor"
           cursor="fuse: false; fuseTimeout: 2000;"
          rayOrigin="mouse"
          raycaster="objects: .clickable; showLine: false; far: 100;"
          animation__click="property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1.4 1.4 1.4; dur: 150"
          animation__mouseenter="property: scale; startEvents: mouseenter; from: 1 1 1; to: 1.4 1.4 1.4; dur: 180"
          animation__mouseleave="property: scale; startEvents: mouseleave; from: 1.4 1.4 1.4; to: 1 1 1; dur: 180"
          line="color: orange; opacity: 0.5"
          far="100"
          position="0 0 -1"
          geometry="primitive: ring; radiusInner: 0.02; radiusOuter: 0.03"
          material="color: white; shader: flat"
        />
      )}
    </a-entity>
  )
}

export default NdmVrCameraHUD
