import React, { useEffect, useState } from 'react'
import NdmVrHistogram from './NdmVrHistogram'
import NdmVrLaboratory from './NdmVrLaboratory'
import { DesktopTablet } from './DesktopTablet'
import { StoreContext } from './contexts/StoreContext'
import binDataDistributor from '../observables/distributorSubject'
import builtInSchema from '../resources/builtinShemas.json'
import 'aframe'
import NdmVrBaseCamera from './NdmVrBaseCamera'
import NdmVrShared from './NdmVrShared/NdmVrShared'
import NdmVrHistogramLimits from './NdmVrHistogramLimits'
import TabletProvider from './Tablet/TabletProvider'
import {useNdmVrRedirect} from "@ndmspc/react-ndmspc-core";
import MobileController from "../controllers/mobileController.jsx";
import NavbarMobile from "../../components/Navbar/NavbarMobile.jsx";
import schemaSubject from "../observables/schemaSubject.jsx";

/**
 * @typedef {Object} Properties property
 * @prop {string} type
 * @prop {string} elem name of component to be used
 * @prop {string|number} [value] current falue as current state
 * @prop {number} [limit] how many select allowed
 * @prop {string[]|number[]} options options to choose
 * @prop {number} [step] when value is numerical and is incresed and decresed by step
 * @prop {number|number[]} range defines min max value when numerical is recuired or specifies how many options could be selected
 */

/**
 * @typedef {Object} Schema
 * @prop {string} description
 * @prop {bool} disabled
 * @prop {Object.<string,Properties>} properties
 */

/**
 * built in schemas
 * @typedef {Object} Schemas
 * @prop {string} name
 * @prop {Object.<string,Schema>} schema
 */

/**
 * @typedef {Object} NdmVrSceneProps
 * @prop {Object} data - Objekt obsahuje objekty histogramu a potrebne data k vizualizacii
 * @prop {Object} states - Objekt obsahuje stavove premenne pre vizualizovany histogram
 * @prop {Object} stores - Objekt obsahuje vsetky potrebne story a exekutory funkcii
 * @prop {any} selectedBins - array with selected bins
 * @prop {any} onClick
 * @prop {Schemas} config
 * @prop {any} onDbClick
 * @prop {any} onHover
 * @prop {any} onView
 * @prop {any} onProgress
 * @prop {any} onExeCommand
 * @prop {boolean} experimental
 * @prop {(type:string,keyboard:boolean,props:Properties,onSelect:(value:any)=>void,defaultProvider:ComponentProvider)=>React.JSX.Element} customComponentProvider
 */

/**
 *
 * React komponent pre vytvorenie VR scény s všetkými potrebnými komponentami.
 * @component
 * @param {NdmVrSceneProps}
 * @example
 * states = {
 *   theme: theme,
 *   selectedView: currentView,
 *   section: currentSection,
 *   range: currentRange
 * }
 * stores = {
 *    sourceStore: histogramSource,
 *    themeStore: colorTheme,
 *    rangeStore: displayedRange,
 *    viewStore: displayViewFunctions
 *    sharedStore: storeShared,
 * }
 * data = {
 *    histogram: histograms.physics,
 *    id: 'Physics',
 *    projections: histograms.projections,
 *    projectionsNames: [string],
 *    selectedBins: [string],
 *    projPanelIds: [string]
 *    background: {
 *        url: './ndmvr/backgrounds/background1.jpg',
 *        radius: '3000',
 *        height: '2048',
 *        width: '2048'
 *     }
 * }
 *  return (
 *  <NdmVrScene data={data} info={info} />
 * )
 */
const NdmVrScene = ({
  data,
  states = null,
  stores = null,
  config = builtInSchema,
  customComponentProvider,
  experimental,
  selectedBins = [],
  onClick = (data) => {
    console.log("click isn't defined!")
  },
  onDbClick = (data) => {
    console.log("DbClick isn't be defined!")
  },
  onHover = (data) => {
    console.log("Hover isn't be defined!")
  },
  onView = null,
  onProgress = null,
  onExeCommand = (data) => {
    console.log("exeCommand isn't be defined!")
  },
  style = {
    zIndex: '1',
    width: '100%',
    minHeight: 'calc(90vh)'
  },
  useShared = false
}) => {
  let subscription
  const [ndmvrInputDevice,] = useNdmVrRedirect('ndmvrInputDevice')
  const [ndmvrSchema, setNdmvrSchema] = useState(null);
  const [showTablet, setShowTablet] = useState(false)

  useEffect(() => {
    if (ndmvrSchema == null) schemaSubject.next(config);
  }, [config])

  useEffect(() => {
    const schemaSubjectRef = schemaSubject
       .getSchemaSubject()
       .subscribe(object => {
         setNdmvrSchema(object);
       })
    return () => {
      schemaSubjectRef.unsubscribe();
    }
  },[])

  useEffect(() => {
    if (ndmvrInputDevice) {
      setShowTablet(true)
    } else {
      setShowTablet(false)
    }
  }, [ndmvrInputDevice])

  // publish event data out of the component
  useEffect(() => {
    subscription = binDataDistributor
      .getBinDistributor()
      .subscribe((binData) => {
        if (binData?.id === data.id) {
          const { zPos, yPos, xPos } = binData.data
          //DATAFORRESPONSE Do we need this structure ? or can it be simplified ?
          const dataForResponse = {
            intersectionAxis: binData.intersect,
            obj: data.histogram,
            id: data.id,
            binId: binData.data.id
          }
          //CHECK Too much boiler plate. serach => DATAFORRESPONSE

          if (binData.data?.zPos) {
            dataForResponse.binz = zPos
            dataForResponse.biny = yPos
            dataForResponse.binx = xPos
            dataForResponse.bin = data.histogram.getBin(xPos, yPos, zPos)
            dataForResponse.con = data.histogram.getBinContent(xPos, yPos, zPos)
          } else if (binData.data?.yPos) {
            dataForResponse.biny = yPos
            dataForResponse.binx = xPos
            dataForResponse.bin = data.histogram.getBin(xPos, yPos)
            dataForResponse.con = data.histogram.getBinContent(xPos, yPos)
          } else if (binData.data?.xPos) {
            dataForResponse.binx = xPos
            dataForResponse.bin = data.histogram.getBin(xPos)
            dataForResponse.con = data.histogram.getBinContent(xPos)
          }

          switch (binData.event) {
            case 'CLICK':
              onClick(dataForResponse)
              break
            case 'HOVER':
              onHover(dataForResponse)
              break
            case 'DB_CLICK':
              onDbClick(dataForResponse)
              break
            default:
              break
          }
        } else {
          onExeCommand(binData.command)
        }
      })
    if (subscription) return () => subscription.unsubscribe()
  }, [data, states, onClick, onDbClick, onHover, onExeCommand])

  return (
    <React.Fragment>
      <StoreContext.Provider
        value={{
          data: {
            id: data.id,
            histogram: data.histogram,
            projectionNames: data?.projectionsNames,
            projPanelIds: data?.projPanelIds
          },
          experimental: !!experimental,
          stores: {
            source: stores?.sourceStore
          },
          states: {
            onViewChange: onView,
            onProgress: onProgress,
            selectedBins: selectedBins,
            selectedView: states?.selectedView
          }
        }}
      >
        <div id="scene" style={{position: 'relative'}}>
          <a-scene embedded vr-mode-detector screen-controls style={style}
                   xr-mode-ui="XRMode: vr">
            <NdmVrLaboratory
               scene={
                 experimental
                    ? ndmvrSchema?.scene?.properties.scene?.value
                    : states?.scene
               }
            />
            {useShared && <NdmVrShared />}
            <NdmVrBaseCamera />
            <NdmVrHistogram/>
            <NdmVrHistogramLimits />
          </a-scene>
          {ndmvrInputDevice === 'mobile' &&
             <>
               <MobileController/>
               <NavbarMobile/>
             </>
          }
        </div>
        {ndmvrInputDevice !== 'oculus' && (
          <>
            {experimental ? (
              <TabletProvider
                controller="keyboard"
                componentProvider={customComponentProvider}
              />
            ) : (
              <DesktopTablet />
            )}
          </>
        )}
      </StoreContext.Provider>
    </React.Fragment>
  )
}

export default NdmVrScene
