import imageRight from '../../../assets/ndmvr/menu/arrow_circle_right_24dp.png'

function RightPaginator({ uniqueKey, onChange }) {

    window.handleNext = () => {
        onChange()
    }

    return (
        <a-entity
            key={uniqueKey}
            position="13.5 0 0"
            rounded="height: 2.2; width: 2.2; opacity: 0.3; color: #000000; radius:0.6"
            scale="0.8 0.8 0.8"
        >
            <a-entity
                position="-0.05 0 0.05"
                rounded="height: 2; width: 2; opacity: 0.7; color: #000000; radius:0.5"
            ></a-entity>
            <a-xyimage
                color="white"
                alpha-test="0.5"
                src={imageRight}
                width="1.2"
                height="1.2"
                position-xy="-0.05 0 0.1"
                active-color="#FFEB3B"
                background-color="#ffffff"
                hover-color="#F0F4C3"
                border-color="#827717"
                font-color="#000"
                onclick="handleNext()"
            ></a-xyimage>
        </a-entity>
    )
}

export default RightPaginator