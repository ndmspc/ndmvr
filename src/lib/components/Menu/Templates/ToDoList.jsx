import 'aframe';
import React, { useEffect, useRef, useState } from 'react';
import '../../../aframe/GUI/aframe-custom-xycomponents';
import '../../../aframe/GUI/aframe-xyinput';
import '../../../aframe/GUI/aframe-xylayout';
import '../../../aframe/GUI/aframe-xywidget';
import image from '../../../assets/ndmvr/menu/check_circle_24dp.png';
import imageTimer from '../../../assets/ndmvr/menu/hourglass_top_24dp.png';
import imageSend from '../../../assets/ndmvr/menu/send_24dp.png';
import { sendEmail } from '../SenderServices/ResponsesSender';
import {
  getEmailParams
} from './../SenderServices/ResponsesContent';

function ToDoList({ configurationFile }) {
  const [containers, setContainers] = useState([]);
  const [count, setCount] = useState(0);
  const [times, setTimes] = useState({});
  const [startTime, setStartTime] = useState(null);
  const [timerRunning, setTimerRunning] = useState(false);
  const timerIconRef = useRef(null);
  const [userID, setUserID] = useState(1);
  const localStorageKey = 'userCountTime'; // Key for the LocalStorage

  useEffect(() => {
    if (!configurationFile) {
      return;
    }
    setContainers(configurationFile?.instructions);
  }, []);

  useEffect(() => {
    const storedCount = localStorage.getItem(localStorageKey);
    if (storedCount) {
      setUserID(parseInt(storedCount));
    }
  }, []);

  const calculateHeight = (text) => {
    const lines = text.split('\n').length;
    return lines * 0.09 + 1.2;
  };

  window.clickAction = (id, title) => {
    const el = document.querySelector('#' + id);
    var colorSchema = '#558c53';
    let originalColor = '#ffffff';

    if (el) {
      const currentColor = el.getAttribute('material.color');
      if (currentColor === colorSchema) {
        el.setAttribute('material', 'color', originalColor);
      } else {
        el.setAttribute('material', 'color', colorSchema);
        const newCount = count + 1;
        setCount(newCount);

        if (timerRunning && startTime !== null) {
          const endTime = new Date();
          const elapsed = (endTime - startTime) / 1000; // in seconds
          console.log('time is: ' + elapsed);
          setTimes((prevTimes) => ({ ...prevTimes, [title]: elapsed }));
          setStartTime(new Date());

          if (newCount === containers.length) {
            console.log('Timer turn offed!');
            setTimerRunning(false);
            setStartTime(null);
            const timer = timerIconRef.current;
            if (timer.getAttribute('animation')) {
              timer.removeAttribute('animation');
              timer.setAttribute('rotation', '0 0 0');
            }
          }
        }
      }
    } else {
      console.error('Element with ID ' + id + ' not found.');
    }
  };

  window.startTimer = () => {
    if (timerRunning) {
      return;
    }
    setTimerRunning(true);
    setStartTime(new Date());

    const el = timerIconRef.current;
    if (el) {
      console.log('Start Timer');
      if (el.getAttribute('animation')) {
        el.removeAttribute('animation');
        el.setAttribute(
          'animation',
          'property: rotation; to: 0 0 360; loop: true; dur: 2000; easing: linear'
        );
      } else {
        el.setAttribute(
          'animation',
          'property: rotation; to: 0 0 360; loop: true; dur: 2000; easing: linear'
        );
      }
    }
  };

  window.handleSendEmail = () => {
    preparedEmail();
  };

  const preparedEmail = () => {
    const totalTasks = containers.length;
    const totalResponses = Object.keys(times).length;

    if (totalResponses === totalTasks) {
      const totalTime = Object.values(times).reduce(
        (acc, time) => acc + time,
        0
      );
      const responses = { ...times, total: totalTime };
      const params = getEmailParams(responses, count, 'sec');
      if (params) sendEmail(params, handleEmailSuccess);
    }
  };

  const handleEmailSuccess = () => {
    const newCount = userID + 1;
    setCount(newCount);
    localStorage.setItem(localStorageKey, newCount.toString());
  };

  return (
    <a-entity position="-0.75 0 0">
      {/* {(count >= 0) && ( */}
      <a-entity
        position="-12 0 0"
        rounded="height: 2.2; width: 2.2; opacity: 0.3; color: #000000; radius:0.6"
        scale="0.8 0.8 0.8"
      >
        <a-entity
          position="0.05 0 0.05"
          rounded="height: 2; width: 2; opacity: 0.7; color: #000000; radius:0.5"
        ></a-entity>
        <a-xyimage
          ref={timerIconRef}
          animation=""
          id="timerIcon"
          color="white"
          alpha-test="0.5"
          src={imageTimer}
          width="1.2"
          height="1.2"
          position-xy="0.05 0 0.1"
          active-color="#FFEB3B"
          background-color="#ffffff"
          hover-color="#F0F4C3"
          border-color="#827717"
          font-color="#000"
          onclick={`startTimer('timerIcon')`}
        ></a-xyimage>
      </a-entity>
      {/* )} */}
      {count < 4 && (
        <a-entity
          position="13.5 0 0"
          rounded="height: 2.2; width: 2.2; opacity: 0.3; color: #000000; radius:0.6"
          scale="0.8 0.8 0.8"
        >
          <a-entity
            position="-0.05 0 0.05"
            rounded="height: 2; width: 2; opacity: 0.7; color: #000000; radius:0.5"
          ></a-entity>
          <a-xyimage
            color="#A6AFAF"
            alpha-test="0.5"
            src={imageSend}
            width="1.2"
            height="1.2"
            position-xy="-0.05 0 0.1"
            active-color="#FFEB3B"
            background-color="#ffffff"
            hover-color="#F0F4C3"
            border-color="#827717"
            font-color="#000"
          ></a-xyimage>
        </a-entity>
      )}
      {count >= 4 && (
        <a-entity
          position="13.5 0 0"
          rounded="height: 2.2; width: 2.2; opacity: 0.3; color: #000000; radius:0.6"
          scale="0.8 0.8 0.8"
        >
          <a-entity
            position="-0.05 0 0.05"
            rounded="height: 2; width: 2; opacity: 0.7; color: #000000; radius:0.5"
          ></a-entity>
          <a-xyimage
            color="white"
            alpha-test="0.5"
            src={imageSend}
            width="1.2"
            height="1.2"
            position-xy="-0.05 0 0.1"
            active-color="#FFEB3B"
            background-color="#FFEB3B"
            hover-color="#F0F4C3"
            border-color="#827717"
            font-color="#000"
            onclick="handleSendEmail()"
          ></a-xyimage>
        </a-entity>
      )}
      <a-xycontainer
        width="23"
        height="10"
        direction="horizontal"
        spacing="0.5"
        justify-items="center"
        position="1 0 0.01"
      >
        {containers.map((container, index) => (
          <a-xycontainer
            key={index}
            width="5"
            height="5"
            direction="vertical"
            spacing="0.1"
            rounded="height: 8; width: 5.3; color:#202425; opacity: 0.8"
            id={container.id}
          >
            <a-text
              xyitem="fixed:true"
              width="5"
              height="0.3"
              position="0 3.4 0.1"
              anchor="center"
              wrap-count="20"
              value={container.title}
            ></a-text>
            {container.texts.map((text, textIndex) => (
              <a-entity key={textIndex} xyitem="fixed:true">
                <a-text
                  color="#ffffff"
                  width="5"
                  height="0.3"
                  position={`0 ${2.3 - textIndex * 1.6} 0.1`}
                  anchor="center"
                  wrap-count="20"
                  value={text}
                ></a-text>
                <a-entity
                  rounded={`width: 5; height: ${calculateHeight(text)}; color:#202425; opacity: 0.1`}
                  position={`0 ${2.3 - textIndex * 1.6} 0.055`}
                ></a-entity>
              </a-entity>
            ))}
            <a-xyimage
              color="#ffffff"
              xyitem="fixed:true"
              alpha-test="0.5"
              src={image}
              width="1"
              height="1"
              position-xy="0 -2.5 0.1"
              active-color="#ffffff"
              background-color="#F9FBE7"
              hover-color="#F0F4C3"
              border-color="#827717"
              font-color="#000"
              onclick={`clickAction('${container.id}', '${container.title}')`}
            ></a-xyimage>
          </a-xycontainer>
        ))}
      </a-xycontainer>
    </a-entity>
  );
}

export default ToDoList;
