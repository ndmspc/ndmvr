import 'aframe'
import React, { useEffect, useState } from 'react'
import '../../../aframe/GUI/aframe-custom-xycomponents'
import '../../../aframe/GUI/aframe-xyinput'
import '../../../aframe/GUI/aframe-xylayout'
import '../../../aframe/GUI/aframe-xywidget'
import imageSend from '../../../assets/ndmvr/menu/send_24dp.png'
import LeftPaginator from '../Paginator/LeftPaginator'
import RightPaginator from '../Paginator/RightPaginator'
import { generateCSV, getEmailParams } from './../SenderServices/ResponsesContent'
import { sendEmail } from './../SenderServices/ResponsesSender'

function SUSArea({ configurationFile, componentKey }) {
  const [questions, setQuestions] = useState([])
  const [options, setOptions] = useState([])
  const [currentIndex, setCurrentIndex] = useState(0)
  const [responses, setResponses] = useState({})
  const [count, setCount] = useState(1) // default start ID
  const localStorageKey = `userID_${componentKey}`

  useEffect(() => {
    // Exit if no configuration file provided
    if (!configurationFile) return
    setQuestions(configurationFile?.questions)
    setOptions(configurationFile?.options)
  }, [])

  useEffect(() => {
    const storedCount = localStorage.getItem(localStorageKey)
    if (storedCount) {
      setCount(parseInt(storedCount))
    }
  }, [])

  const handleNext = () => {
    if (currentIndex + 2 < questions.length) {
      setTimeout(() => {
        setCurrentIndex(currentIndex + 2)
      }, 110)
    }
  }

  const handlePrevious = () => {
    if (currentIndex - 2 >= 0) {
      setTimeout(() => {
        setCurrentIndex(currentIndex - 2)
      }, 110)
    }
  }

  window.handleOptionClick = (questionIndex, optionIndex) => {
    setResponses((prevResponses) => ({
      ...prevResponses,
      [questionIndex]: optionIndex
    }))
    console.log(questionIndex + ' : ' + optionIndex)
  }

  window.handleSendEmail = () => {
    preparedEmail();
  }

  const preparedEmail = () => {
    const totalQuestions = questions.length; // Získajte počet otázok
    const totalResponses = Object.keys(responses).length;

    if (totalResponses === totalQuestions) {
      const params = getEmailParams(responses, count);
      if (params) sendEmail(params, handleEmailSuccess);
    }
  }

  const handleEmailSuccess = () => {
    const newCount = count + 1;
    setCount(newCount);
    localStorage.setItem(localStorageKey, newCount.toString());
  };

  const renderQuestion = (question, index) => (
    <a-xycontainer
      key={index}
      width="11.5"
      height="8"
      direction="vertical"
      spacing="0.1"
      rounded="height: 8; width: 11; opacity: 0.1"
    >
      <a-text
        xyitem="fixed:true"
        width="5"
        height="0.3"
        position="0 3.4 0.1"
        color="#d1d3d4"
        anchor="center"
        wrap-count="20"
        value={`Question n.${index + 1}`}
      ></a-text>
      <a-entity xyitem="fixed:true">
        <a-text
          width="5"
          height="0.3"
          position="0 2.3 0.1"
          anchor="center"
          wrap-count="20"
          value={question.text}
        ></a-text>
        <a-entity
          rounded="width:10; height:4; color:#202425; opacity: 0.5"
          position="0 2.3 0.055"
        ></a-entity>
      </a-entity>
      <a-xycontainer width="8" height="8" direction="horizontal" spacing="0.3">
        {renderOptions(index)}
      </a-xycontainer>
    </a-xycontainer>
  )

  const renderOptions = (questionIndex) => {
    return options.map((option, index) => (
      <a-xycontainer
        key={index}
        width="1.3"
        height="8"
        direction="vertical"
        spacing="0.1"
      >
        <a-entity xyitem="fixed:true">
          <a-text
            width="5"
            height="0.3"
            position="1.9 -1 0.1"
            anchor="center"
            wrap-count="25"
            value={option.value}
          ></a-text>
        </a-entity>
        <a-xyimage
          color="white"
          xyitem="fixed:true"
          alpha-test="0.5"
          src={option.image}
          width="1"
          height="1"
          position-xy="0 -2.5 0.1"
          active-color="#FFEB3B"
          background-color="#FFEB3B"
          hover-color="#F0F4C3"
          border-color="#827717"
          font-color="#000"
          onclick={`handleOptionClick(${questionIndex + 1}, ${index + 1})`}
        ></a-xyimage>
      </a-xycontainer>
    ))
  }

  return (
    <a-entity position="-0.75 0 0">
      {currentIndex - 2 >= 0 && (
        <LeftPaginator
          uniqueKey={'sus-area-2'}
          onChange={handlePrevious}
        />
      )}
      {currentIndex + 2 < questions.length && (
        <RightPaginator
          uniqueKey={'sus-area-1'}
          onChange={handleNext}

        />
      )}
      {currentIndex + 2 >= questions.length && (
        <a-entity
          position="13.5 0 0"
          rounded="height: 2.2; width: 2.2; opacity: 0.3; color: #000000; radius:0.6"
          scale="0.8 0.8 0.8"
        >
          <a-entity
            position="-0.05 0 0.05"
            rounded="height: 2; width: 2; opacity: 0.7; color: #000000; radius:0.5"
          ></a-entity>
          <a-xyimage
            color="white"
            alpha-test="0.5"
            src={imageSend}
            width="1.2"
            height="1.2"
            position-xy="-0.05 0 0.1"
            active-color="#FFEB3B"
            background-color="#FFEB3B"
            hover-color="#F0F4C3"
            border-color="#827717"
            font-color="#000"
            onclick="handleSendEmail()"
          ></a-xyimage>
        </a-entity>
      )}
      <a-xycontainer
        width="23"
        height="10"
        direction="horizontal"
        spacing="0"
        position="0.75 0 0.01"
        id="sus-content"
      >
        {questions
          .slice(currentIndex, currentIndex + 2)
          .map((question, index) =>
            renderQuestion(question, currentIndex + index)
          )}
      </a-xycontainer>
    </a-entity>
  )
}

export default SUSArea
