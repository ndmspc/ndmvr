import 'aframe';
import React from 'react';
import '../../../aframe/GUI/aframe-custom-xycomponents';
import '../../../aframe/GUI/aframe-web-search';
import '../../../aframe/GUI/aframe-xyinput';
import '../../../aframe/GUI/aframe-xylayout';
import '../../../aframe/GUI/aframe-xywidget';
import image2 from '../../../assets/ndmvr/menu/arrow_circle_right_24dp.png';
import image from '../../../assets/ndmvr/menu/google-logo-9834.png';

function WebSearch() {
    return (

        <a-xycontainer search-box width="6" height="0.6" direction="row" align-items="stretch" justify-items="stretch">
            <a-entity position="0 -3 0">
                <a-xyimage
                    color='white'
                    xyitem="fixed:true"
                    alpha-test="0.5"
                    src={image}
                    width="6"
                    height="2"
                    position-xy="0 0 0.1"
                    active-color="#FFEB3B"
                    background-color="#F9FBE7"
                    hover-color="#F0F4C3"
                    border-color="#827717"
                    font-color="#000"
                ></a-xyimage>
            </a-entity>
            <a-entity position="0.18 0 0">
                <a-xycontainer search-box width="23" height="10" direction="row" align-items="center" justify-items="center" position='0 -5 0'>
                    <a-xyinput name="searchKeyword" placeholder="Search..." width="6" height="0.5"></a-xyinput>
                    <a-xycontainer
                    width="1"
                    height="1"
                    direction="vertical"
                    spacing="0.1"
                >
                    <a-xyimage
                        id="search-button"
                        color='#99a3a8'
                        xyitem="fixed:true"
                        alpha-test="0.5"
                        src={image2}
                        width="0.7"
                        height="0.7"
                        position-xy="0 0 0.1"
                        active-color="#99a3a8"
                        background-color="#F9FBE7"
                        hover-color="#F0F4C3"
                        border-color="#827717"
                        font-color="#000"
                    ></a-xyimage>
                    </a-xycontainer>
                </a-xycontainer>
            </a-entity>
        </a-xycontainer>
    )
}

export default WebSearch