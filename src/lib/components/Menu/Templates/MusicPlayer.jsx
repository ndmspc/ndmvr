import 'aframe'
import React, { useEffect, useState } from 'react'
import '../../../aframe/GUI/aframe-custom-xycomponents'
import '../../../aframe/GUI/aframe-xyinput'
import '../../../aframe/GUI/aframe-xylayout'
import '../../../aframe/GUI/aframe-xywidget'
import imagePlay from '../../../assets/ndmvr/menu/play_circle_24dp.png'
import LeftPaginator from '../Paginator/LeftPaginator'
import RightPaginator from '../Paginator/RightPaginator'
import play from '/src/lib/assets/ndmvr/menu/menu-songs/Relaxing Forest Morning.mp3'

const MusicPlayer = ({ configurationFile }) => {
  const [songs, setSongs] = useState([])
  const [currentPage, setCurrentPage] = useState(0)

  useEffect(() => {
    // Exit if no configuration file provided
    if (!configurationFile) {
      return
    }
    setSongs(configurationFile?.songs)
  }, [])

  let uniqueKeyCounter = 0
  const generateUniqueKey = () => {
    uniqueKeyCounter += 1
    return `unique-key-${uniqueKeyCounter}`
  }

  const handlePrevious = () => {
    setCurrentPage(Math.max(currentPage - 1, 0))
  }

  const handleNext = () => {
    setCurrentPage(Math.min(currentPage + 1, Math.ceil(songs.length / 6) - 1))
  }

  window.handlePlay = (songUrl) => {
    console.log(songUrl)
    const audioEl = document.querySelector('#audio-player')
    if (audioEl) {
      audioEl.components.sound.stopSound()
      setTimeout(() => {
        audioEl.setAttribute('src', songUrl)
      }, 10)
      setTimeout(() => {
        audioEl.components.sound.playSound()
      }, 1000)
    }
  }

  const getPageSongs = () => {
    const startIndex = currentPage * 6
    const endIndex = Math.min(startIndex + 6, songs.length)
    return songs.slice(startIndex, endIndex)
  }

  const renderContainers = () => {
    const pageSongs = getPageSongs()
    const containers = []

    for (let i = 0; i < pageSongs.length; i += 3) {
      const firstRowSongs = pageSongs.slice(i, i + 3)

      containers.push(
        <a-xycontainer
          key={generateUniqueKey()}
          align-items="start"
          width="20"
          height="5"
          direction="horizontal"
          spacing="0.8"
          position={`0.5 ${i === 0 ? 0 : -5} 0.01`}
          justify-items="space-between"
        >
          <a-xycontainer
            position="0 0 0"
            width="20"
            height="5"
            direction="horizontal"
            spacing="0.5"
          >
            {renderSongs(firstRowSongs)}
          </a-xycontainer>
        </a-xycontainer>
      )
    }

    return containers
  }

  const renderSongs = (songs) => {
    return songs.map((song, index) => (
      <a-xycontainer
        key={index}
        width="6.5"
        height="5"
        direction="vertical"
        spacing="0.5"
      >
        <a-entity xyitem="fixed:true">
          <a-text
            xyitem="fixed:true"
            width="5"
            height="0.3"
            position="0 3.4 0.1"
            anchor="center"
            wrap-count="20"
            color="#ffffff"
            value={song.title}
          ></a-text>
          <a-entity
            xyitem="fixed:true"
            rounded="width:6; height:3; opacity: 0.12"
            position="0 2.8 0.055"
          ></a-entity>
          <a-text
            width="5"
            height="0.3"
            position="0 2 0.1"
            anchor="center"
            wrap-count="25"
            value={song.description}
          ></a-text>
          <a-entity
            xyitem="fixed:true"
            rounded="width:6.5; height:3.5; opacity: 0.29; color:#000000"
            position="0 2.8 0.01"
          ></a-entity>
          <a-xyimage
            color={`${song.color}`}
            xyitem="fixed:true"
            alpha-test="0.5"
            src={imagePlay}
            width="1"
            height="1"
            position-xy="2 3.5 0.12"
            background-color={`${song.color}`}
            hover-color="#F0F4C3"
            border-color="#827717"
            font-color="#000"
            onclick={`handlePlay('${song.url}')`}
          ></a-xyimage>
        </a-entity>
      </a-xycontainer>
    ))
  }

  return (
    <a-entity position="-0.75 0 0">
      {currentPage - 1 >= 0 && (
        <LeftPaginator
          uniqueKey={'music-paginator-left'}
          onChange={handlePrevious}
        />
      )}
      {currentPage + 1 <= Math.ceil(songs.length / 6) - 1 && (
        <RightPaginator
          uniqueKey={'music-paginator-right'}
          onChange={handleNext}
        />
      )}
      {renderContainers()}
      <a-sound id="audio-player" src={play} position="0 2 0"></a-sound>
    </a-entity>
  )
}

export default MusicPlayer
