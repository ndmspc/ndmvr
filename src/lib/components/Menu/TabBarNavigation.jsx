import { useNdmVrRedirect } from '@ndmspc/react-ndmspc-core'
import 'aframe'
import React, { useEffect, useState } from 'react'
import '../../aframe/GUI/aframe-custom-xycomponents'
import '../../aframe/GUI/aframe-xyinput'
import '../../aframe/GUI/aframe-xylayout'
import '../../aframe/GUI/aframe-xywidget'
import image from '../../assets/ndmvr/menu/check_circle_24dp.png'
import menuConfiguration from '../Menu/Configuration/menu-configuration.json'

function TabBarNavigation({ onSelect, configurationFile }) {
  const [menuItems, setMenuItems] = useState([])
  const [ndmvrInputDevice] = useNdmVrRedirect('ndmvrInputDevice')

  useEffect(() => {
    // Exit if no configuration file provided
    if (!configurationFile) {
      return
    }
    setMenuItems(configurationFile?.menuItems)
  }, [])

  window.openComponent = (component) => {
    setTimeout(() => {
      onSelect(component)
    }, 880)
  }

  return (
    <a-entity
      {...(ndmvrInputDevice !== 'oculus'
        ? { scale: '0.4 0.4 0.4', position: '0 -0.7 -5', rotation: '0 0 0' }
        : {
            scale: '0.15 0.15 0.15',
            position: '0 -0.7 -5',
            rotation: '-20 0 0'
          })}
    >
      <a-entity
        rounded="width:20; height:2; color:#202425; opacity:0.8; radius: 0.8"
        scale="0.7 0.7 0.7"
        position="0 -4.09 -0.05"
        {...(ndmvrInputDevice !== 'oculus' ? {} : { position: '0 -4 -0.05' })}
      ></a-entity>
      <a-xycontainer
        width="23"
        height="10"
        direction="horizontal"
        spacing="0.1"
        justify-items="center"
        position="0 0 0.01"
      >
        {menuItems.map((item, index) => (
          <a-xycontainer
            key={index}
            width="1.3"
            height="2"
            direction="vertical"
            spacing="0.1"
            id="container-window-1"
          >
            <a-menu-icon
              key={index}
              xyitem="fixed:true"
              color={item.color || '#ff0000'}
              src={item.src || image}
              width={item.width || '2'}
              height={item.height || '2'}
              position-xy={item.position || '0 -4 0.1'}
              radius={item.radius || '0.01'}
              active-color={item.activeColor || '#FFEB3B'}
              background-color={item.backgroundColor || '#F9FBE7'}
              hover-color={item.hoverColor || '#F0F4C3'}
              border-color={item.borderColor || '#827717'}
              font-color={item.fontColor || '#000'}
              onclick={`openComponent('${item.component}')`}
            ></a-menu-icon>
          </a-xycontainer>
        ))}
      </a-xycontainer>
    </a-entity>
  )
}

export default TabBarNavigation
