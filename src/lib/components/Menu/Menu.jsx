import { useNdmVrRedirect } from '@ndmspc/react-ndmspc-core'
import 'aframe'
import React, { useState, useEffect } from 'react'
import '../../aframe/GUI/aframe-custom-xycomponents'
import '../../aframe/GUI/aframe-xyinput'
import '../../aframe/GUI/aframe-xylayout'
import '../../aframe/GUI/aframe-xywidget'
import InstructionArea from './Templates/ToDoList' // Import InstructionArea component
import MusicPlayer from './Templates/MusicPlayer'
import SUSArea from './Templates/SUSArea'
import TabBarNavigation from './TabBarNavigation' // Import TabBarNavigation
import WebSearch from './Templates/WebSearch'
import menuConfiguration from '../Menu/Configuration/menu-configuration.json'
import SelectOptions from './Options/SelectOptions'
import schemaSubject from "../../observables/schemaSubject.jsx";

const components = {
  //Component props name : Component.jsx name
  InstructionArea: InstructionArea,
  SUSArea: SUSArea,
  UESArea: SUSArea,
  WebSearch: WebSearch,
  MusicPlayer: MusicPlayer,
  Settings: SelectOptions
  // Add more components
}

//TabBarConfiguration file
const tabBarConfiguration = menuConfiguration.tabBatNavigationComponentContent

//Menu Content configurations files for its components 
const componentProps = {
  InstructionArea: {
    configurationFile: menuConfiguration.instructionsComponentContent,
    componentKey: 'Instructions'
  },
  SUSArea: {
    configurationFile: menuConfiguration.susComponentContent,
    componentKey: 'SUS'
    //props...
  },
  UESArea: {
    configurationFile: menuConfiguration.uesComponentContent,
    componentKey: 'UES'
    //props...
  },
  WebSearch: {},
  MusicPlayer: {
    configurationFile: menuConfiguration.musicPlayerComponentContent,
    componentKey: 'MusicPlayer'
  },
  Settings: {
    configurationFile: menuConfiguration.settingsContent,
    componentKey: 'SelectOption'
  }
  //props for other components as needed
}

function Menu() {
  const [ndmvrInputDevice] = useNdmVrRedirect('ndmvrInputDevice')
  const [selectedComponent, setSelectedComponent] = useState('InstructionArea') // Default to InstructionArea

  const renderComponent = () => {
    const Component = components[selectedComponent]
    const props = componentProps[selectedComponent] || {} // getting props for the selected component
    const key = `${selectedComponent}-${JSON.stringify(props)}`
    return Component ? <Component key={key} {...props} /> : null // share props with selected component
  }

  useEffect(() => {
    const schemaSubjectRef = schemaSubject.getSchemaSubject().subscribe({
      next: object => {
        console.log(object);
        componentProps.Settings = {
          ...componentProps?.Settings,
          schemaConfiguration: object,
        };
  
        // Unsubscribe immediately after receiving the response
        schemaSubjectRef.unsubscribe();
      },
      error: error => {
        console.error("Error fetching schemaSubject:", error);
        // Unsubscribe in case of an error
        schemaSubjectRef.unsubscribe();
      },
    });
  
    // Cleanup function to unsubscribe if the component unmounts before completion
    return () => {
      if (schemaSubjectRef) {
        schemaSubjectRef.unsubscribe();
      }
    };
  }, []);  

  return (
    //whole VR menu entity
    <a-entity
      {...(ndmvrInputDevice !== 'oculus' ? { 'fixed-position': '' } : {})}
      visible="false"
      id="window"
    >
      <a-xywindow
        {...(ndmvrInputDevice !== 'oculus'
          ? { position: '0 0 -7', scale: '0.5 0.5 0.5' }
          : { position: '0 0 -6', scale: '0.2 0.2 0.2' })}
        width="23"
        height="10"
        xywindow="background:true"
        id="window-content"
      >
        {renderComponent()}
      </a-xywindow>
      <TabBarNavigation
        onSelect={(componentName) => setSelectedComponent(componentName)}
        configurationFile={tabBarConfiguration}
      />
      <a-xykeyboard
        ime="true"
        scale="0.3 0.3 0.3"
        position="0 -0.5 -3"
        rotation="-30 0 0"
      ></a-xykeyboard>
    </a-entity>
  )
}

export default Menu
