import emailjs from '@emailjs/browser';

export const sendEmail = (emailParams, onSuccess, onError) => {
  emailjs
    .send(
      'service_4h8t56l',
      'template_451c1cm',
      emailParams,
      'phYti4k4aam5yTRJ5'
    )
    .then(
      (result) => {
        alert('Email successfully sent!', result.text);
        onSuccess();
      },
      (error) => {
        alert('Failed to send email. Error: ' + error.text);
        onError(error);
      }
    );
};

export const sendDataToApi = async (url, data) => {
    try {
      const response = await fetch(url, {
        method: 'POST', 
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data), 
      });
  
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`); 
      }
  
      const responseData = await response.json(); 
      return responseData; 
    } catch (error) {
      console.error('Failed to send data. Error:', error);
      throw error;
    }
  };
  
