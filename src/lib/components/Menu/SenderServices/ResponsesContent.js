export const getEmailParams = (responses, count, type) => {
  const currentTime = new Date().toLocaleString();
  type = (type) ? type : '';

  let responsesText = 'Responses:\n\n';
  Object.keys(responses).forEach((questionIndex) => {
    const optionIndex = responses[questionIndex];
    responsesText += `Question/Task ${(questionIndex)}: Option/Result ${optionIndex} ${type}\n`;
  });

  let responsesTextShort = 'Responses Short:\n\n';
  Object.keys(responses).forEach((questionIndex) => {
    const optionIndex = responses[questionIndex];
    responsesTextShort += `${(questionIndex)}:${optionIndex}\n`;
  });

  let resposenesCsvFormat = 'Responses CSV format:\n\n';
  resposenesCsvFormat += `${generateCSV(responses)}`;

  return {
    to_email: 'ndmvrdptests@gmail.com',
    from_name: 'NDMVR testing KE',
    message: `Email sent at ${currentTime}. User ID is: ${count} \n\n ${responsesText} \n\n ${responsesTextShort} \n\n ${resposenesCsvFormat}`
  };
};

export const generateCSV = (responses) => {
  const questionIndices = Object.keys(responses);
  const optionIndices = Object.values(responses);

  // header
  const csvRows = [];
  csvRows.push(['Question Index', ...questionIndices]); // first row
  csvRows.push(['Option Index', ...optionIndices]); // second row

  return csvRows.map((row) => row.join(',')).join('\n');
};
