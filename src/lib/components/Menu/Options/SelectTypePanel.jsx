
function SelectTypePanel({selectedCategory, configurationFile, currentPage, itemsPerPage, onItemChange}) {
    const category = selectedCategory;
    const selectedOptions = configurationFile?.properties[category].options;
    const startIndex = currentPage * itemsPerPage;
    const paginatedItems = selectedOptions.slice(startIndex, startIndex + itemsPerPage);
    window.handleSelect = (value) => {
        console.warn(value);  //TO DO: fix bin interaction
        onItemChange(value);
    }

    // Divide items into 3 columns
    const columns = [[], [], []]
    paginatedItems.forEach((item, index) => {
        const columnIndex = Math.floor(index / 5);
        columns[columnIndex].push(
            <a-xycontainer
                key={`${item}-${index}`}
                width="3.5"
                height="1"
                direction="vertical"
                spacing="0.5"
            >
                <a-entity xyitem="fixed:true">
                    <a-gui-button-rounded
                        value={item}
                        onclick={`handleSelect('${item}')`}
                        width="3.2"
                        height="1"
                        base-depth="0.1"
                        background-color="#5D696E"
                        hover-color="#F1DBD4"
                        border-color="#827717"
                        font-color="#ffffff"
                    ></a-gui-button-rounded>
                </a-entity>
            </a-xycontainer>
        )
    })

    return (
        <a-xycontainer
            key={currentPage}
            align-items="start"
            width="15"
            height="5"
            direction="horizontal"
            spacing="0.8"
            justify-items="space-between"
        >
            {/* Render each of the 3 columns */}
            <a-xycontainer
                position="0 0 0"
                width="5"
                height="5"
                direction="vertical"
                spacing="0.5"
            >
                {columns[0]}
            </a-xycontainer>
            <a-xycontainer
                position="0 0 0"
                width="5"
                height="5"
                direction="vertical"
                spacing="0.5"
            >
                {columns[1]}
            </a-xycontainer>
            <a-xycontainer
                position="0 0 0"
                width="5"
                height="5"
                direction="vertical"
                spacing="0.5"
            >
                {columns[2]}
            </a-xycontainer>
        </a-xycontainer>
    )
}

export default SelectTypePanel
