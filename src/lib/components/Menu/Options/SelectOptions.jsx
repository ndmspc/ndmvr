import React from 'react'
import 'aframe'
import { useEffect, useState } from 'react'
import '../../../aframe/GUI/aframe-custom-xycomponents'
import '../../../aframe/GUI/aframe-xyinput'
import '../../../aframe/GUI/aframe-xylayout'
import '../../../aframe/GUI/aframe-xywidget'
import schemaSubject from '../../../observables/schemaSubject.jsx'
import LeftPaginator from '../Paginator/LeftPaginator'
import RightPaginator from '../Paginator/RightPaginator'
import NumberTypePanel from './NumberTypePanel'
import SelectTypePanel from './SelectTypePanel'


function SelectOptions({ configurationFile, schemaConfiguration }) {
  const [options, setOptions] = useState([]) // Stores all categories (theme, scene)
  const [selectedCategory, setSelectedCategory] = useState('theme'); // Default category
  const [selectedScheme, setSelectedScheme] = useState('histogram'); // Default scheme of the category
  const [currentPage, setCurrentPage] = useState(0) // For pagination on the right side
  const [itemsPerPage] = useState(15) // 18 items per page (6 per column * 3 columns)
  const [schema, setNdmvrSchema] = useState(null); //change schema for the NDMVRscene component

  useEffect(() => {
    if (configurationFile) {
      // Extract the properties from the configuration
      const configOptions = Object.keys(configurationFile?.properties);
      setOptions(configOptions);
    }

    if (schemaConfiguration) {
      // Extract the scheme structure from the configuration
      setNdmvrSchema(schemaConfiguration);
    }
  }, [])


  let uniqueKeyCounter = 0
  const generateUniqueKey = () => {
    uniqueKeyCounter += 1
    return `unique-key-${uniqueKeyCounter}`
  }

  const handlePrevious = () => {
    if (currentPage > 0) {
      setCurrentPage(currentPage - 1)
    }
  }

  const handleNext = () => {
    const totalItems = configurationFile?.properties[selectedCategory].options.length
    if (currentPage < Math.ceil(totalItems / itemsPerPage) - 1) {
      setCurrentPage(currentPage + 1)
    }
  }

  const handleSchemeChange = (newValue) => {
    // Check if the schema is defined
    if (!schema) {
      console.error(
        "The schema is not defined (useNdmVrRedirect('ndmvrSchema'))... no configuration aspects are present!"
      );
      return; 
    }

    // Check if the selected scheme and category exist in the schema
    const selectedSchemeProperties = schema[selectedScheme]?.properties[selectedCategory];
    if (!selectedSchemeProperties) {
      console.warn(
        `The selected scheme "${selectedScheme}" or category "${selectedCategory}" does not exist in the schema.`
      );
      return;
    }

    // Update the schema and emit the changes
    const updatedSchema = {
      ...schema,
      [selectedScheme]: {
        ...schema[selectedScheme],
        properties: {
          ...schema[selectedScheme].properties,
          [selectedCategory]: {
            ...selectedSchemeProperties,
            value: newValue, // Update the value
          },
        },
      },
    };

    setNdmvrSchema(updatedSchema); // Update local component schema state 
    schemaSubject.next(updatedSchema); // Emit the updated schema
  };


  // Renders left panel options (theme, scene, etc.)
  const renderLeftPanel = () => {

    const handleOptions = (category) => {
      setSelectedCategory(category);
      setSelectedScheme(configurationFile?.properties[category]?.scheme);
      setCurrentPage(0); // Reset to first page when switching category
    }

    window.onClickCategory = (category) => {
      handleOptions(category);
    }

    return options.map((category, index) => (
      <a-xycontainer
        key={index}
        width="3.5"
        height="1"
        direction="vertical"
        spacing="0.5"
      >
        <a-entity xyitem="fixed:true">
          <a-gui-button-rounded
            value={category}
            width="5"
            height="1"
            base-depth="0.2"
            background-color="#F0F4C3"
            hover-color="#FFEB3B"
            border-color="#827717"
            font-color="#000"
            onclick={`onClickCategory('${category}')`}
          >
          </a-gui-button-rounded>
        </a-entity>
      </a-xycontainer>
    ))
  }

  // Renders right panel content in 3 columns with max 6 items per column
  const renderSelectTypePanel = () => (
    <SelectTypePanel
      itemsPerPage={itemsPerPage}
      configurationFile={configurationFile}
      selectedCategory={selectedCategory}
      currentPage={currentPage}
      onItemChange={handleSchemeChange}
    />
  )


  const renderNumberTypePanel = () => (
    <NumberTypePanel
      {...configurationFile?.properties[selectedCategory]?.options}
      onScaleChange={handleSchemeChange} />

  )

  return (
    // content window wrapper (a-entity)
    <a-entity position="-0.75 0 0">
      {/* Options for selecting items */}
      <a-entity position="-6 -0.52 0">
        <a-entity position="-1 0.5 0" rounded="width:6.5;height:10;color:#202425; opacity: 0.8;radius:0.6"></a-entity>
        <a-text
          xyitem="fixed:true"
          width="8"
          height="0.3"
          position="0.5 4.7 0.1"
          anchor="center"
          wrap-count="20"
          value={configurationFile?.description}
        ></a-text>

        <a-xycontainer
          key={generateUniqueKey()}
          position="0 -1.2 0"
          width="5"
          height="5"
          direction="vertical"
          spacing="0.5"
        >
          {renderLeftPanel()}
        </a-xycontainer>
      </a-entity>

      {/* Custom paginator (left arrow) navigation between pages  */}
      {currentPage > 0 && (
        <LeftPaginator
          uniqueKey={generateUniqueKey()}
          onChange={handlePrevious}
        />
      )}

      {/* Custom pagination (right arrow) navigation between pages  */}
      {currentPage < Math.ceil(configurationFile?.properties[selectedCategory].options?.length / itemsPerPage) - 1 && (
        <RightPaginator
          uniqueKey={generateUniqueKey()}
          onChange={handleNext}
        />
      )}


      {/* Right Panel - Content for the selected category */}
      <a-entity position="5.5 -0.52 0" width="15">
        {/* rounded background entity */}
        <a-entity position="-1 0.5 0" rounded="width:15;height:10;color:#202425; opacity: 0.8;radius:0.6"></a-entity>
        <a-text
          width="15"
          height="0.3"
          position="-0.2 4.7 0.1"
          wrap-count="50"
          anchor="center"
          value={configurationFile?.properties[selectedCategory].description}
        ></a-text>
        <a-xycontainer
          position="0 -1.2 0"
          key={currentPage}
          align-items="left"
          width="15"
          height="5"
          direction="horizontal"
          spacing="0.8"
          justify-items="space-between"
          id="content-items"
        >
          {configurationFile?.properties[selectedCategory].type === 'number' && (renderNumberTypePanel())}
          {configurationFile?.properties[selectedCategory].type === 'select' && (renderSelectTypePanel())}
        </a-xycontainer>
      </a-entity>
    </a-entity>
  )
}

export default SelectOptions
