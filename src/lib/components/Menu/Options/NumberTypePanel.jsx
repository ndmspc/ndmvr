import 'aframe'
import { useState } from 'react'
import '../../../aframe/GUI/aframe-custom-xycomponents'
import '../../../aframe/GUI/aframe-xyinput'
import '../../../aframe/GUI/aframe-xylayout'
import '../../../aframe/GUI/aframe-xywidget'
import imagePlus from '../../../assets/ndmvr/menu/add_circle_24dp_E8EAED_FILL1_wght400_GRAD0_opsz24.png'
import imageMinus from '../../../assets/ndmvr/menu/do_not_disturb_on_24dp_E8EAED_FILL1_wght400_GRAD0_opsz24.png'

function NumberTypePanel({ initScale, initStepScale, detailStepScale, detailStartScale, minScale, maxScale, onScaleChange}) {
    const [scaleValue, setScaleValue] = useState(initScale);

    window.setCounter = (action) => {

        let newScaleValue;

        ((action == 'plus') && (scaleValue > detailStartScale)) ?
            newScaleValue = (Math.min(scaleValue + initStepScale, maxScale)) :
            ((action == 'minus') && (scaleValue > detailStartScale + 0.1)) ?
                newScaleValue = (Math.max(scaleValue - initStepScale, minScale)) : null;

        ((action == 'plus') && (scaleValue < detailStartScale)) ?
            newScaleValue = (Math.min(scaleValue + detailStepScale, maxScale)) :
            ((action == 'minus') && (scaleValue <= detailStartScale + 0.1)) ?
                newScaleValue = (Math.max(scaleValue - detailStepScale, minScale)) : null;

        //seting the scale value and calling callback
        if (newScaleValue !== undefined) {
            setScaleValue(newScaleValue);
            onScaleChange(newScaleValue);  //callback
        }


    }

    return (
        <a-xycontainer
            key={'scale-container-654'}
            width="3.5"
            height="1"
            direction="horizontal"
            spacing="0.5"
        >
            <a-entity
                position="0.5 0 0"
                rounded="height: 2.2; width: 2.2; opacity: 0.3; color: #000000; radius:0.6"
                scale="0.8 0.8 0.8"
            >
                <a-entity
                    position="0.05 0 0.05"
                    rounded="height: 2; width: 2; opacity: 0.7; color: #5D696E; radius:0.5"
                ></a-entity>
                <a-xyimage
                    color="#ffffff"
                    alpha-test="0.5"
                    src={imageMinus}
                    width="1.2"
                    height="1.2"
                    position-xy="0.05 0 0.1"
                    active-color="#FFEB3B"
                    background-color="#ffffff"
                    hover-color="#F0F4C3"
                    border-color="#827717"
                    font-color="#000"
                    onclick="setCounter('minus')"
                ></a-xyimage>
            </a-entity>
            <a-entity
                position="5 0 0.05"
                rounded="height: 5.2; width: 5.2; opacity: 0.3; color: #000000; radius:0.6"
                scale="0.8 0.8 0.8"
            >
                <a-entity
                    xyitem="fixed:true"
                    position="0 0 0.05"
                    rounded="width:5; height:5; color:#3F474C"
                >
                    <a-text
                        xyitem="fixed:true"
                        width="15"
                        height="0.3"
                        position="5.55 0 0.1"
                        anchor="center"
                        wrap-count="15"
                        value={(scaleValue < 10) ? scaleValue.toFixed(2) : scaleValue.toFixed(1)}
                    ></a-text>

                </a-entity>
            </a-entity>

            <a-entity
                position="9.5 0 0"
                rounded="height: 2.2; width: 2.2; opacity: 0.3; color: #000000; radius:0.6"
                scale="0.8 0.8 0.8"
            >
                <a-entity
                    position="-0.05 0 0.05"
                    rounded="height: 2; width: 2; opacity: 0.7; color: #5D696E; radius:0.5"
                ></a-entity>
                <a-xyimage
                    color="white"
                    alpha-test="0.5"
                    src={imagePlus}
                    width="1.2"
                    height="1.2"
                    position-xy="-0.05 0 0.1"
                    active-color="#FFEB3B"
                    background-color="#ffffff"
                    hover-color="#F0F4C3"
                    border-color="#827717"
                    font-color="#000"
                    onclick="setCounter('plus')"
                ></a-xyimage>
            </a-entity>
        </a-xycontainer>
    )
}

export default NumberTypePanel
