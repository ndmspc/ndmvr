import AFRAME from 'aframe'
import { openFile } from 'jsroot'
import { NdmVrStorageService } from '../services/ndmVrStorageService'
import { createTH2Histogram } from '../utils/histogramAframeFactory'
import '../aframe/cursor'
import '../aframe/th'
import '../aframe/histogram'
import { histogramTH2Service } from '../observables/histogramSubject'
import {
  initialKeyboardController,
  keyReleaseHandlerFunction,
  keyPressHandlerFunction,
  keyboardUpdateCameraReference
} from '../controllers/keyboardController'
import { CameraService } from '../services/cameraService'
import HistogramDemoFactory from '../utils/DefaultHistogram'

/**
 * @deprecated this solution use histogram rendering without react control, pure javascript
 * actually this is not used in any component in our library
 * maybe this class finds usage in next level of development in future
 */
export default AFRAME.registerComponent('th2-histogram', {
  schema: {
    section: { type: 'string' },
    filename: { type: 'string' },
    theme: { type: 'string', default: 'def' },
    position: {
      name: 'position',
      xValue: 0,
      yValue: 0.1,
      zValue: 0
    },
    scale: {
      name: 'scale',
      xValue: 2,
      yValue: 2,
      zValue: 2
    },
    rotation: {
      name: 'rotation',
      xValue: 0,
      yValue: 0,
      zValue: 0
    }
  },
  init: function () {
    const el = this.el
    initialKeyboardController(this.schema, el)
    // keyboard listeners
    document.addEventListener('keydown', keyPressHandlerFunction)
    document.addEventListener('keyup', keyReleaseHandlerFunction)
    // this.JSROOT = window.JSROOT
    // this.demoHistogramService = new HistogramDemoFactory(this.JSROOT)
    this.demoHistogramService = new HistogramDemoFactory()
    this.localStorageService = new NdmVrStorageService()
    this.cameraService = new CameraService()
    this.render = false
    this.histogram = null
    this.model = {
      bins: {
        elements: [],
        attributes: []
      },
      labels: {
        elements: [],
        attributes: []
      },
      titles: {
        elements: [],
        attributes: []
      },
      banners: {
        elements: [],
        attributes: []
      },
      normalXAxis: {
        elements: [],
        attributes: []
      },
      reversedXAxis: {
        elements: [],
        attributes: []
      },
      normalYAxis: {
        elements: [],
        attributes: []
      },
      reversedYAxis: {
        elements: [],
        attributes: []
      }
    }
    this.section = null
    // functions

    const updateHistogramSection = (newSection) => {
      console.log(newSection)
      this.section.xOffset = newSection.xOffset
      this.section.yOffset = newSection.yOffset
      this.range = newSection.range

      this.cameraService.setCameraPosition(newSection)
      createTH2Histogram(
        this.el,
        this.model,
        this.histogram,
        this.section.xOffset,
        this.section.yOffset,
        this.section.range,
        this.data.theme
      )
      this.render = true
    }
    const handleSubscription = (section) => {
      this.localStorageService.storeTH2Offsets(
        section.xOffset,
        section.yOffset,
        section.range
      )
      updateHistogramSection(section)
    }

    this.subscription = histogramTH2Service
      .getChangedSection()
      .subscribe(handleSubscription)
  },
  update: function () {
    keyboardUpdateCameraReference()
    this.section = JSON.parse(this.data.section)
    openFile(this.data.filename)
      .then((file) => file.readObject('hUsersVsProjects'))
      .then((obj) => {
        this.localStorageService.initHistogramData(obj)
        this.histogram = obj
        createTH2Histogram(
          this.el,
          this.model,
          this.histogram,
          this.section.xOffset,
          this.section.yOffset,
          this.section.range,
          this.data.theme
        )
        this.render = true
      })
      .then(() => console.log('drawing completed'))
      .catch((error) => {
        this.histogram = this.demoHistogramService.createTH2DemoHistogram(
          20,
          20,
          0,
          0.1
        )
        this.localStorageService.initHistogramData(this.histogram)
        createTH2Histogram(
          this.el,
          this.model,
          this.histogram,
          this.section.xOffset,
          this.section.yOffset,
          this.section.range,
          this.data.theme
        )
        this.render = true
        console.log(error)
      })
  },
  tick: function () {
    if (this.render) {
      this.render = false
    }
  },
  remove: function () {
    this.subscription.unsubscribe()
    document.removeEventListener('keydown', keyPressHandlerFunction)
    document.removeEventListener('keyup', keyReleaseHandlerFunction)
  }
})
