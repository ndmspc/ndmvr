import builtInSchema from "../resources/builtinShemas.json";
import React, {useEffect} from "react";
import '../resources/NdmVr.css'
import NdmVrContainer from "./NdmVrContainer.jsx";
import {localStore, NdmVrContext, redirectStore} from '@ndmspc/react-ndmspc-core'



/**
 * Component that wraps (is parent) NdmVrScene.
 * Have logic for toggling fullscreen for mobile devices.*/
const NdmVr = ({
                  data,
                  states,
                  stores,
                  config = builtInSchema,
                  customComponentProvider,
                  experimental,
                  selectedBins,
                  cinemaFunc,
                  onClick,
                  onDbClick,
                  onHover,
                  onView,
                  onProgress,
                  onExeCommand,
                  style,
                  useShared
               }) => {

   return (
      <NdmVrContext.Provider
         value={{
               ndmvrCameraPosRot: localStore('ndmvrCameraPosRot'),
               ndmvrInputDevice: redirectStore('ndmvrInputDevice'),
               ndmvrSchema: redirectStore('ndmvrSchema'),
               ndmvrFullscreen: redirectStore('ndmvrFullscreen'),
            }}>
         <NdmVrContainer
            data={data}
            states={states}
            stores={stores}
            config={config}
            customComponentProvider={customComponentProvider}
            experimental={experimental}
            selectedBins={selectedBins}
            cinemaFunc={cinemaFunc}
            onClick={onClick}
            onDbClick={onDbClick}
            onHover={onHover}
            onView={onView}
            onProgress={onProgress}
            onExeCommand={onExeCommand}
            style={style}
            useShared={useShared}
         />
      </NdmVrContext.Provider>

   )
}

export default NdmVr