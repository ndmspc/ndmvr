import React, { useEffect, useContext, useState } from 'react'
import { useStreamBrokerIn, NdmSpcContext } from '@ndmspc/react-ndmspc-core'
import UserAvatar from './components/UserAvatar'
import Controller from './components/Controller'

const NdmVrShared = ({ wssb = 'shared' }) => {
  const sb = useContext(NdmSpcContext)[wssb]
  const move = useStreamBrokerIn('move', wssb)
  const ws = useStreamBrokerIn('ws', wssb)
  const master = useStreamBrokerIn('master', wssb)

  const [clients, setClients] = useState([])
  const [masterId, setMasterId] = useState(null)
  const [socketId, setSocketId] = useState('')

  useEffect(() => {
    if (move.payload !== undefined) {
      move.payload.clients.length > 0
        ? setClients(move.payload.clients)
        : setClients([])
    }
  }, [move.payload])

  useEffect(() => {
    if (ws.payload !== undefined) {
      setSocketId(ws.payload.id)
    }
  }, [ws.payload])

  useEffect(() => {
    if (master.payload !== undefined) {
      setMasterId(master.payload.master)
    }
  }, [master.payload])

  return (
    <a-entity>
      <Controller></Controller>
      {clients
        .filter((client) => client.id !== socketId)
        .map((client, index) => {
          const position = client.position
          const rotation = client.rotation
          const positionParse = position.join(' ')
          return (
            <UserAvatar
              key={index + client.id}
              id={client.id}
              position={positionParse}
              rotation={rotation}
              masterId={masterId}
              color={client.color}
              username={client.username || 'User'}
            />
          )
        })}
    </a-entity>
  )
}

export default NdmVrShared
