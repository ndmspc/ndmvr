import * as THREE from 'three'
import React, { useEffect, useRef } from 'react'

const UserAvatar = ({ position, rotation, id, color, masterId, username }) => {
  const currentPos = useRef(new THREE.Vector3())
  const targetPos = useRef(new THREE.Vector3())
  const currentRot = useRef(new THREE.Quaternion())
  const targetRot = useRef(new THREE.Quaternion())
  const frameRef = useRef()
  const userRef = useRef()

  useEffect(() => {
    const posArray = position.split(' ').map(Number)
    targetPos.current.set(posArray[0], posArray[1], posArray[2])
    
    if (currentPos.current.lengthSq() === 0) {
      currentPos.current.copy(targetPos.current)
    }

    targetRot.current.fromArray(rotation)
    
    if (currentRot.current.lengthSq() === 0) {
      currentRot.current.copy(targetRot.current)
    }
  }, [position, rotation])

  useEffect(() => {
    const user = document.getElementById(id)
    userRef.current = user

    const animate = () => {
      currentPos.current.lerp(targetPos.current, 0.1)
      currentRot.current.slerp(targetRot.current, 0.1)

      if (userRef.current) {
        userRef.current.setAttribute('position', currentPos.current)
        const threeEuler = new THREE.Euler().setFromQuaternion(
          currentRot.current,
          'YXZ'
        )
        const aframeRotation = {
          x: THREE.MathUtils.radToDeg(threeEuler.x),
          y: THREE.MathUtils.radToDeg(threeEuler.y),
          z: -THREE.MathUtils.radToDeg(threeEuler.z)
        }
        userRef.current.setAttribute('rotation', aframeRotation)
      }

      frameRef.current = requestAnimationFrame(animate)
    }

    animate()

    return () => {
      if (frameRef.current) {
        cancelAnimationFrame(frameRef.current)
      }
    }
  }, [id])

  const renderTopHat = () => {
    return (
      <a-entity position='0 0.25 0'>
        <a-cone
          color='gold'
          radius-bottom='0.5'
          radius-top='0.1'
          height='0.8'
          position='0 0.4 0'
        ></a-cone>

        <a-sphere color={color} radius='0.05' position='0 0.4 -0.3'></a-sphere>
        <a-sphere color={color} radius='0.05' position='0 0.6 -0.2'></a-sphere>
      </a-entity>
    )
  }

    const renderNameTag = () => {
        const namePosition = masterId === id ? "0 1.5 0" : "0 0.8 0"
        return (
            <a-entity position={namePosition}>
                {/* Front text with stroke */}
                <a-text
                    value={username || 'User'}
                    position="0 0 0.02"
                    align="center"
                    color="white"
                    scale="0.5 0.5 0.5"
                    font="mozillavr"
                    width="5"
                    rotation="0 0 0"
                    stroke="#808080"
                    stroke-width="2"
                ></a-text>
                {/* Back text with stroke */}
                <a-text
                    value={username || 'User'}
                    position="0 0 -0.02"
                    align="center"
                    color="white"
                    scale="0.5 0.5 0.5"
                    font="mozillavr"
                    width="5"
                    rotation="0 180 0"
                    stroke="#808080"
                    stroke-width="2"
                ></a-text>
            </a-entity>
        )
    }

  return (
    <a-entity position={position} id={id}>
      {renderNameTag()}
      
      {/* HEAD */}
      <a-sphere radius='0.5' color='#F8DF87'>
        {/* TOP HAT */}
        {masterId === id ? renderTopHat() : <></>}
      </a-sphere>

      <a-entity position='0.2 0.15 -0.45'>
        <a-sphere radius='0.1' color='#FFFFFF'>
          <a-sphere
            radius='0.05'
            color='#000000'
            position='0 0 -0.07'
          ></a-sphere>
        </a-sphere>
      </a-entity>

      <a-entity position='-0.2 0.15 -0.45'>
        <a-sphere radius='0.1' color='#FFFFFF'>
          <a-sphere
            radius='0.05'
            color='#000000'
            position='0 0 -0.07'
          ></a-sphere>
        </a-sphere>
      </a-entity>

      <a-cylinder
        height='0.05'
        radius='0.05'
        color='#FF6B6B'
        position='0 -0.25 -0.4'
        rotation='0 90 90'
      ></a-cylinder>
      <a-sphere radius='0.1' color='#FF6B6B' position='0 -0.1 -0.5'></a-sphere>

      {/* BODY */}
      <a-cylinder
        height='0.8'
        radius='0.3'
        color={color}
        position='0 -0.6 0'
      ></a-cylinder>

      {/* ARMS */}
      <a-entity position='-0.4 -0.4 0'>
        <a-cylinder
          height='0.6'
          radius='0.1'
          color='#F8DF87'
          position='0 -0.3 0'
        ></a-cylinder>
        <a-sphere radius='0.15' color='#F8DF87' position='0 -0.6 0'></a-sphere>
      </a-entity>

      <a-entity position='0.4 -0.4 0'>
        <a-cylinder
          height='0.6'
          radius='0.1'
          color='#F8DF87'
          position='0 -0.3 0'
        ></a-cylinder>
        <a-sphere radius='0.15' color='#F8DF87' position='0 -0.6 0'></a-sphere>
      </a-entity>

      {/* LEGS */}
      <a-entity position='-0.15 -1 0'>
        <a-cylinder
          height='0.8'
          radius='0.2'
          color={color}
          position='0 -0.4 0'
        ></a-cylinder>
        <a-sphere radius='0.25' color='#F8DF87' position='0 -0.8 0'></a-sphere>
      </a-entity>

      <a-entity position='0.15 -1 0'>
        <a-cylinder
          height='0.8'
          radius='0.2'
          color={color}
          position='0 -0.4 0'
        ></a-cylinder>
        <a-sphere radius='0.25' color='#F8DF87' position='0 -0.8 0'></a-sphere>
      </a-entity>
    </a-entity>
  )
}

export default UserAvatar
