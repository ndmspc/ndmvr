const sendSelectedBinsToServer = (context, data) => {
    context.send({
        type: 'selected_bins',
        payload: data
    })
}

export default sendSelectedBinsToServer;
