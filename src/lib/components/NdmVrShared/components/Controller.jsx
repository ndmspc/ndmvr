import React, { useState, useEffect, useContext, useRef } from 'react'
import * as THREE from 'three'
import { NdmSpcContext } from '@ndmspc/react-ndmspc-core'
import AssignMasterController from './AssignMasterController'
import AdjustHistogramAxisController from './AdjustHistogramAxisController'

const Controller = ({ wssb = 'shared' }) => {
  const sb = useContext(NdmSpcContext)[wssb]
  const lastPosition = useRef(new THREE.Vector3())
  const lastRotation = useRef(new THREE.Quaternion())
  const frameRef = useRef()
  const isUpdating = useRef(false)
  const updateTimeout = useRef(null)

  useEffect(() => {
    const camera = document.getElementById('camera')
    if (!camera) return

    const absolutePosition = new THREE.Vector3()
    const rotationQuaternion = new THREE.Quaternion()
    
    const updatePosition = () => {
      if (!camera || isUpdating.current) {
        frameRef.current = requestAnimationFrame(updatePosition)
        return
      }

      camera.object3D.getWorldPosition(absolutePosition)
      camera.object3D.getWorldQuaternion(rotationQuaternion)

      if (
        lastPosition.current.distanceTo(absolutePosition) > 0.01 ||
        lastRotation.current.angleTo(rotationQuaternion) > 0.01
      ) {
        isUpdating.current = true

        sb.send({
          type: 'move',
          payload: {
            position: absolutePosition.toArray(),
            rotation: rotationQuaternion.toArray()
          }
        })

        lastPosition.current.copy(absolutePosition)
        lastRotation.current.copy(rotationQuaternion)

        if (updateTimeout.current) {
          clearTimeout(updateTimeout.current)
        }
        
        updateTimeout.current = setTimeout(() => {
          isUpdating.current = false
        }, 50)
      }

      frameRef.current = requestAnimationFrame(updatePosition)
    }

    updatePosition()

    return () => {
      if (frameRef.current) {
        cancelAnimationFrame(frameRef.current)
      }
      if (updateTimeout.current) {
        clearTimeout(updateTimeout.current)
      }
    }
  }, [sb])

  return (
    <a-entity>
      <AssignMasterController />
      <AdjustHistogramAxisController />
    </a-entity>
  )
}

export default Controller
