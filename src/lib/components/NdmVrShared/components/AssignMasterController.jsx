import React, { useContext, useEffect, useState } from 'react'
import { NdmSpcContext, useStreamBrokerIn } from '@ndmspc/react-ndmspc-core'
import { StoreContext } from '../../contexts/StoreContext'
import { toJSON } from 'jsroot'

const M_KEY = 77 | 109

const AssignMasterController = ({ wssb = 'shared' }) => {
  const sb = useContext(NdmSpcContext)[wssb]
  const master = useStreamBrokerIn('master', wssb)
  const ws = useStreamBrokerIn('ws', wssb)
  const [isMaster, setIsMaster] = useState(false)
  const [socketId, setSocketId] = useState(null)

  const { histogram } = useContext(StoreContext).data

  useEffect(() => {
    if (master.payload !== undefined) {
      setIsMaster(master.payload.master === socketId)
    }
  }, [master.payload, socketId])

  useEffect(() => {
    if (ws.payload !== undefined) {
      setSocketId(ws.payload.id)
    }
  }, [ws.payload])

  const _handleKeyM = (event) => {
    if (event.keyCode === M_KEY) {
      // If already master, deassign. If not master, try to become master
      const actionType = isMaster ? 'deassign_master' : 'assign_master'
      
      sb.send({
        type: actionType,
        payload: {}
      })

      // Only send histogram data when becoming master
      if (!isMaster) {
        sb.send({
          type: 'histogram',
          payload: JSON.stringify(toJSON(histogram))
        })
      }
    }
  }

  useEffect(() => {
    window.addEventListener('keypress', _handleKeyM)
    return () => {
      window.removeEventListener('keypress', _handleKeyM)
    }
  }, [isMaster, socketId])

  return <></>
}

export default AssignMasterController
