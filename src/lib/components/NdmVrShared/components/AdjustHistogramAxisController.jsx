import React, { useContext, useEffect, useState } from 'react'
import { NdmSpcContext, useStreamBrokerIn } from '@ndmspc/react-ndmspc-core'
import {
  histogramTH1Service,
  histogramTH2Service,
  histogramTH3Service
} from '../../../observables/histogramSubject'

const KEYS = new Set([73, 105, 74, 106, 75, 107, 76, 108, 85, 117, 79, 111])

const keyMap = new Map([
  [[73, 105], 'X_UP'],    // I key
  [[75, 107], 'X_DOWN'],  // K key
  [[76, 108], 'Y_UP'],    // L key
  [[74, 106], 'Y_DOWN'],  // J key
  [[85, 117], 'Z_UP'],    // U key
  [[79, 111], 'Z_DOWN']   // O key
])

// Constants for histogram types
const HISTOGRAM_TYPES = {
  TH1: 'TH1',
  TH2: 'TH2',
  TH3: 'TH3'
}

// An axis configuration for each change type
const AXIS_CONFIGS = {
  X_UP: {
    offset: 'yOffset',
    direction: true,
    histograms: [HISTOGRAM_TYPES.TH2, HISTOGRAM_TYPES.TH3]
  },
  X_DOWN: {
    offset: 'yOffset',
    direction: false,
    histograms: [HISTOGRAM_TYPES.TH2, HISTOGRAM_TYPES.TH3]
  },
  Y_UP: {
    offset: 'xOffset',
    direction: true,
    histograms: [HISTOGRAM_TYPES.TH1, HISTOGRAM_TYPES.TH2, HISTOGRAM_TYPES.TH3]
  },
  Y_DOWN: {
    offset: 'xOffset',
    direction: false,
    histograms: [HISTOGRAM_TYPES.TH1, HISTOGRAM_TYPES.TH2, HISTOGRAM_TYPES.TH3]
  },
  Z_UP: {
    offset: 'zOffset',
    direction: true,
    histograms: [HISTOGRAM_TYPES.TH3]
  },
  Z_DOWN: {
    offset: 'zOffset',
    direction: false,
    histograms: [HISTOGRAM_TYPES.TH3]
  }
}

// Mapping of histogram services
const histogramServices = {
  [HISTOGRAM_TYPES.TH1]: histogramTH1Service,
  [HISTOGRAM_TYPES.TH2]: histogramTH2Service,
  [HISTOGRAM_TYPES.TH3]: histogramTH3Service
}

const AdjustHistogramAxisController = ({ wssb = 'shared' }) => {
  const sb = useContext(NdmSpcContext)[wssb]
  const axisScalingLatest = useStreamBrokerIn('axisScalingLatest', wssb)
  const master = useStreamBrokerIn('master', wssb)
  const ws = useStreamBrokerIn('ws', wssb)
  const [isMaster, setIsMaster] = useState(false)
  const [socketId, setSocketId] = useState(null)
  const [currentAxisPositions, setCurrentAxisPositions] = useState({
    xOffset: 0,
    yOffset: 0,
    zOffset: 0
  });
  const [clientPositions, setClientPositions] = useState({
    xOffset: 0,
    yOffset: 0,
    zOffset: 0
  });

  // Add new state for sync control
  const [shouldSyncWithMaster, setShouldSyncWithMaster] = useState(false);
  const P_KEY = 80; // ASCII code for 'P' key

  // Update master status
  useEffect(() => {
    if (master.payload !== undefined && ws.payload !== undefined) {
      const newIsMaster = master.payload.master === ws.payload.id
      //console.log('Setting master status:', {
      //  masterPayload: master.payload,
      //  wsId: ws.payload.id,
      //  isMaster: newIsMaster
      //})
      setIsMaster(newIsMaster)
      setSocketId(ws.payload.id)

      // Initial sync if new master
      if (newIsMaster) {
        // console.log('Syncing initial positions as new master');
        sb.send({
          type: 'axis_scaling_latest',
          payload: { 
            axis: 'INITIAL_SYNC',
            initiator: ws.payload.id,
            positions: currentAxisPositions
          }
        });
      }
    }
  }, [master.payload, ws.payload, currentAxisPositions, sb])

  useEffect(() => {
    const handleKeyDown = (event) => {
      // First check if the key is one of our control keys
      if (!KEYS.has(event.keyCode)) return;

      // Block keys only if there is a master AND sync is enabled AND current client is not master
      if (master.payload?.master && shouldSyncWithMaster && !isMaster) {
        console.log('Blocking histogram control key - synced with master');
        event.preventDefault();
        event.stopPropagation();
        return;
      }
      
      const keyPair = [...keyMap.keys()].find(pair => pair.includes(event.keyCode));
      if (!keyPair) return;

      const action = keyMap.get(keyPair);
      const newPositions = { ...currentAxisPositions };

      switch(action) {
        case 'X_UP':
          newPositions.yOffset++;
          break;
        case 'X_DOWN':
          newPositions.yOffset--;
          break;
        case 'Y_UP':
          newPositions.xOffset++;
          break;
        case 'Y_DOWN':
          newPositions.xOffset--;
          break;
        case 'Z_UP':
          newPositions.zOffset++;
          break;
        case 'Z_DOWN':
          newPositions.zOffset--;
          break;
      }

      setCurrentAxisPositions(newPositions);

      // Only send updates if we're master
      if (isMaster) {
        sb.send({
          type: 'axis_scaling_latest',
          payload: { 
            axis: action,
            initiator: socketId,
            positions: newPositions
          }
        });
      }
    };

    window.addEventListener('keydown', handleKeyDown, true);
    return () => {
      window.removeEventListener('keydown', handleKeyDown, true);
    };
  }, [isMaster, socketId, currentAxisPositions, sb, master.payload, shouldSyncWithMaster]);

  // Add handler for P key
  useEffect(() => {
    const handleSyncToggle = (event) => {
      if (event.keyCode === P_KEY && !isMaster) {
        setShouldSyncWithMaster(prev => {
          const newState = !prev;
          // console.log(`Sync with master ${newState ? 'enabled' : 'disabled'}`);
          
          // If enabling sync, request current master positions
          if (newState) {
            sb.send({
              type: 'request_sync',
              payload: {
                requesterId: socketId
              }
            });
          }
          
          return newState;
        });
      }
    };

    window.addEventListener('keydown', handleSyncToggle);
    return () => {
      window.removeEventListener('keydown', handleSyncToggle);
    };
  }, [isMaster, socketId, sb]);

  // Force sync when enabling sync mode
  useEffect(() => {
    if (shouldSyncWithMaster && !isMaster && master.payload?.master) {
      // Request current master positions
      sb.send({
        type: 'request_sync',
        payload: {
          requesterId: socketId
        }
      });
    }
  }, [shouldSyncWithMaster, isMaster, master.payload, socketId, sb]);

  // Apply histogram position changes
  const applyHistogramChange = (newPositions, oldPositions) => {
    if (!isMaster) return;

    try {
    //  console.log(`[${isMaster ? 'Master' : 'Follower'} ${socketId}] Starting histogram changes:`, {
    //    from: oldPositions,
    //    to: newPositions
    //  });

      Object.entries(newPositions).forEach(([offset, value]) => {
        const diff = value - oldPositions[offset];
        // Skip if no change
        if (diff === 0) {
          return;
        }

        const action = getAxisAction(offset, diff > 0);
        const config = AXIS_CONFIGS[action];
        if (!config) {
          console.warn(`No configuration found for action ${action}`);
          return;
        }

        config.histograms.forEach(histogramType => {
          const service = histogramServices[histogramType];
          if (service) {
            service.changeHistogramSectionByOffset(
              offset,
              histogramType,
              diff > 0,
              Math.abs(diff)
            );
          }
        });
      });

      //console.log(`[${isMaster ? 'Master' : 'Follower'} ${socketId}] Setting new positions:`, {
      //  old: oldPositions,
      //  new: newPositions
      //});
      
      setCurrentAxisPositions(newPositions);
    } catch (error) {
      console.error(`[${isMaster ? 'Master' : 'Follower'} ${socketId}] Error:`, error);
    }
  };

  // Map offset to action type
  const getAxisAction = (offset, isPositive) => {
    switch(offset) {
      case 'xOffset':
        return isPositive ? 'X_UP' : 'X_DOWN';
      case 'yOffset':
        return isPositive ? 'Y_UP' : 'Y_DOWN';
      case 'zOffset':
        return isPositive ? 'Z_UP' : 'Z_DOWN';
      default:
        return null;
    }
  };

  // Check position differences
  const comparePositions = (masterPos, followerPos) => {
    const differences = {};
    let hasDifferences = false;
    
    Object.entries(masterPos).forEach(([axis, value]) => {
      if (value !== followerPos[axis]) {
        differences[axis] = {
          master: value,
          follower: followerPos[axis],
          diff: value - followerPos[axis]
        };
        hasDifferences = true;
      }
    });

    if (hasDifferences) {
      console.log(`Position differences:`, differences);
    }
    
    return { hasDifferences, diffs: differences };
  };

  // Handle axis scaling and sync
  useEffect(() => {
    if (!axisScalingLatest.payload) {
      return;
    }
    
    const { positions: masterPositions, initiator, axis } = axisScalingLatest.payload;

    // Skip own updates if master
    if (isMaster && initiator === socketId) {
      return;
    }

    // Sync with master positions
    if (!isMaster && (shouldSyncWithMaster || axis === 'INITIAL_SYNC' || axis === 'FORCE_SYNC')) {
      try {
        // Calculate position differences
        const differences = {};
        Object.entries(masterPositions).forEach(([offset, targetValue]) => {
          const currentValue = currentAxisPositions[offset];
          const diff = targetValue - currentValue;
          if (diff !== 0) {
            differences[offset] = {
              diff,
              direction: diff > 0
            };
          }
        });

        // Apply changes per axis
        Object.entries(differences).forEach(([offset, { diff, direction }]) => {
          const action = getAxisAction(offset, direction);
          const config = AXIS_CONFIGS[action];
          
          if (config) {
            config.histograms.forEach(histogramType => {
              const service = histogramServices[histogramType];
              if (service) {
                // Step-by-step updates
                for (let i = 0; i < Math.abs(diff); i++) {
                  service.changeHistogramSectionByOffset(
                    offset,
                    histogramType,
                    direction,
                    1
                  );
                }
              }
            });
          }
        });

        // Update positions
        setCurrentAxisPositions(masterPositions);
        setClientPositions(masterPositions);
        
        //console.log('Sync completed:', {
        //  from: currentAxisPositions,
        //  to: masterPositions,
        //  changes: differences
        //});
      } catch (error) {
        console.error('Error during sync:', error);
      }
    }
  }, [axisScalingLatest.payload, socketId, isMaster, currentAxisPositions, shouldSyncWithMaster]);

  return <div></div>
}

export default AdjustHistogramAxisController
