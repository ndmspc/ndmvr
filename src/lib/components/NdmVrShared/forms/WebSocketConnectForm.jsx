import React, { useContext, useEffect, useLayoutEffect, useState } from 'react'
import {
  NdmSpcContext,
  useStreamBrokerOut,
  useStreamBrokerIn
} from '@ndmspc/react-ndmspc-core'

import { Form, TextInput, Button, Flex, FlexItem } from '@patternfly/react-core'

const WebSocketConnectForm = ({ wssb = 'shared' }) => {
  const sb = useContext(NdmSpcContext)[wssb]
  const [url, setUrl] = useState('ws://localhost:8443')
  const [connected, setConnected] = useState(false)
  const [loading, setLoading] = useState(false)

  const [info, setInfo] = useState({})
  const [currentRoom, setCurrentRoom] = useState('')
  const [roomName, setRoomName] = useState('')
  const [error, setError] = useState('')
  const [username, setUsername] = useState('')
  const [roomCreated, setRoomCreated] = useState(false)
  const [showUsernameInput, setShowUsernameInput] = useState(false)
  const [isJoining, setIsJoining] = useState(false)
  const [existingUsers, setExistingUsers] = useState([])

  const _ws = useStreamBrokerOut('_ws', wssb)
  const app = useStreamBrokerIn('app', wssb)
  const joined = useStreamBrokerIn('joined', wssb)
  const left = useStreamBrokerIn('left', wssb)
  const created = useStreamBrokerIn('created', wssb)
  const users = useStreamBrokerIn('users', wssb)

  useEffect(() => {
    if (app.payload !== undefined) {
      setInfo(app.payload)
    }
  }, [app.payload])

  useEffect(() => {
    if (left.payload !== undefined) {
      setCurrentRoom('')
      setError('')
    }
  }, [left.payload])

  useEffect(() => {
    if (joined.payload !== undefined) {
      if (joined.payload.error !== undefined) {
        setError(joined.payload.error)
        return
      }

      setCurrentRoom(joined.payload.roomName)
      setRoomName('')
      setError('')
    }
  }, [joined.payload])

  useEffect(() => {
    if (created.payload !== undefined) {
      if (created.payload.error !== undefined) {
        setError(created.payload.error)
        return
      }

      setCurrentRoom(created.payload.roomName)
      setRoomName('')
      setError('')
    }
  }, [created.payload])

  useEffect(() => {
    if (users.payload !== undefined) {
      setExistingUsers(users.payload)
    }
  }, [users.payload])

  useLayoutEffect(() => {
    if (_ws.type === '_ws') {
      if (_ws.action === 'state') {
        setConnected(_ws.payload.connected)
        setLoading(_ws.payload.loading)
      }
    }
  }, [_ws])

  const handleUrl = (value) => {
    setUrl(value)
  }

  const onSubmitUrl = (event) => {
    event.preventDefault()
    if (sb === null) return
    if (!connected) {
      sb.connect(url)
    } else {
      sb.disconnect()
      setCurrentRoom('')
      setRoomName('')
      setError('')
    }
  }

  const handleRoomName = (value) => {
    setRoomName(value)
  }

  const handleJoin = (roomName) => {
    if (roomName.trim() === '') {
      setError('Room name cannot be empty')
      return
    }

    sb.send({ type: 'joinRoom', payload: roomName })
    setError('')
  }

  const handleCreate = (roomName) => {
    if (roomName.trim() === '') {
      setError('Room name cannot be empty')
      return
    }
    setShowUsernameInput(true)
    setIsJoining(false)
  }

  const handleLeave = (roomName) => {
    sb.send({ type: 'leaveRoom', payload: roomName })
    setCurrentRoom('')
  }

  const handleUsername = (value) => {
    setUsername(value)
  }

  const handleJoinClick = () => {
    if (roomName.trim() === '') {
      setError('Room name cannot be empty')
      return
    }
    setShowUsernameInput(true)
    setIsJoining(true)
  }

  const isUsernameAvailable = (name) => {
    return !existingUsers.some(user => user.username === name)
  }

  const handleConfirmUsername = () => {
    if (username.trim() === '') {
      setError('Username cannot be empty')
      return
    }

    if (!isUsernameAvailable(username)) {
      setError('Username already taken')
      return
    }
    
    console.log('Setting username:', username)
    sb.send({ type: 'setUsername', payload: username })
    
    setTimeout(() => {
      if (isJoining) {
        handleJoin(roomName)
      } else {
        sb.send({ type: 'createRoom', payload: roomName })
      }
    }, 100)
    
    setShowUsernameInput(false)
    setError('')
  }

  return (
    <>
      <Form>
        <Flex>
          <FlexItem>
            <TextInput
              type='url'
              id='url'
              name='url'
              value={url}
              onChange={handleUrl}
              isDisabled={connected ? 'disabled' : null}
            />
          </FlexItem>
          <FlexItem>
            <Button
              type='submit'
              isDisabled={loading ? 'disabled' : null}
              onClick={onSubmitUrl}
            >
              {connected ? 'Disconnect' : loading ? 'Connecting ...' : 'Connect'}
            </Button>
          </FlexItem>

          {connected && currentRoom === '' && !showUsernameInput && (
            <>
              <FlexItem>
                <TextInput
                  type='text'
                  id='roomName'
                  name='roomName'
                  value={roomName}
                  onChange={handleRoomName}
                  placeholder='Enter room name'
                />
              </FlexItem>
              <FlexItem>
                <Button onClick={handleJoinClick}>Join</Button>
              </FlexItem>
              <FlexItem>
                <Button onClick={() => handleCreate(roomName)}>Create</Button>
              </FlexItem>
            </>
          )}

          {showUsernameInput && currentRoom === '' && (
            <>
              <FlexItem>
                <TextInput
                  type='text'
                  id='username'
                  name='username'
                  value={username}
                  onChange={handleUsername}
                  placeholder='Enter username'
                  isRequired
                />
              </FlexItem>
              <FlexItem>
                <Button 
                  onClick={handleConfirmUsername}
                  isDisabled={!username.trim() || !isUsernameAvailable(username)}
                >
                  Confirm and {isJoining ? 'Join' : 'Create'} Room
                </Button>
              </FlexItem>
            </>
          )}

          {currentRoom !== '' && (
            <FlexItem>
              <Button
                isDisabled={!connected ? 'disabled' : null}
                onClick={() => handleLeave(currentRoom)}
              >
                Leave
              </Button>
            </FlexItem>
          )}

          <FlexItem>
            {info.name && info.version ? (
              <div className={'pf-u-default-color-100'}>
                {info.name + ' ' + info.version}
              </div>
            ) : (
              <></>
            )}
          </FlexItem>

          <FlexItem>
            {currentRoom !== '' && (
              <div className={'pf-u-default-color-200'}>
                Current room: {currentRoom} | User: {username || 'User'}
              </div>
            )}
          </FlexItem>

          <FlexItem>
            {error !== '' && (
              <div className={'pf-u-warning-color-100'}>{error}</div>
            )}
          </FlexItem>
        </Flex>
      </Form>
    </>
  )
}

export default WebSocketConnectForm
