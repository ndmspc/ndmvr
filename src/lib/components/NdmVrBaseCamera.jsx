import React, { useEffect, useLayoutEffect, useState } from 'react'
import 'aframe'
import NdmVrCameraHUD from './NdmVrCameraHUD'
import NdmVrHelp from './NdmVrHelp.jsx'
import NdmVrOculusControls from './NdmVrOculusControls.jsx'
import {isObjectEmpty, useNdmVrLocal, useNdmVrRedirect} from '@ndmspc/react-ndmspc-core'
import { CameraService } from '../services/cameraService.jsx'
import { StoreContext } from './contexts/StoreContext.jsx'
import NDMVRMenu from './Menu/Menu.jsx'


const NdmVrBaseCamera = () => {
  const cameraService = new CameraService()
  const [ndmvrCameraPosRot, setNdmvrCameraPosRot] =
    useNdmVrLocal('ndmvrCameraPosRot')
  const [ndmvrInputDevice, ] =
     useNdmVrRedirect('ndmvrInputDevice')
  const [context] = useState(StoreContext._currentValue)
  const [posRot, setPosRot] = useState(null)

  /**
   * If there is saved position of camera it is set,
   * if not it's calculated based on size of histogram
   * */
  useEffect(() => {
    if (!ndmvrCameraPosRot) return
    setPosRot(
      cameraService.parsePosition(
        cameraService.calculateDefaultPosRot(ndmvrCameraPosRot, context)
      )
    )

    const setCameraRotation = () => {
      const camera = document.getElementById('camera')
      if (camera && camera.components['look-controls']) {
        cameraService.setCameraRotation(ndmvrCameraPosRot?.rotation)
      } else {
        setTimeout(setCameraRotation, 100)
      }
    }
    setCameraRotation()
  }, [ndmvrCameraPosRot])

  useEffect(() => {
    if (isObjectEmpty(ndmvrCameraPosRot)){
      setNdmvrCameraPosRot.setData(cameraService.getCurrentPosRot())
    }

  },[posRot])


  /**
   * Before user leaves scene position and rotation is saved to local storage
   * */
  useLayoutEffect(() => {
    return () => {
      setNdmvrCameraPosRot.setData(cameraService.getCurrentPosRot())
    }
  }, [])

  return (
    <a-entity id="cameraRig" cursor="rayOrigin: mouse; fuse: false" position={posRot?.position} raycaster="objects: .clickable, [gui-interactable], .collidable;far:5500">
      <NdmVrHelp />
      <a-camera
        id="camera"
        wasd-controls-custom="">
        <NdmVrCameraHUD />
      </a-camera>
      <NdmVrOculusControls />
      <NDMVRMenu/>
    </a-entity>
  )
}
export default NdmVrBaseCamera
