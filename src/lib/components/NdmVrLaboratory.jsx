import React from 'react'
import '../aframe/aframe-environment-component'
import { StoreContext } from './contexts/StoreContext'
export default function NdmVrLaboratory(props) {
  const preset = props?.scene || 'labs'; // kontrolovať, či existuje props a vlastnosť scene
  return (
    <React.Fragment>
        <a-entity /*scale="3 3 3"*/ environment={`preset: ${preset};`}></a-entity>
    </React.Fragment>
  )
}
/*
<React.Fragment>
      <a-entity environment={`preset: ${preset}; groundTexture: none; horizonColor: silver; skyColor: skyblue; shadow: true`}></a-entity>
    </React.Fragment>
    horizonColor: silver; skyColor: skyblue;
*/
/*
<a-sky color='#dbf0fe'></a-sky>
      <a-plane
        rotation='-90 0 0'
        position='0 -0.5 0'
        width='1000'
        height='1000'
        color='#aaa'
        material='shader:flat;'
      ></a-plane>
*/

//---MAPS---
//<a-entity environment="preset: labs; groundTexture: none; horizonColor: silver; skyColor: skyblue; shadow: true"></a-entity>
    //"default",
    //"contact",
    //"egypt",
    //"checkerboard",
    //"forest",
    //"goaland",
    //"yavapai",
    //"goldmine",
    //"arches",
    //"threetowers",
    //"poison",
    //"tron",
    //"japan",
    //"dream",
    //"volcano",
    //"starry",
    //"osiris",

//<a-sky /*color='#dbf0fe'*/ src = "./assets/ndmvr/backgrounds/room.jpeg"></a-sky>
    /*
    <a-plane
    rotation='-90 0 0'
    position='0 -0.5 0' //0 -0.5 0
    width='1000'
    height='1000'
    //color='#aaa'
    //material='shader:flat;'
    material="opacity: 0.0; shader:flat; transparent: true"
    ></a-plane>
    <a-entity environment="preset: forest; groundColor: #f0f0f0; groundTexture: none; horizonColor: silver; skyColor: skyblue; dressingColor: #fcfcfc; shadow: true"></a-entity>
    <a-entity environment="preset: forest; groundTexture: none; horizonColor: silver; skyColor: skyblue; shadow: true"></a-entity>
    */
