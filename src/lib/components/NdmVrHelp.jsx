import React, { useContext, useEffect, useState } from 'react'
import 'aframe'
import cameraSubject from '../observables/cameraSubject'
import { tabletComunicator } from './DesktopTablet'
import { StoreContext } from './contexts/StoreContext'
import { useNdmVrRedirect } from '@ndmspc/react-ndmspc-core'
import keyboard_asset from '../assets/ndmvr/keyboardControls/keyboard.png'
import keyboard_asset1 from '../assets/ndmvr/keyboardControls/keyboard1.png';
import keyboard_asset2 from '../assets/ndmvr/keyboardControls/keyboard2.png';
import keyboard_asset3 from '../assets/ndmvr/keyboardControls/keyboard3.png';
import oculus_asset from '../assets/ndmvr/oculusControls/oculus.png';
import oculus_asset1 from '../assets/ndmvr/oculusControls/oculus1.png';
import oculus_asset2 from '../assets/ndmvr/oculusControls/oculus2.png';
import oculus_asset3 from '../assets/ndmvr/oculusControls/oculus3.png';

const AFRAME = window.AFRAME
/**
 * React komponent pre vytvorenie kamery a jej menežment.
 * @component
 * @example
 * projPanelIds: [],
 * return (
 *  <NdmVrHelp />
 * )
 */

const NdmVrHelp = () => {
    let subscription
    const offset = 30
    const { projPanelIds } = useContext(StoreContext).data
    const [rotation, setRotation] = useState(0)
    const [prevPosition, setPrevPosition] = useState(null)
    const [ndmvrInputDevice, setNdmvrInputDevice] =
        useNdmVrRedirect('ndmvrInputDevice')
    const [show, setShow] = useState(false)
    const [showProjection, setShowProjection] = useState(false)
    const [showHint, setShowHint] = useState(true)

    const handleSubscription = (data) => {
        if (data.device) {
            if (ndmvrInputDevice !== data.device) {
                setNdmvrInputDevice.setData(data.device)
            }
            setShow(!show)
        } else if (data === 'shift') {
            compileRotation()
        } else if (data === 'show') {
            setShowProjection(!showProjection)
        }
    }

    const handleTabletSubscription = (data) => {
        if (data.changeTabletState) {
            setShowHint(false)
        }
    }

    const compileRotation = () => {
        let currentRotation = rotation + 90
        if (currentRotation >= 360) currentRotation = 0
        setRotation(currentRotation)
    }

    const getPanelId = (index) => {
        return projPanelIds[index] ? projPanelIds[index] : `panel_${index}`
    }

    // render banners for projection
    const renderProjectionsBanners = () => {
        return (
            <a-entity
                position={
                    prevPosition === null
                        ? `0 ${offset} 0`
                        : `${prevPosition.x} ${offset} ${prevPosition.z}`
                }
                animation__rotation={`property: rotation; to: 0 ${rotation} 0; dur: 1000; easing: linear;`}
                material="color: white"
            >
                <a-entity
                    // geometry='primitive: box; width: 18; height: 5; depth: 0.5;'
                    /* geometry={
            ndmvrInputDevice === 'oculus'
              ? 'primitive: box; width: 40; height: 10; depth: 0.1;'
              : 'primitive: box; width: 18; height: 5; depth: 0.1;'
          } */
                    geometry="primitive: box; width: 50; height: 10; depth: 0.1;"
                    // position={`8 ${ndmvrInputDevice === 'oculus' ? 3 - offset : 3} -5`}
                    // position={ndmvrInputDevice === 'oculus' ? '-30 28 0' : '8 3 -5'}
                    position="-30 28 0"
                    // rotation='0 -45 0'
                    // rotation={ndmvrInputDevice === 'oculus' ? '45 90 0' : '0 -45 0'}
                    rotation="45 90 0"
                    material="color: white"
                >
                    <a-text
                        color="black"
                        // width='20'
                        // width={ndmvrInputDevice === 'oculus' ? '50' : '20'}
                        width={50}
                        value={
                            ndmvrInputDevice === 'keyboard'
                                ? 'Press C to show help panels'
                                : 'Press Button X to show help panels'
                        }
                        // position={ndmvrInputDevice === 'oculus' ? '-18 0 1' : '-8 0 1'}
                        position="-20 2.8 1"
                    />
                    <a-text
                        color="black"
                        // width='20'
                        // width={ndmvrInputDevice === 'oculus' ? '50' : '20'}
                        width={50}
                        value={
                            ndmvrInputDevice === 'keyboard'
                                ? 'Press V to switch to histogram view'
                                : 'Press Button Y to switch to histogram view'
                        }
                        // position='-8 1 1'
                        // position={ndmvrInputDevice === 'oculus' ? '-18 2.8 1' : '-8 1 1'}
                        position="-20 0 1"
                    />
                    <a-text
                        color="black"
                        // width='20'
                        // width={ndmvrInputDevice === 'oculus' ? '50' : '20'}
                        width={50}
                        value={
                            ndmvrInputDevice === 'keyboard'
                                ? 'Press X to shift panels'
                                : 'Press right Trigger to shift panels'
                        }
                        // position='-8 -1 1'
                        // position={ndmvrInputDevice === 'oculus' ? '-18 -2.8 1' : '-8 -1 1'}
                        position="-20 -2.8 1"
                    />
                </a-entity>
                <a-box
                    id={getPanelId(1)}
                    scale="80 50 2"
                    animation__position="property: position; from: 0 0 -60; to: 0 -1 -42; delay: 0; dur: 800; easing: linear;"
                    material="transparent:true"
                />
                <a-box
                    scale="80 52 2"
                    animation__position="property: position; from: 0 0 -60; to: 0 0 -42.5; delay: 0; dur: 800; easing: linear;"
                    material="color: white;"
                />
                <a-box
                    id={getPanelId(2)}
                    scale="80 50 2"
                    animation__position="property: position; from: 60 0 0; to: 42 -1 0; delay: 200; dur: 800; easing: linear;"
                    rotation="0 90 0"
                    material="transparent: true"
                />
                <a-box
                    scale="80 52 2"
                    animation__position="property: position; from: 60 0 0; to: 42.5 0 0; delay: 200; dur: 800; easing: linear;"
                    rotation="0 90 0"
                    material="color: white;"
                />
                <a-box
                    id={getPanelId(3)}
                    scale="80 50 2"
                    animation__position="property: position; from: 0 0 60; to: 0 -1 42; delay: 400; dur: 800; easing: linear;"
                    material="transparent: true"
                />
                <a-box
                    scale="80 52 2"
                    animation__position="property: position; from: 0 0 60; to: 0 0 42.5; delay: 400; dur: 800; easing: linear;"
                    material="color: white;"
                />
                <a-box
                    id={getPanelId(4)}
                    scale="80 50 2"
                    animation__position="property: position; from: -60 0 0; to: -42 -1 0; delay: 600; dur: 800; easing: linear;"
                    rotation="0 90 0"
                    material="transparent: true"
                />
                <a-box
                    scale="80 52 2"
                    animation__position="property: position; from: -60 0 0; to: -42.5 0 0; delay: 600; dur: 800; easing: linear;"
                    rotation="0 90 0"
                    material="color: white;"
                />
            </a-entity>
        )
    }

    // render banners for controls
    const renderBannersWithControls = () => {
        if (ndmvrInputDevice === 'keyboard') {
            return (
                <a-entity
                    position={
                        prevPosition === null
                            ? `0 ${offset} 0`
                            : `${prevPosition.x} ${offset} ${prevPosition.z}`
                    }
                    animation__rotation={`property: rotation; to: 0 ${rotation} 0; dur: 1000; easing: linear;`}
                    material="color: white"
                >
                    <a-entity
                        // geometry='primitive: box; width: 15; height: 5; depth: 0.5;'
                        // position='8 3 -5'
                        // rotation='0 -45 0'
                        geometry="primitive: box; width: 50; height: 10; depth: 0.1;"
                        position="-30 28 0"
                        rotation="45 90 0"
                        material="color: white"
                    >
                        <a-text
                            color="black"
                            // width='20'
                            width="50"
                            // value='Press C to show projection panels'
                            value={
                                showProjection
                                    ? 'Press C to show help panels'
                                    : 'Press C to show projection panels'
                            }
                            // position='-6 0 1'
                            position="-20 2.8 1"
                        />
                        <a-text
                            color="black"
                            // width='20'
                            width="50"
                            value="Press V to switch to histogram view"
                            // position='-6 1 1'
                            position="-20 0 1"
                        />
                        <a-text
                            color="black"
                            // width='20'
                            width="50"
                            value="Press X to shift panels"
                            // position='-6 -1 1'
                            position="-20 -2.8 1"
                        />
                    </a-entity>
                    <a-box
                        scale="80 50 2"
                        animation__position="property: position; from: -30 0 60; to: -30 0 30; delay: 0; dur: 800; easing: linear;"
                        rotation="0 -45 0"
                        // src='https://i.imgur.com/x78XYHM.png'
                        src={keyboard_asset}
                    />
                    <a-box
                        scale="80 50 2"
                        animation__position="property: position; from: -30 0 -60; to: -30 0 -30; delay: 200; dur: 800; easing: linear;"
                        rotation="0 45 0"
                        src={keyboard_asset1}
                    />
                    <a-box
                        scale="80 50 2"
                        animation__position="property: position; from: 30 0 -60; to: 30 0 -30; delay: 400; dur: 800; easing: linear;"
                        rotation="0 -45 0"
                        src={keyboard_asset2}
                    />
                    <a-box
                        scale="80 50 2"
                        animation__position="property: position; from: 30 0 60; to: 30 0 30; delay: 600; dur: 800; easing: linear;"
                        rotation=" 0 45 0"
                        src={keyboard_asset3}
                    />
                </a-entity>
            )
        } else {
            return (
                <a-entity
                    position={
                        prevPosition === null
                            ? `0 ${offset} 0`
                            : `${prevPosition.x} ${offset} ${prevPosition.z}`
                    }
                    animation__rotation={`property: rotation; to: 0 ${rotation} 0; dur: 1000; easing: linear;`}
                >
                    <a-entity
                        geometry="primitive: box; width: 50; height: 10; depth: 0.1;"
                        position="-30 28 0"
                        rotation="45 90 0"
                        material="color: white"
                    >
                        <a-text
                            color="black"
                            width="50"
                            // value='Press Button X to show projection panels'
                            value={
                                showProjection
                                    ? 'Press Button X to show help panels'
                                    : 'Press Button X to show projection panels'
                            }
                            position="-20 2.8 1"
                        />
                        <a-text
                            color="black"
                            width="50"
                            value="Press Button Y to switch to histogram view"
                            position="-20 0 1"
                        />
                        <a-text
                            color="black"
                            width="50"
                            value="Press right Trigger to shift panels"
                            position="-20 -2.8 1"
                        />
                    </a-entity>
                    <a-box
                        scale="80 50 2"
                        animation__position="property: position; from: -35 0 60; to: -32 0 32; delay: 0; dur: 800; easing: linear;"
                        rotation="-10 -45 0"
                        src={oculus_asset}
                    />
                    <a-box
                        scale="80 50 2"
                        animation__position="property: position; from: -35 0 -60; to: -32 0 -32; delay: 200; dur: 800; easing: linear;"
                        rotation="10 45 0"
                        src={oculus_asset1}
                    />
                    <a-box
                        scale="80 50 2"
                        animation__position="property: position; from: 35 0 -60; to: 32 0 -32; delay: 400; dur: 800; easing: linear;"
                        rotation="10 -45 0"
                        src={oculus_asset2}
                    />
                    <a-box
                        scale="80 50 2"
                        animation__position="property: position; from: 35 0 60; to: 32 0 32; delay: 600; dur: 800; easing: linear;"
                        rotation="-10 45 0"
                        src={oculus_asset3}
                    />
                </a-entity>
            )
        }
    }

    const hideMappingBanner = () => {
        const banner = document.getElementById(getPanelId(0))
        if (banner) {
            banner.setAttribute('material', 'opacity', 0)
        }
    }

    const showMappingBanner = () => {
        const banner = document.getElementById(getPanelId(0))
        if (banner) {
            banner.setAttribute('material', 'opacity', 1)
        }
    }

    useEffect(() => {
        subscription = tabletComunicator.subscribe(handleTabletSubscription)
        return () => {
            subscription.unsubscribe()
        }
    }, [])

    useEffect(() => {
        subscription = cameraSubject
            .getCameraSubject()
            .subscribe(handleSubscription)

        return () => subscription.unsubscribe()
    }, [show, showProjection, rotation])

    useEffect(() => {
        const camera = document.getElementById('camera')
        if (!show) {
            camera.object3D.position.y = 1.6
            if (prevPosition !== null) {
                camera.object3D.position.x = prevPosition.x
                camera.object3D.position.z = prevPosition.z
                camera.object3D.position.y = prevPosition.y
            }
            showMappingBanner()
        } else {
            setPrevPosition({
                x: camera.object3D.position.x,
                y: camera.object3D.position.y,
                z: camera.object3D.position.z
            })
            camera.object3D.position.y = camera.object3D.position.y + offset
            hideMappingBanner()
        }
    }, [show, ndmvrInputDevice])

    return (
        <a-entity>
            {show && !showProjection && renderBannersWithControls()}
            {show && showProjection && renderProjectionsBanners()}
        </a-entity>
    )
}

export default NdmVrHelp
