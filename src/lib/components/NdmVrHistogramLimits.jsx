import React, {useContext, useEffect, useState} from 'react'
import 'aframe'
import '../aframe/cursor'
import '../aframe/th'
import '../aframe/histogram'
import '../aframe/labelHandler'
import {StoreContext} from './contexts/StoreContext'

/**
 * React component that shows limits of histogram in format of borders.
 * Consists of rectangle at bottom,
 * if histogram _typename is TH3 ONLY THEN whole cage(+ upper rectangle, edges) around histogram is displayed.
 * Component takes starting position as React prop
 * Example of start position (prop) passed into component: "<NdmVrHistogramLimits startPosition={{x:0, y:0, z:2}}/>"*/
const NdmVrHistogramLimits = ({startPosition}) => {
  const context = useContext(StoreContext);
  const binScaleCoefficient = 2;
  let binWidth = 1;
  const material = "color: white;"
  const opacity = "1.0"
  const startPos = startPosition || {
    x: 0,
    y: 0,
    z: 0
  }
  //    context.states.section neviem preco je undefined

  let [dimensions, setDimensions] = useState(calculateDimensions())
  useEffect(() => {
    setDimensions(calculateDimensions)
  }, [context])

  return (
    <a-entity>
      {/*min X bottom axis*/}
      <a-box
        position={`${startPos.x + dimensions.xCenter} ${startPos.z} ${startPos.y - dimensions.yStart}`}
        width={`${dimensions.xWidth * binScaleCoefficient}`}
        height="0.1"
        depth="0.25"
        material={material}
        opacity={opacity}
      ></a-box>
      {/*min Y bottom axis*/}
      <a-box
        position={`${startPos.x + dimensions.xStart} ${startPos.z} ${startPos.y - dimensions.yCenter}`}
        width={`${dimensions.yWidth * binScaleCoefficient}`}
        height="0.1"
        depth="0.25"
        rotation="0 90 0"
        material={material}
        opacity={opacity}
      ></a-box>
      {/*max Y bottom axis*/}
      <a-box
        position={`${startPos.x + dimensions.xCenter} ${startPos.z} ${startPos.y - dimensions.yCenter + dimensions.yWidth}`}
        width={`${dimensions.xWidth * binScaleCoefficient}`}
        height="0.1"
        depth="0.25"
        material={material}
        opacity={opacity}
      ></a-box>
      {/*max X bottom axis*/}
      <a-box
        position={`${startPos.x + dimensions.xCenter - dimensions.xWidth} ${startPos.z} ${startPos.y - dimensions.yCenter}`}
        width={`${dimensions.yWidth * binScaleCoefficient}`}
        height="0.1"
        depth="0.25"
        rotation="0 90 0"
        material={material}
        opacity={opacity}
      ></a-box>

      {context.data.histogram._typename.includes("TH3") &&
        <a-entity>
          {/* min X top axis */}
          <a-box
            position={`${startPos.x + dimensions.xCenter} ${startPos.z - dimensions.zStart + dimensions.zWidth * 2} ${startPos.y - dimensions.yStart}`}
            width={`${dimensions.xWidth * binScaleCoefficient}`}
            height="0.1"
            depth="0.25"
            material={material}
            opacity={opacity}
          ></a-box>
          {/* min Y top axis */}
          <a-box
            position={`${startPos.x + dimensions.xStart} ${startPos.z - dimensions.zStart + dimensions.zWidth * 2} ${startPos.y - dimensions.yCenter}`}
            width={`${dimensions.yWidth * binScaleCoefficient}`}
            height="0.1"
            depth="0.25"
            rotation="0 90 0"
            material={material}
            opacity={opacity}
          ></a-box>
          {/*max Y top axis*/}
          <a-box
            position={`${startPos.x + dimensions.xCenter} ${startPos.z - dimensions.zStart + dimensions.zWidth * 2} ${startPos.y - dimensions.yCenter + dimensions.yWidth}`}
            width={`${dimensions.xWidth * binScaleCoefficient}`}
            height="0.1"
            depth="0.25"
            material={material}
            opacity={opacity}
          ></a-box>
          {/*max X top axis*/}
          <a-box
            position={`${startPos.x + dimensions.xCenter - dimensions.xWidth} ${startPos.z - dimensions.zStart + dimensions.zWidth * 2} ${startPos.y - dimensions.yCenter}`}
            width={`${dimensions.yWidth * binScaleCoefficient}`}
            height="0.1"
            depth="0.25"
            rotation="0 90 0"
            material={material}
            opacity={opacity}
          ></a-box>
          {/*min X min Y edge*/}
          <a-box
            position={`${startPos.x + dimensions.xStart} ${startPos.z - dimensions.zCenter} ${startPos.y - dimensions.yStart}`}
            width={`${dimensions.zWidth * binScaleCoefficient}`}
            height="0.1"
            depth="0.25"
            rotation="0 0 90"
            material={material}
            opacity={opacity}
          ></a-box>
          {/*min X max Y edge*/}
          <a-box
            position={`${startPos.x + dimensions.xStart} ${startPos.z - dimensions.zCenter} ${startPos.y - dimensions.yStart + dimensions.yWidth * 2}`}
            width={`${dimensions.zWidth * binScaleCoefficient}`}
            height="0.1"
            depth="0.25"
            rotation="0 0 90"
            material={material}
            opacity={opacity}
          ></a-box>
          {/*max X max Y edge*/}
          <a-box
            position={`${startPos.x + dimensions.xStart - dimensions.xWidth * 2} ${startPos.z - dimensions.zCenter} ${startPos.y - dimensions.yStart + dimensions.yWidth * 2}`}
            width={`${dimensions.zWidth * binScaleCoefficient}`}
            height="0.1"
            depth="0.25"
            rotation="0 0 90"
            material={material}
            opacity={opacity}
          ></a-box>
          {/*max X min Y edge*/}
          <a-box
            position={`${startPos.x + dimensions.xStart - dimensions.xWidth * 2} ${startPos.z - dimensions.zCenter} ${startPos.y - dimensions.yStart}`}
            width={`${dimensions.zWidth * binScaleCoefficient}`}
            height="0.1"
            depth="0.25"
            rotation="0 0 90"
            material={material}
            opacity={opacity}
          ></a-box>
        </a-entity>
      }
    </a-entity>
  );

  function calculateDimensions() {
    const xStart = (binWidth * 3) / 4
    // *(-2 * binWidth) => histogram goes to negative position, space between bins is equal to 1 bin
    // ((xWidth * 3) / 4) => 1/2 width to edge of bin, 1/4 width of limit component
    const xEnd = ((context.data.histogram.fXaxis.fNbins - 1) * binWidth) * (-2 * binWidth) - (binWidth * 3) / 4
    const xWidth = (xStart - xEnd) / 2
    const xCenter = xStart - xWidth

    // 1/2 width to edge of bin, 1/4 width of limit component
    const yStart = (binWidth * 3) / 4
    // *(-2) => histogram goes to negative position, space between bins is equal to 1 bin
    // ((xWidth * 3) / 4) => 1/2 width to edge of bin, 1/4 width of limit component
    const yEnd = ((context.data.histogram.fYaxis.fNbins - 1) * binWidth) * (-2 * binWidth) - (binWidth * 3) / 4
    const yWidth = (yStart - yEnd) / 2
    const yCenter = yStart - yWidth

    // 1/2 width to edge of bin, 1/4 width of limit component
    const zStart = 0
    // *(-2) => histogram goes to negative position, space between bins is equal to 1 bin
    // ((xWidth * 3) / 4) => 1/2 width to edge of bin, 1/4 width of limit component
    const zEnd = ((context.data.histogram.fZaxis.fNbins - 1) * binWidth) * (-2 * binWidth) - (binWidth * 3) / 4
    const zWidth = (zStart - zEnd) / 2
    const zCenter = zStart - zWidth
    return {
      xStart: xStart,
      xWidth: xWidth,
      xCenter: xCenter,
      yStart: yStart,
      yWidth: yWidth,
      yCenter: yCenter,
      zStart: zStart,
      zWidth: zWidth,
      zCenter: zCenter
    }
  }
};

export default NdmVrHistogramLimits;
