import React from "react";
import "aframe";
import { TabletContext } from "../TabletProvider";
import { dragElement } from "../utils/dragElem";

/**
 * Renders SideBar on tablet
 * @component
 * @param {Object} props props for SideBar object
 * @param {boolean} props.keyboard if true render tablet for desktop, else for VR.
 * @param {number} props.offset
 * @param {React.JSX.Element[]} props.children children of SideBar component.
 */
const SideBar = (props) => {
  const { screenWidth, screenHeight, controller, sideBarWidth } =
    React.useContext(TabletContext);

  React.useEffect(() => {
    if (document.getElementById("desktop--newtablet--container"))
      dragElement(document.getElementById("desktop--newtablet--container"));
  }, []);

  //#region VR
  const renderVR = () => {
    return (
      <a-plane
        color="#fff8a7"
        width={sideBarWidth - 0.004}
        height={screenHeight - 0.004}
        position={
          -screenWidth / 2 +
          sideBarWidth / 2 +
          sideBarWidth * (props.offset || 0) +
          " 0 0.002"
        }
        rotation="0 0 0"
      >
        {props.children}
      </a-plane>
    );
  };
  //#endregion

  //region DESKTOP
  const renderKeyboard = () => {
    return (
      <div
        id="desktop--newtablet--container--header"
        style={{
          height: "100%",
          overflowY: "auto",
          cursor: "grab",
          width: "20%",
          border: "2px outset ňfdsa",
          backgroundColor: "white",
          borderRadius: "4px",
        }}
      >
        {props.children}
      </div>
    );
  };
  //#endregion

  return <>{controller === "keyboard" ? renderKeyboard() : renderVR()}</>;
};

export default SideBar;
