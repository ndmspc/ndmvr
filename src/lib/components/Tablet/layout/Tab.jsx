import React from "react";
import "aframe";
import { TabletContext } from "../TabletProvider";
import "../../../aframe/interaction";
import styled from "styled-components";

const Button = styled.div`
  background: yellow;
  cursor: pointer;
  border: 2px outset;

  &:hover {
    background: #e0a0b3;
  }
`;
/**
 * Renders Tab on tablet
 * @component
 * @param {Object} props props for Tab object
 * @param {string} props.label
 * @param {boolean} props.checked
 * @param {(label:string)=>void} props.onClick click with label in argument
 * @param {number} props.height height of the tab
 * @param {number} props.verPosition position in row as offset in VR units
 */

const Tab = (props) => {
  const providerData = React.useContext(TabletContext);

  const handleTabClick = () => {
    props.onClick(props.label);
  };

  //#region VR
  const renderVR = () => {
    return (
      <a-plane
        interaction
        class="clickable"
        color="blue"
        onClick={() => handleTabClick()}
        height={props.height}
        width={providerData.sideBarWidth - 0.005}
        position={"0.001 " + props.verPosition + " 0.002"}
        rotation="0 0 0"
      >
        <a-text
          position={"-0.030 0 0"}
          scale={"2.2 2.2 2.2"}
          height={props.height}
          width={providerData.sideBarWidth}
          value={props.label}
        />
      </a-plane>
    );
  };
  //#endregion

  //region DESKTOP
  const renderKeyboard = () => {
    return (
      <Button
        style={{ backgroundColor: props.checked ? "orange" : "yellow" }}
        onClick={() => handleTabClick()}
      >
        {props.label}
      </Button>
    );
  };
  //#endregion

  return (
    <>
      {providerData.controller === "keyboard" ? renderKeyboard() : renderVR()}
    </>
  );
};

export default Tab;
