import React from "react";
import "aframe";
import { TabletContext } from "../TabletProvider";

/**
     * Renders ComponentArea
     on tablet
     * @component
     * @param {Object} props props for ComponentArea
     object
     */
const ComponentArea = (props) => {
  const providerData = React.useContext(TabletContext);
  //#region VR
  const renderVR = () => {
    return (
      <a-plane
        color="gray"
        width={providerData.componentAreaWidth}
        height={providerData.screenHeight - 0.002}
        position="0.08 0 0.001"
        rotation="0 0 0"
      >
        {props.children}
      </a-plane>
    );
  };
  //#endregion

  //region DESKTOP
  const renderKeyboard = () => {
    return <div>{props.children}</div>;
  };
  //#endregion

  return (
    <>{providerData.controller == "keyboard" ? renderKeyboard() : renderVR()}</>
  );
};

export default ComponentArea;
