import React from "react";
import "aframe";
import "../../../aframe/interaction";
import { TabletContext } from "../TabletProvider";

/**
 * Renders Tablet screen depending on control
 * @component
 * @param {Object} props props for Screen object
 * @param {number} props.width defines width of the screen.
 * @param {number} props.height defines height of the screen.
 * @param {Array<React.JSX.Element[]} props.children children of Screen component.
 */

export const Screen = (props) => {
  const { screenWidth, screenHeight, controller } =
    React.useContext(TabletContext);
  //#region VR
  const renderVR = () => {
    return (
      <a-plane
        color="white"
        width={`${screenWidth}`}
        height={`${screenHeight}`}
        position="0.2 0 -0.1"
        rotation="-70 0 0"
      >
        {props.children}
      </a-plane>
    );
  };
  //#endregion

  //region DESKTOP
  const renderKeyboard = () => {
    return (
      <div
        id="desktop--newtablet--container"
        style={{
          border: "4px solid gray",
          borderRadius: "8px",
          position: "absolute",
          zIndex: 1000,
          bottom: 20,
          right: 20,
          overflow: "hidden",
          display: "flex",
          flexDirection: "row",
          width: "30vw",
          height: "40vh",
          resize: "both",
          backgroundColor: "lightgray",
        }}
      >
        {props.children}
      </div>
    );
  };
  //#endregion

  return <>{controller === "keyboard" ? renderKeyboard() : renderVR()}</>;
};
