import React, {useEffect, useState} from 'react'
import 'aframe'
import { Screen } from './layout/Screen'
import SideBar from './layout/SideBar'
import Tab from './layout/Tab'
import { ComponentProvider } from './components/ComponentProvider'
import ComponentArea from './layout/ComponentArea'
import { useNdmVrRedirect } from '@ndmspc/react-ndmspc-core'
import schemaSubject from "../../observables/schemaSubject.jsx";

export const TabletContext = React.createContext({
  screenWidth: 0.44,
  screenHeight: 0.25,
  componentAreaWidth: 0.28,
  sideBarWidth: 0.08,
  controller: 'keyboard'
})
/**
 * Table Provider needs to be placed for every controler separately
 * @component
 * @param {Object} props
 * @param {boolean} props.controller specify controller type
 * @param {React.JSX.Element} props.componentProvider
 *
 */
const TabletProvider = (props) => {
  const [visible, setVisible] = React.useState(
    !(props.controller === 'keyboard')
  )

  const [selScheme, setSelScheme] = React.useState('histogram')
  const [property, setProperty] = React.useState('scale')
  const [schema, setNdmvrSchema] = useState(null);

  const handleSelect = (value) => {
    // if (!schema) return
    // if (!schema[selScheme]) return
    // if (!schema[selScheme]?.properties) return
    if (!schema[selScheme]?.properties[property]) return
    schemaSubject.next({
      ...schema,
      [selScheme]: {
        ...schema[selScheme],
        properties: {
          ...schema[selScheme].properties,
          [property]: {
            ...schema[selScheme].properties[property],
            value: value
          }
        }
      }
    })
  }

  useEffect(() => {
    const handleTablet = (event) => {
      console.log(event)
      if (event.key === 't' || event.key === 'T') {
        setVisible((current) => !current)
      }
    }
    document.addEventListener('keydown', handleTablet);

    const schemaSubjectRef = schemaSubject
       .getSchemaSubject()
       .subscribe(object => {
         setNdmvrSchema(object);
       })

    return () => {
      document.removeEventListener('keydown', handleTablet);
      schemaSubjectRef.unsubscribe();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <TabletContext.Provider
      value={{
        screenWidth: 0.44,
        screenHeight: 0.25,
        componentAreaWidth: 0.28,
        sideBarWidth: 0.08,
        controller: props.controller || 'keyboard'
      }}
    >
      {visible && schema && (
        <Screen>
          <SideBar>
            {schema &&
              Object.keys(schema).map((key, i) => (
                <Tab
                  key={i}
                  i={i + '1'}
                  label={key}
                  height={0.02}
                  checked={key === selScheme ? true : false}
                  verPosition={0.11 - i * 0.022}
                  onClick={(arg) => {
                    setSelScheme(arg)
                    if (Object.keys(schema[arg].properties).length > 0) {
                      setProperty(Object.keys(schema[arg].properties)[0])
                    }
                  }}
                />
              ))}
          </SideBar>
          {selScheme && (
            <SideBar offset={1}>
              {Object.keys(schema[selScheme].properties).map((key, i) => (
                <Tab
                  key={i}
                  i={i}
                  label={key}
                  height={0.02}
                  checked={key === property ? true : false}
                  verPosition={0.11 - i * 0.022}
                  onClick={(arg) => {
                    setProperty(arg)
                  }}
                />
              ))}
            </SideBar>
          )}
          {selScheme && property && (
            <ComponentArea>
              {props.componentProvider
                ? props.componentProvider(
                    schema[selScheme].properties[property].type,
                    props.keyboard,
                    schema[selScheme].properties[property],
                    handleSelect,
                    ComponentProvider
                  )
                : ComponentProvider(
                    schema[selScheme].properties[property].type,
                    schema[selScheme].properties[property],
                    handleSelect
                  )}
            </ComponentArea>
          )}
        </Screen>
      )}
    </TabletContext.Provider>
  )
}

export default TabletProvider
