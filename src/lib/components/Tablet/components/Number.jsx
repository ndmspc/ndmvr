import React from "react";
import "aframe";
import { TabletContext } from "../TabletProvider";
import "../../../aframe/interaction";
import styled from "styled-components";

const ArrowButton = styled.div`
  padding: 10px;
  border: 2px solid gray;
  border-radius: 6px;
  &:hover {
    background-color: lightblue;
  }
`;

/**
 * Renders Number on tablet
 * @component
 * @param {Object} props for Number object
 * @param {(value:number)=>void} props.onSelect
 * @param {number} props.step
 */
const Number = (props) => {
  const providerData = React.useContext(TabletContext);

  const handleIncrease = () => {
    props.onSelect(props.value + props.step || 1);
  };
  const handleDecrease = () => {
    props.onSelect(props.value - props.step || 1);
  };
  //#region VR
  const renderVR = () => {
    return (
      <a-plane
        color="white"
        width="0.1"
        height="0.1"
        position="0 0 0.002"
        rotation="0 0 0"
      >
        <a-plane
          width="0.03"
          color="black"
          interaction
          class="clickable"
          onClick={() => handleDecrease()}
          height="0.03"
          position="-0.05 0 0.001"
        >
          <a-text
            width="0.03"
            color="white"
            scale="10 10 10"
            align="center"
            height="0.03"
            z-offset="0.001"
            value={"<"}
          />
        </a-plane>
        <a-text
          height="0.1"
          width="0.1"
          align="center"
          scale={"10 10 10"}
          z-offset="0.001"
          value={props.value}
        />
        <a-plane
          width="0.03"
          color="black"
          height="0.03"
          interaction
          onClick={() => handleIncrease()}
          class="clickable"
          position="0.05 0 0.001"
        >
          <a-text
            width="0.03"
            color="white"
            scale="10 10 10"
            height="0.03"
            align="center"
            z-offset="0.001"
            value={">"}
          />
        </a-plane>
      </a-plane>
    );
  };
  //#endregion

  //region DESKTOP
  const renderKeyboard = () => {
    return (
      <div
        style={{
          display: "flex",
          width: "300px",
          height: "100%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            backgroundColor: "white",
            borderRadius: "6px",
            boxShadow: "inset 0px 0px 2px 2px black",
          }}
        >
          <ArrowButton onClick={() => handleDecrease()}>{"<"}</ArrowButton>
          <p style={{ padding: "10px" }}>{props.value}</p>
          <ArrowButton onClick={() => handleIncrease()}>{">"}</ArrowButton>
        </div>
      </div>
    );
  };
  //#endregion

  return (
    <>
      {providerData.controller === "keyboard" ? renderKeyboard() : renderVR()}
    </>
  );
};

export default Number;
