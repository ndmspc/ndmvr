import React from "react";
import "aframe";
import { TabletContext } from "../TabletProvider";
import styled from "styled-components";
import "../../../aframe/interaction";

const Button = styled.div`
  background: #dadaa3;
  cursor: pointer;
  border: 2px outset;
  &:hover {
    background: #a0e0b0;
  }
  &:active {
    border: 2px inset;
  }
`;
/**
 * Renders Select on tablet
 * @component
 * @param {Object} props props for Select object
 * @param {string[]|number[]} props.options
 * @param {(value:string)=>void} props.onSelect
 */

const Select = (props) => {
  const providerData = React.useContext(TabletContext);

  //#region VR

  const renderVR = () => {
    return (
      <a-plane
        color="#b58484"
        width={providerData.componentAreaWidth - 0.002}
        height={providerData.screenHeight - 0.002}
        position="0 0 0.001"
        rotation="0 0 0"
      >
        {props.options.map((o, i) => (
          <a-plane
            key={i}
            onClick={() => props.onSelect(o)}
            interaction
            color="black"
            class="clickable"
            width="0.065"
            height="0.014"
            position={`${Math.floor(i / 15) * 0.08 - 0.097} ${
              0.105 - (i % 15) * 0.015
            } 0.002`}
            rotation="0 0 0"
          >
            <a-text
              position={"-0.03 0 0"}
              scale={"2.2 2.2 2.2"}
              height={"0.05"}
              width={"0.1"}
              value={o}
            />
          </a-plane>
        ))}
      </a-plane>
    );
  };
  //#endregion

  //region DESKTOP
  const renderKeyboard = () => {
    return (
      <div
        style={{
          overflowY: "auto",
          border: "2px inset",
        }}
      >
        {props.options &&
          props.options.map((d, i) => (
            <Button
              onClick={() => {
                props.onSelect(d);
              }}
              key={i}
            >
              {d}
            </Button>
          ))}
      </div>
    );
  };
  //#endregion

  return (
    <>
      {providerData.controller === "keyboard" ? renderKeyboard() : renderVR()}
    </>
  );
};
export default Select;
