import Select from "./Select";
import Number from "./Number";

export const ComponentProvider = (type, props, onSelect) => {
  switch (type) {
    case "select":
      return <Select {...props} onSelect={onSelect} />;
    case "number":
      return <Number {...props} onSelect={onSelect} />;
    default:
      return <div>{type + " => No such component"}</div>;
  }
};
