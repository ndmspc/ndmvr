import React, { useState, useEffect, useContext, useRef } from 'react'
import 'aframe'
import '../aframe/cursor'
import '../aframe/th'
import '../aframe/histogram'
import '../aframe/labelHandler'
import HistogramReactFactory from '../utils/histogramGenerator'
import {
  histogramTH1Service,
  histogramTH2Service,
  histogramTH3Service
} from '../observables/histogramSubject'
import { StoreContext } from './contexts/StoreContext'
import { useNdmVrRedirect } from '@ndmspc/react-ndmspc-core'
import componentSubject from "../observables/componentSubject.jsx";
import { filter } from 'rxjs/operators';
import schemaSubject from "../observables/schemaSubject.jsx";

/**
 * React komponent pre vytvorenie histogramu.
 * @example
 * return (
 *   <NdmVrHistogram />
 * )
 */
const NdmVrHistogram = () => {
  const cinemaButtonClickFunction = useRef(null);
  // pressed keys
  let subscription
  const [ndmvrInputDevice, ] = useNdmVrRedirect('ndmvrInputDevice')
  const [activePoints, setActivePoints] = useState(0);

  const sc = useContext(StoreContext)

  // create factory
  const { selectedView, onViewChange, onProgress, section, selectedBins } =
    sc.states
  const experimental = sc.experimental
  //const schema = sc.schema

  //#region HISTOGRAM DATA

  const [schema, setNdmvrSchema] = useState(null);

  const range = schema?.histogram?.properties.range?.value
  const theme = schema?.histogram?.properties.theme?.value
  const scale = schema?.histogram?.properties.scale?.value
  //#endregion

  const { id, histogram, projPanelIds } = sc.data

  const factoryRef = useRef(
    new HistogramReactFactory(
      id,
      histogram,
      section || null,
      selectedBins,
      onViewChange,
      onProgress
    )
  )
  const factory = factoryRef.current
  const [elements, setElements] = useState(factory.getElements())
  const [binMarkState] = useState(1)

  //#region HANDLER FUNCTIONS
  // handle subscription

  const handleSubscription = (data) => {
    if (histogram._typename.includes(data.name)) {
      // save new section to local storage
      if (data.axis) {
        factory.updateSection(data.axis, data.increment, data.defaultRange)
      } else if (data.minValue) {
        factory.updateMinDisplayedContent(data.minValue)
      }
    }
    data.change
    setElements(factory.getElements())
  }

  /**
   * Handle subscription for active points(clicked bins),
   * functions for cinema/cinema button click functions
   * */
  useEffect(() => {
    let subFunc = componentSubject.getBinDistributor()
       .pipe(filter(ev => ev.cinemaButtonClickFunction !== undefined))
       .subscribe(subjectObject => {
         let cinemaButtonClickFunc = subjectObject.cinemaButtonClickFunction.find(func => func.name === 'handleCinemaButtonClick');
         if (cinemaButtonClickFunc) {
           cinemaButtonClickFunction.current = cinemaButtonClickFunc;
         }
       })
    let subResultPoints = componentSubject.getBinDistributor()
       .pipe(filter(ev => ev.resultPoints !== undefined))
       .subscribe(data => {
         setActivePoints(data.resultPoints.filter(item => item.val !== null).length);
       })

    const schemaSubjectRef = schemaSubject
       .getSchemaSubject()
       .subscribe(object => {
         setNdmvrSchema(object);
       })

    return() => {
      subFunc.unsubscribe();
      subResultPoints.unsubscribe();
      schemaSubjectRef.unsubscribe();
    }
  },[])

  //#endregion ----------------------------

  //#region USE EFFECT HOOKS

  // useEffect(() => {
  //   console.log(schema)
  // }, [schema])
  // update histogram unique name
  useEffect(() => {
    factory.setUniqueName(id)
    setElements(factory.getElements())
  }, [id])

  useEffect(() => {
    factory.updateContentScale(scale)
    setElements(factory.getElements())
  }, [scale])
  // update histogram and projection
  useEffect(() => {
    factory.setHistogram(histogram)
    setElements(factory.getElements())
  }, [histogram])

  // update histogram section
  useEffect(() => {
    factory.setSection(section)
    setElements(factory.getElements())
  }, [section])

  // update histogram section range
  useEffect(() => {
    factory.setRange(range)
    setElements(factory.getElements())
  }, [range])

  // update histogram unique name
  useEffect(() => {
    factory.setTheme(theme)
    setElements(factory.getElements())
  }, [theme])

  useEffect(() => {
    factory.setSelectedBin(selectedBins)
    setElements(factory.getElements())
  }, [selectedBins])

  useEffect(() => {
    factory.setViewFunction(onViewChange)
    setElements(factory.getElements())
  }, [onViewChange])

  useEffect(() => {
    factory.setViewFunction(onViewChange)
    setElements(factory.getElements())
  }, [selectedView])

  useEffect(() => {
    factory.setProgressFunction(onProgress)
    setElements(factory.getElements())
  }, [onProgress])

  //#endregion ------------------------------------

  // subscription after render histogram
  useEffect(() => {
    if (histogram._typename.includes('TH2')) {
      subscription = histogramTH2Service
        .getChangedSection()
        .subscribe(handleSubscription)
    } else if (histogram._typename.includes('TH3')) {
      subscription = histogramTH3Service
        .getChangedSection()
        .subscribe(handleSubscription)
    } else if (histogram._typename.includes('TH1')) {
      subscription = histogramTH1Service
        .getChangedSection()
        .subscribe(handleSubscription)
    }
    if (subscription) {
      return () => subscription.unsubscribe()
    }
  }, [elements, scale])

  if (elements?.bins) {
    return (
      <a-entity histogram-control>
        {elements.bins.map((bin) => (
          <a-box // bin box
            //#region attributes
            id={bin.id}
            key={bin.id}
            class="clickable"
            material={`color: ${bin.color}; transparent: true`}
            binth={bin.binData}
            mouseevent
            geometry={`width:${bin.animation.toWidth}; height:${bin.animation.toHeight}; depth:${bin.animation.toDepth};`}
            //  animation={`property: scale; from: ${bin.animation.fromWidth} ${bin.animation.fromHeight} ${bin.animation.fromDepth}; to: ${bin.animation.toWidth} ${bin.animation.toHeight} ${bin.animation.toDepth}; delay:100; dur: 1500; easing: linear;`}
            animation__1="property: material.opacity; from: 0; to: 0.75; dur: 2000;"
            animation__2={`property: position; from: ${bin.animation2.fromX} ${bin.animation2.fromY} ${bin.animation2.fromZ}; to: ${bin.animation2.toX} ${bin.animation2.toY} ${bin.animation2.toZ}; dur: 800;`}
            //animation__3={`property: material.color; from: ${bin.animation3.fromColor}; to: ${bin.animation3.toColor}; dur: 200;`}
            //#endregion attributes ------------------------------------------
          >
            <a-text // bin number
              //#region attributes
              value={bin.content}
              align="center"
              width="10"
              color="black"
              geometry="primitive:plane; width: 0.5; height: 0.5"
              material={`color: ${bin.color}; opacity: 0.50; transparent: true`}
              rotation="270 180 0"
              animation="property: text.opacity; from: -2; to: 2; dur: 1500;"
              animation__1="property: material.opacity; from: -0.4; to: 0.8; dur: 1500;"
              wrap-count="50"
              position={`0 ${
                0.001 +
                ((binMarkState === 1 ? -1 : 1) * bin.animation.toHeight) / 2
              } 0`}
              //#endregion attributes ------------------------------------------
            />
          </a-box>
        ))}
        {elements.labels.map((label) => (
          <a-entity // Axis numbers
            //#region attributes
            key={label.key}
            geometry={`primitive: ${label.geometry.primitive}; width:${label.geometry.width}; height:${label.geometry.height};`}
            text={`color: ${label.text.color}; width: ${
              label.text.width
            }; height: ${label.text.height}; align:center; xOffset:${
              label.axis === 'x' ? -(label.text.width / 12) : 0
            };baseline:top;`}
            label-handler={`value: ${label.labelHandler.value}; delay:${label.labelHandler.delay};`}
            rotation={`${label.rotation.x} ${label.rotation.y} ${label.rotation.z}`}
            material={`color: ${label.material.color}; transparent: true`}
            animation="property: text.width; from: 0; to: 4; dur: 1000;"
            animation__1="property: material.opacity; from: 0; to: 0.8; dur: 1500;"
            animation__2={`property: position; from: ${label.animation.fromX} ${label.animation.fromY} ${label.animation.fromZ}; to: ${label.animation.toX} ${label.animation.toY} ${label.animation.toZ}; dur: 1005;`}
            //#endregion attributes ------------------------------------------
          />
        ))}
        {elements.titles.map((title) => (
          <a-entity // Board Title
            //#region attributes
            key={title.key}
            //text={`value: ${title.text.value}; color: ${title.text.color}; width: 20; height:auto; align:center; baseline:bottom;`}
            rotation={`${title.rotation.x} ${title.rotation.y} ${title.rotation.z}`}
            position={`${title.position.x} ${title.position.y} ${title.position.z}`}
            material="transparent: true"
            //#endregion attributes ------------------------------------------
          >
            <a-text // Board Title name
              //#region attributes
              value={title.text.value}
              position="0 0.5 0"
              width="20"
              height="auto"
              align="center"
              color={title.text.color}
              //#endregion attributes ------------------------------------------
            />
            <a-text // Board Title sublabel
              //#region attributes
              value={title.innerText.value}
              position="0 -0.5 0"
              width="15"
              height="auto"
              align="center"
              color={title.innerText.color}
              //#endregion attributes ------------------------------------------
            />
            <a-entity // Board Title sublabel shadow ASK WHYY TWO LABELS ?
              //#region attributes
              text={`value: ${title.innerText.value}; color: ${title.innerText.color}; width: 15; height:auto; align:center;`}
              position="0 -0.5 0"
              material="transparent: true"
              //#endregion attributes ------------------------------------------
            />
            <a-entity // Board Title background
              //#region attributes
              geometry="primitive: plane; width:15; height:2;"
              position="0 0 -0.1"
              material="opacity: 0.9; color: white; transparent: true"
              //#endregion attributes ------------------------------------------
            />
          </a-entity>
        ))}
        {elements.axisLabels.map((axisLabel) => (
          <a-entity // Axis labels
            //#region attributes
            id={axisLabel.id}
            key={axisLabel.key}
            text={`value: ${axisLabel.text.value}; color: "${axisLabel.text.color}"; width: 10; height:auto; align:center;`}
            rotation={`${axisLabel.rotation.x} ${axisLabel.rotation.y} ${axisLabel.rotation.z}`}
            animation={`property: position; from: ${axisLabel.animation.fromX} ${axisLabel.animation.fromY} ${axisLabel.animation.fromZ}; to: ${axisLabel.animation.toX} ${axisLabel.animation.toY} ${axisLabel.animation.toZ}; dur: 1000;`}
            material="opacity: 0; transparent: true"
            //#endregion attributes ------------------------------------------
          />
        ))}
        {elements.ground && (
          <a-box // AKS IDK WHAT IS IT
            //#region attributes
            key={elements.ground.key}
            width={elements.ground.scale.width}
            material="color: #fafafa"
            height={elements.ground.scale.height}
            depth={elements.ground.scale.depth}
            animation={`property: position; from: ${elements.ground.animation.fromX} ${elements.ground.animation.fromY} ${elements.ground.animation.fromZ};
           to:
           ${elements.ground.animation.toX} ${elements.ground.animation.toY} ${elements.ground.animation.toZ}; dur: 1000;`}
            //#endregion attributes ------------------------------------------
          />
        )}
        {elements.banners.map((banner) => (
           <React.Fragment>
             <a-box // Board
                //#region attributes
                key={projPanelIds[0] ? projPanelIds[0] : 'panel_0'}
                id={projPanelIds[0] ? projPanelIds[0] : 'panel_0'}
                class="clickable"
                geometry={`width:${banner.geometry.width}; height:${banner.geometry.height}; depth:${banner.geometry.depth};`}
                position={`${banner.position.x} ${banner.position.y} ${banner.position.z}`}
                rotation={`${banner.rotation.x} ${banner.rotation.y} ${banner.rotation.z}`}
                material={`color: ${banner.material.color}; transparent: ${banner.material.transparent}`}
                //#endregion attributes ------------------------------------------
             />
             <a-entity box={`width: 6;
                             height: 3;
                             depth: 0.25;
                             color: #f3554d;
                             points: ${activePoints};`}
                       visible={activePoints > 0}
                       enabled={activePoints > 0}
                       id="cinema-button"
                       position={`${banner.position.x -10} ${-8 +15} ${banner.position.z-11}`}
                       rotation={`${banner.rotation.x} ${banner.rotation.y + 90} ${banner.rotation.z}`}
                       class={activePoints > 0 ? 'clickable' : ''} onClick={cinemaButtonClickFunction.current}></a-entity>
           </React.Fragment>
        ))}
      </a-entity>
    )
  } else {
    return (
      <a-box
        //#region attributes
        animation__1="property: rotation; to: 0 180 360; dur: 10000; easing: linear; loop: true"
        animation__2="property: scale; from: 1 1 1; to: 10 10 10; dur: 15000; timeout: 15000; loop: true; easing: linear; dir: alternate"
        animation__color="property: material.color; from: #003682; to: #7d0002; dur: 80000: easing: linear: loop: true; dir: alternate"
        position="0 1.5 -2"
        //#endregion attributes ------------------------------------------
      />
    )
  }
}

// const NdmVrHistogramOld = () => {
//     // pressed keys
//     let subscription

//     const sc = useContext(StoreContext)

//     // create factory
//     const {
//         selectedView,
//         onViewChange,
//         onProgress,
//         section,
//         range,
//         theme,
//         selectedBins
//     } = sc.states
//     const experimental = sc.experimental
//     const schema = sc.schema
//     const { id, histogram, projPanelIds } = sc.data

//     const [factory] = useState(
//         new HistogramReactFactory(
//             id,
//             histogram,
//             section || null,
//             range,
//             theme || 'def',
//             selectedBins,
//             onViewChange,
//             onProgress
//         )
//     )
//     const [elements, setElements] = useState(factory.getElements())
//     const [binMarkState] = useState(1)

//     //#region HANDLER FUNCTIONS
//     // handle subscription

//     const handleSubscription = (data) => {
//         if (histogram._typename.includes(data.name)) {
//             // save new section to local storage
//             if (data.axis) {
//                 factory.updateSection(
//                     data.axis,
//                     data.increment,
//                     data.defaultRange
//                 )
//                 setElements(factory.getElements())
//             } else if (data.scale) {
//                 factory.updateContentScale(data.scale)
//                 setElements(factory.getElements())
//             } else if (data.minValue) {
//                 factory.updateMinDisplayedContent(data.minValue)
//                 setElements(factory.getElements())
//             }
//         }
//     }
//     //#endregion ----------------------------

//     //#region USE EFFECT HOOKS

//     // useEffect(() => {
//     //   console.log(schema)
//     // }, [schema])
//     // update histogram unique name
//     useEffect(() => {
//         factory.setUniqueName(id)
//         setElements(factory.getElements())
//     }, [id])

//     // update histogram and projection
//     useEffect(() => {
//         factory.setHistogram(histogram)
//         setElements(factory.getElements())
//     }, [histogram])

//     // update histogram section
//     useEffect(() => {
//         factory.setSection(section)
//         setElements(factory.getElements())
//     }, [section])

//     // update histogram section range
//     useEffect(() => {
//         if (experimental)
//             factory.setRange(schema.histogram.properties.range.value)
//         else factory.setRange(range)
//         setElements(factory.getElements())
//     }, [range, schema.histogram.properties.range.value])

//     // update histogram unique name
//     useEffect(() => {
//         if (experimental)
//             factory.setTheme(schema.histogram.properties.theme.value)
//         else factory.setTheme(theme)
//         setElements(factory.getElements())
//     }, [theme, schema.histogram.properties.theme.value])

//     useEffect(() => {
//         factory.setSelectedBin(selectedBins)
//         setElements(factory.getElements())
//     }, [selectedBins])

//     useEffect(() => {
//         factory.setViewFunction(onViewChange)
//         setElements(factory.getElements())
//     }, [onViewChange])

//     useEffect(() => {
//         factory.setViewFunction(onViewChange)
//         setElements(factory.getElements())
//     }, [selectedView])

//     useEffect(() => {
//         factory.setProgressFunction(onProgress)
//         setElements(factory.getElements())
//     }, [onProgress])

//     //#endregion ------------------------------------

//     // subscription after render histogram
//     useEffect(() => {
//         if (histogram._typename.includes('TH2')) {
//             subscription = histogramTH2Service
//                 .getChangedSection()
//                 .subscribe(handleSubscription)
//         } else if (histogram._typename.includes('TH3')) {
//             subscription = histogramTH3Service
//                 .getChangedSection()
//                 .subscribe(handleSubscription)
//         } else if (histogram._typename.includes('TH1')) {
//             subscription = histogramTH1Service
//                 .getChangedSection()
//                 .subscribe(handleSubscription)
//         }
//         if (subscription) {
//             return () => subscription.unsubscribe()
//         }
//     }, [elements])

//     if (elements?.bins) {
//         return (
//             <a-entity histogram-control>
//                 {elements.bins.map((bin) => (
//                     <a-box // bin box
//                         //#region attributes
//                         id={bin.id}
//                         key={bin.id}
//                         class="clickable"
//                         material={`color: ${bin.color}; transparent: true`}
//                         binth={bin.binData}
//                         mouseevent
//                         geometry={`width:${bin.animation.toWidth}; height:${bin.animation.toHeight}; depth:${bin.animation.toDepth};`}
//                         //  animation={`property: scale; from: ${bin.animation.fromWidth} ${bin.animation.fromHeight} ${bin.animation.fromDepth}; to: ${bin.animation.toWidth} ${bin.animation.toHeight} ${bin.animation.toDepth}; delay:100; dur: 1500; easing: linear;`}
//                         animation__1="property: material.opacity; from: 0; to: 0.75; dur: 2000;"
//                         animation__2={`property: position; from: ${bin.animation2.fromX} ${bin.animation2.fromY} ${bin.animation2.fromZ}; to: ${bin.animation2.toX} ${bin.animation2.toY} ${bin.animation2.toZ}; dur: 800;`}
//                         animation__3={`property: material.color; from: ${bin.animation3.fromColor}; to: ${bin.animation3.toColor}; dur: 200;`}
//                         //#endregion attributes ------------------------------------------
//                     >
//                         <a-text // bin number
//                             //#region attributes
//                             value={bin.content}
//                             align="center"
//                             width="10"
//                             color="black"
//                             geometry="primitive:plane; width: 0.5; height: 0.5"
//                             material={`color: ${bin.color}; opacity: 0.50; transparent: true`}
//                             rotation="270 180 0"
//                             animation="property: text.opacity; from: -2; to: 2; dur: 1500;"
//                             animation__1="property: material.opacity; from: -0.4; to: 0.8; dur: 1500;"
//                             wrap-count="50"
//                             position={`0 ${
//                                 0.001 +
//                                 ((binMarkState === 1 ? -1 : 1) *
//                                     bin.animation.toHeight) /
//                                     2
//                             } 0`}
//                             //#endregion attributes ------------------------------------------
//                         />
//                     </a-box>
//                 ))}
//                 {elements.labels.map((label) => (
//                     <a-entity // Axis numbers
//                         //#region attributes
//                         key={label.key}
//                         geometry={`primitive: ${label.geometry.primitive}; width:${label.geometry.width}; height:${label.geometry.height};`}
//                         text={`color: ${label.text.color}; width: ${
//                             label.text.width
//                         }; height: ${label.text.height}; align:center; xOffset:${
//                             label.axis === 'x' ? -(label.text.width / 12) : 0
//                         };baseline:top;`}
//                         label-handler={`value: ${label.labelHandler.value}; delay:${label.labelHandler.delay};`}
//                         rotation={`${label.rotation.x} ${label.rotation.y} ${label.rotation.z}`}
//                         material={`color: ${label.material.color}; transparent: true`}
//                         animation="property: text.width; from: 0; to: 4; dur: 1000;"
//                         animation__1="property: material.opacity; from: 0; to: 0.8; dur: 1500;"
//                         animation__2={`property: position; from: ${label.animation.fromX} ${label.animation.fromY} ${label.animation.fromZ}; to: ${label.animation.toX} ${label.animation.toY} ${label.animation.toZ}; dur: 1005;`}
//                         //#endregion attributes ------------------------------------------
//                     />
//                 ))}
//                 {elements.titles.map((title) => (
//                     <a-entity // Board Title
//                         //#region attributes
//                         key={title.key}
//                         //text={`value: ${title.text.value}; color: ${title.text.color}; width: 20; height:auto; align:center; baseline:bottom;`}
//                         rotation={`${title.rotation.x} ${title.rotation.y} ${title.rotation.z}`}
//                         position={`${title.position.x} ${title.position.y} ${title.position.z}`}
//                         material="transparent: true"
//                         //#endregion attributes ------------------------------------------
//                     >
//                         <a-text // Board Title name
//                             //#region attributes
//                             value={title.text.value}
//                             position="0 0.5 0"
//                             width="20"
//                             height="auto"
//                             align="center"
//                             color={title.text.color}
//                             //#endregion attributes ------------------------------------------
//                         />
//                         <a-text // Board Title sublabel
//                             //#region attributes
//                             value={title.innerText.value}
//                             position="0 -0.5 0"
//                             width="15"
//                             height="auto"
//                             align="center"
//                             color={title.innerText.color}
//                             //#endregion attributes ------------------------------------------
//                         />
//                         <a-entity // Board Title sublabel shadow ASK WHYY TWO LABELS ?
//                             //#region attributes
//                             text={`value: ${title.innerText.value}; color: ${title.innerText.color}; width: 15; height:auto; align:center;`}
//                             position="0 -0.5 0"
//                             material="transparent: true"
//                             //#endregion attributes ------------------------------------------
//                         />
//                         <a-entity // Board Title background
//                             //#region attributes
//                             geometry="primitive: plane; width:15; height:2;"
//                             position="0 0 -0.1"
//                             material="opacity: 0.9; color: white; transparent: true"
//                             //#endregion attributes ------------------------------------------
//                         />
//                     </a-entity>
//                 ))}
//                 {elements.axisLabels.map((axisLabel) => (
//                     <a-entity // Axis labels
//                         //#region attributes
//                         id={axisLabel.id}
//                         key={axisLabel.key}
//                         text={`value: ${axisLabel.text.value}; color: "${axisLabel.text.color}"; width: 10; height:auto; align:center;`}
//                         rotation={`${axisLabel.rotation.x} ${axisLabel.rotation.y} ${axisLabel.rotation.z}`}
//                         animation={`property: position; from: ${axisLabel.animation.fromX} ${axisLabel.animation.fromY} ${axisLabel.animation.fromZ}; to: ${axisLabel.animation.toX} ${axisLabel.animation.toY} ${axisLabel.animation.toZ}; dur: 1000;`}
//                         material="opacity: 0; transparent: true"
//                         //#endregion attributes ------------------------------------------
//                     />
//                 ))}
//                 {elements.ground && (
//                     <a-box // AKS IDK WHAT IS IT
//                         //#region attributes
//                         key={elements.ground.key}
//                         width={elements.ground.scale.width}
//                         material="color: #fafafa"
//                         height={elements.ground.scale.height}
//                         depth={elements.ground.scale.depth}
//                         animation={`property: position; from: ${elements.ground.animation.fromX} ${elements.ground.animation.fromY} ${elements.ground.animation.fromZ};
//            to:
//            ${elements.ground.animation.toX} ${elements.ground.animation.toY} ${elements.ground.animation.toZ}; dur: 1000;`}
//                         //#endregion attributes ------------------------------------------
//                     />
//                 )}
//                 {elements.banners.map((banner) => (
//                     <a-box // Board
//                         //#region attributes
//                         key={projPanelIds[0] ? projPanelIds[0] : 'panel_0'}
//                         id={projPanelIds[0] ? projPanelIds[0] : 'panel_0'}
//                         geometry={`width:${banner.geometry.width}; height:${banner.geometry.height}; depth:${banner.geometry.depth};`}
//                         position={`${banner.position.x} ${banner.position.y} ${banner.position.z}`}
//                         rotation={`${banner.rotation.x} ${banner.rotation.y} ${banner.rotation.z}`}
//                         material={`color: ${banner.material.color}; transparent: ${banner.material.transparent}`}
//                         //#endregion attributes ------------------------------------------
//                     />
//                 ))}
//             </a-entity>
//         )
//     } else {
//         return (
//             <a-box
//                 //#region attributes
//                 animation__1="property: rotation; to: 0 180 360; dur: 10000; easing: linear; loop: true"
//                 animation__2="property: scale; from: 1 1 1; to: 10 10 10; dur: 15000; timeout: 15000; loop: true; easing: linear; dir: alternate"
//                 animation__color="property: material.color; from: #003682; to: #7d0002; dur: 80000: easing: linear: loop: true; dir: alternate"
//                 position="0 1.5 -2"
//                 //#endregion attributes ------------------------------------------
//             />
//         )
//     }
// }

export default NdmVrHistogram
