import React, { useContext } from 'react'
import 'aframe'
import { DesktopTablet } from './DesktopTablet'
import { useNdmVrRedirect } from '@ndmspc/react-ndmspc-core'
import TabletProvider from './Tablet/TabletProvider'
import { StoreContext } from './contexts/StoreContext'
import '../aframe/GUI/aframe-custom-xycomponents.js'

const NdmVrOculusControls = () => {
    const [ndmvrInputDevice] = useNdmVrRedirect('ndmvrInputDevice')
    const { experimental } = useContext(StoreContext)

    const renderOculusControls = () => {
        return (
            <React.Fragment>
                <a-entity
                    id="oculus-raycaster"
                    laser-controls="hand: right"
                    raycaster="objects: .clickable, [gui-interactable], .collidable;far:5500;
          lineOpacity: 0.5;
          far: 120;"
                ></a-entity>
                <a-entity
                    oculus-touch-controls="hand: left"
                    left-controller-logging
                    oculus-thumbstick-movement-controller=""
                >
                    {experimental ? (
                        <TabletProvider controller="vr" />
                    ) : (
                        <DesktopTablet />
                    )}
                </a-entity>
                <a-entity
                   id="oculus-right"
                   oculus-touch-controls="hand: right"
                    right-controller-logging
                    b-button-listener=""             />
            </React.Fragment>
        )
    }
    return (
        <a-entity>
            {ndmvrInputDevice === 'oculus' && renderOculusControls()}
        </a-entity>
    )
}
export default NdmVrOculusControls
