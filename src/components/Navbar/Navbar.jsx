import React, { useState } from 'react'
import { Nav, NavExpandable, NavList, NavItem } from '@patternfly/react-core'
import { Link } from 'react-router-dom'
import WebSocketConnectForm from '../../lib/components/NdmVrShared/forms/WebSocketConnectForm'

const Navbar = () => {
  const [activeItem, setActiveItem] = useState('grp-1_itm-1')
  const items = [
    {
      name: 'DYNAMIC TH3 histogram example',
      itemId: 'itm-1',
      url: '/aframe/th3demo'
    },
    {
      name: 'GITLAB data projection example',
      itemId: 'itm-2',
      url: '/aframe/gitlabProjection'
    },
    {
      name: 'PHYSICS data Analysis projection example',
      itemId: 'itm-3',
      url: '/aframe/physicsProjectionAna'
    },
    {
      name: 'PHYSICS data projection example',
      itemId: 'itm-4',
      url: '/aframe/physicsProjection'
    },
    {
      name: 'NDMVR monitoring adaptation',
      itemId: 'itm-5',
      url: '/aframe/thAdaptation'
    },
    {
      name: 'NDMVR Shared Projection example',
      itemId: 'itm-6',
      url: '/aframe/sharedProjection'
    }
  ]

  const onSelect = (result) => {
    setActiveItem(result.itemId)
  }

    return (
       <div style={{ background: '#151515' }}>
         <Nav onSelect={onSelect}>
           {activeItem === 'itm-6' ? <WebSocketConnectForm></WebSocketConnectForm> : <></>}
           <NavList>
             <NavExpandable title='NDMVR' groupId='grp-1' isActive={true}>
               {items.map((item) => (
                  <NavItem
                     key={item.name}
                     itemId={item.itemId}
                     isActive={activeItem === item.itemId}
                  >
                    <Link to={item.url}>{item.name}</Link>
                  </NavItem>
               ))}
             </NavExpandable>
           </NavList>
         </Nav>
       </div>
    )
}

export default Navbar
