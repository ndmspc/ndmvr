import React, {useState} from 'react'
import './Navbar.css'
import {useNdmVrRedirect} from "@ndmspc/react-ndmspc-core";
import { Nav, NavList, NavItem } from '@patternfly/react-core'
import { Link } from 'react-router-dom'
import WebSocketConnectForm from '../../lib/components/NdmVrShared/forms/WebSocketConnectForm.jsx'
import x_icon from '../../lib/assets/ndmvr/mobileControls/X.png';
import maximize_icon from '../../lib/assets/ndmvr/mobileControls/maximize.png';
import back_arrow from '../../lib/assets/ndmvr/mobileControls/back-arrow.png';


/**
 * Alternative to Navbar component for when user uses mobile device.
 * Contains expendable menu for various histogram demos and button for toggling fullscreen
 * */
const NavbarMobile = () => {
   const [activeItem, setActiveItem] = useState('grp-1_itm-1')
   const [isRotated, setIsRotated] = useState(false);
   const [isMenuExpanded, setIsMenuExpanded] = useState(false);
   const [fullScreen, setFullScreen] = useNdmVrRedirect('ndmvrFullscreen')

   const items = [
      {
         name: 'DYNAMIC TH3 histogram example',
         itemId: 'itm-1',
         url: '/aframe/th3demo'
      },
      {
         name: 'GITLAB data projection example',
         itemId: 'itm-2',
         url: '/aframe/gitlabProjection'
      },
      {
         name: 'PHYSICS data projection example',
         itemId: 'itm-3',
         url: '/aframe/physicsProjection'
      },
      {
         name: 'NDMVR monitoring adaptation',
         itemId: 'itm-4',
         url: '/aframe/thAdaptation'
      },
      {
         name: 'NDMVR Shared Projection example',
         itemId: 'itm-5',
         url: '/aframe/sharedProjection'
      }
   ]

   const onSelect = (result) => {
      setActiveItem(result.itemId)
   }

  const handleImageClick = () => {
      setIsRotated(!isRotated);
      setIsMenuExpanded(!isMenuExpanded);
   };

   const handleFullscreenClick = () => {
      setFullScreen.setData(prevState => !prevState);
   }

   return (
      <div>
         <div className="regionUI skyColor" style={{top: '16px', left: '16px'}} >
            <div className="buttonUI">
               {fullScreen ?
                  <img onClick={handleFullscreenClick} className="exit-button"
                       src={x_icon} alt='Exit'/>
                  :
                  <img onClick={handleFullscreenClick} className="maximize-button"
                       src = {maximize_icon} alt='Maximize'/>
               }
            </div>
         </div>
         <div className="regionUI skyColor" style={{top: '0px', right: '0px'}} >
            <div className="buttonUI">
               {isMenuExpanded && (
                  <div style={{ background: '#151515', width:'100vw' }}>
                     <Nav onSelect={onSelect}>
                        {activeItem === 'itm-5' ? <WebSocketConnectForm /> : <></>}
                        <NavList>
                           {items.map((item) => (
                              <NavItem
                                 key={item.name}
                                 itemId={item.itemId}
                                 isActive={activeItem === item.itemId}
                              >
                                 <Link to={item.url}>{item.name}</Link>
                              </NavItem>
                           ))}
                        </NavList>
                     </Nav>
                  </div>
               )}
               <img
                  src={back_arrow}
                  alt="Button"
                  className={`rotate-animation ${isRotated ? 'rotated' : ''}`}
                  onClick={handleImageClick}
                  id="rotateImage"
               />
            </div>
         </div>
      </div>
   )
}
export default NavbarMobile