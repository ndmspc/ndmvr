import React, { useState } from 'react'
import {
  Modal,
  ModalVariant,
  Button,
  Banner,
  Bullseye,
  Dropdown,
  DropdownToggle,
  DropdownItem
} from '@patternfly/react-core'

const InfoInterface = ({ selectedBins, currentBin }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [isOpenDP, setIsOpenDP] = useState(false)

  const handleToggleModal = () => {
    if (!isOpen) setIsOpenDP(false)
    setIsOpen(!isOpen)
  }

  const handleToggleDP = () => {
    setIsOpenDP(!isOpenDP)
  }

  const onSelectDP = () => {
    setIsOpenDP(!isOpenDP)
  }

  return (
    <>
      <Button variant='primary' onClick={handleToggleModal} style={{ margin: 1, width: '50%' }}>
        Show bins
      </Button>
      <Modal
        variant={ModalVariant.small}
        title='Ndmvr selected bins'
        description='Here you can see the selected bins and the last selected bin'
        isOpen={isOpen}
        onClose={handleToggleModal}
        style={{ paddingBottom: 200 }}
        actions={[
          <Button key='cancel' variant='link' onClick={handleToggleModal}>
            Ok
          </Button>
        ]}>
        <Banner style={{ margin: 10, padding: 20 }}>
          Focused histogram: {currentBin.name}, bin: {currentBin.bin}
        </Banner>
        {selectedBins.length > 0 &&
          <Bullseye style={{ margin: 10 }}>
            <Dropdown
              onSelect={onSelectDP}
              toggle={
                <DropdownToggle
                  onToggle={handleToggleDP}
                >
                  Selected bins: {selectedBins.length}
                </DropdownToggle>
              }
              isOpen={isOpenDP}
              dropdownItems={selectedBins.map(bin => <DropdownItem key={bin.bin} isDisabled>{bin.bin}</DropdownItem>)}
            />
          </Bullseye>}
      </Modal>
    </>
  )
}

export default InfoInterface
