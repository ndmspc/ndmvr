export default function registerCustomButton() {
   if (AFRAME.components['box']) return
   AFRAME.registerComponent('box', {
      schema: {
         width: {type: 'number', default: 1},
         height: {type: 'number', default: 1},
         depth: {type: 'number', default: 1},
         color: {type: 'color', default: '#AAA'},
         hoverColor: {type: 'color', default: '#F00'},
         borderRadius: {type: 'number', default: 0.1},
         outlineColor: {type: 'color', default: '#000'},
         outlineThickness: {type: 'number', default: 0.05},
         text: {type: 'string', default: 'calculate'},
         textColor: {type: 'color', default: '#000'},
         points: {type: 'number', default: 0}
      },

      init: function () {
         let data = this.data;
         let el = this.el;
         let originalColor = new THREE.Color(data.color);

         // Create central rounded rectangle
         let shape = new THREE.Shape();
         let x = -data.width / 2;
         let y = -data.height / 2;
         let radius = data.borderRadius;

         shape.moveTo(x + radius, y);
         shape.lineTo(x + data.width - radius, y);
         shape.quadraticCurveTo(x + data.width, y, x + data.width, y + radius);
         shape.lineTo(x + data.width, y + data.height - radius);
         shape.quadraticCurveTo(x + data.width, y + data.height, x + data.width - radius, y + data.height);
         shape.lineTo(x + radius, y + data.height);
         shape.quadraticCurveTo(x, y + data.height, x, y + data.height - radius);
         shape.lineTo(x, y + radius);
         shape.quadraticCurveTo(x, y, x + radius, y);

         let extrudeSettings = {depth: data.depth, bevelEnabled: false};
         let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
         this.material = new THREE.MeshStandardMaterial({color: originalColor});
         this.mesh = new THREE.Mesh(geometry, this.material);
         el.setObject3D('mesh', this.mesh);

         // Create thicker outline
         let outlineShape = new THREE.Shape();
         let outlineThickness = data.outlineThickness;

         outlineShape.moveTo(x - outlineThickness + radius, y - outlineThickness);
         outlineShape.lineTo(x + data.width + outlineThickness - radius, y - outlineThickness);
         outlineShape.quadraticCurveTo(x + data.width + outlineThickness, y - outlineThickness, x + data.width + outlineThickness, y + radius - outlineThickness);
         outlineShape.lineTo(x + data.width + outlineThickness, y + data.height + outlineThickness - radius);
         outlineShape.quadraticCurveTo(x + data.width + outlineThickness, y + data.height + outlineThickness, x + data.width + outlineThickness - radius, y + data.height + outlineThickness);
         outlineShape.lineTo(x - outlineThickness + radius, y + data.height + outlineThickness);
         outlineShape.quadraticCurveTo(x - outlineThickness, y + data.height + outlineThickness, x - outlineThickness, y + data.height + outlineThickness - radius);
         outlineShape.lineTo(x - outlineThickness, y + radius - outlineThickness);
         outlineShape.quadraticCurveTo(x - outlineThickness, y - outlineThickness, x - outlineThickness + radius, y - outlineThickness);

         let outlineExtrudeSettings = {depth: data.depth + 2 * outlineThickness, bevelEnabled: false};
         let outlineGeometry = new THREE.ExtrudeGeometry(outlineShape, outlineExtrudeSettings);
         this.outlineMaterial = new THREE.MeshStandardMaterial({color: data.outlineColor});
         this.outline = new THREE.Mesh(outlineGeometry, this.outlineMaterial);
         el.setObject3D('outline', this.outline);

         // Position the outline slightly behind the main shape
         this.outline.position.z = +outlineThickness * 4;
         // Add text element
         let textEl = document.createElement('a-entity');
         textEl.setAttribute('text', {
            value: data.text + ' (' + data.points + ')',
            align: 'center',
            color: data.textColor,
            width: data.width * 4,
            zOffset: 0.01 // Ensure text is slightly above the box surface to prevent z-fighting
         });
         textEl.setAttribute('position', {x: 0, y: 0, z: data.depth / 2 - 0.14});
         textEl.setAttribute('rotation', {x: 180, y: 0, z: 180})
         el.appendChild(textEl);

         // Event listeners for hover effect
         el.addEventListener('mouseenter', () => {
            this.mesh.material.color.set(data.hoverColor);
         });

         el.addEventListener('mouseleave', () => {
            this.mesh.material.color.set(data.color);
         });

         el.addEventListener('click', () => {
            this.mesh.material.color.set('#3e0072')
         })
      },

      update: function (oldData) {
         let data = this.data;
         let el = this.el;

         // If `oldData` is empty, then this means we're in the initialization process.
         if (Object.keys(oldData).length === 0) {
            return;
         }


         // Update central rectangle geometry and material if changed
         if (data.width !== oldData.width ||
            data.height !== oldData.height ||
            data.depth !== oldData.depth ||
            data.borderRadius !== oldData.borderRadius ||
            data.outlineThickness !== oldData.outlineThickness ||
            data.points !== oldData.points) {


            let shape = new THREE.Shape();
            let x = -data.width / 2;
            let y = -data.height / 2;
            let radius = data.borderRadius;

            shape.moveTo(x + radius, y);
            shape.lineTo(x + data.width - radius, y);
            shape.quadraticCurveTo(x + data.width, y, x + data.width, y + radius);
            shape.lineTo(x + data.width, y + data.height - radius);
            shape.quadraticCurveTo(x + data.width, y + data.height, x + data.width - radius, y + data.height);
            shape.lineTo(x + radius, y + data.height);
            shape.quadraticCurveTo(x, y + data.height, x, y + data.height - radius);
            shape.lineTo(x, y + radius);
            shape.quadraticCurveTo(x, y, x + radius, y);

            let extrudeSettings = {depth: data.depth, bevelEnabled: false};
            let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
            el.getObject3D('mesh').geometry = geometry;

            // Update outline
            let outlineShape = new THREE.Shape();
            let outlineThickness = data.outlineThickness;

            outlineShape.moveTo(x - outlineThickness + radius, y - outlineThickness);
            outlineShape.lineTo(x + data.width + outlineThickness - radius, y - outlineThickness);
            outlineShape.quadraticCurveTo(x + data.width + outlineThickness, y - outlineThickness, x + data.width + outlineThickness, y + radius - outlineThickness);
            outlineShape.lineTo(x + data.width + outlineThickness, y + data.height + outlineThickness - radius);
            outlineShape.quadraticCurveTo(x + data.width + outlineThickness, y + data.height + outlineThickness, x + data.width + outlineThickness - radius, y + data.height + outlineThickness);
            outlineShape.lineTo(x - outlineThickness + radius, y + data.height + outlineThickness);
            outlineShape.quadraticCurveTo(x - outlineThickness, y + data.height + outlineThickness, x - outlineThickness, y + data.height + outlineThickness - radius);
            outlineShape.lineTo(x - outlineThickness, y + radius - outlineThickness);
            outlineShape.quadraticCurveTo(x - outlineThickness, y - outlineThickness, x - outlineThickness + radius, y - outlineThickness);

            let outlineExtrudeSettings = {depth: data.depth + 2 * outlineThickness, bevelEnabled: false};
            let outlineGeometry = new THREE.ExtrudeGeometry(outlineShape, outlineExtrudeSettings);
            el.getObject3D('outline').geometry = outlineGeometry;

            // Position the outline slightly behind the main shape
            el.getObject3D('outline').position.z = +outlineThickness * 4;
         }

         if (data.color !== oldData.color) {
            el.getObject3D('mesh').material.color = new THREE.Color(data.color);
         }

         if (data.outlineColor !== oldData.outlineColor) {
            el.getObject3D('outline').material.color = new THREE.Color(data.outlineColor);
         }

         // Update text if changed
         let textEl = el.querySelector('[text]');
         if (data.text !== oldData.text || data.points !== oldData.points) {
            textEl.setAttribute('text', 'value', data.text + ' (' + data.points + ')');
         }
         if (data.textColor !== oldData.textColor) {
            textEl.setAttribute('text', 'color', data.textColor);
         }
      }
   });


}