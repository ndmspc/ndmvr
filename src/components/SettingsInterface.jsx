import React, { useState } from 'react'
import {
  Modal,
  ModalVariant,
  Button,
  Form,
  FormGroup,
  Select,
  SelectOption,
  SelectVariant,
  Flex,
  FlexItem
} from '@patternfly/react-core'

const SettingsInterface = ({
  onChangeHisto,
  onChangeTheme,
  onChangeRange,
  onChangeProj,
  onChangeView,
  panels,
  objectLabels,
  selection,
  projectionNames
}) => {
  const [isOpen, setIsOpen] = useState(false)
  const [isOpenHisto, setIsOpenHisto] = useState(false)
  const [isOpenTheme, setIsOpenTheme] = useState(false)
  const [isOpenRange, setIsOpenRange] = useState(false)
  const [isOpenView, setIsOpenView] = useState(false)
  const [isOpenProjection1, setIsOpenProjection1] = useState(false)
  const [isOpenProjection2, setIsOpenProjection2] = useState(false)
  const [isOpenProjection3, setIsOpenProjection3] = useState(false)
  const [isOpenProjection4, setIsOpenProjection4] = useState(false)

  const handleToggleModal = () => {
    setIsOpen(!isOpen)
  }

  const onToggleHisto = (isOpenHisto) => {
    setIsOpenHisto(isOpenHisto)
  }

  const onToggleTheme = (isOpenTheme) => {
    setIsOpenTheme(isOpenTheme)
  }

  const onToggleRange = (isOpenRange) => {
    setIsOpenRange(isOpenRange)
  }

  const onToggleView = (isOpenView) => {
    setIsOpenView(isOpenView)
  }

  return (
    <React.Fragment>
      <Button
        variant="primary"
        onClick={handleToggleModal}
        style={{ margin: 1, width: '50%' }}
      >
        Show Settings
      </Button>
      <Modal
        variant={ModalVariant.small}
        title="Ndmvr demo settings"
        description="Here you can change the settings of your visualization"
        isOpen={isOpen}
        onClose={handleToggleModal}
        style={{ paddingBottom: 200 }}
        actions={[
          <Button key="cancel" variant="link" onClick={handleToggleModal}>
            Ok
          </Button>
        ]}
      >
        <Form id="modal-with-form-form">
          <Flex>
            <FlexItem>
              {objectLabels && (
                <FormGroup
                  label="Select histogram"
                  fieldId="modal-with-form-form-name"
                >
                  <Select
                    variant={SelectVariant.single}
                    aria-label="Select histogram"
                    onToggle={onToggleHisto}
                    onSelect={onChangeHisto}
                    selections={selection}
                    isOpen={isOpenHisto}
                    aria-labelledby="Select histogram"
                  >
                    {objectLabels.map((value, index) => {
                      return (
                        <SelectOption value={value} key={`${value}${index}`}>
                          {value}
                        </SelectOption>
                      )
                    })}
                  </Select>
                </FormGroup>
              )}
            </FlexItem>
          </Flex>
          <Flex>
            <FlexItem>
              <FormGroup
                label="Select projection for panel 1"
                fieldId="modal-with-form-form-address"
              >
                <Select
                  variant={SelectVariant.single}
                  aria-label="Select projection for panel 1"
                  onToggle={(isOpenProjection) => {
                    setIsOpenProjection1(isOpenProjection)
                  }}
                  onSelect={(event, value, isPlaceholder) => {
                    setIsOpenProjection1(false)
                    onChangeProj(1, value)
                  }}
                  selections={panels['1']}
                  isOpen={isOpenProjection1}
                  aria-labelledby="Select projection for panel 1"
                >
                  {projectionNames.map((value, index) => {
                    return (
                      <SelectOption value={value} key={`${value}${index}`}>
                        {value}
                      </SelectOption>
                    )
                  })}
                </Select>
              </FormGroup>
            </FlexItem>
            <FlexItem>
              <FormGroup
                label="Select projection for panel 2"
                fieldId="modal-with-form-form-address"
              >
                <Select
                  variant={SelectVariant.single}
                  aria-label="Select projection for panel 2"
                  onToggle={(isOpenProjection) => {
                    setIsOpenProjection2(isOpenProjection)
                  }}
                  onSelect={(event, value, isPlaceholder) => {
                    setIsOpenProjection1(false)
                    onChangeProj(2, value)
                  }}
                  selections={panels['2']}
                  isOpen={isOpenProjection2}
                  aria-labelledby="Select projection for panel 2"
                >
                  {projectionNames.map((value, index) => {
                    return (
                      <SelectOption value={value} key={`${value}${index}`}>
                        {value}
                      </SelectOption>
                    )
                  })}
                </Select>
              </FormGroup>
            </FlexItem>
          </Flex>
          <Flex>
            <FlexItem>
              <FormGroup
                label="Select projection for panel 3"
                fieldId="modal-with-form-form-email"
              >
                <Select
                  variant={SelectVariant.single}
                  aria-label="Select projection for panel 3"
                  onToggle={(isOpenProjection) => {
                    setIsOpenProjection3(isOpenProjection)
                  }}
                  onSelect={(event, value, isPlaceholder) => {
                    setIsOpenProjection1(false)
                    onChangeProj(3, value)
                  }}
                  selections={panels['3']}
                  isOpen={isOpenProjection3}
                  aria-labelledby="Select projection for panel 3"
                >
                  {projectionNames.map((value, index) => {
                    return (
                      <SelectOption value={value} key={`${value}${index}`}>
                        {value}
                      </SelectOption>
                    )
                  })}
                </Select>
              </FormGroup>
            </FlexItem>
            <FlexItem>
              <FormGroup
                label="Select projection for panel 4"
                fieldId="modal-with-form-form-email"
              >
                <Select
                  variant={SelectVariant.single}
                  aria-label="Select projection for panel 4"
                  onToggle={(isOpenProjection) => {
                    setIsOpenProjection4(isOpenProjection)
                  }}
                  onSelect={(event, value, isPlaceholder) => {
                    setIsOpenProjection1(false)
                    onChangeProj(4, value)
                  }}
                  selections={panels['4']}
                  isOpen={isOpenProjection4}
                  aria-labelledby="Select projection for panel 4"
                >
                  {projectionNames.map((value, index) => {
                    return (
                      <SelectOption value={value} key={`${value}${index}`}>
                        {value}
                      </SelectOption>
                    )
                  })}
                </Select>
              </FormGroup>
            </FlexItem>
          </Flex>
        </Form>
      </Modal>
    </React.Fragment>
  )
}

export default SettingsInterface
