import React from "react";
import "aframe";
import { TabletContext } from "../../lib/components/Tablet/TabletProvider";
import styled from "styled-components";

const Button = styled.div`
  background: #d1a3da;
  cursor: pointer;
  border: 2px black;
  &:hover {
    background: #a0a5e0;
  }
  &:active {
    border: 2px white;
  }
`;
/**
 * Renders Select on tablet
 * @component
 * @param {Object} props props for Select object
 * @param {string[]|number[]} props.options
 * @param {(value:string)=>void} props.onSelect
 */
const Select = (props) => {
  const providerData = React.useContext(TabletContext);
  //#region VR
  const renderVR = () => {
    return (
      <a-plane
        color="white"
        width="0.1"
        height="0.1"
        position="0 0 0"
        rotation="0 0 0"
      ></a-plane>
    );
  };
  //#endregion

  //region DESKTOP
  const renderKeyboard = () => {
    return (
      <div
        style={{
          overflowY: "auto",
          border: "2px inset",
        }}
      >
        {props.options &&
          props.options.map((d, i) => (
            <Button
              onClick={() => {
                props.onSelect(d);
              }}
              key={i}
            >
              {d}
            </Button>
          ))}
      </div>
    );
  };
  //#endregion

  return (
    <>{providerData.controller == "keyboard" ? renderKeyboard() : renderVR()}</>
  );
};
export default Select;
