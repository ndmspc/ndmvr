import Select from "./Select";

export const ComponentProvider = (
  type,
  controller,
  props,
  onSelect,
  defaultProvider
) => {
  switch (type) {
    // case 'select':
    //   return controller == 'keyboard' ? (
    //     <Select {...props} onSelect={onSelect} />
    //   ) : (
    //     defaultProvider(type, props, onSelect)
    //   )
    default:
      return defaultProvider(type, props, onSelect);
  }
};
