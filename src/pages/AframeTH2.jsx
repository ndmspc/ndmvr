import React, { useEffect, useState } from "react";

const AframeTH2 = () => {
  const [filename] = useState(
    "https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.roott"
  );
  const [section] = useState({
    name: "TH2",
    xOffset: 1,
    yOffset: 1,
    range: 6,
  });

  useEffect(() => {
    console.log(filename);
    console.log(section);
  }, [filename, section]);

  return (
    <React.Fragment>
      <a-scene
        embedded
        style={{
          zIndex: "1",
          width: "100%",
          minHeight: "calc(90vh)",
        }}
      >
        <a-entity id="cameraWrapper" position="-1.5 0 -1.5" rotation="0 180 0">
          <a-camera look-control wasd-controls-custom="">
            <a-entity
              cursor="fuse: false; fuseTimeout: 2000;"
              raycaster="objects: .clickable; showLine: true; far: 100;"
              animation__click="property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1.4 1.4 1.4; dur: 150"
              animation__mouseenter="property: scale; startEvents: mouseenter; from: 1 1 1; to: 1.4 1.4 1.4; dur: 180"
              animation__mouseleave="property: scale; startEvents: mouseleave; from: 1.4 1.4 1.4; to: 1 1 1; dur: 180"
              line="color: orange; opacity: 0.5"
              far="100"
              position="0 0 -1"
              geometry="primitive: ring; radiusInner: 0.02; radiusOuter: 0.03"
              material="color: white; shader: flat"
            />
            <a-entity
              oculus-touch-controls="hand: left"
              left-controller-logging
            />
            <a-entity
              oculus-touch-controls="hand: right"
              right-controller-logging
            />
          </a-camera>
        </a-entity>
        <a-entity
          th2-histogram={`filename: ${filename}; section: ${JSON.stringify(
            section
          )}; theme: dark;`}
        />
      </a-scene>
    </React.Fragment>
  );
};

export default AframeTH2;
