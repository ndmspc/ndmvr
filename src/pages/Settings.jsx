import React, { useState } from 'react'
import {
  Button,
  ButtonGroup,
  Grid,
  Icon,
  IconButton,
  makeStyles,
  Paper,
  Snackbar,
  TextField,
  Typography
} from '@material-ui/core'
import { ndmVrStorage } from '../lib'
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  paper: {
    margin: theme.spacing(2),
    padding: theme.spacing(2),
    minHeight: 520
  },
  firstPaper: {
    margin: theme.spacing(2),
    padding: theme.spacing(2)
  },
  buttonGroup: {
    display: 'flex',
    justifyContent: 'center'
  }
}))

const RootFile = [
  {
    id: 'rootFile',
    label: 'ROOT file',
    helperText: 'URL address of a ROOT file'
  }
]

const TH2Inputs = [
  {
    id: 'th2Name',
    label: 'TH2 name',
    helperText: 'Histogram name'
  },
  {
    id: 'th2XOffset',
    label: 'X offset',
    type: 'number',
    helperText: 'Offset on X-axis'
  },
  {
    id: 'th2YOffset',
    label: 'Y offset',
    type: 'number',
    helperText: 'Offset on Y-axis'
  },
  {
    id: 'th2Range',
    label: 'Range',
    type: 'number',
    helperText: ''
  }
]

const TH3Inputs = [
  {
    id: 'th3Name',
    label: 'TH3 name',
    helperText: 'Histogram name'
  },
  {
    id: 'th3XOffset',
    label: 'X offset',
    type: 'number',
    helperText: 'Offset on X-axis'
  },
  {
    id: 'th3YOffset',
    label: 'Y offset',
    type: 'number',
    helperText: 'Offset on Y-axis'
  },
  {
    id: 'th3ZOffset',
    label: 'Z offset',
    type: 'number',
    helperText: 'Offset on Z-axis'
  },
  {
    id: 'th3Range',
    label: 'Range',
    type: 'number',
    helperText: ''
  }
]

const Settings = () => {
  const classes = useStyles()
  const [state, setState] = useState({
    'rootFile': {'rootFile': 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root'},
    'TH2': {
      th2Name: 'TH2',
      th2XOffset: 1,
      th2YOffset: 1,
      th2Range: 8
    },
    'TH3': {
      th3Name: 'TH3',
      th3XOffset: 1,
      th3YOffset: 1,
      th3ZOffset: 1,
      th3Range: 4
    }
  })
  const [snackbarMsg, setSnackbarMsg] = useState('')
  const [isSnackbarOpen, setIsSnackbarOpen] = useState(false)

  React.useEffect(() => {
    ;(async () => {
      if(!window.localStorage.getItem('rootFile')) return
      let newState = {}
      ;['rootFile', 'TH2', 'TH3'].forEach((key) => {
        let item = window.localStorage.getItem(key)
        if (key === 'rootFile') {
          if (item === null){
            item = ''
          }
          newState = { ...newState, [key]: { [key]: item } }
          return
        }
        if (item === null) {
          let section
          if (key === 'TH2'){
            section = {
              name: 'TH2',
              xOffset: 1,
              yOffset: 1,
              range: 8
            }
          } else {
            section = {
              name: 'TH3',
              xOffset: 1,
              yOffset: 1,
              zOffset: 1,
              range: 4
            }
          }
          item = {
            section: section
          }
        } else {
          item = JSON.parse(item)
        }
        item = item.section
        let newInnerState = {}
        Object.keys(item).forEach((objKey) => {
          const newKey =
            key.slice(0, 3).toLowerCase() +
            objKey.charAt(0).toUpperCase() +
            objKey.slice(1)
          newInnerState = { ...newInnerState, [newKey]: item[objKey] }
        })
        newState = { ...newState, [key]: { ...newInnerState } }
        return
      })
      setState(newState)
    })()
  }, [])

  const handleChange = (obj) => (key) => (event) => {
    setState({
      ...state,
      [obj]: {
        ...state[obj],
        [key]:
          event.target.type === 'number'
            ? parseInt(event.target.value)
            : event.target.value
      }
    })
}

  const renderInputs = (storageKey) => (array) =>
    array.map((input, index) => (
      <TextField
        key={index}
        fullWidth
        margin='normal'
        defaultValue={state[storageKey][input.id]}
        onChange={handleChange(storageKey)(input.id)}
        {...input}
      />
    ))

  const handleSaveToStorage = () => {
    Object.keys(state).forEach((key) => {
      if (key === 'rootFile') {
        // window.localStorage.setItem(key, state[key][key])
        ndmVrStorage.storeFilePath(state[key][key])
        return
      }
      let obj = {}
      Object.keys(state[key]).forEach((subKey) => {
        let tmpKey = subKey.slice(3)
        tmpKey = tmpKey.charAt(0).toLowerCase() + tmpKey.slice(1)
        obj = { ...obj, [tmpKey]: state[key][subKey] }
      })
      // window.localStorage.setItem(key, JSON.stringify(obj))
      ndmVrStorage.storeOffsets(obj)
    })
    setSnackbarMsg('Saved successfully to storage')
    setIsSnackbarOpen(true)
  }

  const handleClearStorage = () => {
    window.localStorage.clear()
    setSnackbarMsg('Storage cleared successfully')
    setIsSnackbarOpen(true)
  }

  if (!state) {
    return <h1>Loading ...</h1>
  }

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12}>
          <Paper elevation={3} className={classes.firstPaper}>
            <Typography variant='h5'>ROOT configuration</Typography>
            {renderInputs('rootFile')(RootFile)}
          </Paper>
        </Grid>
        <Grid item xs={12} md={6}>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant='h5'>TH2 configuration</Typography>
            {renderInputs('TH2')(TH2Inputs)}
          </Paper>
        </Grid>
        <Grid item xs={12} md={6}>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant='h5'>TH3 configuration</Typography>
            {renderInputs('TH3')(TH3Inputs)}
          </Paper>
        </Grid>
        <Grid item xs={12} className={classes.buttonGroup}>
          <ButtonGroup
            size='large'
            color='primary'
            variant='contained'
            disableElevation
          >
            <Button
              onClick={handleSaveToStorage}
              startIcon={<Icon>saveAlt</Icon>}
            >
              Save to storage
            </Button>
            <Button
              color='secondary'
              onClick={handleClearStorage}
              startIcon={<Icon>delete</Icon>}
            >
              Clear storage
            </Button>
          </ButtonGroup>
        </Grid>
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        open={isSnackbarOpen}
        autoHideDuration={2500}
        onClose={() => setIsSnackbarOpen(false)}
        message={snackbarMsg}
        action={
          <>
            <IconButton
              size='small'
              aria-label='close'
              color='inherit'
              onClick={() => setIsSnackbarOpen(false)}
            >
              <Icon>close</Icon>
            </IconButton>
          </>
        }
      />
    </div>
  )
}

export default Settings
