import React, { useState, useEffect, useLayoutEffect, useId } from 'react'
import {
  Sidebar,
  SidebarContent,
  SidebarPanel,
  Bullseye,
  Spinner,
  Flex,
  FlexItem,
  TextInput
} from '@patternfly/react-core'
import { NdmVr, defaultScheme } from '../lib'
import SettingsInterface from '../components/SettingsInterface'
import InfoInterface from '../components/InfoInterface'
import {
  cleanup,
  draw,
  redraw,
  parse as jsroot_parse,
  toJSON as jsroot_stringify
} from 'jsroot'
import {
  useExecutorStore,
  // readProjectionFromFile,
  executorStore,
  openFile,
  displayImageOfProjection,
  createTH1Projection
} from '@ndmspc/react-ndmspc-core'
import { ComponentProvider } from '../components/tablet/ComponentProvider'

let store = {
  source: executorStore('source')
}

const styles = {
  projection: {
    display: 'block',
    margin: 'auto',
    width: '420px',
    height: '350px'
  },
  p: {
    display: 'block',
    margin: 'auto',
    width: '520px',
    height: '420px'
  },
  projectionsSpace: {
    marginTop: '4rem',
    paddingBottom: '0'
  }
}

const projectionPanelIds = [
  'projectionContainer',
  'projectionContainer1',
  'projectionContainer2',
  'projectionContainer3',
  'projectionContainer4'
]

// const readProjectionFromFile = async (
//   filename,
//   basePath,
//   ix,
//   iy,
//   projName,
//   targetElmIds
// ) => {
//   const rootFile = await openFile(filename)
//   const proj = await rootFile.readObject(`${basePath}/${ix}/${iy}/${projName}`)
//   const painters = new Map()
//   for (const targetElmId of targetElmIds) {
//     cleanup(targetElmId)
//     const hPainter = await redraw(targetElmId, proj, 'colz')
//     painters.set(targetElmId, hPainter)
//   }
//   return painters
// }

/* TODO get feet attr *const projectionFunction = (projectionFunction, xMin, yMin) => {
  if (projectionFunction === 'feet' && this.#projections !== null) {
    const xIndex = xMin / this.#xWidth
    const yIndex = yMin / this.#yWidth
    try {
      const projectionHistogram =
        this.#projections.fFolders.arr[xIndex].fFolders.arr[yIndex].fFolders
          .arr[0]
      const LineColor = projectionHistogram.fFunctions.arr[0].fLineColor

      if (LineColor === 600) {
        return 'blue'
      } else if (LineColor === 632) {
        return 'red'
      }
    } catch (e) {
      return ''
    }
  }
  return ''
}*/

const THProjectionAna = ({
  inputDir = 'https://eos.ndmspc.io/eos/ndmspc/scratch/ndmspc/demo/rsn',
  objPrefix = 'content/7/1/2/6',
  objPostfix = '1/1/2/1'
}) => {
  const [mapHistogram, setMapHistogram] = useState('pt')
  const [mapHistograms, setMapHistograms] = useState(['pt', 'pt_mult'])
  const [resultPoints, setresultPoints] = useState([])

  const [filename, setFilename] = useState(null)
  const [mainObject, setMainObject] = useState(null)
  const [theme, setTheme] = useState('def')
  const [scene, setScene] = useState('labs')
  const [currentView, setCurrentView] = useState('def')
  const [range, setRange] = useState(10) //default range
  const [file, setFile] = useState(null)
  // const [currentBinObject, setCurrentBinObject] = useState(null)
  const [selectedBins, setSelectedBins] = useState([])
  const [focusedBin, setFocusedBin] = useState({})
  const mainHistId = useId()
  const [panel, setPanel] = useState({
    1: 'projectionContainer1',
    2: 'projectionContainer2',
    3: 'projectionContainer3',
    4: 'projectionContainer4'
  })

  const openRootFile = async (fn) => {
    if (fn) {
      console.log('openRootFile:: ', fn)
      const rootFile = await openFile(fn)
      setFile(rootFile)
    }
  }

  const readObjectFromFile = async (
    f,
    prefix,
    ix,
    iy = null,
    postfix,
    projName,
    divId
  ) => {
    if (iy) {
      const objName = `${prefix}/${ix}/${iy}/${projName}`
      console.log(objName)
      const proj = await f.readObject(objName)
      // proj.fMaximum = -1111
      // proj.fMinimum = -1111
      const painter = await redraw(divId, proj)
      // console.log("readObjectFromFile", proj)
      // setCurrentBinObject(proj)
      return proj
    } else {
      const objName = `${prefix}/${ix}/${postfix}/${projName}`
      console.log(objName)
      const proj = await f.readObject(objName)
      // proj.fMaximum = -1111
      // proj.fMinimum = -1111
      const painter = await redraw(divId, proj)
      console.log('readObjectFromFile', proj)
      // setCurrentBinObject(proj)
      return proj
    }
  }

  const sourceFunctions = mainObject?.names.map((option) => (data) => {
    console.log('sourceFunctions', data)
    setMainObject((prevState) => {
      return { ...prevState, selected: data }
    })
  })

  useExecutorStore(store.source, sourceFunctions)

  const handleChange = (e, selection, placeholder) => {
    console.log('sourceFunctions', selection)
    setMapHistogram(selection)
    setMainObject((prevState) => {
      return { ...prevState, selected: selection }
    })
  }

  const handleThemeChange = (e, selection, placeholder) => {
    setTheme(selection)
  }

  const handleSceneChange = (e, selection, placeholder) => {
    setScene(selection)
  }

  const handleRangeChange = (e, selection, placeholder) => {
    setRange(Number.parseInt(selection))
  }

  const handleChangeProj = (index, value) => {
    setPanel((prev) => {
      return { ...prev, ['' + index]: value }
    })
  }

  const handleChangeView = (e, selection, placeholder) => {
    setCurrentView(selection)
  }

  const handleViewChange = (data) => {
    // const histogram = data.histogram
    // if (histogram._typename.includes('TH2')) {
    //   if (currentView === '<10') {
    //     if (data.content < 0.2) return '#b7245c'
    //   } else if (currentView === '<30') {
    //     if (data.content < 0.6) return '#809bce'
    //   } else if (currentView === '<50') {
    //     if (data.content < 2.8) return '#593d3b'
    //   }
    // }
    return null
  }

  const handleCinemaClick = (resPoints) => {
    // console.log('CINEMA CLICK', resPoints);

    //return true => visible button | false => not visible button
    return false
  }

  const handleCinemaButtonClick = async () => {
    console.log('CINEMA BUTTON CLICK', resultPoints)

    const resultHist = jsroot_parse(jsroot_stringify(mainObject?.main))
    resultHist.fName = 'm'
    resultHist.fTitle = `Mass ${mapHistogram} distribution`
    resultHist.fFunctions.Clear()
    resultHist.fMinimum = 1.01
    resultHist.fMaximum = 1.025
    resultHist.fYaxis.fTitle = 'm (GeV/c^{2})'
    resultHist.fYaxis.fLabelSize = 0.03
    resultHist.fYaxis.fTitleOffset = 1.5
    resultHist.fSt
    // resultHist.fMaximum = -1111
    // resultHist.fMinimum = -1111
    // console.log(resultHist)
    for (let i = 1; i < resultPoints.length; i++) {
      if (resultPoints[i].val) {
        resultHist.fArray[i] = resultPoints[i].val
        resultHist.fSumw2[i] = resultPoints[i].err * resultPoints[i].err
      } else {
        resultHist.fArray[i] = 0
        resultHist.fSumw2[i] = 0
      }
    }

    const nextCounters = resultPoints.map((c, i) => {
      return { val: null, err: null }
    })
    // console.log("setresultPoints", nextCounters)
    setresultPoints(nextCounters)

    // console.log(resultHist)

    // cleanup(projectionPanelIds[1])
    const painter = await redraw(projectionPanelIds[1], resultHist, 'gridy')

    await displayImageOfProjection(
      projectionPanelIds[1],
      ['proj0'],
      '800px',
      '800px'
    )
    //return true => visible button | false => not visible button
    return true
  }

  const handleClick = async (data) => {
    // console.log("handleClick", data)
    // if (!focusedBin.bin) setFocusedBin(data)

    /*
    TO DO:
    - Fix the error where states do not change after interactions with the bins.
    - The issue is caused by setSelectedBins and setMainObject.
    - If prevState is changed, the schema is not defined, so no configuration aspects are present.
  */

    setSelectedBins((prevState) => [...prevState, data])

    const proj = await readObjectFromFile(
      file,
      objPrefix,
      data.binx,
      data?.biny,
      objPostfix,
      mainObject.projNames[0],
      projectionPanelIds[0]
    )
    const idx = 1
    // console.log("handleClick Fit: ", proj.fFunctions.arr[0].GetParName(idx), proj.fFunctions.arr[0].GetParValue(idx), proj.fFunctions.arr[0].GetParError(idx))
    // setresultPoints[data.bin  x] = {val:proj.fFunctions.arr[0].GetParValue(idx),err:proj.fFunctions.arr[0].GetParError(idx)}

    const nextCounters = resultPoints.map((c, i) => {
      if (i === data.binx) {
        // Increment the clicked counter
        if (c.val == null)
          return {
            val: proj.fFunctions.arr[0].GetParValue(idx),
            err: proj.fFunctions.arr[0].GetParError(idx)
          }
        else return { val: null, err: null }
      } else {
        // The rest haven't changed
        return c
      }
    })
    // console.log("setresultPoints", nextCounters)
    setresultPoints(nextCounters)
    setMainObject((prevState) => {
      return { ...prevState, proj }
    })

    await displayImageOfProjection(
      projectionPanelIds[0],
      ['proj0'],
      '800px',
      '800px'
    )

    //   console.log(data)
    //   console.log(EOS_URL_BASE + 'scratch/out.root')

    //   https://eos.ndmspc.io/eos/ndmspc/scratch/ndmspc/demo/rsn

    //   if (filename === EOS_URL_BASE + 'scratch/ndmspc/demo/rsn/results.root') {

    //     // const inputfile = filename + data.id + "results.root"
    //     console.log(filename)

    //   }
    //   else if (filename === EOS_URL_BASE + 'scratch/out.root') {
    //     // console.log(mainObject?.projNames)
    //     await readProjectionFromFile(
    //       EOS_URL_BASE + 'scratch/out.root',
    //       'pt_vs_mult/bins',
    //       data.binx,
    //       data.biny,
    //       mainObject?.projNames[mainObject?.projNames.length - 1],
    //       ['projectionContainer']
    //     )
    //   } else if (filename === EOS_URL_BASE + 'scratch/test/gitlab-hist.root') {
    //     await createTH1Projection('X', data, ['projectionContainer'], ['X', 'Y'])
    //   }
    //   await displayImageOfProjection(
    //     'projectionContainer',
    //     ['proj0'],
    //     '800px',
    //     '800px'
    //   )
  }

  const handleExecuteCommand = async (data) => {
    //   await renderProjOnPanels()
  }

  const handleDbClick = async (data) => {
    //   if (filename === EOS_URL_BASE + 'scratch/out.root') {
    //     for (let index = 1; index <= 5; index++) {
    //       await readProjectionFromFile(
    //         EOS_URL_BASE + 'scratch/out.root',
    //         'pt_vs_mult/bins',
    //         data.binx,
    //         data.biny,
    //         mainObject?.projNames[index - 1],
    //         [`projectionContainer${index}`]
    //       )
    //     }
    //   } else if (filename === EOS_URL_BASE + 'scratch/test/gitlab-hist.root') {
    //     await createTH1Projection('X', data, ['projectionContainer1'], ['X', 'Y'])
    //     await createTH1Projection('Y', data, ['projectionContainer2'], ['X', 'Y'])
    //   }
    // }
    // const renderProjOnPanels = async () => {
    //   for (let index = 1; index <= 5; index++) {
    //     if (panel['' + index]) {
    //       await displayImageOfProjection(
    //         panel['' + index],
    //         [`proj${index}`],
    //         '800px',
    //         '800px'
    //       )
    //     }
    //   }
  }

  const handleHover = async (data) => {
    //   // if (filename === 'https://eos.ndmspc.io/eos/ndmspc/scratch/out.root') {
    //   //   console.log(mainObject?.projNames)
    //   //   await readProjectionFromFile('https://eos.ndmspc.io/eos/ndmspc/scratch/out.root', 'pt_vs_mult/bins', data.binx, data.biny, mainObject?.projNames[mainObject?.projNames.length-1], ['projectionContainer'])
    //   // } else if (filename === 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root') {
    //   //   await createTH1Projection('X', data, ['projectionContainer'], ['X', 'Y'])
    //   // }
    //   // await displayImageOfProjection('projectionContainer', ['proj0'], '800px', '800px')
  }

  const readAllLabels = async () => {
    if (file) {
      // console.log("readAllLabels", file)
      const objectNames = file.fKeys
        // .filter((key) => key.fClassName === 'TDirectory')
        .map((key) => key.fName)
      if (!objectNames?.length) return null
      // console.log("readAllLabels", objectNames)

      const main = await file.readObject('hProjMap')
      console.log('readAllLabels', main)
      // console.log("readAllLabels", main.fAmapAxesTyperray.length)
      const arr = Array(main.fArray.length).fill({ val: null, err: null })
      setresultPoints(arr)
      setMainObject({
        main,
        names: mapHistograms,
        selected: mapHistogram,
        projNames: ['hPeak']
      })
      // }
    }
  }

  // const readAllObjects = async () => {
  // console.log("readAllObjects",file,mainObject?.selected)
  //   if (file && mainObject?.selected) {
  //     //     if (filename === EOS_URL_BASE + 'scratch/out.root') {
  //     //       const main = await file.readObject(`${mainObject.selected}/hist`)
  //     //       const mainProjectionNames = await file.readObject(
  //     //         `${mainObject.selected}/names`
  //     //       )
  //     //       setMainObject((prevState) => {
  //     //         return {
  //     //           ...prevState,
  //     //           main: main,
  //     //           projNames: mainProjectionNames?.arr.map((item) => item.fString)
  //     //         }
  //     //       })
  //     //     } else if (filename === EOS_URL_BASE + 'scratch/test/gitlab-hist.root') {
  //     //       const main = await file.readObject(`${mainObject.selected}`)
  //     //       const mainProjectionNames = ['X', 'Y']
  //     //       setMainObject((prevState) => {
  //     //         return {
  //     //           ...prevState,
  //     //           main: main,
  //     //           projNames: mainProjectionNames
  //     //         }
  //     //       })
  //     //     }
  //   }
  // }

  const getStores = () => {
    return {
      sourceStore: store.source,
      themeStore: store.theme,
      rangeStore: store.scale,
      viewStore: store.view,
      sceneStore: store.scene
    }
  }

  const getData = () => {
    // console.log(mainObject)
    return mainObject?.main
      ? {
          histogram: mainObject.main,
          id: mainObject?.selected,
          projectionsNames: mainObject?.projNames,
          projPanelIds: ['proj0', 'proj1', 'proj2', 'proj3', 'proj4'],
          background: {
            url: './assets/ndmvr/backgrounds/background1.jpg',
            radius: '3000',
            height: '2048',
            yPosition: '1000',
            width: '2048'
          }
        }
      : null
  }

  const getStates = () => {
    return {
      currentView: currentView,
      theme: theme,
      range: range,
      scene: scene,
      resultPoints: resultPoints
    }
  }

  useEffect(() => {
    readAllLabels()
  }, [file])

  useEffect(() => {
    let subscription = store.source.subscribe((data) => {
      console.log('subscription ', data)
      if (sourceFunctions?.length && data) {
        sourceFunctions[data.index](data.args)
      }
    })

    return () => subscription.unsubscribe()
  }, [mainObject?.main])

  useEffect(() => {
    openRootFile(filename)
  }, [filename])

  useEffect(() => {
    // console.log("mapHistogram::useEffect : ", inputDir, mapHistogram)
    setFilename(`${inputDir}/${mapHistogram}/results.root`)
  }, [inputDir, mapHistogram])
  // useEffect(() => {
  //   // console.log("currentBinObject", mainObject)
  // }, [mainObject])

  // useEffect(() => {
  //   console.log("resultPoints", resultPoints)
  // }, [resultPoints])

  return mainObject?.main ? (
    <>
      <Sidebar style={{ overflow: 'auto' }}>
        <SidebarContent>
          {getData() ? (
            <NdmVr
              data={getData()}
              stores={getStores()}
              states={getStates()}
              customComponentProvider={ComponentProvider}
              config={{
                ...defaultScheme,
                scene: {
                  ...defaultScheme.scene,
                  properties: {
                    ...defaultScheme.scene.properties,
                    scene: {
                      ...defaultScheme.scene.properties.scene,
                      value: scene
                    }
                  }
                }
              }}
              selectedBins={[]}
              cinemaFunc={[handleCinemaClick, handleCinemaButtonClick]}
              onClick={handleClick}
              onDbClick={handleDbClick}
              onHover={handleHover}
              onView={handleViewChange}
              onExeCommand={handleExecuteCommand}
              minHeight="calc(90vh)"
              experimental
            />
          ) : (
            <Bullseye>
              <Spinner isSVG size="xl" />
            </Bullseye>
          )}
        </SidebarContent>
        <SidebarPanel
          width={{ default: 'width_33' }}
          style={{ paddingTop: 50 }}
        >
          {getData() ? (
            <Flex>
              <div style={{ width: 400, height: 400 }}>
                <div id={projectionPanelIds[0]} style={styles.projection} />
              </div>
              <div style={{ width: 400, height: 400 }}>
                <div id={projectionPanelIds[1]} style={styles.projection} />
              </div>
            </Flex>
          ) : (
            <Bullseye>
              <Spinner isSVG size="xl" />
            </Bullseye>
          )}
          <Bullseye style={{ marginTop: 16 }}>
            <SettingsInterface
              onChangeHisto={handleChange}
              onChangeTheme={handleThemeChange}
              onChangeScene={handleSceneChange}
              onChangeRange={handleRangeChange}
              onChangeProj={handleChangeProj}
              onChangeView={handleChangeView}
              panels={panel}
              objectLabels={mainObject?.names}
              selection={mainObject?.selected}
              projectionNames={projectionPanelIds}
            />
          </Bullseye>
        </SidebarPanel>
      </Sidebar>
    </>
  ) : (
    <Bullseye>
      <Spinner isSVG size="xl" />
    </Bullseye>
  )
}

export default THProjectionAna
