import React, { useState, useEffect, useLayoutEffect } from 'react'
import {
  Sidebar,
  SidebarContent,
  SidebarPanel,
  Bullseye,
  Spinner,
  Flex,
  FlexItem,
  TextInput
} from '@patternfly/react-core'
import { NdmVr, defaultScheme } from '../lib'
import SettingsInterface from '../components/SettingsInterface'
import InfoInterface from '../components/InfoInterface'
import { cleanup, redraw } from 'jsroot'
import {
  useExecutorStore,
  // readProjectionFromFile,
  executorStore,
  openFile,
  displayImageOfProjection,
  createTH1Projection
} from '@ndmspc/react-ndmspc-core'
import { ComponentProvider } from '../components/tablet/ComponentProvider'

const EOS_URL_BASE = 'https://eos.ndmspc.io/eos/ndmspc/'

let store = {
  source: executorStore('source')
}

const styles = {
  projection: {
    display: 'block',
    margin: 'auto',
    width: '420px',
    height: '350px'
  },
  p: {
    display: 'block',
    margin: 'auto',
    width: '520px',
    height: '420px'
  },
  projectionsSpace: {
    marginTop: '4rem',
    paddingBottom: '0'
  }
}

const projectionPanelIds = [
  'projectionContainer',
  'projectionContainer1',
  'projectionContainer2',
  'projectionContainer3',
  'projectionContainer4'
]

const readProjectionFromFile = async (
  filename,
  basePath,
  ix,
  iy,
  projName,
  targetElmIds
) => {
  const rootFile = await openFile(filename)
  const proj = await rootFile.readObject(`${basePath}/${ix}/${iy}/${projName}`)
  const painters = new Map()
  for (const targetElmId of targetElmIds) {
    cleanup(targetElmId)
    const hPainter = await redraw(targetElmId, proj, 'colz')
    painters.set(targetElmId, hPainter)
  }
  return painters
}

/* TODO get feet attr *const projectionFunction = (projectionFunction, xMin, yMin) => {
  if (projectionFunction === 'feet' && this.#projections !== null) {
    const xIndex = xMin / this.#xWidth
    const yIndex = yMin / this.#yWidth
    try {
      const projectionHistogram =
        this.#projections.fFolders.arr[xIndex].fFolders.arr[yIndex].fFolders
          .arr[0]
      const LineColor = projectionHistogram.fFunctions.arr[0].fLineColor

      if (LineColor === 600) {
        return 'blue'
      } else if (LineColor === 632) {
        return 'red'
      }
    } catch (e) {
      return ''
    }
  }
  return ''
}*/

const THProjection = ({ filename }) => {
  const [theme, setTheme] = useState('def')
  const [scene, setScene] = useState('labs')
  const [currentView, setCurrentView] = useState('def')
  const [range, setRange] = useState(10) //default range
  const [file, setFile] = useState(null)
  const [mainObject, setMainObject] = useState(null)
  // const [selectedBins, setSelectedBins] = useState([])
  // const [focusedBin, setFocusedBin] = useState({})
  const [panel, setPanel] = useState({
    1: 'projectionContainer1',
    2: 'projectionContainer2',
    3: 'projectionContainer3',
    4: 'projectionContainer4'
  })

  const sourceFunctions = mainObject?.names.map((option) => (data) => {
    setMainObject((prevState) => {
      return { ...prevState, selected: data }
    })
  })

  useExecutorStore(store.source, sourceFunctions)

  const handleChange = (e, selection, placeholder) => {
    setMainObject((prevState) => {
      return { ...prevState, selected: selection }
    })
  }

  const handleThemeChange = (e, selection, placeholder) => {
    setTheme(selection)
  }

  const handleSceneChange = (e, selection, placeholder) => {
    setScene(selection)
  }

  const handleRangeChange = (e, selection, placeholder) => {
    setRange(Number.parseInt(selection))
  }

  const handleChangeProj = (index, value) => {
    setPanel((prev) => {
      return { ...prev, ['' + index]: value }
    })
  }

  const handleChangeView = (e, selection, placeholder) => {
    setCurrentView(selection)
  }

  const handleViewChange = (data) => {
    const histogram = data.histogram
    if (histogram._typename.includes('TH2')) {
      if (currentView === '<10') {
        if (data.content < 0.2) return '#b7245c'
      } else if (currentView === '<30') {
        if (data.content < 0.6) return '#809bce'
      } else if (currentView === '<50') {
        if (data.content < 2.8) return '#593d3b'
      }
    }
    return null
  }

  const handleCinemaClick = () => {
    console.log('CINEMA CLICK');
    //return true => visible button | false => not visible button
    return true;
  }

  const handleCinemaButtonClick = () => {
    console.log('CINEMA BUTTON CLICK');
    //return true => visible button | false => not visible button
    return true;
  }

  const handleClick = async (data) => {
    // if (!focusedBin.bin) setFocusedBin(data)
    // setSelectedBins((prevState) => [...prevState, data])
    if (filename === EOS_URL_BASE + 'scratch/out.root') {
      // console.log(mainObject?.projNames)
      await readProjectionFromFile(
        EOS_URL_BASE + 'scratch/out.root',
        'pt_vs_mult/bins',
        data.binx,
        data.biny,
        mainObject?.projNames[mainObject?.projNames.length - 1],
        ['projectionContainer']
      )
    } else if (filename === EOS_URL_BASE + 'scratch/test/gitlab-hist.root') {
      await createTH1Projection('X', data, ['projectionContainer'], ['X', 'Y'])
    }
    await displayImageOfProjection(
      'projectionContainer',
      ['proj0'],
      '800px',
      '800px'
    )
  }

  const handleExecuteCommand = async (data) => {
    await renderProjOnPanels()
  }

  const handleDbClick = async (data) => {
    if (filename === EOS_URL_BASE + 'scratch/out.root') {
      for (let index = 1; index <= 5; index++) {
        await readProjectionFromFile(
          EOS_URL_BASE + 'scratch/out.root',
          'pt_vs_mult/bins',
          data.binx,
          data.biny,
          mainObject?.projNames[index - 1],
          [`projectionContainer${index}`]
        )
      }
    } else if (filename === EOS_URL_BASE + 'scratch/test/gitlab-hist.root') {
      await createTH1Projection('X', data, ['projectionContainer1'], ['X', 'Y'])
      await createTH1Projection('Y', data, ['projectionContainer2'], ['X', 'Y'])
    }
  }

  const renderProjOnPanels = async () => {
    for (let index = 1; index <= 5; index++) {
      if (panel['' + index]) {
        await displayImageOfProjection(
          panel['' + index],
          [`proj${index}`],
          '800px',
          '800px'
        )
      }
    }
  }

  const handleHover = async (data) => {
    // if (filename === 'https://eos.ndmspc.io/eos/ndmspc/scratch/out.root') {
    //   console.log(mainObject?.projNames)
    //   await readProjectionFromFile('https://eos.ndmspc.io/eos/ndmspc/scratch/out.root', 'pt_vs_mult/bins', data.binx, data.biny, mainObject?.projNames[mainObject?.projNames.length-1], ['projectionContainer'])
    // } else if (filename === 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root') {
    //   await createTH1Projection('X', data, ['projectionContainer'], ['X', 'Y'])
    // }
    // await displayImageOfProjection('projectionContainer', ['proj0'], '800px', '800px')
  }

  const openRootFile = async () => {
    if (filename) {
      const rootFile = await openFile(filename)
      setFile(rootFile)
    }
  }

  const readAllLabels = async () => {
    if (file) {
      if (filename === EOS_URL_BASE + 'scratch/out.root') {
        const objectNames = file.fKeys
          .filter((key) => key.fClassName === 'TDirectory')
          .map((key) => key.fName)
        if (!objectNames?.length) return null

        setMainObject({
          names: objectNames,
          selected: objectNames[2]
        })
      } else if (filename === EOS_URL_BASE + 'scratch/test/gitlab-hist.root') {
        setMainObject({
          names: ['hUsersVsProjects'],
          selected: 'hUsersVsProjects'
        })
      }
    }
  }

  const readAllObjects = async () => {
    if (file && mainObject?.selected) {
      if (filename === EOS_URL_BASE + 'scratch/out.root') {
        const main = await file.readObject(`${mainObject.selected}/hist`)
        const mainProjectionNames = await file.readObject(
          `${mainObject.selected}/names`
        )
        setMainObject((prevState) => {
          return {
            ...prevState,
            main: main,
            projNames: mainProjectionNames?.arr.map((item) => item.fString)
          }
        })
      } else if (filename === EOS_URL_BASE + 'scratch/test/gitlab-hist.root') {
        const main = await file.readObject(`${mainObject.selected}`)
        const mainProjectionNames = ['X', 'Y']
        setMainObject((prevState) => {
          return {
            ...prevState,
            main: main,
            projNames: mainProjectionNames
          }
        })
      }
    }
  }

  const getStores = () => {
    return {
      sourceStore: store.source,
      themeStore: store.theme,
      rangeStore: store.scale,
      viewStore: store.view,
      sceneStore: store.scene
    }
  }

  const getData = () => {
    return mainObject?.main
      ? {
          histogram: mainObject.main,
          id: mainObject.selected,
          projectionsNames: mainObject.projNames,
          projPanelIds: ['proj0', 'proj1', 'proj2', 'proj3', 'proj4'],
          background: {
            url: './assets/ndmvr/backgrounds/background1.jpg',
            radius: '3000',
            height: '2048',
            yPosition: '1000',
            width: '2048'
          }
        }
      : null
  }

  const getStates = () => {
    return {
      currentView: currentView,
      theme: theme,
      range: range,
      scene: scene
    }
  }

  useEffect(() => {
    openRootFile()
  }, [filename])

  useEffect(() => {
    readAllLabels()
  }, [file])

  useEffect(() => {
    readAllObjects()
  }, [mainObject?.selected])

  useEffect(() => {
    let subscription = store.source.subscribe((data) => {
      if (sourceFunctions?.length && data) {
        sourceFunctions[data.index](data.args)
      }
    })

    return () => subscription.unsubscribe()
  }, [mainObject?.main])

  return mainObject?.main ? (
    <>
      <Sidebar style={{ overflow: 'auto' }}>
        <SidebarContent>
          {getData() ? (
            <NdmVr
              data={getData()}
              stores={getStores()}
              states={getStates()}
              customComponentProvider={ComponentProvider}
              config={{
                ...defaultScheme
                //  ...customScheme
              }}
              selectedBins={[]}
              cinemaFunc={[handleCinemaClick, handleCinemaButtonClick]}
              onClick={handleClick}
              onDbClick={handleDbClick}
              onHover={handleHover}
              onView={handleViewChange}
              onExeCommand={handleExecuteCommand}
              minHeight="calc(90vh)"
              experimental
            />
          ) : (
            <Bullseye>
              <Spinner isSVG size="xl" />
            </Bullseye>
          )}
        </SidebarContent>
        <SidebarPanel
          width={{ default: 'width_33' }}
          style={{ paddingTop: 50 }}
        >
          {getData() ? (
            <div style={{ width: 400, height: 400 }}>
              <div id="projectionContainer" style={styles.projection} />
            </div>
          ) : (
            <Bullseye>
              <Spinner isSVG size="xl" />
            </Bullseye>
          )}
          <Bullseye style={{ marginTop: 16 }}>
            <SettingsInterface
              onChangeHisto={handleChange}
              onChangeTheme={handleThemeChange}
              onChangeScene={handleSceneChange}
              onChangeRange={handleRangeChange}
              onChangeProj={handleChangeProj}
              onChangeView={handleChangeView}
              panels={panel}
              objectLabels={mainObject?.names}
              selection={mainObject?.selected}
              projectionNames={projectionPanelIds}
            />
          </Bullseye>
          <Bullseye>
            {/* <InfoInterface
              selectedBins={selectedBins}
              currentBin={focusedBin}
            /> */}
          </Bullseye>
        </SidebarPanel>
      </Sidebar>
      {getData() ? (
        <>
          <Flex style={styles.projectionsSpace}>
            <FlexItem>
              <div style={{ width: 500, height: 400 }}>
                <div id="projectionContainer1" style={styles.p} />
              </div>
            </FlexItem>
            <FlexItem>
              <div style={{ width: 500, height: 400 }}>
                <div id="projectionContainer2" style={styles.p} />
              </div>
            </FlexItem>
          </Flex>
          <Flex style={{ paddingTop: 0, margin: 0 }}>
            <FlexItem>
              <div style={{ width: 500, height: 400 }}>
                <div id="projectionContainer3" style={styles.p} />
              </div>
            </FlexItem>
            <FlexItem>
              <div style={{ width: 500, height: 400 }}>
                <div id="projectionContainer4" style={styles.p} />
              </div>
            </FlexItem>
          </Flex>
        </>
      ) : (
        <Bullseye>
          <Spinner isSVG size="xl" />
        </Bullseye>
      )}
    </>
  ) : (
    <Bullseye>Loading...</Bullseye>
  )
}

export default THProjection
