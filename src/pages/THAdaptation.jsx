import React, { useEffect, useState } from 'react'
import {
  openFile
} from '@ndmspc/react-ndmspc-core'
import { NdmVrScene } from '../lib'
import NdmVr from "../lib/components/NdmVr.jsx";

const THAdaptation = () => {
  const [mainObject, setMainObject] = useState(null)
  const [xbin, setXbin] = useState(2)
  const [yBin, setYbin] = useState(2)

  const loadHistogram = async () => {
    const file = await openFile('https://eos.ndmspc.io/eos/ndmspc/scratch/out.root')
    const obj = await file.readObject(`pt_vs_mult/hist`)

    setMainObject(obj)
  }


  useEffect(() => {
    loadHistogram()
  }, [])

  const handleClick = async (data) => {
    console.log('click')
    console.log(data)
  }

  const handleExecuteCommand = async (data) => {
    console.log('exe command')
    console.log(data)
  }

  const handleDbClick = async (data) => {
    console.log('db click')
    console.log(data)
    setXbin(data.binx)
    setYbin(data.biny)
  }

  const handleHover = async (data) => {
    console.log('hover')
    console.log(data)
  }

  const handleCinemaClick = () => {
    // console.log('CINEMA CLICK');
    //return true => visible button | false => not visible button
    return false;
  }

  const handleCinemaButtonClick = () => {
    // console.log('CINEMA BUTTON CLICK');
    //return true => visible button | false => not visible button
    return false;
  }

  const getStates = () => {
    return {
      theme: 'def'
    }
  }

  const getData = () => {
    return mainObject ? {
      histogram: mainObject,
      id: 'example',
      projectionsNames: [],
      projPanelIds: ['proj0', 'proj1', 'proj2', 'proj3', 'proj4'],
      background: {
        url: './assets/ndmvr/backgrounds/background1.jpg',
        radius: '3000',
        height: '2048',
        yPosition: '1000',
        width: '2048'
      },
    } : null
  }

  const handleBinProgress = (data) => {
    const histogram = data.histogram
    if (histogram._typename.includes('TH2')) {
      if (data.x === xbin && data.y === yBin) {
        return 0.2
      }
    }
    return 1
  }

  return (
    <React.Fragment>
      {mainObject != null &&
        <NdmVr
          data={getData()}
          states={getStates()}
          selectedBins={[]}
          cinemaFunc={[handleCinemaClick, handleCinemaButtonClick]}
          onClick={handleClick}
          onDbClick={handleDbClick}
          onHover={handleHover}
          onProgress={handleBinProgress}
          onExeCommand={handleExecuteCommand}
        />}
    </React.Fragment>
  )
}

export default THAdaptation
