import React, { useEffect, useState, useLayoutEffect, useContext } from 'react'
import { NdmVrScene } from '../lib'
import { createDemoTH3Example } from '../utils/utilsPageScript'
import { Bullseye, Spinner } from '@patternfly/react-core'
import { useExecutorStore, executorStore } from '@ndmspc/react-ndmspc-core'
import { NdmSpcContext, useStreamBrokerIn } from '@ndmspc/react-ndmspc-core'
import sendSelectedBinsToServer from '../lib/components/NdmVrShared/components/SelectedBinsController'
import { parse } from 'jsroot'
import NdmVr from "../lib/components/NdmVr.jsx";

let store = {
  histogramSource: executorStore('histogramSource'),
  colorTheme: executorStore('colorTheme'),
  displayedRange: executorStore('displayedRange'),
  view: executorStore('view')
}

const views = ['def']

const dataSources = ['th3gitlab']

const themeColors = ['nebula']

const displayRanges = [4]

const AframeReactShared = ({ wssb = 'shared' }) => {
  const [histogramLoaded, setHistogramLoaded] = useState(false)
  const [currentView] = useState('<30')
  const [histogram, setHistogram] = useState(null)
  const [theme, setTheme] = useState('nebula')
  const [range, setRange] = useState(4)
  const [histo, setHisto] = useState('th3gitlab')
  const [selectedBins, setSelectedBins] = useState([])
  const [interval, setInterval] = useState()
  const [masterId, setMasterId] = useState(null)
  const [socketId, setSocketId] = useState(null)


  const sb = useContext(NdmSpcContext)[wssb]
  const binsResponse = useStreamBrokerIn('selectedBins', wssb)
  const histogramResponse = useStreamBrokerIn('histogram', wssb)
  const masterResponse = useStreamBrokerIn('master', wssb)
  const ws = useStreamBrokerIn('ws', wssb);

  const dataSourceFunctions = dataSources.map((option) => (data) => {
    setHisto(data)
    return histo
  })

  const themeColorsFunctions = themeColors.map((option) => (data) => {
    setTheme(data)
    return theme
  })

  const displayRangesFunctions = displayRanges.map((option) => (data) => {
    setRange(data)
    return range
  })

  const viewFunctions = views.map((option) => (data) => {
    return data
  })

  const handleClick = (data) => {
    setSelectedBins((previousState) => {
      let newState

      if (previousState.some((x) => x.id === data.binId)) {
        newState = previousState
          ? [...previousState.filter((x) => x.id !== data.binId)]
          : []
      } else {
        newState = previousState
          ? [...previousState, { id: data.binId }]
          : [{ id: data.binId }]
      }

      sendSelectedBinsToServer(sb, newState)

      return newState
    })
  }

  useEffect(() => {
    if (binsResponse.payload !== undefined) {
      setSelectedBins(binsResponse.payload.selectedBins)
    }
  }, [binsResponse.payload])

  useEffect(() => {
    if (histogramResponse.payload !== undefined) {
      setHistogram(parse(JSON.parse(histogramResponse.payload.histogram)))
    }
  }, [histogramResponse.payload])

  useEffect(() => {
    if(masterResponse.payload !== undefined){
      setMasterId(masterResponse.payload.master)

      if(masterId === null){
        clearInterval(interval);
        setInterval(null);
        setIntervalFunc();
      } else if(masterId !== socketId){
        clearInterval(interval);
        setInterval(null);
      }
    }
  }, [masterResponse.payload])

  useEffect(() => {
    if (ws.payload !== undefined) {
      setSocketId(ws.payload.id)
    }
  }, [ws.payload])

  const handleDbClick = (data) => {
    console.log(data)
  }

  const handleHover = (data) => {
    console.log(data)
  }

  const handleViewChange = (data) => {
    const histogram = data.histogram
    if (histogram._typename.includes('TH3')) {
      if (currentView === '<10') {
        if (data.content < 0.2) return '#b7245c'
      } else if (currentView === '<30') {
        if (data.content < 0.3) return '#809bce'
      } else if (currentView === '<50') {
        if (data.content > 0.7) return '#593d3b'
      }
    }

    return null
  }

  const handleCinemaClick = () => {
    // console.log('CINEMA CLICK');
    //return true => visible button | false => not visible button
    return false;
  }

  const handleCinemaButtonClick = () => {
    // console.log('CINEMA BUTTON CLICK');
    //return true => visible button | false => not visible button
    return false;
  }

  useExecutorStore(store.histogramSource, dataSourceFunctions)
  useExecutorStore(store.colorTheme, themeColorsFunctions)
  useExecutorStore(store.displayedRange, displayRangesFunctions)
  useExecutorStore(store.view, viewFunctions)

  useLayoutEffect(() => {
    store.histogramSource.setOptions(dataSources)
    store.colorTheme.setOptions(themeColors)
    store.displayedRange.setOptions(displayRanges)
    store.view.setOptions(views)
  }, [])

  const background = {
    url: './assets/ndmvr/backgrounds/background1.jpg',
    radius: '3000',
    height: '2048'
  }

  useEffect(() => {
    const histogram = createDemoTH3Example()
    setHistogram(histogram)
    setHistogramLoaded(true)
  }, [])

  const setIntervalFunc = () => {
    const intervalTemp = setInterval(() => {
      const histogram = createDemoTH3Example()
      setHistogram(histogram)
    }, 8000)

    setInterval(intervalTemp)
  }

  return (
    <React.Fragment>
      {histogramLoaded !== false ? (
        <NdmVr
          useShared={true}
          masterId={masterId}
          socketId={socketId}
          data={{
            id: 'th3gitlab',
            histogram: histogram,
            projectionsNames: [],
            projPanelIds: [],
            background: background
          }}
          stores={{
            sourceStore: store.histogramSource,
            themeStore: store.colorTheme,
            rangeStore: store.displayedRange,
            viewStore: store.view
          }}
          states={{
            theme: 'nebula',
            range: 4,
            selectedView: currentView
          }}
          cinemaFunc={[handleCinemaClick, handleCinemaButtonClick]}
          selectedBins={selectedBins}
          onClick={handleClick}
          //   onDbClick={handleDbClick}
          //   onHover={handleHover}
          onView={handleViewChange}
          />
      ) : (
        <Bullseye>
          <Spinner isSVG size='xl' />
        </Bullseye>
      )}
    </React.Fragment>
  )
}

export default AframeReactShared
