import React, { useEffect, useLayoutEffect, useState } from 'react'
import { NdmVrScene } from '../lib'
import { Bullseye, Spinner } from '@patternfly/react-core'
import { useExecutorStore, executorStore, openFile } from '@ndmspc/react-ndmspc-core'
import NdmVr from "../lib/components/NdmVr.jsx";


let store = {
  histogramSource: executorStore('histogramSource'),
  colorTheme: executorStore('colorTheme'),
  displayedRange: executorStore('displayedRange')
}

const dataSources = ['th3gitlab']

const themeColors = ['water']

const displayRanges = [30]

const AframeReactTH1 = ({ storeShared }) => {
  const [histogramLoaded, setHistogramLoaded] = useState(false)
  const [histogram, setHistogram] = useState(null)

  const [theme, setTheme] = useState('nebula')
  const [range, setRange] = useState(4)
  const [histo, setHisto] = useState('th3gitlab')

  const dataSourceFunctions = dataSources.map(() => (data) => {
    setHisto(data)
    return histo
  })

  const themeColorsFunctions = themeColors.map(() => (data) => {
    setTheme(data)
    return theme
  })

  const displayRangesFunctions = displayRanges.map(() => (data) => {
    setRange(data)
    return range
  })

  const handleClick = (data) => {
    console.log(data)
  }

  const handleDbClick = (data) => {
    console.log(data)
  }

  const handleHover = (data) => {
    console.log(data)
  }

  useExecutorStore(store.histogramSource, dataSourceFunctions)
  useExecutorStore(store.colorTheme, themeColorsFunctions)
  useExecutorStore(store.displayedRange, displayRangesFunctions)

  useLayoutEffect(() => {
    store.histogramSource.setOptions(dataSources)
    store.colorTheme.setOptions(themeColors)
    store.displayedRange.setOptions(displayRanges)
  }, [])

  const background = {
    url: '',
    radius: '3000',
    height: '2048'
  }
  //open file and load histogram

  useEffect(() => {
    if (!histogramLoaded) {
      let filename = 'https://eos.ndmspc.io/eos/ndmspc/scratch/daos/test/daos_phys.root'
      openFile(filename)
        .then(file => {
          console.log(file)
          return file.readObject('root')
        })
        .then(obj => {
          setHistogram(obj.fFolders.arr[0].fFolders.arr[0].fFolders.arr[0])
          setHistogramLoaded(true)
        })
        .then(() => console.log('drawing completed'))
        .catch(error => {
          console.log(error)
        })
    }
  }, [histogramLoaded])

  return (
    <React.Fragment>
      {histogramLoaded !== false ?
        <NdmVr
          data={{
            name: 'th1Physics',
            histogram: histogram,
            background: background,
            projections: null
          }}
          states={{
            theme: 'water',
            range: 4,
          }}
          stores={{
            sourceStore: store.histogramSource,
            themeStore: store.colorTheme,
            rangeStore: store.displayedRange,
            sharedStore: storeShared,
          }}
          selectedBins={[]}
          onClick={handleClick}
          onDbClick={handleDbClick}
          onHover={handleHover}
        />
        :
        <Bullseye>
          <Spinner isSVG size='xl' />
        </Bullseye>
      }
    </React.Fragment>
  )
}

export default AframeReactTH1
