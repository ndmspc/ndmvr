// import React from 'react'
// import ReactDOM from 'react-dom'
// import App from './App'
// import { HashRouter as Router } from 'react-router-dom'
// // import { JsRootProvider } from '@ndmspc/react-ndmspc-core'

// ReactDOM.render(
//   <React.StrictMode>
//     {/* <JsRootProvider> */}
//     <Router>
//       <App />
//       </Router>
//     {/* </JsRootProvider> */}
//   </React.StrictMode>,
//   document.getElementById('root')
// )

import React from 'react'
// import ReactDOM from 'react-dom'
import { createRoot } from 'react-dom/client';
import App from './App'

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// )


const container = document.getElementById('app');
const root = createRoot(container); // createRoot(container!) if you use TypeScript
root.render(<App tab="home" />);