import express from 'express'
import cors from 'cors'
import { WebSocketServer } from 'ws'
import { v4 as uuidv4 } from 'uuid'

import store from './store/store.js'

const app = express()
app.use(cors())

const wss = new WebSocketServer({ noServer: true })

const createRoom = (roomName, client) => {
  store.dispatch({
    type: 'CREATE_ROOM',
    payload: { roomName: roomName }
  })

  store.dispatch({
    type: 'REMOVE_USER',
    payload: { id: client.id, roomName: 'defaultRoom' }
  })

  store.dispatch({
    type: 'ADD_USER',
    payload: { 
      id: client.id, 
      roomName: roomName,
      username: client.username,
      color: client.color
    }
  })
  console.log(`Created new room: ${roomName}`)
}

const joinRoom = (roomName, client) => {
  store.dispatch({
    type: 'REMOVE_USER',
    payload: { id: client.id, roomName: 'defaultRoom' }
  })

  store.dispatch({
    type: 'ADD_USER',
    payload: { 
      id: client.id, 
      roomName: roomName,
      username: client.username,
      color: client.color
    }
  })

  // Send current master info to the new client
  const currentMaster = store.getState().shared[roomName].master;
  client.send(JSON.stringify({
    type: 'master',
    payload: {
      master: currentMaster
    }
  }));

  // Send current axis positions to the new client
  const currentState = store.getState().shared[roomName];
  client.send(JSON.stringify({
    type: 'axisScalingLatest',
    payload: {
      latestAxis: currentState.latestAxis,
      positions: currentState.axisPositions,
      initiator: null
    }
  }));

  console.log(`Client joined room: ${roomName}`)
}

const leaveRoom = (roomName, client) => {
  store.dispatch({
    type: 'REMOVE_USER',
    payload: { id: client.id, roomName: roomName }
  })

  store.dispatch({
    type: 'ADD_USER',
    payload: { 
      id: client.id, 
      roomName: 'defaultRoom',
      username: client.username,
      color: client.color
    }
  })
  console.log(`Client left room: ${roomName}`)
}

const getCurrentRoomForUser = (client) => {
  let state = store.getState().shared

  for (let [room, value] of Object.entries(state)) {
    if (value.clients?.some((x) => x.id === client.id)) {
      return room
    }
  }
}

const axisScalingLatest = (data, positions, initiator) => {
  return {
    type: 'axisScalingLatest',
    payload: {
      latestAxis: data,
      positions: positions,
      initiator: initiator
    }
  }
}

const clientsInfo = (data) => {
  return {
    type: 'move',
    payload: {
      clients: data.map(client => ({
        id: client.id,
        position: client.position,
        rotation: client.rotation,
        color: client.color,
        username: client.username
      }))
    }
  }
}

const masterInfo = (data) => {
  return {
    type: 'master',
    payload: {
      master: data
    }
  }
}

const selectedBins = (data) => {
  return {
    type: 'selectedBins',
    payload: {
      selectedBins: data
    }
  }
}

const axisScaling = (data) => {
  return {
    type: 'axisScaling',
    payload: {
      axisMap: data
    }
  }
}

const histogram = (data) => {
  return {
    type: 'histogram',
    payload: {
      histogram: data
    }
  }
}

const otherData = (data) => {
  return {
    type: 'otherData',
    payload: {
      data: data
    }
  }
}

const syncData = (currentRoom) => {
  wss.broadcast(
    currentRoom,
    JSON.stringify(masterInfo(store.getState().shared[currentRoom].master))
  )

  wss.broadcast(
    currentRoom,
    JSON.stringify(
      selectedBins(store.getState().shared[currentRoom].selectedBins)
    )
  )

  wss.broadcast(
    currentRoom,
    JSON.stringify(axisScaling(store.getState().shared[currentRoom].axisMap))
  )

  wss.broadcast(
    currentRoom,
    JSON.stringify(histogram(store.getState().shared[currentRoom].histogram))
  )

  wss.broadcast(
    currentRoom,
    JSON.stringify(otherData(store.getState().shared[currentRoom].otherData))
  )
}

const clientRooms = new Map();

wss.on('connection', (ws) => {
  ws.id = uuidv4()
  ws.color = '#' + Math.floor(Math.random() * 16777215).toString(16)
  ws.username = 'User'

  const wsInfo = {
    type: 'ws',
    payload: { id: ws.id }
  }

  const app = {
    type: 'app',
    payload: {
      version: '20230417.1',
      name: 'NDMVR Shared Server'
    }
  }

  ws.send(JSON.stringify(wsInfo))
  ws.send(JSON.stringify(app))
  ws.send(
    JSON.stringify({
      type: 'move',
      payload: {
        clients: []
      }
    })
  )

  store.dispatch({
    type: 'ADD_USER',
    payload: { 
      id: ws.id, 
      roomName: 'defaultRoom',
      username: ws.username,
      color: ws.color
    }
  })

  console.log('New client ' + ws.id + 'has connected')

  console.log(store.getState().shared)

  const currentRoom = getCurrentRoomForUser(ws);
  if (currentRoom && store.getState().shared[currentRoom]) {
    const currentState = store.getState().shared[currentRoom];
    ws.send(JSON.stringify(axisScalingLatest(
      currentState.latestAxis,
      currentState.axisPositions,
      null
    )));
  }

  let confirmationTimeout = null;

  ws.on('message', (msg) => {
    const parsedMessage = JSON.parse(msg.toString())
    const currentRoom = getCurrentRoomForUser(ws)

    switch (parsedMessage.type) {
      case 'move':
        store.dispatch({
          type: 'UPDATE_USER',
          payload: {
            roomName: currentRoom,
            id: ws.id,
            rotation: parsedMessage.payload.rotation,
            position: parsedMessage.payload.position,
            color: ws.color,
            username: ws.username
          }
        })
        wss.broadcast(
          currentRoom,
          JSON.stringify(
            clientsInfo(store.getState().shared[currentRoom].clients)
          )
        )
        break
      case 'assign_master':
        store.dispatch({
          type: 'ASSIGN_MASTER',
          payload: {
            master: ws.id,
            roomName: currentRoom
          }
        })
        wss.broadcast(
          currentRoom,
          JSON.stringify(
            masterInfo(store.getState().shared[currentRoom].master)
          )
        )
        break
      case 'deassign_master':
        store.dispatch({
          type: 'DEASSIGN_MASTER',
          payload: {
            master: ws.id,
            roomName: currentRoom
          }
        })
        wss.broadcast(
          currentRoom,
          JSON.stringify(
            masterInfo(store.getState().shared[currentRoom].master)
          )
        )
        break
      case 'selected_bins':
        store.dispatch({
          type: 'SET_SELECTED_BINS',
          payload: {
            master: ws.id,
            roomName: currentRoom,
            selectedBins: parsedMessage.payload
          }
        })
        wss.broadcast(
          currentRoom,
          JSON.stringify(
            selectedBins(store.getState().shared[currentRoom].selectedBins)
          )
        )
        break
      case 'axis_scaling_latest':
        if (!store.getState().shared[currentRoom]) {
          console.error(`Room ${currentRoom} not found`);
          return;
        }
        
        if (!parsedMessage.payload?.axis) {
          console.error('Invalid axis_scaling_latest payload:', parsedMessage.payload);
          return;
        }

        // Only update store if master is sending
        // eslint-disable-next-line no-case-declarations
        const isMasterUser = store.getState().shared[currentRoom].master === ws.id;
        if (isMasterUser) {
          store.dispatch({
            type: 'AXIS_SCALING',
            payload: {
              roomName: currentRoom,
              axis: parsedMessage.payload.axis,
              positions: parsedMessage.payload.positions
            }
          });
        }

        // Always broadcast the current master positions
        // eslint-disable-next-line no-case-declarations
        const currentState = store.getState().shared[currentRoom];
        wss.broadcast(
          currentRoom,
          JSON.stringify(
            axisScalingLatest(
              currentState.latestAxis,
              currentState.axisPositions,
              parsedMessage.payload.initiator
            )
          ),
          ws
        );
        break
      case 'axis_scaling_confirm':
        if (confirmationTimeout) {
          clearTimeout(confirmationTimeout);
          confirmationTimeout = null;
        }
        break
      case 'histogram':
        store.dispatch({
          type: 'SET_HISTOGRAM',
          payload: {
            master: ws.id,
            roomName: currentRoom,
            histogram: parsedMessage.payload
          }
        })
        wss.broadcast(
          currentRoom,
          JSON.stringify(
            histogram(store.getState().shared[currentRoom].histogram)
          )
        )
        break
      case 'syncData':
        syncData(currentRoom)
        break
      case 'otherData':
        store.dispatch({
          type: 'SET_OTHER_DATA',
          payload: {
            master: ws.id,
            roomName: currentRoom,
            otherData: parsedMessage.payload.otherData
          }
        })
        wss.broadcast(
          currentRoom,
          JSON.stringify(
            otherData(store.getState().shared[currentRoom].otherData)
          )
        )
        break
      case 'createRoom':
        console.log('Creating room with username:', ws.username)
        if (store.getState().shared[parsedMessage.payload]) {
          ws.send(
            JSON.stringify({
              type: 'created',
              payload: { error: 'Room already exists.' }
            })
          )
          break
        } else {
          createRoom(parsedMessage.payload, ws)
          ws.send(
            JSON.stringify({
              type: 'created',
              payload: { roomName: parsedMessage.payload }
            })
          )
        }
        clientRooms.set(ws.id, parsedMessage.payload);
        break
      case 'joinRoom':
        console.log('Joining room with username:', ws.username)
        if (store.getState().shared[parsedMessage.payload]) {
          joinRoom(parsedMessage.payload, ws)
          ws.send(
            JSON.stringify({
              type: 'joined',
              payload: {
                roomName: parsedMessage.payload
              }
            })
          )
        } else {
          ws.send(
            JSON.stringify({
              type: 'joined',
              payload: {
                error: 'Room does not exist.'
              }
            })
          )
        }
        clientRooms.set(ws.id, parsedMessage.payload);
        break
      case 'leaveRoom':
        leaveRoom(parsedMessage.payload, ws)
        ws.send(
          JSON.stringify({
            type: 'left',
            payload: parsedMessage.payload
          })
        )
        clientRooms.set(ws.id, 'defaultRoom');
        break
      case 'setUsername':
        ws.username = parsedMessage.payload
        store.dispatch({
          type: 'UPDATE_USER',
          payload: {
            roomName: currentRoom,
            id: ws.id,
            username: parsedMessage.payload,
            color: ws.color,
            position: [0, 0, 0],
            rotation: [0, 0, 0, 0]
          }
        })
        wss.broadcast(
          currentRoom,
          JSON.stringify(
            clientsInfo(store.getState().shared[currentRoom].clients)
          )
        )
        break
      case 'request_sync':
        const roomState = store.getState().shared[currentRoom];
        if (roomState.master) {
          ws.send(JSON.stringify({
            type: 'axisScalingLatest',
            payload: {
              axis: 'FORCE_SYNC',
              positions: roomState.axisPositions,
              initiator: roomState.master
            }
          }));
        }
        break
    }
  })

  ws.on('close', () => {
    if (confirmationTimeout) clearTimeout(confirmationTimeout);
    clientRooms.delete(ws.id);
    let currentRoom = getCurrentRoomForUser(ws)
    store.dispatch({
      type: 'REMOVE_USER',
      payload: { id: ws.id, roomName: currentRoom }
    })

    if (store.getState().shared[currentRoom].master === ws.id) {
      store.dispatch({
        type: 'DEASSIGN_MASTER',
        payload: { master: ws.id, roomName: currentRoom }
      })
    }

    ws.send(
      JSON.stringify(clientsInfo(store.getState().shared[currentRoom].clients))
    )

    console.log('Client ' + ws.id + ' has disconnected')
    console.log(store.getState())
  })
})

wss.broadcast = (roomName, msg, excludeClient = null) => {
  wss.clients.forEach(function each(client) {
    if (
      roomName !== 'defaultRoom' &&
      store.getState().shared[roomName].clients?.some((x) => x.id === client.id) &&
      (!excludeClient || client.id !== excludeClient.id)
    ) {
      client.send(msg)
    }
  })
}

const server = app.listen(8443)

console.log('NDMSPC Shared Server established...')

server.on('upgrade', (request, socket, head) => {
  wss.handleUpgrade(request, socket, head, (socket) => {
    wss.emit('connection', socket, request)
  })
})

export default app
