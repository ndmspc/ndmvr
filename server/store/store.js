import reduxToolkit from '@reduxjs/toolkit'
import sharedReducer from './reducers/sharedReducer.js'
const { configureStore } = reduxToolkit

const store = configureStore({
  reducer: {
    shared: sharedReducer
  }
})

export default store
