const initialRoomState = {
  clients: [],
  master: null,
  selectedBins: [],
  axisPositions: {
    xOffset: 0,
    yOffset: 0,
    zOffset: 0
  },
  latestAxis: null,
  histogram: null,
  otherData: null,
  pendingChanges: [],
  lastConfirmedChange: null
}

const initialState = {
  defaultRoom: initialRoomState
}

export default function reducer(state = initialState, action) {
  let roomName = 'defaultRoom',
    userId

  switch (action.type) {
    case 'CREATE_ROOM':
      roomName = action.payload.roomName
      return {
        ...state,
        [roomName]: Object.assign({}, initialRoomState)
      }
    case 'ADD_USER':
      roomName = action.payload.roomName
      userId = action.payload.id
      console.log(`User ${userId} added to roomName ${roomName} with username ${action.payload.username}`)
      return {
        ...state,
        [roomName]: {
          ...state[roomName],
          clients: [
            ...state[roomName].clients,
            {
              id: userId,
              position: [0, 0, 0],
              rotation: [0, 0, 0, 0],
              color: action.payload.color || '#FFFFFF',
              username: action.payload.username
            }
          ]
        }
      }
    case 'UPDATE_USER':
      roomName = action.payload.roomName
      userId = action.payload.id
      return {
        ...state,
        [roomName]: {
          ...state[roomName],
          clients: state[roomName]?.clients?.map((client) =>
            client.id === userId
              ? {
                  ...client,
                  position: action.payload.position,
                  rotation: action.payload.rotation,
                  color: action.payload.color,
                  username: action.payload.username
                }
              : client
          )
        }
      }
    case 'REMOVE_USER':
      roomName = action.payload.roomName
      userId = action.payload.id
      console.log(`User ${userId} removed from roomName ${roomName}`)
      return {
        ...state,
        [roomName]: {
          ...state[roomName],
          clients: state[roomName].clients?.filter(
            (client) => client.id !== userId
          )
        }
      }
    case 'ASSIGN_MASTER':
      roomName = action.payload.roomName
      userId = action.payload.master
      console.log(`User ${userId} set to master for roomName ${roomName}`)
      return {
        ...state,
        [roomName]: {
          ...state[roomName],
          master: userId,
          selectedBins: [],
          latestAxis: null,
          axisPositions: {
            xOffset: 0,
            yOffset: 0,
            zOffset: 0
          },
          pendingChanges: [],
          lastConfirmedChange: null
        }
      }
    case 'DEASSIGN_MASTER':
      roomName = action.payload.roomName
      userId = action.payload.master
      if (
        state[roomName].master &&
        state[roomName].master === action.payload.master
      ) {
        console.log(`Master has been deassigned from roomName ${roomName}`)
        return {
          ...state,
          [roomName]: {
            ...state[roomName],
            master: null,
            selectedBins: [],
            latestAxis: state[roomName].latestAxis,
            axisPositions: state[roomName].axisPositions,
            histogram: null,
            otherData: null
          }
        }
      }
      return state
    case 'SET_SELECTED_BINS':
      roomName = action.payload.roomName
      console.log(
        `Setting selected bins (${action.payload.selectedBins}) for roomName ${action.payload.roomName}`
      )
      if (
        state[action.payload.roomName].master &&
        state[action.payload.roomName].master !== action.payload.master
      ) {
        return state
      } else {
        return {
          ...state,
          [roomName]: {
            ...state[roomName],
            selectedBins: action.payload.selectedBins
          }
        }
      }
    case 'AXIS_SCALING':
      if (!action.payload.axis || !action.payload.roomName) {
        console.error('Invalid axis scaling payload:', action.payload);
        return state;
      }
      
      console.log(`[Reducer] Processing axis scaling:`, {
        axis: action.payload.axis,
        room: action.payload.roomName
      });

      // eslint-disable-next-line no-case-declarations
      const currentPositions = state[action.payload.roomName].axisPositions;
      // eslint-disable-next-line no-case-declarations
      let newPositions = { ...currentPositions };
      
      switch(action.payload.axis) {
        case 'X_UP':
          newPositions.yOffset = currentPositions.yOffset + 1;
          break;
        case 'X_DOWN':
          newPositions.yOffset = currentPositions.yOffset - 1;
          break;
        case 'Y_UP':
          newPositions.xOffset = currentPositions.xOffset + 1;
          break;
        case 'Y_DOWN':
          newPositions.xOffset = currentPositions.xOffset - 1;
          break;
        case 'Z_UP':
          newPositions.zOffset = currentPositions.zOffset + 1;
          break;
        case 'Z_DOWN':
          newPositions.zOffset = currentPositions.zOffset - 1;
          break;
      }
      
      console.log(`[Reducer] Updated positions:`, {
        newPositions,
        latestAxis: action.payload.axis
      });
      
      return {
        ...state,
        [action.payload.roomName]: {
          ...state[action.payload.roomName],
          axisPositions: newPositions,
          latestAxis: action.payload.axis
        }
      };
    case 'SET_HISTOGRAM':
      console.log(
        `Setting histogram data in roomName ${action.payload.roomName}`,
        action.payload.histogram
      )
      if (
        state[action.payload.roomName].master &&
        state[action.payload.roomName].master !== action.payload.master
      ) {
        return state
      } else {
        return {
          ...state,
          [action.payload.roomName]: {
            ...state[action.payload.roomName],
            histogram: action.payload.histogram
          }
        }
      }
    case 'SET_OTHER_DATA':
      console.log(
        `Setting other data in roomName ${action.payload.roomName}`,
        action.payload.data
      )
      if (
        state[action.payload.roomName].master &&
        state[action.payload.roomName].master !== action.payload.master
      ) {
        return state
      } else {
        return {
          ...state,
          [action.payload.roomName]: {
            ...state[action.payload.roomName],
            otherData: action.payload.data
          }
        }
      }
    case 'CONFIRM_AXIS_CHANGE':
      return {
        ...state,
        [action.payload.roomName]: {
          ...state[action.payload.roomName],
          pendingChanges: state[action.payload.roomName].pendingChanges
            .filter(change => change.id !== action.payload.changeId),
          lastConfirmedChange: action.payload.changeId
        }
      }
    default:
      return state
  }
}
