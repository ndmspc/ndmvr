## Modules

<dl>
<dt><a href="#module_HistogramReactFactory">HistogramReactFactory</a></dt>
<dd></dd>
<dt><a href="#module_CameraService">CameraService</a></dt>
<dd></dd>
<dt><a href="#module_JsrootService">JsrootService</a></dt>
<dd></dd>
<dt><a href="#module_NdmVrStorageService">NdmVrStorageService</a></dt>
<dd></dd>
<dt><a href="#module_ThemeProvider">ThemeProvider</a></dt>
<dd></dd>
<dt><a href="#module_BinSubject">BinSubject</a></dt>
<dd></dd>
<dt><a href="#module_CameraSubject">CameraSubject</a></dt>
<dd></dd>
<dt><a href="#module_HistogramSubject">HistogramSubject</a></dt>
<dd></dd>
<dt><a href="#module_JsrootSubject">JsrootSubject</a></dt>
<dd></dd>
<dt><a href="#module_keyboardController">keyboardController</a></dt>
<dd><p>Modul obsahuje implementované metódy, ktoré reagujú na vstupy používateľa.
Funkcie sú určené pre komponenty slúžiace na definovanie listenerov pre klávesnicu počítača.</p>
</dd>
<dt><a href="#module_oculusController">oculusController</a></dt>
<dd><p>Modul obsahuje implementované metódy, ktoré reagujú na vstupy používateľa.
Funkcie sú určené pre komponenty slúžiace na definovanie listenerov pre ovládač Oculusu.</p>
</dd>
<dt><a href="#module_cursorEventAframeComponent">cursorEventAframeComponent</a></dt>
<dd><p>A-Frame interakčný komponent pridávajúci funkcionalitu entite, ktorá spracuváva prieniky medzi entitou a raycastrom používateľa.</p>
</dd>
<dt><a href="#module_histogramAframeComponent">histogramAframeComponent</a></dt>
<dd><p>A-Frame interakčný komponent pridávajúci funkcionalitu entite histogram, ktorá umožňuje zmenu atribútov entity.</p>
</dd>
<dt><a href="#module_labelHandlerAframeComponent">labelHandlerAframeComponent</a></dt>
<dd><p>A-Frame komponent zabezpečí oneskorené aktualizovanie označenia na entite.</p>
</dd>
<dt><a href="#module_leftOculusAframeComponent">leftOculusAframeComponent</a></dt>
<dd><p>A-Frame interakčný komponent zabezpečí možnosť používateľa interagovať s ľavým ovládačom zariadenia Oculus.</p>
</dd>
<dt><a href="#module_rightOculusAframeComponent">rightOculusAframeComponent</a></dt>
<dd><p>A-Frame interakčný komponent zabezpečí možnosť používateľa interagovať s pravým ovládačom zariadenia Oculus.</p>
</dd>
<dt><a href="#module_th2AframeComponent">th2AframeComponent</a></dt>
<dd><p>A-Frame interakčný komponent pridávajúci funkcionalitu entite binu TH2 histogram, ktorá umožňuje zmenu atribútov entity.</p>
</dd>
<dt><a href="#module_th2AframeComponent">th2AframeComponent</a></dt>
<dd><p>A-Frame interakčný komponent pridávajúci funkcionalitu entite binu TH3 histogram, ktorá umožňuje zmenu atribútov entity.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#JsrootHistogram">JsrootHistogram(histogram, projectionAxes, projections, projIndex)</a></dt>
<dd><p>React komponent pre zobrazenie bočného panelu s JSROOT projekciami.
Zobrazenie SVG elementu s histogramom a potrebným formulárom pre nastavenie atribútov.</p>
</dd>
<dt><a href="#NdmVrCamera">NdmVrHelp()</a></dt>
<dd><p>React komponent pre vytvorenie kamery a jej menežment.</p>
</dd>
<dt><a href="#NdmVrHistogram3D">NdmVrHistogram(name, histogram, histoSection, projections, range, theme)</a></dt>
<dd><p>React komponent pre vytvorenie histogramu.</p>
</dd>
<dt><a href="#NdmVrScene">NdmVrScene(data, info)</a></dt>
<dd><p>React komponent pre vytvorenie VR scény so všetkými potrebnými komponentami.</p>
</dd>
</dl>

<a name="module_HistogramReactFactory"></a>

## HistogramReactFactory

* [HistogramReactFactory](#module_HistogramReactFactory)
    * [module.exports](#exp_module_HistogramReactFactory--module.exports) ⏏
        * [new module.exports(uniqueName, histogram, section, range, projections, theme)](#new_module_HistogramReactFactory--module.exports_new)
        * [.getXOffset](#module_HistogramReactFactory--module.exports+getXOffset) ⇒ <code>number</code>
        * [.getYOffset](#module_HistogramReactFactory--module.exports+getYOffset) ⇒ <code>number</code>
        * [.getZOffset](#module_HistogramReactFactory--module.exports+getZOffset) ⇒ <code>number</code>
        * [.getRange](#module_HistogramReactFactory--module.exports+getRange) ⇒ <code>number</code>
        * [.initializeFactory(uniqueName, histogram, section, range, projections, theme)](#module_HistogramReactFactory--module.exports+initializeFactory) ⇒ <code>void</code>
        * [.createTH3Histogram()](#module_HistogramReactFactory--module.exports+createTH3Histogram) ⇒ <code>Object</code>
        * [.createTH2Histogram(projectionFunction)](#module_HistogramReactFactory--module.exports+createTH2Histogram) ⇒ <code>Object</code>

<a name="exp_module_HistogramReactFactory--module.exports"></a>

### module.exports ⏏
HistogramReactFactory - Histogram generator

**Kind**: Exported class
<a name="new_module_HistogramReactFactory--module.exports_new"></a>

#### new module.exports(uniqueName, histogram, section, range, projections, theme)
Inicializácia objektu pre vytváranie elementov histogramu.


| Param | Type | Description |
| --- | --- | --- |
| uniqueName | <code>string</code> | Identifikátor histogramu |
| histogram | <code>Object</code> | Objekt histogramu určený na vizualizáciu |
| section | <code>Object</code> | Objekt definujúci ofsety a rozsah histogramu |
| range | <code>number</code> | Rozsah, ak sa nedefinujú ofsety ale len rozsah |
| projections | <code>Object</code> | Objekt definujúci súborovú štruktúru s projekciami |
| theme | <code>string</code> | string Definujúci názov témy, ktorá má byť použitá pri vizualizovaní |

<a name="module_HistogramReactFactory--module.exports+getXOffset"></a>

#### module.exports.getXOffset ⇒ <code>number</code>
Vracia atribút objektu offset x.

**Kind**: instance property of [<code>module.exports</code>](#exp_module_HistogramReactFactory--module.exports)
**Returns**: <code>number</code> - offsetX - Offset na osi x
<a name="module_HistogramReactFactory--module.exports+getYOffset"></a>

#### module.exports.getYOffset ⇒ <code>number</code>
Vracia atribút objektu offset y.

**Kind**: instance property of [<code>module.exports</code>](#exp_module_HistogramReactFactory--module.exports)
**Returns**: <code>number</code> - offsetX - Offset na osi y
<a name="module_HistogramReactFactory--module.exports+getZOffset"></a>

#### module.exports.getZOffset ⇒ <code>number</code>
Vracia atribút objektu offset y.

**Kind**: instance property of [<code>module.exports</code>](#exp_module_HistogramReactFactory--module.exports)
**Returns**: <code>number</code> - offsetX - Offset na osi y
<a name="module_HistogramReactFactory--module.exports+getRange"></a>

#### module.exports.getRange ⇒ <code>number</code>
Vracia atribút objektu range.

**Kind**: instance property of [<code>module.exports</code>](#exp_module_HistogramReactFactory--module.exports)
**Returns**: <code>number</code> - range - Vracia rozsah sekcie
<a name="module_HistogramReactFactory--module.exports+initializeFactory"></a>

#### module.exports.initializeFactory(uniqueName, histogram, section, range, projections, theme) ⇒ <code>void</code>
Inicializácia objektu pre vytváranie elementov histogramu.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_HistogramReactFactory--module.exports)

| Param | Type | Description |
| --- | --- | --- |
| uniqueName | <code>string</code> | Identifikátor histogramu |
| histogram | <code>Object</code> | Objekt histogramu určený na vizualizáciu |
| section | <code>Object</code> | Objekt definujúci ofsety a rozsah histogramu |
| range | <code>number</code> | Rozsah, ak sa nedefinujú ofsety ale len rozsah |
| projections | <code>Object</code> | Objekt definujúci súborovú štruktúru s projekciami |
| theme | <code>string</code> | String definujúci názov témy, ktorá má byť použitá pri vizualizovaní |

<a name="module_HistogramReactFactory--module.exports+createTH3Histogram"></a>

#### module.exports.createTH3Histogram() ⇒ <code>Object</code>
Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH3.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_HistogramReactFactory--module.exports)
**Returns**: <code>Object</code> - elements - Vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR
<a name="module_HistogramReactFactory--module.exports+createTH2Histogram"></a>

#### module.exports.createTH2Histogram(projectionFunction) ⇒ <code>Object</code>
Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH2.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_HistogramReactFactory--module.exports)
**Returns**: <code>Object</code> - elements - vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR

| Param | Type | Description |
| --- | --- | --- |
| projectionFunction | <code>string</code> | parameter určuje, ktorý vizualizačný mód bude použitý pri vytvorení entít |

<a name="module_CameraService"></a>

## CameraService

* [CameraService](#module_CameraService)
    * [.CameraService](#module_CameraService.CameraService)
        * [.verticalMoveCamera](#module_CameraService.CameraService+verticalMoveCamera) ⇒ <code>void</code>
        * [.horizontalMoveCamera](#module_CameraService.CameraService+horizontalMoveCamera) ⇒ <code>void</code>
        * [.setCameraPosition](#module_CameraService.CameraService+setCameraPosition) ⇒ <code>void</code>
        * [.setPredefinedDownPosition](#module_CameraService.CameraService+setPredefinedDownPosition) ⇒ <code>void</code>
        * [.setPredefinedUpPosition](#module_CameraService.CameraService+setPredefinedUpPosition) ⇒ <code>void</code>
        * [.setPredefinedRightPosition](#module_CameraService.CameraService+setPredefinedRightPosition) ⇒ <code>void</code>
        * [.setPredefinedLeftPosition](#module_CameraService.CameraService+setPredefinedLeftPosition) ⇒ <code>void</code>
        * [.setPredefinedDownPositionWithOffset](#module_CameraService.CameraService+setPredefinedDownPositionWithOffset) ⇒ <code>void</code>
        * [.setPredefinedUpPositionWithOffset](#module_CameraService.CameraService+setPredefinedUpPositionWithOffset) ⇒ <code>void</code>
        * [.setPredefinedRightPositionWithOffset](#module_CameraService.CameraService+setPredefinedRightPositionWithOffset) ⇒ <code>void</code>
        * [.setPredefinedLeftPositionWithOffset](#module_CameraService.CameraService+setPredefinedLeftPositionWithOffset) ⇒ <code>void</code>

<a name="module_CameraService.CameraService"></a>

### CameraService.CameraService
Servis pre ovládanie a manipuláciu pozície kamery.

**Kind**: static class of [<code>CameraService</code>](#module_CameraService)

* [.CameraService](#module_CameraService.CameraService)
    * [.verticalMoveCamera](#module_CameraService.CameraService+verticalMoveCamera) ⇒ <code>void</code>
    * [.horizontalMoveCamera](#module_CameraService.CameraService+horizontalMoveCamera) ⇒ <code>void</code>
    * [.setCameraPosition](#module_CameraService.CameraService+setCameraPosition) ⇒ <code>void</code>
    * [.setPredefinedDownPosition](#module_CameraService.CameraService+setPredefinedDownPosition) ⇒ <code>void</code>
    * [.setPredefinedUpPosition](#module_CameraService.CameraService+setPredefinedUpPosition) ⇒ <code>void</code>
    * [.setPredefinedRightPosition](#module_CameraService.CameraService+setPredefinedRightPosition) ⇒ <code>void</code>
    * [.setPredefinedLeftPosition](#module_CameraService.CameraService+setPredefinedLeftPosition) ⇒ <code>void</code>
    * [.setPredefinedDownPositionWithOffset](#module_CameraService.CameraService+setPredefinedDownPositionWithOffset) ⇒ <code>void</code>
    * [.setPredefinedUpPositionWithOffset](#module_CameraService.CameraService+setPredefinedUpPositionWithOffset) ⇒ <code>void</code>
    * [.setPredefinedRightPositionWithOffset](#module_CameraService.CameraService+setPredefinedRightPositionWithOffset) ⇒ <code>void</code>
    * [.setPredefinedLeftPositionWithOffset](#module_CameraService.CameraService+setPredefinedLeftPositionWithOffset) ⇒ <code>void</code>

<a name="module_CameraService.CameraService+verticalMoveCamera"></a>

#### cameraService.verticalMoveCamera ⇒ <code>void</code>
Zmena vertikálnej polohy kamery.

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)

| Param | Type | Description |
| --- | --- | --- |
| moveUp | <code>boolean</code> | Ak je True kamera vyššie, ak je False kamera nižšie |
| speed | <code>number</code> | Rýchlosť zmeny pozície kamery |

<a name="module_CameraService.CameraService+horizontalMoveCamera"></a>

#### cameraService.horizontalMoveCamera ⇒ <code>void</code>
Zmena horizontálnej polohy kamery.

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)

| Param | Type | Description |
| --- | --- | --- |
| axis | <code>string</code> | Os po ktorej je realizovaný pohyb kamery |
| increment | <code>number</code> | Hodnota posunu kamery |
| speed | <code>number</code> | Rýchlosť zmeny pozície kamery |

<a name="module_CameraService.CameraService+setCameraPosition"></a>

#### cameraService.setCameraPosition ⇒ <code>void</code>
Zmena vertikálnej polohy kamery podľa ofsetov.

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)

| Param | Type | Description |
| --- | --- | --- |
| offsets | <code>Object</code> | Ofsety zobrazenej sekcie |

<a name="module_CameraService.CameraService+setPredefinedDownPosition"></a>

#### cameraService.setPredefinedDownPosition ⇒ <code>void</code>
Zmena polohy kamery podľa predefinovanej pozície.

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)
<a name="module_CameraService.CameraService+setPredefinedUpPosition"></a>

#### cameraService.setPredefinedUpPosition ⇒ <code>void</code>
Zmena polohy kamery podľa predefinovanej pozície

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)
<a name="module_CameraService.CameraService+setPredefinedRightPosition"></a>

#### cameraService.setPredefinedRightPosition ⇒ <code>void</code>
Zmena polohy kamery podľa predefinovanej pozície

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)
<a name="module_CameraService.CameraService+setPredefinedLeftPosition"></a>

#### cameraService.setPredefinedLeftPosition ⇒ <code>void</code>
Zmena polohy kamery podľa predefinovanej pozície

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)
<a name="module_CameraService.CameraService+setPredefinedDownPositionWithOffset"></a>

#### cameraService.setPredefinedDownPositionWithOffset ⇒ <code>void</code>
Zmena polohy kamery podľa predefinovanej pozície

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)
<a name="module_CameraService.CameraService+setPredefinedUpPositionWithOffset"></a>

#### cameraService.setPredefinedUpPositionWithOffset ⇒ <code>void</code>
Zmena polohy kamery podľa predefinovanej pozície

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)
<a name="module_CameraService.CameraService+setPredefinedRightPositionWithOffset"></a>

#### cameraService.setPredefinedRightPositionWithOffset ⇒ <code>void</code>
Zmena polohy kamery podľa predefinovanej pozície

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)
<a name="module_CameraService.CameraService+setPredefinedLeftPositionWithOffset"></a>

#### cameraService.setPredefinedLeftPositionWithOffset ⇒ <code>void</code>
Zmena polohy kamery podľa predefinovanej pozície

**Kind**: instance property of [<code>CameraService</code>](#module_CameraService.CameraService)
<a name="module_JsrootService"></a>

## JsrootService

* [JsrootService](#module_JsrootService)
    * [~JsrootService](#module_JsrootService..JsrootService)
        * [.jsrootLibrary](#module_JsrootService..JsrootService+jsrootLibrary) ⇒ <code>void</code>
        * [.createTH1Projection(projectionAxis, info, idTargetElm, axisArray)](#module_JsrootService..JsrootService+createTH1Projection) ⇒ <code>void</code>
        * [.openTH1Projection(projectionAxis, info, idTargetElm, axisArray, projections, projIndex)](#module_JsrootService..JsrootService+openTH1Projection) ⇒ <code>void</code>
        * [.displayImageOfProjection(idSourceElement, idTargetElement, width, height)](#module_JsrootService..JsrootService+displayImageOfProjection) ⇒ <code>void</code>

<a name="module_JsrootService..JsrootService"></a>

### JsrootService~JsrootService
Servis pre vytváranie JSROOT projekcií.

**Kind**: inner class of [<code>JsrootService</code>](#module_JsrootService)

* [~JsrootService](#module_JsrootService..JsrootService)
    * [.jsrootLibrary](#module_JsrootService..JsrootService+jsrootLibrary) ⇒ <code>void</code>
    * [.createTH1Projection(projectionAxis, info, idTargetElm, axisArray)](#module_JsrootService..JsrootService+createTH1Projection) ⇒ <code>void</code>
    * [.openTH1Projection(projectionAxis, info, idTargetElm, axisArray, projections, projIndex)](#module_JsrootService..JsrootService+openTH1Projection) ⇒ <code>void</code>
    * [.displayImageOfProjection(idSourceElement, idTargetElement, width, height)](#module_JsrootService..JsrootService+displayImageOfProjection) ⇒ <code>void</code>

<a name="module_JsrootService..JsrootService+jsrootLibrary"></a>

#### jsrootService.jsrootLibrary ⇒ <code>void</code>
Funkcia zabezpečí nastavenie objektu JSROOT servisu.

**Kind**: instance property of [<code>JsrootService</code>](#module_JsrootService..JsrootService)

| Param | Type | Description |
| --- | --- | --- |
| JSROOT | <code>Object</code> | Object JSROOT |

<a name="module_JsrootService..JsrootService+createTH1Projection"></a>

#### jsrootService.createTH1Projection(projectionAxis, info, idTargetElm, axisArray) ⇒ <code>void</code>
Funkcia pre vytvorenie TH1 projekcie.

**Kind**: instance method of [<code>JsrootService</code>](#module_JsrootService..JsrootService)

| Param | Type | Description |
| --- | --- | --- |
| projectionAxis | <code>string</code> | Hlavná os podľa, ktorej sa vytvorí projekcia |
| info | <code>Object</code> | Objekt obsahujúci histogramy a objekt obsahujúci informácie o zvolenom bine |
| idTargetElm | <code>string</code> | Identifikátor cieľovej entity, do ktorej sa vykreslí výsledná projekcia |
| axisArray | <code>Array</code> | Pole osí, obsahuje 2 osi na základe ktorých sa projekcia vytvorí |

<a name="module_JsrootService..JsrootService+openTH1Projection"></a>

#### jsrootService.openTH1Projection(projectionAxis, info, idTargetElm, axisArray, projections, projIndex) ⇒ <code>void</code>
Funkcia pre prečítanie objektu projekcie z adresárovej štruktúry získanej z root súboru.

**Kind**: instance method of [<code>JsrootService</code>](#module_JsrootService..JsrootService)

| Param | Type | Description |
| --- | --- | --- |
| projectionAxis | <code>string</code> | Hlavná os podľa, ktorej sa vytvorí projekcia |
| info | <code>Object</code> | Objekt obsahujúci histogramy a objekt obsahujúci informácie o zvolenom bine |
| idTargetElm | <code>string</code> | Identifikátor cieľovej entity, do ktorej sa vykreslí výsledná projekcia |
| axisArray | <code>Array</code> | Pole osí, obsahuje 2 osi na základe ktorých sa projekcia vytvorí |
| projections | <code>Object</code> | Adresárová štruktúra obsahujúca projekcie |
| projIndex | <code>number</code> | Index projekcie, ktorá sa má vykresliť |

<a name="module_JsrootService..JsrootService+displayImageOfProjection"></a>

#### jsrootService.displayImageOfProjection(idSourceElement, idTargetElement, width, height) ⇒ <code>void</code>
Funkcia získa aktuálny náhľad projekcie SVG elementu a zobrazí ju na panely vo VR

**Kind**: instance method of [<code>JsrootService</code>](#module_JsrootService..JsrootService)

| Param | Type | Description |
| --- | --- | --- |
| idSourceElement | <code>string</code> | Identifikátor zdrojového elementu obsahujúceho SVG projekciu |
| idTargetElement | <code>string</code> | Identifikátor cieľového elementu vo VR, kde bude vložený náhľad projekcie |
| width | <code>number</code> | Šírka pre získanie náhľadu projekcie |
| height | <code>number</code> | Výška pre získanie náhľadu projekcie |

<a name="module_NdmVrStorageService"></a>

## NdmVrStorageService

* [NdmVrStorageService](#module_NdmVrStorageService)
    * [.NdmVrStorageService](#module_NdmVrStorageService.NdmVrStorageService)
        * [new exports.NdmVrStorageService()](#new_module_NdmVrStorageService.NdmVrStorageService_new)
        * [.containThisBin](#module_NdmVrStorageService.NdmVrStorageService+containThisBin) ⇒ <code>boolean</code>
        * [.storeBinToLocalStorage](#module_NdmVrStorageService.NdmVrStorageService+storeBinToLocalStorage) ⇒ <code>void</code>
        * [.deleteBinFromLocalStorage](#module_NdmVrStorageService.NdmVrStorageService+deleteBinFromLocalStorage) ⇒ <code>void</code>
        * [.getBinsFromLocalStorage](#module_NdmVrStorageService.NdmVrStorageService+getBinsFromLocalStorage) ⇒ <code>Array</code>
        * [.storeCurrentBinToLocalStorage(binData)](#module_NdmVrStorageService.NdmVrStorageService+storeCurrentBinToLocalStorage) ⇒ <code>void</code>
        * [.loadProperty(property)](#module_NdmVrStorageService.NdmVrStorageService+loadProperty) ⇒ <code>Object</code>
        * [.storeProperty(property, object)](#module_NdmVrStorageService.NdmVrStorageService+storeProperty) ⇒ <code>void</code>
        * [.storeTH2Offsets(xOffset, yOffset, range)](#module_NdmVrStorageService.NdmVrStorageService+storeTH2Offsets) ⇒ <code>void</code>
        * [.storeTH3Offsets(xOffset, yOffset, zOffset, range)](#module_NdmVrStorageService.NdmVrStorageService+storeTH3Offsets) ⇒ <code>void</code>
        * [.storeOffsets(section)](#module_NdmVrStorageService.NdmVrStorageService+storeOffsets) ⇒ <code>void</code>
        * [.storeFilePath(filePath)](#module_NdmVrStorageService.NdmVrStorageService+storeFilePath) ⇒ <code>void</code>
        * [.loadFilePath()](#module_NdmVrStorageService.NdmVrStorageService+loadFilePath) ⇒ <code>string</code>
        * [.loadTH2Offsets()](#module_NdmVrStorageService.NdmVrStorageService+loadTH2Offsets) ⇒ <code>Object</code>
        * [.loadTH3Offsets()](#module_NdmVrStorageService.NdmVrStorageService+loadTH3Offsets) ⇒ <code>Object</code>
        * [.loadOffsets(type)](#module_NdmVrStorageService.NdmVrStorageService+loadOffsets) ⇒ <code>Object</code>

<a name="module_NdmVrStorageService.NdmVrStorageService"></a>

### NdmVrStorageService.NdmVrStorageService
Servis pre ovládanie lokálneho úložiska.
Ukladanie a načítavanie dát o ofsetoch a histograme.

**Kind**: static class of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService)

* [.NdmVrStorageService](#module_NdmVrStorageService.NdmVrStorageService)
    * [new exports.NdmVrStorageService()](#new_module_NdmVrStorageService.NdmVrStorageService_new)
    * [.containThisBin](#module_NdmVrStorageService.NdmVrStorageService+containThisBin) ⇒ <code>boolean</code>
    * [.storeBinToLocalStorage](#module_NdmVrStorageService.NdmVrStorageService+storeBinToLocalStorage) ⇒ <code>void</code>
    * [.deleteBinFromLocalStorage](#module_NdmVrStorageService.NdmVrStorageService+deleteBinFromLocalStorage) ⇒ <code>void</code>
    * [.getBinsFromLocalStorage](#module_NdmVrStorageService.NdmVrStorageService+getBinsFromLocalStorage) ⇒ <code>Array</code>
    * [.storeCurrentBinToLocalStorage(binData)](#module_NdmVrStorageService.NdmVrStorageService+storeCurrentBinToLocalStorage) ⇒ <code>void</code>
    * [.loadProperty(property)](#module_NdmVrStorageService.NdmVrStorageService+loadProperty) ⇒ <code>Object</code>
    * [.storeProperty(property, object)](#module_NdmVrStorageService.NdmVrStorageService+storeProperty) ⇒ <code>void</code>
    * [.storeTH2Offsets(xOffset, yOffset, range)](#module_NdmVrStorageService.NdmVrStorageService+storeTH2Offsets) ⇒ <code>void</code>
    * [.storeTH3Offsets(xOffset, yOffset, zOffset, range)](#module_NdmVrStorageService.NdmVrStorageService+storeTH3Offsets) ⇒ <code>void</code>
    * [.storeOffsets(section)](#module_NdmVrStorageService.NdmVrStorageService+storeOffsets) ⇒ <code>void</code>
    * [.storeFilePath(filePath)](#module_NdmVrStorageService.NdmVrStorageService+storeFilePath) ⇒ <code>void</code>
    * [.loadFilePath()](#module_NdmVrStorageService.NdmVrStorageService+loadFilePath) ⇒ <code>string</code>
    * [.loadTH2Offsets()](#module_NdmVrStorageService.NdmVrStorageService+loadTH2Offsets) ⇒ <code>Object</code>
    * [.loadTH3Offsets()](#module_NdmVrStorageService.NdmVrStorageService+loadTH3Offsets) ⇒ <code>Object</code>
    * [.loadOffsets(type)](#module_NdmVrStorageService.NdmVrStorageService+loadOffsets) ⇒ <code>Object</code>

<a name="new_module_NdmVrStorageService.NdmVrStorageService_new"></a>

#### new exports.NdmVrStorageService()
Nastavenie referencií a potrebných identifikátorov.
Načítanie poľa označených binov z lokálneho úložiska, ak nejaké sú.

<a name="module_NdmVrStorageService.NdmVrStorageService+containThisBin"></a>

#### ndmVrStorageService.containThisBin ⇒ <code>boolean</code>
Overenie, či je bin v poli označených binov.

**Kind**: instance property of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)
**Returns**: <code>boolean</code> - - Ak je True bin v poli označených binov, ak False bin nie je v poli označených binov

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahujúci informácie o bine |

<a name="module_NdmVrStorageService.NdmVrStorageService+storeBinToLocalStorage"></a>

#### ndmVrStorageService.storeBinToLocalStorage ⇒ <code>void</code>
Uloženie binu, uloženie do poľa označených binov.

**Kind**: instance property of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahujúci informácie o bine |

<a name="module_NdmVrStorageService.NdmVrStorageService+deleteBinFromLocalStorage"></a>

#### ndmVrStorageService.deleteBinFromLocalStorage ⇒ <code>void</code>
Odstránenie binu, vymazanie binu z poľa označených binov.

**Kind**: instance property of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahujúci informácie o bine |

<a name="module_NdmVrStorageService.NdmVrStorageService+getBinsFromLocalStorage"></a>

#### ndmVrStorageService.getBinsFromLocalStorage ⇒ <code>Array</code>
Načítanie všetkých binov z poľa označených binov.

**Kind**: instance property of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)
**Returns**: <code>Array</code> - - Pole s označenými binmi
<a name="module_NdmVrStorageService.NdmVrStorageService+storeCurrentBinToLocalStorage"></a>

#### ndmVrStorageService.storeCurrentBinToLocalStorage(binData) ⇒ <code>void</code>
Funkcia pre uloženie binu do zoznamu označených binov.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahujúci všetky dáta o zvolenom bine |

<a name="module_NdmVrStorageService.NdmVrStorageService+loadProperty"></a>

#### ndmVrStorageService.loadProperty(property) ⇒ <code>Object</code>
Funkcia pre načítanie aktuálnych informácií o histograme.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)
**Returns**: <code>Object</code> - - Objekt obsahujúci informácie o sekciách určitého histogramu

| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | Objekt označujúci typ histogramu, pre ktorý sa majú získať ofsety |

<a name="module_NdmVrStorageService.NdmVrStorageService+storeProperty"></a>

#### ndmVrStorageService.storeProperty(property, object) ⇒ <code>void</code>
Funkcia pre uloženie aktuálnych informácií o histograme.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)

| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | Objekt označujúci typ histogramu, pre ktorý sa majú získať ofsety |
| object | <code>Object</code> | Objekt obsahujúci informácie o sekciách určitého histogramu |

<a name="module_NdmVrStorageService.NdmVrStorageService+storeTH2Offsets"></a>

#### ndmVrStorageService.storeTH2Offsets(xOffset, yOffset, range) ⇒ <code>void</code>
Funkcia pre uloženie aktuálnych informácií o histograme TH2.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)

| Param | Type | Description |
| --- | --- | --- |
| xOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi X |
| yOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi Y |
| range | <code>number</code> | Hodnota reprezentujúca rozsah binov na osiach |

<a name="module_NdmVrStorageService.NdmVrStorageService+storeTH3Offsets"></a>

#### ndmVrStorageService.storeTH3Offsets(xOffset, yOffset, zOffset, range) ⇒ <code>void</code>
Funkcia pre uloženie aktuálnych informácií o histograme TH3.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)

| Param | Type | Description |
| --- | --- | --- |
| xOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi X |
| yOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi Y |
| zOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi Z |
| range | <code>number</code> | Hodnota reprezentujúca rozsah binov na osiach |

<a name="module_NdmVrStorageService.NdmVrStorageService+storeOffsets"></a>

#### ndmVrStorageService.storeOffsets(section) ⇒ <code>void</code>
Funkcia pre uloženie aktuálnych informácií o histograme.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)

| Param | Type | Description |
| --- | --- | --- |
| section | <code>Object</code> | Objekt obsahujúci ofsety a rozsah pre histogramy. Objekt obsahuje aj typ histogramu podľa ktorého sa uloží správna sekcia do lokálneho úložiska |

<a name="module_NdmVrStorageService.NdmVrStorageService+storeFilePath"></a>

#### ndmVrStorageService.storeFilePath(filePath) ⇒ <code>void</code>
Uloženie názvu root súboru.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)

| Param | Type | Description |
| --- | --- | --- |
| filePath | <code>string</code> | URL cesta k súboru |

<a name="module_NdmVrStorageService.NdmVrStorageService+loadFilePath"></a>

#### ndmVrStorageService.loadFilePath() ⇒ <code>string</code>
Načítanie názvu root súboru.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)
**Returns**: <code>string</code> - - URL root súboru
<a name="module_NdmVrStorageService.NdmVrStorageService+loadTH2Offsets"></a>

#### ndmVrStorageService.loadTH2Offsets() ⇒ <code>Object</code>
Načítanie informácií o TH2 histograme

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)
**Returns**: <code>Object</code> - - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom úložisku vracia null
<a name="module_NdmVrStorageService.NdmVrStorageService+loadTH3Offsets"></a>

#### ndmVrStorageService.loadTH3Offsets() ⇒ <code>Object</code>
Načítanie informácií o TH3 histograme.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)
**Returns**: <code>Object</code> - - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom úložisku vracia null
<a name="module_NdmVrStorageService.NdmVrStorageService+loadOffsets"></a>

#### ndmVrStorageService.loadOffsets(type) ⇒ <code>Object</code>
Načítanie informácií o histograme.

**Kind**: instance method of [<code>NdmVrStorageService</code>](#module_NdmVrStorageService.NdmVrStorageService)
**Returns**: <code>Object</code> - - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom úložisku vracia null

| Param | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | Typ histogramu podľa, ktorého sa získa správny objekt obsahujúci informácie o histograme |

<a name="module_ThemeProvider"></a>

## ThemeProvider

* [ThemeProvider](#module_ThemeProvider)
    * [module.exports](#exp_module_ThemeProvider--module.exports) ⏏
        * [new module.exports(theme)](#new_module_ThemeProvider--module.exports_new)
        * [.getAxisColor(axis)](#module_ThemeProvider--module.exports+getAxisColor) ⇒ <code>string</code>
        * [.getPrimaryAccentColor()](#module_ThemeProvider--module.exports+getPrimaryAccentColor) ⇒ <code>string</code>
        * [.getSecondaryAccentColor()](#module_ThemeProvider--module.exports+getSecondaryAccentColor) ⇒ <code>string</code>
        * [.getPrimaryFontColor()](#module_ThemeProvider--module.exports+getPrimaryFontColor) ⇒ <code>string</code>
        * [.getSecondaryFontColor()](#module_ThemeProvider--module.exports+getSecondaryFontColor) ⇒ <code>string</code>
        * [.getBinColor(content)](#module_ThemeProvider--module.exports+getBinColor) ⇒ <code>string</code>

<a name="exp_module_ThemeProvider--module.exports"></a>

### module.exports ⏏
Provider pre zabezpečenie všetkých potrebných farieb a štýlov.

**Kind**: Exported class
<a name="new_module_ThemeProvider--module.exports_new"></a>

#### new module.exports(theme)
Konštruktor pre nastavenie všetkých potrebných atribútov objektu.


| Param | Type | Description |
| --- | --- | --- |
| theme | <code>Object</code> | Objekt obsahujúci všetky potrebné palety a údaje o témach a fontoch |

<a name="module_ThemeProvider--module.exports+getAxisColor"></a>

#### module.exports.getAxisColor(axis) ⇒ <code>string</code>
Získanie kompetentnej farby pre entitu osi.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_ThemeProvider--module.exports)
**Returns**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu

| Param | Type | Description |
| --- | --- | --- |
| axis | <code>string</code> | Označenie osi |

<a name="module_ThemeProvider--module.exports+getPrimaryAccentColor"></a>

#### module.exports.getPrimaryAccentColor() ⇒ <code>string</code>
Získanie hlavnej farby pre zvýraznenie a označenie.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_ThemeProvider--module.exports)
**Returns**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu
<a name="module_ThemeProvider--module.exports+getSecondaryAccentColor"></a>

#### module.exports.getSecondaryAccentColor() ⇒ <code>string</code>
Získanie sekundárnej farby pre zvýraznenie a označenie.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_ThemeProvider--module.exports)
**Returns**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu
<a name="module_ThemeProvider--module.exports+getPrimaryFontColor"></a>

#### module.exports.getPrimaryFontColor() ⇒ <code>string</code>
Získanie hlavnej farby písma.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_ThemeProvider--module.exports)
**Returns**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu
<a name="module_ThemeProvider--module.exports+getSecondaryFontColor"></a>

#### module.exports.getSecondaryFontColor() ⇒ <code>string</code>
Získanie sekundárnej farby písma.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_ThemeProvider--module.exports)
**Returns**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu
<a name="module_ThemeProvider--module.exports+getBinColor"></a>

#### module.exports.getBinColor(content) ⇒ <code>string</code>
Získanie kompetentnej farby pre bin podľa obsahu binu.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_ThemeProvider--module.exports)
**Returns**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre bin, ktorá náleží binu podľa obsahu

| Param | Type | Description |
| --- | --- | --- |
| content | <code>number</code> | Hodnota definujúca obsah binu |

<a name="module_BinSubject"></a>

## BinSubject

* [BinSubject](#module_BinSubject)
    * [~BinSubject](#module_BinSubject..BinSubject)
        * [.saveSelectedBinToLocalStorage()](#module_BinSubject..BinSubject+saveSelectedBinToLocalStorage) ⇒ <code>void</code>
        * [.deleteBinFromLocalStorage()](#module_BinSubject..BinSubject+deleteBinFromLocalStorage) ⇒ <code>void</code>
        * [.getKeyboardEvent()](#module_BinSubject..BinSubject+getKeyboardEvent) ⇒ <code>Subject</code>

<a name="module_BinSubject..BinSubject"></a>

### BinSubject~BinSubject
Subjekt pre publikáciu signálov a dát pre biny.

**Kind**: inner class of [<code>BinSubject</code>](#module_BinSubject)

* [~BinSubject](#module_BinSubject..BinSubject)
    * [.saveSelectedBinToLocalStorage()](#module_BinSubject..BinSubject+saveSelectedBinToLocalStorage) ⇒ <code>void</code>
    * [.deleteBinFromLocalStorage()](#module_BinSubject..BinSubject+deleteBinFromLocalStorage) ⇒ <code>void</code>
    * [.getKeyboardEvent()](#module_BinSubject..BinSubject+getKeyboardEvent) ⇒ <code>Subject</code>

<a name="module_BinSubject..BinSubject+saveSelectedBinToLocalStorage"></a>

#### binSubject.saveSelectedBinToLocalStorage() ⇒ <code>void</code>
Publikácia signálu s hodnotou True pre označenie daného binu.

**Kind**: instance method of [<code>BinSubject</code>](#module_BinSubject..BinSubject)
<a name="module_BinSubject..BinSubject+deleteBinFromLocalStorage"></a>

#### binSubject.deleteBinFromLocalStorage() ⇒ <code>void</code>
Publikácia signálu s hodnotou False pre odznačenie daného binu.

**Kind**: instance method of [<code>BinSubject</code>](#module_BinSubject..BinSubject)
<a name="module_BinSubject..BinSubject+getKeyboardEvent"></a>

#### binSubject.getKeyboardEvent() ⇒ <code>Subject</code>
Získanie objektu pre prístup k odberu signálov a dát.

**Kind**: instance method of [<code>BinSubject</code>](#module_BinSubject..BinSubject)
**Returns**: <code>Subject</code> - - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
<a name="module_CameraSubject"></a>

## CameraSubject

* [CameraSubject](#module_CameraSubject)
    * [~CameraSubject](#module_CameraSubject..CameraSubject)
        * [.setVisibilityOfBanners()](#module_CameraSubject..CameraSubject+setVisibilityOfBanners) ⇒ <code>void</code>
        * [.shiftBanners()](#module_CameraSubject..CameraSubject+shiftBanners) ⇒ <code>void</code>
        * [.setUserState()](#module_CameraSubject..CameraSubject+setUserState) ⇒ <code>void</code>
        * [.getCameraSubject()](#module_CameraSubject..CameraSubject+getCameraSubject) ⇒ <code>Subject</code>

<a name="module_CameraSubject..CameraSubject"></a>

### CameraSubject~CameraSubject
Subjekt pre publikáciu signálov a dát pre kameru.

**Kind**: inner class of [<code>CameraSubject</code>](#module_CameraSubject)

* [~CameraSubject](#module_CameraSubject..CameraSubject)
    * [.setVisibilityOfBanners()](#module_CameraSubject..CameraSubject+setVisibilityOfBanners) ⇒ <code>void</code>
    * [.shiftBanners()](#module_CameraSubject..CameraSubject+shiftBanners) ⇒ <code>void</code>
    * [.setUserState()](#module_CameraSubject..CameraSubject+setUserState) ⇒ <code>void</code>
    * [.getCameraSubject()](#module_CameraSubject..CameraSubject+getCameraSubject) ⇒ <code>Subject</code>

<a name="module_CameraSubject..CameraSubject+setVisibilityOfBanners"></a>

#### cameraSubject.setVisibilityOfBanners() ⇒ <code>void</code>
Publikácia signálu s hodnotou špecifikujúcou vstupné zariadenie pre zobrazenie príslušných panelov.

**Kind**: instance method of [<code>CameraSubject</code>](#module_CameraSubject..CameraSubject)
<a name="module_CameraSubject..CameraSubject+shiftBanners"></a>

#### cameraSubject.shiftBanners() ⇒ <code>void</code>
Publikácia signálu s hodnotou "shift" pre posúvanie panelov.

**Kind**: instance method of [<code>CameraSubject</code>](#module_CameraSubject..CameraSubject)
<a name="module_CameraSubject..CameraSubject+setUserState"></a>

#### cameraSubject.setUserState() ⇒ <code>void</code>
Publikácia signálu s hodnotou "show" pre zobrazenie prislúchajúcich panelov.

**Kind**: instance method of [<code>CameraSubject</code>](#module_CameraSubject..CameraSubject)
<a name="module_CameraSubject..CameraSubject+getCameraSubject"></a>

#### cameraSubject.getCameraSubject() ⇒ <code>Subject</code>
Získanie objektu pre prístup k odberu signálov a dát.

**Kind**: instance method of [<code>CameraSubject</code>](#module_CameraSubject..CameraSubject)
**Returns**: <code>Subject</code> - - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
<a name="module_HistogramSubject"></a>

## HistogramSubject

* [HistogramSubject](#module_HistogramSubject)
    * [~HistogramSubject](#module_HistogramSubject..HistogramSubject)
        * [.changeHistogramSectionByOffset(axis, histogramType, increment, defaultRange)](#module_HistogramSubject..HistogramSubject+changeHistogramSectionByOffset) ⇒ <code>void</code>
        * [.changeHistogramFunction(typeFunction, histogramType)](#module_HistogramSubject..HistogramSubject+changeHistogramFunction) ⇒ <code>void</code>
        * [.getChangedSection()](#module_HistogramSubject..HistogramSubject+getChangedSection) ⇒ <code>Subject</code>

<a name="module_HistogramSubject..HistogramSubject"></a>

### HistogramSubject~HistogramSubject
Subjekt pre publikáciu signálov a dát pre histogram.
Prezobrazovanie sekcií histogramu.

**Kind**: inner class of [<code>HistogramSubject</code>](#module_HistogramSubject)

* [~HistogramSubject](#module_HistogramSubject..HistogramSubject)
    * [.changeHistogramSectionByOffset(axis, histogramType, increment, defaultRange)](#module_HistogramSubject..HistogramSubject+changeHistogramSectionByOffset) ⇒ <code>void</code>
    * [.changeHistogramFunction(typeFunction, histogramType)](#module_HistogramSubject..HistogramSubject+changeHistogramFunction) ⇒ <code>void</code>
    * [.getChangedSection()](#module_HistogramSubject..HistogramSubject+getChangedSection) ⇒ <code>Subject</code>

<a name="module_HistogramSubject..HistogramSubject+changeHistogramSectionByOffset"></a>

#### histogramSubject.changeHistogramSectionByOffset(axis, histogramType, increment, defaultRange) ⇒ <code>void</code>
Publikácia signálu pre prezobrazenie sekcie histogramu.

**Kind**: instance method of [<code>HistogramSubject</code>](#module_HistogramSubject..HistogramSubject)

| Param | Type | Description |
| --- | --- | --- |
| axis | <code>string</code> | Názov osi na ktorej sa budú posúvať ofsety |
| histogramType | <code>string</code> | Typ histogramu (TH2 alebo TH3) |
| increment | <code>boolean</code> | Ak je True tak sa hodnota prípočíta k ofsetu, ak False tak sa odpočíta |
| defaultRange | <code>boolean</code> | Ak je True tak sa použije predvolená hodnota (1), ak False tak nastavený rozsah |

<a name="module_HistogramSubject..HistogramSubject+changeHistogramFunction"></a>

#### histogramSubject.changeHistogramFunction(typeFunction, histogramType) ⇒ <code>void</code>
Publikácia signálu pre zmenu vizuálneho módu.

**Kind**: instance method of [<code>HistogramSubject</code>](#module_HistogramSubject..HistogramSubject)

| Param | Type | Description |
| --- | --- | --- |
| typeFunction | <code>string</code> | Názov funkcie podľa, ktorej sa zmení vizualizačný mód |
| histogramType | <code>string</code> | Typ histogramu (TH2 alebo TH3) |

<a name="module_HistogramSubject..HistogramSubject+getChangedSection"></a>

#### histogramSubject.getChangedSection() ⇒ <code>Subject</code>
Získanie objektu pre prístup k odberu signálov a dát.

**Kind**: instance method of [<code>HistogramSubject</code>](#module_HistogramSubject..HistogramSubject)
**Returns**: <code>Subject</code> - - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
<a name="module_JsrootSubject"></a>

## JsrootSubject

* [JsrootSubject](#module_JsrootSubject)
    * [~JsrootSubject](#module_JsrootSubject..JsrootSubject)
        * [.showBinProjection(binData)](#module_JsrootSubject..JsrootSubject+showBinProjection)
        * [.getServiceEvent()](#module_JsrootSubject..JsrootSubject+getServiceEvent) ⇒ <code>Subject</code>

<a name="module_JsrootSubject..JsrootSubject"></a>

### JsrootSubject~JsrootSubject
Subjekt pre publikáciu signálov a dát pre JSROOT.

**Kind**: inner class of [<code>JsrootSubject</code>](#module_JsrootSubject)

* [~JsrootSubject](#module_JsrootSubject..JsrootSubject)
    * [.showBinProjection(binData)](#module_JsrootSubject..JsrootSubject+showBinProjection)
    * [.getServiceEvent()](#module_JsrootSubject..JsrootSubject+getServiceEvent) ⇒ <code>Subject</code>

<a name="module_JsrootSubject..JsrootSubject+showBinProjection"></a>

#### jsrootSubject.showBinProjection(binData)
Zobrazenie projekcie pre zvolený bin histogramu.

**Kind**: instance method of [<code>JsrootSubject</code>](#module_JsrootSubject..JsrootSubject)

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahuje dáta o zvolenom bine |

<a name="module_JsrootSubject..JsrootSubject+getServiceEvent"></a>

#### jsrootSubject.getServiceEvent() ⇒ <code>Subject</code>
Získanie objektu pre prístup k odberu signálov a dát.

**Kind**: instance method of [<code>JsrootSubject</code>](#module_JsrootSubject..JsrootSubject)
**Returns**: <code>Subject</code> - - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
<a name="module_keyboardController"></a>

## keyboardController
Modul obsahuje implementované metódy, ktoré reagujú na vstupy používateľa.
Funkcie sú určené pre komponenty slúžiace definovanie listenerov pre klávesnicu počítača.


* [keyboardController](#module_keyboardController)
    * [~handlePositioning(property, event)](#module_keyboardController..handlePositioning) ⇒ <code>void</code>
    * [~handleChangeHistogramSectionByDefaultRange(event)](#module_keyboardController..handleChangeHistogramSectionByDefaultRange) ⇒ <code>void</code>
    * [~handleChangeHistogramSectionByOwnRange(event)](#module_keyboardController..handleChangeHistogramSectionByOwnRange) ⇒ <code>void</code>
    * [~keyPressHandlerFunction(event)](#module_keyboardController..keyPressHandlerFunction) ⇒ <code>void</code>
    * [~keyReleaseHandlerFunction(event)](#module_keyboardController..keyReleaseHandlerFunction) ⇒ <code>void</code>
    * [~initialKeyboardController(data, object)](#module_keyboardController..initialKeyboardController) ⇒ <code>void</code>
    * [~keyboardUpdateCameraReference()](#module_keyboardController..keyboardUpdateCameraReference) ⇒ <code>void</code>

<a name="module_keyboardController..handlePositioning"></a>

### keyboardController~handlePositioning(property, event) ⇒ <code>void</code>
Získanie objektu pre prístup k odberu signálov a dát.

**Kind**: inner method of [<code>keyboardController</code>](#module_keyboardController)

| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | Definuje názov atribútu histogramu (poloha, rozmer, rotácia), ktorý bude modifikovaný |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_keyboardController..handleChangeHistogramSectionByDefaultRange"></a>

### keyboardController~handleChangeHistogramSectionByDefaultRange(event) ⇒ <code>void</code>
Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na klávesnici

**Kind**: inner method of [<code>keyboardController</code>](#module_keyboardController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_keyboardController..handleChangeHistogramSectionByOwnRange"></a>

### keyboardController~handleChangeHistogramSectionByOwnRange(event) ⇒ <code>void</code>
Zmena zobrazovanej sekcie histogramu o vlastný rozsah (podľa nastavení používateľa) na klávesnici

**Kind**: inner method of [<code>keyboardController</code>](#module_keyboardController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_keyboardController..keyPressHandlerFunction"></a>

### keyboardController~keyPressHandlerFunction(event) ⇒ <code>void</code>
Funkcia, vukonávajúca sa na klik používateľa.
Funkcia detekuje, uloží stlačenú klávesu do poľa stlašených kláves a zavolá náležitú funkciu

**Kind**: inner method of [<code>keyboardController</code>](#module_keyboardController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_keyboardController..keyReleaseHandlerFunction"></a>

### keyboardController~keyReleaseHandlerFunction(event) ⇒ <code>void</code>
Funkcia vymaže klávesu z poľa stlačených kláves

**Kind**: inner method of [<code>keyboardController</code>](#module_keyboardController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcií |

<a name="module_keyboardController..initialKeyboardController"></a>

### keyboardController~initialKeyboardController(data, object) ⇒ <code>void</code>
Funkcia inicializuje všetky údaje o histograme a samotnú referenciu na histogram

**Kind**: inner method of [<code>keyboardController</code>](#module_keyboardController)

| Param | Type | Description |
| --- | --- | --- |
| data | <code>Object</code> | Objekt obsahuje všetky atribúty (rozmer, pozícia, rotácia) |
| object | <code>HTMLElement</code> | Referencia na element histogramu, ktorý sa má modifikovať |

<a name="module_keyboardController..keyboardUpdateCameraReference"></a>

### keyboardController~keyboardUpdateCameraReference() ⇒ <code>void</code>
Funkcia aktualizuje referenciu servis kamery

**Kind**: inner method of [<code>keyboardController</code>](#module_keyboardController)
<a name="module_oculusController"></a>

## oculusController
Modul obsahuje implementované metódy, ktoré reagujú na vstupy používateľa.
Funkcie sú určené pre komponenty slúžiace definovanie listenerov pre ovládač Oculusu.


* [oculusController](#module_oculusController)
    * [~thumbStickByOwnOffset(event)](#module_oculusController..thumbStickByOwnOffset) ⇒ <code>void</code>
    * [~thumbStickByDefaultOffset(event)](#module_oculusController..thumbStickByDefaultOffset) ⇒ <code>void</code>
    * [~thumbStickForMoving(event, speed)](#module_oculusController..thumbStickForMoving) ⇒ <code>void</code>
    * [~thumbStickPredefinedPosition(event)](#module_oculusController..thumbStickPredefinedPosition) ⇒ <code>void</code>
    * [~thumbStickPredefinedPositionWithOffset(event)](#module_oculusController..thumbStickPredefinedPositionWithOffset) ⇒ <code>void</code>
    * [~oculusUpSection(defaultRange)](#module_oculusController..oculusUpSection) ⇒ <code>void</code>
    * [~oculusDownSection(defaultRange)](#module_oculusController..oculusDownSection) ⇒ <code>void</code>
    * [~oculusUpdateCameraReference()](#module_oculusController..oculusUpdateCameraReference) ⇒ <code>void</code>
    * [~oculusThumbStickFunction(event)](#module_oculusController..oculusThumbStickFunction) ⇒ <code>void</code>
    * [~oculusThumbStickWithGripFunction(event)](#module_oculusController..oculusThumbStickWithGripFunction) ⇒ <code>void</code>
    * [~oculusThumbStickPredefinedCameraPosition(event)](#module_oculusController..oculusThumbStickPredefinedCameraPosition) ⇒ <code>void</code>
    * [~oculusThumbStickPredefinedCameraPositionWithOffset(event)](#module_oculusController..oculusThumbStickPredefinedCameraPositionWithOffset) ⇒ <code>void</code>
    * [~oculusThumbStickForMoving(event, speed)](#module_oculusController..oculusThumbStickForMoving) ⇒ <code>void</code>
    * [~oculusXButtonDownFunction(speed)](#module_oculusController..oculusXButtonDownFunction) ⇒ <code>void</code>
    * [~oculusYButtonDownFunction(speed)](#module_oculusController..oculusYButtonDownFunction) ⇒ <code>void</code>
    * [~oculusThumbStickMarkBin()](#module_oculusController..oculusThumbStickMarkBin) ⇒ <code>void</code>
    * [~oculusThumbStickUnmarkBin()](#module_oculusController..oculusThumbStickUnmarkBin) ⇒ <code>void</code>
    * [~oculusShowFunctionView()](#module_oculusController..oculusShowFunctionView) ⇒ <code>void</code>
    * [~oculusShowDefaultView()](#module_oculusController..oculusShowDefaultView) ⇒ <code>void</code>
    * [~oculusSwitchViewWithBanners()](#module_oculusController..oculusSwitchViewWithBanners) ⇒ <code>void</code>
    * [~oculusChangeBannerContent()](#module_oculusController..oculusChangeBannerContent) ⇒ <code>void</code>
    * [~oculusShiftBanners()](#module_oculusController..oculusShiftBanners) ⇒ <code>void</code>
    * [~oculusRedrawHistogramBanners()](#module_oculusController..oculusRedrawHistogramBanners) ⇒ <code>void</code>

<a name="module_oculusController..thumbStickByOwnOffset"></a>

### oculusController~thumbStickByOwnOffset(event) ⇒ <code>void</code>
Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na ovládači Oculusu.

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_oculusController..thumbStickByDefaultOffset"></a>

### oculusController~thumbStickByDefaultOffset(event) ⇒ <code>void</code>
Zmena zobrazovanej sekcie histogramu o špecifikovaný rozsah (podľa nastavení používateľa) na ovládači Oculusu.

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_oculusController..thumbStickForMoving"></a>

### oculusController~thumbStickForMoving(event, speed) ⇒ <code>void</code>
Funkcia, zabezpečujúca horizontálny pohyb kamery po osiach.

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |
| speed | <code>number</code> | Definuje rýchlosť pohybu |

<a name="module_oculusController..thumbStickPredefinedPosition"></a>

### oculusController~thumbStickPredefinedPosition(event) ⇒ <code>void</code>
Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (len X a Z súradnica).

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_oculusController..thumbStickPredefinedPositionWithOffset"></a>

### oculusController~thumbStickPredefinedPositionWithOffset(event) ⇒ <code>void</code>
Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (X a Y a Z súradnica).

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_oculusController..oculusUpSection"></a>

### oculusController~oculusUpSection(defaultRange) ⇒ <code>void</code>
Funkcia, zobrazenie novej sekcie smerom hore.

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| defaultRange | <code>boolean</code> | Ak je True tak bude použitý range (1), ak je False tak (nastavený rozsah) |

<a name="module_oculusController..oculusDownSection"></a>

### oculusController~oculusDownSection(defaultRange) ⇒ <code>void</code>
Funkcia, zobrazenie novej sekcie smerom dolu

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| defaultRange | <code>boolean</code> | Ak je True tak bude použitý range (1), ak je False tak (nastavený rozsah) |

<a name="module_oculusController..oculusUpdateCameraReference"></a>

### oculusController~oculusUpdateCameraReference() ⇒ <code>void</code>
Funkcia, aktualizuje servis kamery.

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_oculusController..oculusThumbStickFunction"></a>

### oculusController~oculusThumbStickFunction(event) ⇒ <code>void</code>
Zmena zobrazovanej sekcie histogramu o špecifikovaný rozsah (podľa nastavení používateľa) na ovládači Oculusu.

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_oculusController..oculusThumbStickWithGripFunction"></a>

### oculusController~oculusThumbStickWithGripFunction(event) ⇒ <code>void</code>
Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na ovládači Oculusu.

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_oculusController..oculusThumbStickPredefinedCameraPosition"></a>

### oculusController~oculusThumbStickPredefinedCameraPosition(event) ⇒ <code>void</code>
Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (len X a Z súradnica).

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_oculusController..oculusThumbStickPredefinedCameraPositionWithOffset"></a>

### oculusController~oculusThumbStickPredefinedCameraPositionWithOffset(event) ⇒ <code>void</code>
Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (X a Y a Z súradnica).

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

<a name="module_oculusController..oculusThumbStickForMoving"></a>

### oculusController~oculusThumbStickForMoving(event, speed) ⇒ <code>void</code>
Funkcia, zabezpečujúca horizontálny pohyb kamery po osiach.

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |
| speed | <code>Number</code> | Definuje rýchlosť pohybu |

<a name="module_oculusController..oculusXButtonDownFunction"></a>

### oculusController~oculusXButtonDownFunction(speed) ⇒ <code>void</code>
Funkcia, zabezpečí posunutie kamery smerom hore

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| speed | <code>Number</code> | Určuje rýchlosť pohybu kamery |

<a name="module_oculusController..oculusYButtonDownFunction"></a>

### oculusController~oculusYButtonDownFunction(speed) ⇒ <code>void</code>
Funkcia, zabezpečí posunutie kamery smerom dolu

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)

| Param | Type | Description |
| --- | --- | --- |
| speed | <code>Number</code> | Určuje rýchlosť pohybu kamery |

<a name="module_oculusController..oculusThumbStickMarkBin"></a>

### oculusController~oculusThumbStickMarkBin() ⇒ <code>void</code>
Funkcia, zabezpečí označenie binu

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_oculusController..oculusThumbStickUnmarkBin"></a>

### oculusController~oculusThumbStickUnmarkBin() ⇒ <code>void</code>
Funkcia, zabezpečí odznačenie binu

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_oculusController..oculusShowFunctionView"></a>

### oculusController~oculusShowFunctionView() ⇒ <code>void</code>
Funkcia, zabezpečí prepnutie do špeciálneho módu, určeného funkciou

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_oculusController..oculusShowDefaultView"></a>

### oculusController~oculusShowDefaultView() ⇒ <code>void</code>
Funkcia, zabezpečí prepnutie do predvoleného vizualizačného módu

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_oculusController..oculusSwitchViewWithBanners"></a>

### oculusController~oculusSwitchViewWithBanners() ⇒ <code>void</code>
Funkcia, zabezpečí prepínanie medzi zobrazeniami

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_oculusController..oculusChangeBannerContent"></a>

### oculusController~oculusChangeBannerContent() ⇒ <code>void</code>
Funkcia, zabezpečí zmenu obsahu na paneloch

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_oculusController..oculusShiftBanners"></a>

### oculusController~oculusShiftBanners() ⇒ <code>void</code>
Funkcia, zabezpečí rotovanie panelov

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_oculusController..oculusRedrawHistogramBanners"></a>

### oculusController~oculusRedrawHistogramBanners() ⇒ <code>void</code>
Funkcia, prekreslí náhľady na paneloch vo VR

**Kind**: inner method of [<code>oculusController</code>](#module_oculusController)
<a name="module_cursorEventAframeComponent"></a>

## cursorEventAframeComponent
A-Frame interakčný komponent pridávajúci funkcionalitu entite, ktorá spracúvava prieniky medzi entitou a raycastrom používateľa.

**Component**: mouseevent
<a name="module_histogramAframeComponent"></a>

## histogramAframeComponent
A-Frame interakčný komponent pridávajúci funkcionalitu entite histogram, ktorá umožňuje zmenu atribútov entity.

**Component**: histogram-control

| Param | Type | Description |
| --- | --- | --- |
| histogram | <code>Object</code> | Objekt histogramu z ktorého je potrebné spraviť projekciu |
| projectionAxes | <code>Array</code> | Pole obsahuje názvy osí, podľa ktorých sa má vytvoriť projekcia |
| projections | <code>Object</code> | Objekt obsahujúci súborovú štruktúru s projekciami |
| projIndex | <code>Number</code> | Index projekcie v poli, ktorá sa má zobraziť |

<a name="module_labelHandlerAframeComponent"></a>

## labelHandlerAframeComponent
A-Frame komponent zabezpečí oneskorené aktualizovanie označenia na entite.

**Component**: label-handler

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | Hodnota označenia, na ktorú sa má aktuálne označenie zmeniť |
| delay | <code>number</code> | Hodnota oneskorenia aktualizácie označenia na entite |

<a name="module_leftOculusAframeComponent"></a>

## leftOculusAframeComponent
A-Frame interakčný komponent zabezpečí možnosť používateľa interagovať s ľavým ovládačom zariadenia Oculus.

**Component**: left-controller-logging
<a name="module_rightOculusAframeComponent"></a>

## rightOculusAframeComponent
A-Frame interakčný komponent zabezpečí možnosť používateľa interagovať s pravým ovládačom zariadenia Oculus.

**Component**: right-controller-logging
<a name="module_th2AframeComponent"></a>

## th2AframeComponent
A-Frame interakčný komponent pridávajúci funkcionalitu entite binu TH2 histogram, ktorá umožňuje zmenu atribútov entity.

**Component**: binth2

| Param | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | Identifikátor binu |
| content | <code>number</code> | Hodnota optimalizovaného obsahu binu (predvolená 0) |
| absoluteContent | <code>number</code> | Hodnota neoptimalizovaného obsahu binu (predvolená 0) |
| binName | <code>string</code> | Meno binu |
| xTitle | <code>string</code> | Titulok na osi x |
| yTitle | <code>string</code> | Titulok na osi y |
| xMin | <code>number</code> | Súradnica min binu na osi x |
| yMin | <code>number</code> | Súradnica min binu na osi y |
| xMax | <code>number</code> | Súradnica max binu na osi x |
| yMax | <code>number</code> | Súradnica max binu na osi y |
| xCenter | <code>number</code> | Súradnica stredu binu na osi x |
| yCenter | <code>number</code> | Súradnica stredu binu na osi y |
| xWidth | <code>number</code> | Šírka binu na osi x |
| yWidth | <code>number</code> | Šírka binu na osi y |
| color | <code>string</code> | Farba binu |
| axisX | <code>string</code> | Farba označení na osi x |
| axisY | <code>string</code> | Farba označení na osi y |
| axisZ | <code>string</code> | Farba označení na osi z |
| selectColor | <code>string</code> | Farba určená za zvolenie binu |
| markedColor | <code>string</code> | Farba určená pre označenie binu (v poli označených binov) |

<a name="module_th2AframeComponent"></a>

## th2AframeComponent
A-Frame interakčný komponent pridávajúci funkcionalitu entite binu TH3 histogram, ktorá umožňuje zmenu atribútov entity.

**Component**: binth3

| Param | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | Identifikátor binu |
| content | <code>number</code> | Hodnota optimalizovaného obsahu binu (predvolená 0) |
| absoluteContent | <code>number</code> | Hodnota neoptimalizovaného obsahu binu (predvolená 0) |
| binName | <code>string</code> | Meno binu |
| xTitle | <code>string</code> | Titulok na osi x |
| yTitle | <code>string</code> | Titulok na osi y |
| xMin | <code>number</code> | Súradnica min binu na osi x |
| yMin | <code>number</code> | Súradnica min binu na osi y |
| zMin | <code>number</code> | Súradnica min binu na osi z |
| xMax | <code>number</code> | Súradnica max binu na osi x |
| yMax | <code>number</code> | Súradnica max binu na osi y |
| zMax | <code>number</code> | Súradnica max binu na osi z |
| xCenter | <code>number</code> | Súradnica stredu binu na osi x |
| yCenter | <code>number</code> | Súradnica stredu binu na osi y |
| zCenter | <code>number</code> | Súradnica stredu binu na osi z |
| xWidth | <code>number</code> | Šírka binu na osi x |
| yWidth | <code>number</code> | Šírka binu na osi y |
| zWidth | <code>number</code> | Šírka binu na osi z |
| color | <code>string</code> | Farba binu |
| axisX | <code>string</code> | Farba označení na osi x |
| axisY | <code>string</code> | Farba označení na osi y |
| axisZ | <code>string</code> | Farba označení na osi z |
| selectColor | <code>string</code> | Farba určená za zvolenie binu |
| markedColor | <code>string</code> | Farba určená pre označenie binu (v poli označených binov) |

<a name="JsrootHistogram"></a>

## JsrootHistogram(histogram, projectionAxes, projections, projIndex)
React komponent pre zobrazenie bočného panelu s JSROOT projekciami.
Zobrazenie SVG elementu s histogramom a potrebným formulárom pre nastavenie atribútov.

**Kind**: global function
**Component**:

| Param | Type | Description |
| --- | --- | --- |
| histogram | <code>Object</code> | Objekt histogramu, z ktorého je potrebné spraviť projekciu |
| projectionAxes | <code>Array</code> | Pole obsahuje názvy osí, podľa ktorých sa má vytvoriť projekcia |
| projections | <code>Object</code> | Objekt obsahujúci súborovú štruktúru s projekciami |
| projIndex | <code>Number</code> | Index projekcie v poli, ktorá sa má zobraziť |

**Example**
```js
const histogram = {
  gitlab: '', // vytvorený alebo prečítany z root súboru JSROOT-tom
  projections: '' // prečítaná z root súboru JSROOT-tom
}
return (
  <JSrootHistogram
     histogram={histogram.gitlab}
     projectionAxes={['X','Y']}
     projections={histogram.projections}
     projIndex={0}/>
)
```
<a name="NdmVrHelp"></a>

## NdmVrHelp()
React komponent pre vytvorenie kamery a jej menežment.

**Kind**: global function
**Component**:
**Example**
```js
return (
 <NdmVrHelp />
)
```
<a name="NdmVrHistogram"></a>

## NdmVrHistogram(name, histogram, histoSection, projections, range, theme)
React komponent pre vytvorenie histogramu.

**Kind**: global function
**Component**:

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | Názov pre histogram |
| histogram | <code>Object</code> | Objekt histogramu, ktorý sa bude v komponente vizualizovať |
| histoSection | <code>Object</code> | Objekt obsahujúci údaje o sekcii a rozsahu, ktorý bude zobrazený |
| projections | <code>Object</code> | Objekt obsahujúci súborovú štruktúru s projekciami |
| range | <code>number</code> | Definuje rozsah, v prípade ak sa vizualizuje bez určených ofsetov iba určitý rozsah |
| theme | <code>string</code> | Definuje tému pre vizualizáciu histogramu |

**Example**
```js
return (
  <NdmVrHistogram
   name={info.name}
   histogram={data.histogram}
   histoSection={data.section}
   projections={data.projections}
   range={data.range}
   theme={info.theme}/>
)
```
<a name="NdmVrScene"></a>

## NdmVrScene(data, info)
React komponent pre vytvorenie VR scény so všetkými potrebnými komponentami.

**Kind**: global function
**Component**:

| Param | Type | Description |
| --- | --- | --- |
| data | <code>Object</code> | Objekt obsahuje objekty histogramu, sekcie poprípade projekcie |
| info | <code>Object</code> | Objekt obsahuje sekundárne údaje (meno, témy, atribúty pozadia v scéne) |

**Example**
```js
data = {
   histogram: histograms.physics,
   projections: histograms.projections,
   section: section
}
info = {
   name: 'Physics',
   theme: theme,
   background: {
       url: './ndmvr/backgrounds/background1.jpg',
       radius: '3000',
       height: '2048',
       width: '2048'
    }
 }
 return (
 <NdmVrScene data={data} info={info} />
)
```
