  

# UI Menu - Configuration Documentation

This documentation describes the JSON configuration structure used for the entire UI menu, from the tab bar navigation of the menu to specific views in the window menu (contents, or predefined templates with layout and style that can be freely context modified based on the configuration). The tab bar navigation includes various components that are triggered upon clicking a specific icon. Components include instructions, evaluation questions, a music player, and settings. The tab bar navigation structure, as well as each component (template), has its properties, which are explained in detail below along with usage examples.
  

## 1. `tabBatNavigationComponentContent`

This component defines the content of the navigation panel with various menu items. Each menu item has specific settings such as colors, images, actions, and components.


### Example Structure of a Menu Item:

```json
{
	"color": "#bf8f8c",
	"src": "menu/home_24dp.png",
	"width": "1",
	"height": "1",
	"radius": "0.2",
	"activeColor": "#FFEB3B",
	"backgroundColor": "#F9FBE7",
	"hoverColor": "#F0F4C3",
	"borderColor": "#827717",
	"fontColor": "#000",
	"onclick": "loadToDoPlan()",
	"component": "InstructionArea"
}
```

### Explanation of Fields:
  
- **`menuItems`**: List of menu items, each with the following properties:

	-  **`color`**: Defines the menu item color in hex format.

	-  **`src`**: Path to the icon image.

	-  **`width`**, **`height`**: Dimensions of the menu item.

	-  **`radius`**: Rounding radius of the menu item's edges.

	-  **`activeColor`**, **`backgroundColor`**, **`hoverColor`**: Colors for different states of the item.

	-  **`borderColor`**: Border color of the item.

	-  **`fontColor`**: Text color for labels or descriptions.

	-  **`onclick`**: JavaScript function triggered when the item is clicked.

	-  **`component`**: The associated component rendered upon selection.

### Usage:

If you click on the "Home" item, the `loadToDoPlan()` function is called, and the `InstructionArea` component opens.

---

## 2. `instructionsComponentContent`

Defines step-by-step instructions for various scenarios. (Template number 1)

### **Example Usage**

```json
"instructionsComponentContent": {
"instructions": [
	{
	"id": "container-window-1",
	"title": "Analysis #1",
	"texts": ["1. Move closer to the histogram (interactive actions).",
	"2. Display the contents of the bin (coordinates: pt 0).",
	"3. Analyze the displayed contents of the bin (read the maximum value)."]
	}
  ]
}
```
### **Explanation of Fields**

-  **`instructions`**: Array of instruction sets, each containing:

	-  **`id`**: Unique identifier for the instruction set.

	-  **`title`**: Title describing the set.

	-  **`texts`**: Step-by-step textual instructions

### Goal:

Example: The instruction "Analysis no.1" includes three steps that guide the user on how to zoom into the histogram and analyze the contents of a bin. (To do list)
  
---
## 3. `susComponentContent`

The component is used to display questions and possible answers, record them, and send the data to storage. (Template number 2)

### Example Usage:

```json
"questions": [
	{
		"text": "I think that I would like to use this system frequently."
	},
	{
		"text": "I thought the system \nwas easy to use."
	}
], 
"options": [
	{
		"value": "Strongly \nDisagree",
		"image": "menu/looks_1_24dp.png"
	},
	{
		"value": "Disagree",
		"image": "menu/looks_2_24dp.png"
	},
]
```

### Explanation of Fields:

- **`questions`**: An array of objects, where each object represents a question. Each question includes the following property:
	- **`text`**: The text of the question displayed to the user.

- **`options`**: An array of objects that represent the possible answers to the questions in questions array.

	-   **`value`**: A textual label for the answer option.

	-   **`image`**: The path to an image file that visually represents the answer option.

### Goal:

The user is asked to provide feedback (etc. on whether they would like to...)

 
---

## 4. `uesComponentContent`

The component is used to display questions and possible answers, record them, and send the data to storage. (Another usage of the Tample Number 2).

  

### Example Usage:

```json
"questions": [
	{
		"text": "FA -S.1: Prestal som \nvnimat svoje okolie\npri tomto zazitku."
	},
	{
		"text": "RW -S.2: Moj zazitok \nbol obohacujuci."
	},
	{
		"text": "FA -S.3: Bol som pohlteny\ntymto zazitkom."
	},
],
"options": [
	{
		"value": "Silny \nnesuhlas",
		"image": "menu/counter_1_24dp.png"
	},
	{
		"value": "Nesuhlas",
		"image": "menu/counter_2_24dp.png"
	},
]
```

### Explanation of Fields:

- **`questions`**: An array of objects, where each object represents a question. Each question includes the following:

	- **`text`**: The text of the question displayed to the user.

- **`options`**: An array of objects that represent the possible answers to the questions in questions array.

	-   **`value`**: A textual label for the answer option.

	-   **`image`**: The path to an image file that visually represents the answer option.

### Goal:

The user is asked to provide feedback 

---

## 5. `musicPlayerComponentContent`

Template number 3 -> This component allows for music playback. It contains a list of tracks from which the user can choose and includes a predefined layout for displaying content in a 3x3 grid. In this particular example, it is used for the play action (it can also be used for redirection, displaying something else, etc.). 
  

### Example Usage:

```json
"songs": [
	{
		"title": "Relaxing Forest \nMorning",
		"description": "Music designed for a calm working\nenvironment.",
		"url": "menu/menu-songs/Relaxing Forest Morning.mp3",
		"color": "#8783D1"
	},
	{
		"title": "Tranquil Waters",
		"description": "Music that promotes relaxation\nand serenity.",
		"url": "menu/menu-songs/water.mp3",
		"color": "#E3B9BC"
	},
```

  

### Explanation of Fields:

- The **`songs`** array contains a list of music tracks, where each track is an object with the following properties:

	-  **`title`**: The name of the song, which may contain line breaks for formatting.

	-  **`description`**:  A brief description of the song, which may also contain line breaks.

	-  **`url`**:  The path to the audio file for the song.

	-  **`color`**:  A color code associated with the song, used for display purposes in the UI.

### Goal:

This structure can be used in various contexts, such as a music player where users can choose tracks to play, a background soundtrack for specific scenes or activities, or even as part of a calming or productivity-focused environment. The color coding and descriptions can also be used to enhance the user experience by providing visual cues or thematic associations for each track.

---

## 6. `settingsContent`

This component contains application settings where the user can select various options such as theme, scene, range, or scale. It is divided into multiple levels:
1.  The first nesting level is at the `description: Options` and its properties.
2.  The second nesting level is defined by attributes like `theme`, `scene`, etc.
3.  The `scheme` is an extension to add a potential third level in the future, which would serve as a root for further property separation.

  

### Example Usage:

```json
{
	"description": "Options:",
	"properties": {
		"theme": {
			"scheme": "histogram",
			"description": "Choose a theme from the options below: ",
			"type": "select",
			"value": "def",
			"options": [
				"def",
				"fire",
				"nebula",
			]
		},
		"scene": {
			"scheme": "scene",
			"description": "Choose a scene from the options below: ",
			"name": "scene",
			"value": "ndmvr",
			"type": "select",
			"options": [
				"ndmvr",
				"govorun",
				"cern",
				"default",
				"labs",
				"labs2",
				"contact",
				"egypt",
				"checkerboard",
				"forest",
			]
		},
	},
}
```

### Explanation of Fields:
-  **`settingsContent`** (Object) : Container for the settings content. 
	- **`description`**: Provides a general description of the settings top-level (text displayed to the user).

	-  **`properties`** (Object): Contains individual setting objects such as:
		- **`Theme`**: Defines the specific property (etc. theme-related settings).
			-  **`scheme`**: Defines the theme type, here it is set to "histogram" (FUTURE - Top Level).
			-  **`Description`**: Defines the theme-related settings description. A user-facing description prompting the user to select a theme.

			-  **`type`**: Indicates the input type, in this case, a select dropdown (etc. `select`, `number`).

			-  **`value`**: Default selected value for the theme, which is set to `"def"` (default).

			-  **`options`**: (Array of Strings) A list of available theme options for the user to choose from. These are displayed in the select dropdown.

  

### Goal:

The goal of this `settingsContent` configuration is to allow users to customize the application settings, specifically by selecting a property from a predefined set of options. It provides a user-friendly interface for selecting different visual themes, which can change the appearance of the application.

---

