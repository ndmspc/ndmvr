---
title: ndmvr - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# Installation

---

The component is connected with technologies based on framework React, for that reason
 it will easy to install using [npm](https://www.npmjs.com/package/@ndmspc/ndmvr) package manager and invoke `npm install @ndmspc/ndmvr`.

After installation, you have 3 components which you can import and use.

<code>import { ndmVrStorage, NdmVrScene, JSrootHistogram } from '@ndmspc/ndmvr'</code>

This library contains components which visualize histograms and manage data about visualization. You have read ROOT files and objects to in your client application.

The components do not solve the way of obtaining this data, but they process the already prepared histogram objects and subsequently create a visualization.

---
To display the controls and background, you need to provide a file that contains the necessary resources, such as images. The user can then add their own backgrounds to the scene to the file structure and then use them to integrate the component. It is also possible to customize the controls according to the nature of the client application by editing textures.

Otherwise, the user will have functional components that do not contain the appropriate textures.

File structure:

``` sh
ndmvr
├── backgrounds
│   └── background1.png
│   └── background2.jpg
│   └── ...
├── keybordControls
│   └── keyboard.png
│   └── keuboard1.png
│   └── keyboard2.png
│   └── keuboard3.png
├── oculusControls
│   └── oculus.png
│   └── oculus1.png
│   └── oculus2.png
│   └── oculus3.png
└── ...
```

The file must be placed in the `public` directory in the client application to ensure access to resources in the project.

To load all the default textures and resources for control and interaction, you must obtain the default file by downloading project resources:

>
>- [zip](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.zip)
>- [tar.gz](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar.gz)
>- [tar.bz2](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar.bz2)
>- [tar](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar)
>

After obtaining the resources, it is necessary to move the `etc / ndmvr` directory to the` public` directory in the client application.

Subsequently, everything is ready to use the package in the client application.
