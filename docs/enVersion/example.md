---
title: NDMVR - Príklad načítania objektov
summary: Dokumentácia pre NDMVR
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# Usage of ***NDMVR***

---

The previous section described the creation of the React application and the provision of all necessary libraries for the client application, so that it is possible to create a visualization of histograms in VR. If we have a project ready with everything needed to create components for visualization.

The first step is to get the necessary objects that will need to be visualized using the **ndmVrHistogramScene** component. For this purpose, we can use the JSROOT object to create a histogram but also to obtain objects from **ROOT files**.

## get object from ***ROOT file***

---

In this case, the url address of the root file is required. We should get all the objects needed for visualization from the file.

To load an object, assume that we have a reference to the JSROOT object stored in the jsroot variable, which contains all the necessary functions.

```js
// url root suboru
let filename = 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root'
// JSROOT funkcia pre otvorenie súboru, ako návratovú hodnotu ziskavame Promise.
JSROOT.openFile(filename)
// ak funkcia otvorí súbor následne dôjde k prečítaniu objektu "main" a vracia sa prečítaný objekt
.then(file => file.readObject('main'))
.then(obj => {
/*
* V tomto bloku spracujeme samotný objekt
* Objekt získame ako parameter obj a vieme s ním pracovať
* Vhodné je uloženie objektu do premennej a nastavenie stavu indikujúceho prístupnosť objektu
*/
})
.catch(error => {
/*
* V prípade ak dôjde k chybe vykoná sa tento blok
*/
});
```


## Create histogram

---

The JSROOT object also contains functions for creating histograms, this procedure is less recommended due to the fact that creating complex histograms can be difficult for the browser on the client side.

Therefore, it is most recommended to create statistics in the ROOT application on a special device and distribute to the client already finished objects in the ROOT file, which are then only loaded and visualized by the client's browser.

```js
// Fragment, ktorý vytvorí histogram TH2I s rozmermi 20x20 a vyplní obsahy binov
const histo = jsroot.createHistogram('TH2I', 20, 20)
// Počítadlo
let cnt = 0;
// Cykly na zabezpečenie prechodu všetkých binov vytvoreného histogramu
for (var iy=1;iy<=20;iy++)
  for (var ix=1;ix<=20;ix++) {
    // Získanie konkrétneho objektu binu na základe jeho súradnic v histograme
    var bin = histo.getBin(ix, iy), val = 0;
    val=cnt;
    // Nastavenie hodnoty binu
    histo.setBinContent(bin, val);
    cnt+=0.1;
  }
// Nastavenie globálnych údajov histogramu
histo.fXaxis.fTitle = 'x Axis'
histo.fYaxis.fTitle = 'y Axis'
histo.fName = "You don't have a valid file path"
histo.fTitle = "This is a TH2 histogram demo"
histo.fMaximum = cnt
```

## Create visualization

---

Ak už sú načítané potrebné objekty v aplikácii, tak je možné histogramy vizualizovať pomocou komponentov **NDMVR**.

If the required objects in the application have already been loaded, the histograms can be visualized using the **NDMVR** components.

```jsx
<NdmVrScene data={data} info={info} />

...

<JSrootHistogram histogram={histograms.physics} projectionAxes={['X','Y']} projections={histograms.projections} projIndex={0}/>
```

Components create everything you need. The parameters must be provided to the components as they were in the NDMVR documentation.
