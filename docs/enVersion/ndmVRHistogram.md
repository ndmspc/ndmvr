---
title: ndmvr - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# NdmVRHistogramScene

---

The component is primary maintained for displaying VR scene with TH histogram. It contains 2 properties such as data and info.

```jsx
<NdmVrScene data={data} info={info} />
```

Properties are defined as an object with the necessary data.

- **data** - primary data such as histogram object, projections...
- **info** - secondary data for define schemes, background attributes...

## attribute ***data***

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **histogram** | Object | Main object defines histogram for displaying | `null` | `False` |
| **projections** | Object | File structure with projection | `null` | `True` |
| **section** |  Object | Data about offsets and histogram type | -- | `True` |

### object ***histogram***

This object defines main histogram TH.
We get this object from ROOT file.
More info about th histograms and objects are [here]()

### object ***projections***

File structure contains organised directories and projections for each bin of the main histogram.
Structure of the files can look like that:

``` sh
.
├── 1
│   └── 1
│   │   └── projectionArray
│   └── 2
│   └── 3
│   └── 4
│   └── ...
├── 2
│   └── 1
│   └── 2
│   └── ...
└── ...
```

Displayed file structure describes the template where each bin has her own projection array addressed in directories with path, which determines the bin's coordinates.
As we can see, the bin with x coordinate 1 and y coordinate 1 has a projection stored in a directory with path `root/1/1/projArray[0]`.

### object ***section***

The object defines data for displaying the right amount of data. The face of the object represents attributes for offset throughout axes, range and the typename of the histogram. The parameter is optional.

Range defines how many bins will be displayed on an axis. In overall in Th3 histogram will be displayed (range+1 * range+1 * range+1) bins.

The object should look like that:

``` json
{
  name: 'TH3',
  xOffset: 1,
  yOffset: 1,
  zOffset: 1,
  range: 20
}
```

## attribute ***info***

This parameter is used for defining general information about histograms and themes.

The object defines the scale and url of the image background, a user could use his or her own background image so that the user could save these images in the `public` directory of the client application.

Main properties of the object info:

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **name** | String | Unique name of the histogram | `null` | `True` |
| **theme** | String | String defines theme | `def` | `False` |
| **background** | Object | Object defines background properties | `null` | `True` |

Main properties of the object background:

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **url** | String | defines url of the background image | `./ndmvr/backgrounds/background1.jpg` | `False` |
| **radius** | number | background radius | `3000` | `False` |
| **height** | number | height of the background image | `2048` | `False` |
| **width** | number | width of the background image | `2048` | `False` |

``` json
{
  name: 'histogram unique name',
  theme: 'string characterizing the theme',
  background: {
      url: 'image url',
      radius: 'radius of the background in VR',
      height: 'height',
      width: 'height'
  }
}
```
