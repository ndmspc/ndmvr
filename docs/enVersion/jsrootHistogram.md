---
title: ndmvr - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# JsrootHistogram

---

The component displays JSROOT projections.

The example shows a projection from the root structure. In this case,`projIndex` is important and determines projection in the array.
```jsx
<JSrootHistogram histogram={histogram} projectionAxes={['X','Y']} projections={projections} projIndex={0}/>
```

The example shows a creating projection on axes X and Y. In this case, `projIndex` is useless.

```jsx
<JSrootHistogram histogram={histogram} projectionAxes={['X','Y']} projections={null} projIndex={0}
```

## attribute ***histogram***

This object defines main histogram TH.
We get this object from ROOT file.
More info about th histograms and objects are [here]()

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **histogram** | Object | Main object defines histogram for displaying | `null` | `False` |

## attribute ***projections***

File structure contains organised directories and projections for each bin of the main histogram.

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **projections** | Object | File structure with projection | `null` | `True` |

Structure of the files can look like that:

``` sh
.
├── 1
│   └── 1
│   │   └── projectionArray
│   └── 2
│   └── 3
│   └── 4
│   └── ...
├── 2
│   └── 1
│   └── 2
│   └── ...
└── ...
```

Displayed file structure describes the template where each bin has her own projection array addressed in directories with path, which determines the bin's coordinates.
As we can see, the bin with x coordinate 1 and y coordinate 1 has a projection stored in a directory with path `root/1/1/projArray[0]`.

## attribute ***projectionAxes***

To create 2-dimensional projection we define attribute as an array with names of the axes.

This attribute is optional.

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **projectionAxes** | Array | Array of projection objects | `null` | `True` |

## attribute ***projIndex***

Choose the right projection from the projection array of the root directory.

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **projIndex** | number | index of the projection in array | `0` | `True` |
