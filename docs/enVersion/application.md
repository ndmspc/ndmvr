---
title: NDMVR - Aplikácia
summary: Dokumentácia pre NDMVR
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# Application

---

In order to use these components, the client application must be based on the [React](https://reactjs.org/docs/getting-started.html#try-react).

The first step is to create a new project. First of all, it is necessary to have a ready environment for development. If we have [Node >= 10.16](https://nodejs.org/en/) and [npm >= 5.6](https://nodejs.org/en/) installed, then we can create an application with the command:

```sh
npx create-react-app my-app
cd my-app
npm start
```

The sequence of commands defines the creation of the application, entering the application directory and starting the application.

We got a project based on the React library and the development of a client application can begin. Components have only a visualization task, ie we need to obtain or create objects to be visualized.

The first important library that allows us to read ROOT files is the library [JSROOT](https://github.com/root-project/jsroot/blob/master/docs/JSROOT.md), which we install with the command.

>`npm install jsroot`

After installing the package into the application, we then need to ensure that **Provider** loads all the necessary modules of the JSROOT library before the application itself is rendered. The provider must be placed in a component in which the elements depend on JSROOT. In our case, we will use this **Provider** in the main application file `App.js`.

```js
import { JSROOTContext } from '@ndmspc/react-jsroot'

//zistenie či sú načítané všetky potrebné moduly JSROOT
const status = useContext(JSROOTContext)

// vyrenderovanie aplikácie
return (
    <React.Fragment>
      {(status === 'ready' && (
        /*
           elementy vyrenderované ak sú potrebné moduly načítané
        */
      ))
      ||
        /*
           elementy vyrenderované ak nie sú potrebné moduly načítané
        */
      }
    </React.Fragment>
  )
```

Subsequently, after loading all the necessary modules, we have loaded in the main HTML all the necessary scripts that we need to create an application based on JSROOT.

We have a global JSROOT object that we need to load directly from the browser window. The object can then be used in any component that will later be embedded in the HTML DOM.

In the example, we have a way to select the JSROOT object into a variable that we can then work with.

```js
const jsroot = window.JSROOT
```

This part of the documentation describes the creation of the application and the import of the JSROOT library with everything necessary for the application to run and all the necessary library modules that are necessary for the development of the application are provided. In the following sections, the development of components according to user requirements already follows.

# Import **NDMVR**

---

For visualization in VR it is necessary to install the dependency **ndmvr**, which will provide us with components for visualization of histograms.

>npm install @ndmspc/ndmvr





