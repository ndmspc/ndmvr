---
title: NDMVR - Aplikácia
summary: Dokumentácia pre NDMVR
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# Aplikácia

---

Aby spomínané komponenty bolo možné použiť je nutné aby klientska aplikácia bola založená na knižnici [React](https://reactjs.org/docs/getting-started.html#try-react).

Ako prvý krok je vytvorenie nového projektu. Najskôr je potrebné mať pripravené prostredie pre vývoj. Ak máme nainštalovaný [Node >= 10.16](https://nodejs.org/en/) a [npm >= 5.6](https://nodejs.org/en/) potom môžeme vytvoriť aplikáciu príkazom:

```sh
npx create-react-app my-app
cd my-app
npm start
```

Sekvencia príkazov definuje vytvorenie aplikácie, vstup do adresára aplikácie a spustenie aplikácie.

Získali sme tak projekt založený na knižnici React a môže začať vývoj klientskej aplikácie. Komponenty majú len vizualizačnú úlohu, teda objekty ktoré sa majú vizualizovať potrebujeme získať alebo vytvoriť.

Prvou dôležitou knižnicou, ktorá nám umožní čítanie ROOT súborov je samotná knižnica [JSROOT](https://github.com/root-project/jsroot/blob/master/docs/JSROOT.md), ktorú nainštalujeme príkazom.

>`npm install jsroot`

Po doinštalovaní balíka do aplikácie následne potrebujeme zabezpečiť aby **Provider** načítal všetky potrebné moduly knižnice JSROOT predtým než sa vyrenderuje samotná aplikácia.
Provider je potrebné umiestniť do komponentu v ktorom sú prvky zavislé na JSROOT-e. V našom prípade použijeme tento **Provider** v hlavnom súbore aplikácie `App.js`.

```js
import { JSROOTContext } from '@ndmspc/react-jsroot'

//zistenie či sú načítané všetky potrebné moduly JSROOT
const status = useContext(JSROOTContext)

// vyrenderovanie aplikácie
return (
    <React.Fragment>
      {(status === 'ready' && (
        /*
           elementy vyrenderované ak sú potrebné moduly načítané
        */
      ))
      ||
        /*
           elementy vyrenderované ak nie sú potrebné moduly načítané
        */
      }
    </React.Fragment>
  )
```

Následne po načítaní všetkých potrebných modulov máme v hlavnom HTML načítané všetky potrebné skripty, ktoré potrebujeme pre vytvorenie aplikácie založenej na JSROOT.

Máme k dispozícii globálny objekt JSROOT, ktorý potrebujeme načítať priamo z okna prehliadača. Objekt potom môžeme používať v ľubovoľnom komponente, ktorý bude neskôr vložený do HTML DOM.

V príklade máme spôsob výberu objektu JSROOT do premennej, s ktorou následne vieme pracovať.

```js
const jsroot = window.JSROOT
```
V tejto časti dokumentácie je popísané vytvorenie aplikácie aj import knižnice JSROOT so všetkým potrebným aby aplikácia bola spustená a boli poskytnuté všetky potrebné moduly knižnice, ktoré sú nevyhnutné pre vývoj aplikácie.
V nasledujúcich častiach už nasleduje vývoj komponentov podľa požiadaviek používateľa.

# Import komponentu **NDMVR**

---

Pre vizualizáciu vo VR je potrebné doinštalovať závislosť **ndmvr**, ktorá nám poskytne komponenty pre vizualizáciu histogramov.

>npm install @ndmspc/ndmvr





