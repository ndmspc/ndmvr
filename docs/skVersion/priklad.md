---
title: NDMVR - Príklad načítania objektov
summary: Dokumentácia pre NDMVR
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# Príklad použítia ***NDMVR***

---

V predchádzajúcej časti bolo popísané vytvorenie React aplikácie a zabezpečenie všetkých potrebných knižníc pre klientskú aplikáciu, tak aby bolo možné vytvoriť vizualizáciu histogramov v VR.
Ak máme pripravený projekt so všetkým potrebným pre vytvorenie komponentov pre vizualizáciu.

Prvý krok je získať potrebné objekty, ktoré bude potrebné vizualizovať pomocou komponentu **ndmVrHistogramScene**. Na tento účel vieme pomocou objektu JSROOT vytvoriť histogram ale rovnako aj získať objekty z **ROOT suborov**.

## Získanie objektov z ***ROOT súborov***

---

V tomto prípade je potrebná url adresa root súboru. Zo súboru by sme mali získať všetky objekty, potrebné pre vizualizáciu.

Pre načítanie objektu predpokladajme, že máme umiestnenú v premennej jsroot uloženú referenciu na objekt JSROOT, ktorý obsahuje všetky potrebné funkcie.

```js
// url root suboru
let filename = 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root'
// JSROOT funkcia pre otvorenie súboru, ako návratovú hodnotu ziskavame Promise.
JSROOT.openFile(filename)
// ak funkcia otvorí súbor následne dôjde k prečítaniu objektu "main" a vracia sa prečítaný objekt
.then(file => file.readObject('main'))
.then(obj => {
/*
* V tomto bloku spracujeme samotný objekt
* Objekt získame ako parameter obj a vieme s ním pracovať
* Vhodné je uloženie objektu do premennej a nastavenie stavu indikujúceho prístupnosť objektu
*/
})
.catch(error => {
/*
* V prípade ak dôjde k chybe vykoná sa tento blok
*/
});
```


## Vytvorenie objektu histogramu

---

JSROOT objekt obsahuje aj funkcie slúžiace na vytváranie histogramov, tento postup je menej odporúčaný vzhľadom na to, že vytváranie zložitých histogramov môže byť náročné pre prehliadač na strane klienta.

Najviac odporúčané je preto vytvorenie štatistík v ROOT aplikácii na špeciálnom zariadení a distribuovať klientovi už hotové objekty v ROOT súbore, ktoré následne prehliadač klienta len načíta a vizualizuje.

```js
// Fragment, ktorý vytvorí histogram TH2I s rozmermi 20x20 a vyplní obsahy binov
const histo = jsroot.createHistogram('TH2I', 20, 20)
// Počítadlo
let cnt = 0;
// Cykly na zabezpečenie prechodu všetkých binov vytvoreného histogramu
for (var iy=1;iy<=20;iy++)
  for (var ix=1;ix<=20;ix++) {
    // Získanie konkrétneho objektu binu na základe jeho súradnic v histograme
    var bin = histo.getBin(ix, iy), val = 0;
    val=cnt;
    // Nastavenie hodnoty binu
    histo.setBinContent(bin, val);
    cnt+=0.1;
  }
// Nastavenie globálnych údajov histogramu
histo.fXaxis.fTitle = 'x Axis'
histo.fYaxis.fTitle = 'y Axis'
histo.fName = "You don't have a valid file path"
histo.fTitle = "This is a TH2 histogram demo"
histo.fMaximum = cnt
```

## Vytvorenie vizualizácie

---

Ak už sú načítané potrebné objekty v aplikácii, tak je možné histogramy vizualizovať pomocou komponentov **NDMVR**.

```jsx
<NdmVrScene data={data} info={info} />

...

<JSrootHistogram histogram={histograms.physics} projectionAxes={['X','Y']} projections={histograms.projections} projIndex={0}/>
```

Komponenty vytvoria všetko potrebné. Parametre je nutné poskytnúť komponentom tak ako to bolo v dokumentácii NDMVR.
