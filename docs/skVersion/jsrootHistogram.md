---
title: NDMVR - JsrootHistogram
summary: Dokumentácia pre NDMVR
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# ***JsrootHistogram***

---

Komponent zobrazuje JSROOT projekcie.

Príklad zobrazenia projekcie **uloženej** v adresárovej štruktúre. v tomto prípade `projIndex` je dôležitý parameter, ktorý určuje index projekcie, ktorá má byť zobrazená. V súborovej štruktúre je uložené pole s projekciami a vieme si vybrať ktorý typ projekcie je potrebné zobraziť.

```jsx
<JSrootHistogram histogram={histogram} projectionAxes={['X','Y']} projections={projections} projIndex={0}/>
```

Príklad zobrazenia projekcie, **vytvorenej** závislosťou osí X a Y. V tomto prípade `projIndex` nie je potrebné definovať.

```jsx
<JSrootHistogram histogram={histogram} projectionAxes={['X','Y']} projections={null} projIndex={0}
```

## Atribút ***histogram***

Tento objekt definuje hlavný histogram TH.

Najčastejšie dostaneme tento objekt načítaním z **ROOT súboru** alebo vytvorením pomocou funkcií knižnice [JSROOT](https://github.com/root-project/jsroot/blob/master/docs/JSROOT.md).

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **histogram** | Object | Hlavný objekt histogramu určený na vizualizáciu | `null` | `False` |

## Atribút ***projections***

Súborová štruktúra obsahuje organizované adresáre a projekcie pre každý bin hlavného histogramu.

Samotná projekcia je taktiež objekt histogramu. Tento histogram obsahuje konkrétnejšie dáta. Každý bin hlavného histogramu môže obsahovať viac projekcií zamerané na konkrétnu informáciu.

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **projections** | Object | Súborová štruktúra obsahujúca projekcie | `null` | `True` |

Štruktúra súborov `root` môže vyzerať nasledovne:

``` sh
.root
├── 1
│   └── 1
│   │   └── projectionObject
│   │   └── ...
│   └── 3
│   └── 4
│   └── ...
├── 2
│   └── 1
│   └── 2
│   └── ...
└── ...
```

Zobrazená súborová štruktúra opisuje vzor, kde každý bin má svoje projekcie adresované v adresároch s cestou, ktorá určuje súradnice binu.
Ako môžme vidieť, bin s x súradnicou = 1 a y súradnica = 1 má projekcie uložené v adresári s cestou `root/1/1/projArray[0]`

## Atribút ***projectionAxes***

Ak chceme vytvoriť 2D projekciu na základe údajov v hlavnom histograme, tak je potrebné definovať osi podľa ktorých sa vytvorí výsledná projekcia.

V prípade ak potrebujeme načítať projekciu z **ROOT súboru** je tento parameter nepotrebný.

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **projectionAxes** | Array | pole obsahujúce názvy osí | `null` | `True` |

## Atribút ***projIndex***

Definuje na akom indexe je v poli projekcií uložená projekcia, ktorú chceme vizualizovať.

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **projIndex** | number | index projekcie uloženej v poli | `0` | `True` |
