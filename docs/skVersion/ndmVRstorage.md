---
title: NDMVR - NdmVrStorage
summary: Dokumentácia pre NDMVR
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# ***NdmVRStorage***

---

Servis zabezpečuje menežment lokálneho úložiska. Je možné uschovať aktuálne údaje o zobrazovaných sekciách v lokálnom úložisku klientskej aplikácie. Service umožní načítať uložené dáta ale aj uložiť dáta v požadovanom formáte, aby následne bolo možné tieto dáta poskytnúť komponentu pre vizualizáciu. Servis poskytuje nasledovné funkcie:

- Funkcia pre uloženie údajov:

```js
const section = {
  name: 'TH3',
  xOffset: 1,
  yOffset: 1,
  zOffset: 1,
  range: 8
}
storeOffsets(section)
```
- Funkcia pre načítanie údajov z lokálneho úložiska:

```js
loadOffsets('TH3')
```

Ak v lokálnom úložisku sú uložené dáta, získame objekt, ktorý predstavuje sekciu určenú na zobrazenie.
