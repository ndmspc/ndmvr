---
title: NDMVR - Inštalácia
summary: Dokumentácia pre NDMVR
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# Inštalácia

---
Komponenty spájajú technológie, založené na softvérových rámcoch **React** a **A-Frame**. Z tohoto dôvodu je balík komponentov možné jednoducho nainštalovať použitím [npm](https://www.npmjs.com/package/@ndmspc/ndmvr) package managerom a to príkazom `npm install @ndmspc/ndmvr`.

Po inštalácii, máme k dispozícií 3 komponenty, ktoré môžeme importovať a použiť v našej klientskej aplikácii založenej na softvérovom rámci react.

<code>import { ndmVrStorage, NdmVrScene, JSrootHistogram } from '@ndmspc/ndmvr'</code>

Knižnica obsahuje komponenty, ktoré vizualizujú histogramy a spracuvávajú dáta potrebné pre správne vizualizovanie dát. Pre použitie komponentov je potrebné získať objekty histogramov, ktoré je najčastejšie možné získať prečítaním **ROOT súboru**.

Komponenty neriešia spôsob získania týchto dát, ale už pripravené objekty histogramov spracovávajú a následne vytvárajú vizualizáciu.

---
Pre zobrazenie ovládaní a pozadí je potrebné zabezpečiť súbor, ktorý bude obsahovať potrebné zdroje, ako sú obrázky. Používateľ si následne môže do súborovej štruktúry pridávať svoje vlastné pozadia do scény a následne ich použiť pri integrácií komponentu.
Taktiež je možné aj prispôsobenie ovládaní podľa charakteru klientskej aplikácie úpravou textúr.

V opačnom prípade používateľ bude mať funkčné komponenty, ktoré nebudú obsahovať príslušné textúry.

Súborová štruktúra:

``` sh
ndmvr
├── backgrounds
│   └── background1.png
│   └── background2.jpg
│   └── ...
├── keybordControls
│   └── keyboard.png
│   └── keuboard1.png
│   └── keyboard2.png
│   └── keuboard3.png
├── oculusControls
│   └── oculus.png
│   └── oculus1.png
│   └── oculus2.png
│   └── oculus3.png
└── ...
```

Súbor musí byť vložený do adresára `public` v klientskej aplikácií aby sa zabezpečil prístup k zdrojom v projekte.

Pre načítanie všetkých predvolených textúr a zdrojov pre ovládanie a interakciu je nutné získať predvolený súbor stiahnutím zdrojov projektu:

>
>- [zip](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.zip)
>- [tar.gz](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar.gz)
>- [tar.bz2](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar.bz2)
>- [tar](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar)
>

Po získaní zdrojov je potrebné presunúť adresár `etc/ndmvr` do adresára `public` v klientskej aplikácii.

Následne tak je všetko pripravené pre používanie balíka v klientskej aplikácii.
