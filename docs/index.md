---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# JavaScript NDMVR

---

Library for displaying TH histograms in virtual reality.

> #### Library provides:
>
> - ndmVrHistogramScene
> - ndmVrStorage
> - jsrootHistogram
>

<figure>
  <img src="src/lib/assets/image1.png" width="1400"  alt="img1"/>
  <figcaption>Application view. On the left side of the screen, there is a scene with an imported histogram (ndmVrHistogramScene), on the right side of the screen there is displayed a projection.(jsrootHistogram).</figcaption>
</figure>

---

<figure>
  <img src="src/lib/assets/image2.png" width="1400"  alt="img2"/>
  <figcaption>View with displayed control panels. The user can rotate panels and change the displayed content.</figcaption>
</figure>

---

<figure>
  <img src="src/lib/assets/image3.png" width="1400"  alt="img3"/>
  <figcaption>View with displayed projection panels. User can rotate panels and change the displayed content. Each panel has space to display a different projection for the selected bin.</figcaption>
</figure>

---

<figure>
  <img src="src/lib/assets/image4.png" width="1400"  alt="img4"/>
  <figcaption>To display the projections, after clicking on the bin, a preview of the projection is displayed on the board directly in the VR, so that the user can at least find out in detail the nature of the examined data.</figcaption>
</figure>