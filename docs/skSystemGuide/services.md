---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Adresár ***services***

Adresár obsahuje servisne triedy, ktoré zapúzdrujú logiku do celkov.

Triedy sú využívané v rôzných komponentoch pre zabezpečenie potrebnej funkcionality.

---

## class CameraService

---
Servis pre ovládanie a manipuláciu pozície kamery.

### verticalMoveCamera ⇒ <code>void</code>

---
Zmena vertikálnej polohy kamery.

| Param | Type | Description |
| --- | --- | --- |
| moveUp | <code>boolean</code> | Ak je True kamera vyššie, ak je False kamera nižšie |
| speed | <code>number</code> | Rýchlosť zmeny pozície kamery |

### horizontalMoveCamera ⇒ <code>void</code>

---
Zmena horizontálnej polohy kamery.

| Param | Type | Description |
| --- | --- | --- |
| axis | <code>string</code> | Os po ktorej je realizovaný pohyb kamery |
| increment | <code>number</code> | Hodnota posunu kamery |
| speed | <code>number</code> | Rýchlosť zmeny pozície kamery |

### setCameraPosition ⇒ <code>void</code>

---
Zmena vertikálnej polohy kamery podľa ofsetov.


| Param | Type | Description |
| --- | --- | --- |
| offsets | <code>Object</code> | Ofsety zobrazenej sekcie |

### setPredefinedDownPosition ⇒ <code>void</code>

---
Zmena polohy kamery podľa predefinovanej pozície.

Pozícia na začiatku ofsetu na osi Z

### setPredefinedUpPosition ⇒ <code>void</code>

---
Zmena polohy kamery podľa predefinovanej pozície

Pozícia na konci ofsetu na osi Z

### setPredefinedRightPosition ⇒ <code>void</code>

---
Zmena polohy kamery podľa predefinovanej pozície

Pozícia na začiatku ofsetu na osi X

### setPredefinedLeftPosition ⇒ <code>void</code>

---
Zmena polohy kamery podľa predefinovanej pozície

Pozícia na začiatku ofsetu na osi X

### setPredefinedDownPositionWithOffset ⇒ <code>void</code>

---
Zmena polohy kamery podľa predefinovanej pozície.

Pozícia na začiatku ofsetu na osi Z s ofsetom na osi Y

### setPredefinedUpPositionWithOffset ⇒ <code>void</code>

---
Zmena polohy kamery podľa predefinovanej pozície

Pozícia na konci ofsetu na osi Z s ofsetom na osi Y

### setPredefinedRightPositionWithOffset ⇒ <code>void</code>

---
Zmena polohy kamery podľa predefinovanej pozície

Pozícia na začiatku ofsetu na osi X s ofsetom na osi Y

### setPredefinedLeftPositionWithOffset ⇒ <code>void</code>

---
Zmena polohy kamery podľa predefinovanej pozície

Pozícia na konci ofsetu na osi X s ofsetom na osi Y


## class JsrootService

---
Servis pre vytváranie JSROOT projekcií.

### jsrootLibrary ⇒ <code>void</code>

---
Funkcia zabezpečí nastavenie objektu JSROOT servisu.

| Param | Type | Description |
| --- | --- | --- |
| JSROOT | <code>Object</code> | Object JSROOT |

### createTH1Projection(projectionAxis, info, idTargetElm, axisArray) ⇒ <code>void</code>

---
Funkcia pre vytvorenie TH1 projekcie.

| Param | Type | Description |
| --- | --- | --- |
| projectionAxis | <code>string</code> | Hlavná os podľa, ktorej sa vytvorí projekcia |
| info | <code>Object</code> | Objekt obsahujúci histogramy a objekt obsahujúci informácie o zvolenom bine |
| idTargetElm | <code>string</code> | Identifikátor cieľovej entity, do ktorej sa vykreslí výsledná projekcia |
| axisArray | <code>Array</code> | Pole osí, obsahuje 2 osi na základe ktorých sa projekcia vytvorí |

### openTH1Projection(projectionAxis, info, idTargetElm, axisArray, projections, projIndex) ⇒ <code>void</code>

---
Funkcia pre prečítanie objektu projekcie z adresárovej štruktúry získanej z root súboru.

| Param | Type | Description |
| --- | --- | --- |
| projectionAxis | <code>string</code> | Hlavná os podľa, ktorej sa vytvorí projekcia |
| info | <code>Object</code> | Objekt obsahujúci histogramy a objekt obsahujúci informácie o zvolenom bine |
| idTargetElm | <code>string</code> | Identifikátor cieľovej entity, do ktorej sa vykreslí výsledná projekcia |
| axisArray | <code>Array</code> | Pole osí, obsahuje 2 osi na základe ktorých sa projekcia vytvorí |
| projections | <code>Object</code> | Adresárová štruktúra obsahujúca projekcie |
| projIndex | <code>number</code> | Index projekcie, ktorá sa má vykresliť |


### displayImageOfProjection(idSourceElement, idTargetElement, width, height) ⇒ <code>void</code>

---
Funkcia získa aktuálny náhľad projekcie SVG elementu a zobrazí ju na panely vo VR

| Param | Type | Description |
| --- | --- | --- |
| idSourceElement | <code>string</code> | Identifikátor zdrojového elementu obsahujúceho SVG projekciu |
| idTargetElement | <code>string</code> | Identifikátor cieľového elementu vo VR, kde bude vložený náhľad projekcie |
| width | <code>number</code> | Šírka pre získanie náhľadu projekcie |
| height | <code>number</code> | Výška pre získanie náhľadu projekcie |

## class NdmVrStorageService

---
Servis pre ovládanie lokálneho úložiska.
Ukladanie a načítavanie dát o ofsetoch a histograme.

### new NdmVrStorageService()

---
Nastavenie referencií a potrebných identifikátorov.
Načítanie poľa označených binov z lokálneho úložiska, ak nejaké sú.

### containThisBin ⇒ <code>boolean</code>

---
Overenie, či je bin v poli označených binov.

**Návratová hodnota**: <code>boolean</code> - - Ak je True bin v poli označených binov, ak False bin nie je v poli označených binov

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahujúci informácie o bine |

### storeBinToLocalStorage ⇒ <code>void</code>

---
Uloženie binu, uloženie do poľa označených binov.

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahujúci informácie o bine |

### deleteBinFromLocalStorage ⇒ <code>void</code>

---
Odstránenie binu, vymazanie binu z poľa označených binov.

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahujúci informácie o bine |

### getBinsFromLocalStorage ⇒ <code>Array</code>

---
Načítanie všetkých binov z poľa označených binov.

**Návratová hodnota**: <code>Array</code> - - Pole s označenými binmi

### storeCurrentBinToLocalStorage(binData) ⇒ <code>void</code>

---
Funkcia pre uloženie binu do zoznamu označených binov.

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahujúci všetky dáta o zvolenom bine |

### loadProperty(property) ⇒ <code>Object</code>

---
Funkcia pre načítanie aktuálnych informácií o histograme.

**Návratová hodnota**: <code>Object</code> - - Objekt obsahujúci informácie o sekciách určitého histogramu

| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | Objekt označujúci typ histogramu, pre ktorý sa majú získať ofsety |

### storeProperty(property, object) ⇒ <code>void</code>

---
Funkcia pre uloženie aktuálnych informácií o histograme.

| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | Objekt označujúci typ histogramu, pre ktorý sa majú získať ofsety |
| object | <code>Object</code> | Objekt obsahujúci informácie o sekciách určitého histogramu |

### storeTH2Offsets(xOffset, yOffset, range) ⇒ <code>void</code>

---
Funkcia pre uloženie aktuálnych informácií o histograme TH2.

| Param | Type | Description |
| --- | --- | --- |
| xOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi X |
| yOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi Y |
| range | <code>number</code> | Hodnota reprezentujúca rozsah binov na osiach |

### storeTH3Offsets(xOffset, yOffset, zOffset, range) ⇒ <code>void</code>

---
Funkcia pre uloženie aktuálnych informácií o histograme TH3.

| Param | Type | Description |
| --- | --- | --- |
| xOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi X |
| yOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi Y |
| zOffset | <code>number</code> | Hodnota reprezentujúca ofset na osi Z |
| range | <code>number</code> | Hodnota reprezentujúca rozsah binov na osiach |

### storeOffsets(section) ⇒ <code>void</code>

---
Funkcia pre uloženie aktuálnych informácií o histograme.

| Param | Type | Description |
| --- | --- | --- |
| section | <code>Object</code> | Objekt obsahujúci ofsety a rozsah pre histogramy. Objekt obsahuje aj typ histogramu podľa ktorého sa uloží správna sekcia do lokálneho úložiska |

### storeFilePath(filePath) ⇒ <code>void</code>

---
Uloženie názvu root súboru.

| Param | Type | Description |
| --- | --- | --- |
| filePath | <code>string</code> | URL cesta k súboru |

### loadFilePath() ⇒ <code>string</code>

---
Načítanie názvu root súboru.

**Návratová hodnota**: <code>string</code> - - URL root súboru

### loadTH2Offsets() ⇒ <code>Object</code>

---
Načítanie informácií o TH2 histograme

**Návratová hodnota**: <code>Object</code> - - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom úložisku vracia null

### loadTH3Offsets() ⇒ <code>Object</code>

---
Načítanie informácií o TH3 histograme.

**Návratová hodnota**: <code>Object</code> - - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom úložisku vracia null

### loadOffsets(type) ⇒ <code>Object</code>

---
Načítanie informácií o histograme.

**Návratová hodnota**: <code>Object</code> - - Object obsahujúci ofsety, rozsah, typ histogramu. Ak sa nenachádza v lokálnom úložisku vracia null

| Param | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | Typ histogramu podľa, ktorého sa získa správny objekt obsahujúci informácie o histograme |
