---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Adresár ***subjects***

Adresár obsahuje triedy, ktoré umožňujú vytvorenie komunikačného kanálu na komunikáciu medzi 2 odlišnými časťami aplikácie v reálnom čase.

Triedy umožnia publikovať dáta a signály.

Inštancia umožní komponentu na druhej strane prihlásenie k odberu dát a signálov.

---
## class BinSubject

Subjekt pre publikáciu signálov a dát pre biny.

### saveSelectedBinToLocalStorage() ⇒ <code>void</code>

---
Publikácia signálu s hodnotou `True` pre označenie daného binu.

### deleteBinFromLocalStorage() ⇒ <code>void</code>

---
Publikácia signálu s hodnotou `False` pre odznačenie daného binu.

### getKeyboardEvent() ⇒ <code>Subject</code>

---
Získanie objektu pre prístup k odberu signálov a dát.

**Návratová hodnota**: <code>Subject</code> - - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály

## CameraSubject

---
Subjekt pre publikáciu signálov a dát pre kameru.

### setVisibilityOfBanners() ⇒ <code>void</code>

---
Publikácia signálu s hodnotou špecifikujúcou vstupné zariadenie pre zobrazenie príslušných panelov.

### shiftBanners() ⇒ <code>void</code>

---
Publikácia signálu s hodnotou "shift" pre posúvanie panelov.

### setUserState() ⇒ <code>void</code>

---
Publikácia signálu s hodnotou "show" pre zobrazenie prislúchajúcich panelov.

### getCameraSubject() ⇒ <code>Subject</code>

---
Získanie objektu pre prístup k odberu signálov a dát.

**Návratová hodnota**: <code>Subject</code> - - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály

## HistogramSubject

---
Subjekt pre publikáciu signálov a dát pre histogram.
Prezobrazovanie sekcií histogramu.

#### histogramSubject.changeHistogramSectionByOffset(axis, histogramType, increment, defaultRange) ⇒ <code>void</code>

---
Publikácia signálu pre prezobrazenie sekcie histogramu.

| Param | Type | Description |
| --- | --- | --- |
| axis | <code>string</code> | Názov osi na ktorej sa budú posúvať ofsety |
| histogramType | <code>string</code> | Typ histogramu (TH2 alebo TH3) |
| increment | <code>boolean</code> | Ak je True tak sa hodnota prípočíta k ofsetu, ak False tak sa odpočíta |
| defaultRange | <code>boolean</code> | Ak je True tak sa použije predvolená hodnota (1), ak False tak nastavený rozsah |

### changeHistogramFunction(typeFunction, histogramType) ⇒ <code>void</code>

---
Publikácia signálu pre zmenu vizuálneho módu.

| Param | Type | Description |
| --- | --- | --- |
| typeFunction | <code>string</code> | Názov funkcie podľa, ktorej sa zmení vizualizačný mód |
| histogramType | <code>string</code> | Typ histogramu (TH2 alebo TH3) |

### getChangedSection() ⇒ <code>Subject</code>

---
Získanie objektu pre prístup k odberu signálov a dát.

**Návratová hodnota**: <code>Subject</code> - - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály

## JsrootSubject

---
Subjekt pre publikáciu signálov a dát pre JSROOT.

### showBinProjection(binData)

---
Zobrazenie projekcie pre zvolený bin histogramu.

| Param | Type | Description |
| --- | --- | --- |
| binData | <code>Object</code> | Objekt obsahuje dáta o zvolenom bine |

### getServiceEvent() ⇒ <code>Subject</code>

---
Získanie objektu pre prístup k odberu signálov a dát.

**Návratová hodnota**: <code>Subject</code> - - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály

