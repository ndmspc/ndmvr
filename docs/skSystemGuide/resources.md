---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Adresár ***resources***

Adresár obsahuje provider a objekty definujúce štýly pre vykreslenie histogramov.

---

## class ThemeProvider

---
Provider pre zabezpečenie všetkých potrebných farieb a štýlov.

### new ThemeProvider(theme)

---
Konštruktor pre nastavenie všetkých potrebných atribútov objektu.


| Param | Type | Description |
| --- | --- | --- |
| theme | <code>Object</code> | Objekt obsahujúci všetky potrebné palety a údaje o témach a fontoch |

### getAxisColor(axis) ⇒ <code>string</code>

---
Získanie kompetentnej farby pre entitu osi.

**Návratová hodnota**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu

| Param | Type | Description |
| --- | --- | --- |
| axis | <code>string</code> | Označenie osi |

### getPrimaryAccentColor() ⇒ <code>string</code>

---
Získanie hlavnej farby pre zvýraznenie a označenie.

**Návratová hodnota**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu

### getSecondaryAccentColor() ⇒ <code>string</code>

---
Získanie sekundárnej farby pre zvýraznenie a označenie.

**Návratová hodnota**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu

### getPrimaryFontColor() ⇒ <code>string</code>

---
Získanie hlavnej farby písma.

**Návratová hodnota**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu

### getSecondaryFontColor() ⇒ <code>string</code>

---
Získanie sekundárnej farby písma.

**Návratová hodnota**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre entitu

### getBinColor(content) ⇒ <code>string</code>

---
Získanie kompetentnej farby pre bin podľa obsahu binu.

**Návratová hodnota**: <code>string</code> - - Vracia null alebo aktuálnu farbu pre bin, ktorá náleží binu podľa obsahu

| Param | Type | Description |
| --- | --- | --- |
| content | <code>number</code> | Hodnota definujúca obsah binu |

## Object Theme

---
Súbor obsahuje pole objektov, ktoré definujú jednotlivé témy pre zobrazenie histogramu.

Príklad objektu a všetkých jeho atribútov, ktoré definujú farebné palety pre biny, osi, označenia.

```js
{
    colorPalette: [
      '#4b0066',
      '#8300b3',
      '#bb00ff',
      '#cf4dff',
      '#e499ff',
      '#ffffcc',
      '#ffff99',
      '#ffff66',
      '#ffff33',
      '#fcba03'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#4b0066',
      secondary: '#FF3700'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
}
```

