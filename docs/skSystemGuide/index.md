---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Architektúra

Pre implementáciu bolo potrebné nájsť spôsob zobrazenia jednotlivých entít histogramu a zabezpečiť potrebnú interaktivitu.
Používateľ musí byť schopný vykonávať všetky nutné úkony s vizualizáciou. Keďže vizualizácia je integrovaná do virtuálnej reality, tak je potrebné zabezpečiť aj možnosť interakcie so špeciálnymi ovládačmi.

Havnú architektúru riešenia môžme rozdeliť na:

- ***A-Frame vrstvu***
- ***React vrstvu***

<figure>
  <img src="../assets/layers.png" width="381"  alt="layers"/>
</figure>

Obmieňanie entít histogramu je zabezpečené Reactom na signál používateľa, ktorý spracuje komponent A-Frame. Tento prístup tak zabezpečí používateľovi možnosť použiť aj špeciálne zariadenia, ktoré majú v A-Frame vytvorené komponenty na spracovanie špecifických udalostí.

Okrem React a A-Frame komponentov, projekt obsahuje pomocné moduly, servisné objekty zapuzdrujúce funkcionalitu do celkov a subjekty pre komunikáciu rôznych komponentov medzi sebou pre zabezpečenie komunikácie medzi vstvami.
