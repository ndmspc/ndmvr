---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Adresár ***controllers***

Adresár obsahuje moduly definujúce funkcie pre spracovanie vstupov používateľa.

Každé vstupné zariadenie by malo obsahovať modul, ktorý definuje funkcie na spracovanie vstupov z určitého vstupného zariadenia.

---
## keyboardController

---
Modul obsahuje implementované metódy, ktoré reagujú na vstupy používateľa z klávesnice.
Funkcie sú určené pre komponenty slúžiace definovanie listenerov pre klávesnicu počítača.

### handlePositioning(property, event) ⇒ <code>void</code>

---
Získanie objektu pre prístup k odberu signálov a dát.

| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | Definuje názov atribútu histogramu (poloha, rozmer, rotácia), ktorý bude modifikovaný |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### handleChangeHistogramSectionByDefaultRange(event) ⇒ <code>void</code>

---
Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na klávesnici

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### handleChangeHistogramSectionByOwnRange(event) ⇒ <code>void</code>

---
Zmena zobrazovanej sekcie histogramu o vlastný rozsah (podľa nastavení používateľa) na klávesnici

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### keyPressHandlerFunction(event) ⇒ <code>void</code>

---
Funkcia, vukonávajúca sa na klik používateľa.
Funkcia detekuje, uloží stlačenú klávesu do poľa stlašených kláves a zavolá náležitú funkciu

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### keyReleaseHandlerFunction(event) ⇒ <code>void</code>

---
Funkcia vymaže klávesu z poľa stlačených kláves

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcií |

### initialKeyboardController(data, object) ⇒ <code>void</code>

---
Funkcia inicializuje všetky údaje o histograme a samotnú referenciu na histogram

| Param | Type | Description |
| --- | --- | --- |
| data | <code>Object</code> | Objekt obsahuje všetky atribúty (rozmer, pozícia, rotácia) |
| object | <code>HTMLElement</code> | Referencia na element histogramu, ktorý sa má modifikovať |

### keyboardUpdateCameraReference() ⇒ <code>void</code>

---
Funkcia aktualizuje referenciu na servis kamery


## oculusController

---
Modul obsahuje implementované metódy, ktoré reagujú na vstupy používateľa z ovládača zariadenia Oculus.
Funkcie sú určené pre komponenty slúžiace definovanie listenerov pre ovládač Oculusu.


### thumbStickByOwnOffset(event) ⇒ <code>void</code>

---
Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na ovládači Oculusu.

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### thumbStickByDefaultOffset(event) ⇒ <code>void</code>

---
Zmena zobrazovanej sekcie histogramu o špecifikovaný rozsah (podľa nastavení používateľa) na ovládači Oculusu.

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### thumbStickForMoving(event, speed) ⇒ <code>void</code>

---
Funkcia, zabezpečujúca horizontálny pohyb kamery po osiach.

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |
| speed | <code>number</code> | Definuje rýchlosť pohybu |

### thumbStickPredefinedPosition(event) ⇒ <code>void</code>

---
Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (len X a Z súradnica).

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### thumbStickPredefinedPositionWithOffset(event) ⇒ <code>void</code>

---
Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (X a Y a Z súradnica).

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### oculusUpSection(defaultRange) ⇒ <code>void</code>

---
Funkcia, zobrazenie novej sekcie smerom hore.

| Param | Type | Description |
| --- | --- | --- |
| defaultRange | <code>boolean</code> | Ak je True tak bude použitý range (1), ak je False tak (nastavený rozsah) |

### oculusDownSection(defaultRange) ⇒ <code>void</code>

---
Funkcia, zobrazenie novej sekcie smerom dolu

| Param | Type | Description |
| --- | --- | --- |
| defaultRange | <code>boolean</code> | Ak je True tak bude použitý range (1), ak je False tak (nastavený rozsah) |

### oculusUpdateCameraReference() ⇒ <code>void</code>

---
Funkcia, aktualizuje servis kamery.


### oculusThumbStickFunction(event) ⇒ <code>void</code>

---
Zmena zobrazovanej sekcie histogramu o špecifikovaný rozsah (podľa nastavení používateľa) na ovládači Oculusu.

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### oculusThumbStickWithGripFunction(event) ⇒ <code>void</code>

---
Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na ovládači Oculusu.

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### oculusThumbStickPredefinedCameraPosition(event) ⇒ <code>void</code>

---
Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (len X a Z súradnica).

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### oculusThumbStickPredefinedCameraPositionWithOffset(event) ⇒ <code>void</code>

---
Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (X a Y a Z súradnica).

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |

### oculusThumbStickForMoving(event, speed) ⇒ <code>void</code>

---
Funkcia, zabezpečujúca horizontálny pohyb kamery po osiach.

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | Definuje Event obsahujúci informácie o používateľovej interakcii |
| speed | <code>Number</code> | Definuje rýchlosť pohybu |

### oculusXButtonDownFunction(speed) ⇒ <code>void</code>

---
Funkcia, zabezpečí posunutie kamery smerom hore

| Param | Type | Description |
| --- | --- | --- |
| speed | <code>Number</code> | Určuje rýchlosť pohybu kamery |

### oculusYButtonDownFunction(speed) ⇒ <code>void</code>

---
Funkcia, zabezpečí posunutie kamery smerom dolu

| Param | Type | Description |
| --- | --- | --- |
| speed | <code>Number</code> | Určuje rýchlosť pohybu kamery |

### oculusThumbStickMarkBin() ⇒ <code>void</code>

---
Funkcia, zabezpečí označenie binu

### oculusThumbStickUnmarkBin() ⇒ <code>void</code>

---
Funkcia, zabezpečí odznačenie binu

### oculusShowFunctionView() ⇒ <code>void</code>

---
Funkcia, zabezpečí prepnutie do špeciálneho módu, určeného funkciou

### oculusShowDefaultView() ⇒ <code>void</code>

---
Funkcia, zabezpečí prepnutie do predvoleného vizualizačného módu

### oculusSwitchViewWithBanners() ⇒ <code>void</code>

---
Funkcia, zabezpečí prepínanie medzi zobrazeniami

### oculusChangeBannerContent() ⇒ <code>void</code>

---
Funkcia, zabezpečí zmenu obsahu na paneloch

### oculusShiftBanners() ⇒ <code>void</code>

---
Funkcia, zabezpečí rotovanie panelov

### oculusRedrawHistogramBanners() ⇒ <code>void</code>

---
Funkcia, prekreslí náhľady na paneloch vo VR
