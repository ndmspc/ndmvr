---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Štruktúra projektu

Súborová štruktúra je zložená z nasledujúcich adresárov:

``` sh
src
├── aframeComponents
├── components
├── controllers
├── observables
├── resources
├── services
├── utils
└── AframeComponent.js
```

Adresár ***aframeComponents*** obsahuje:

``` sh
src
├── aframeComponents
    └── cursor.js
    └── histogram.js
    └── labelHandler.js
    └── leftOculusController.js
    └── rightOculusController.js
    └── binth.js
    └── th3AframeComponent.js
```

Adresár ***components*** obsahuje:

``` sh
src
├── components
    │── JsrootHistogram
    │    └── AframeComponent.js
    │── NdmVrHelp
    │    └── AframeComponent.js
    │── NdmVrHistogram
    │    └── AframeComponent.js
    └── NdmVrHistogramScene
         └── AframeComponent.js
```

Adresár ***controllers*** obsahuje:

``` sh
src
├── controllers
    └── keyboardController.js
    └── oculusController.js
```

Adresár ***observables*** obsahuje:

``` sh
src
├── observables
    └── binSubject.js
    └── cameraSubject.js
    └── histogramSubject.js
    └── jsrootSubject.js
```

Adresár ***resources*** obsahuje:

``` sh
src
├── resources
    └── theme.js
    └── themeProvider.js
```

Adresár ***services*** obsahuje:

``` sh
src
├── services
    └── cameraService.js
    └── jsrootService.js
    └── ndmVrStorageService.js
```

Adresár ***utils*** obsahuje:

``` sh
src
├── utils
    └── histogramReactFactory.js
```
