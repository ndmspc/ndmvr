---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Adresár ***aframeComponents***

Adresár obsahuje A-Frame komponenty určené pre inicializáciu entít.

Rovnako komponenty zabezpečia potrebnú interaktivitu medzi entitami a používateľom.

---
## cursorEventAframeComponent

---
A-Frame interakčný komponent pridávajúci funkcionalitu entite, ktorá spracúvava prieniky medzi entitou a raycastrom používateľa.

**Component**: mouseevent

## histogramAframeComponent

---
A-Frame interakčný komponent pridávajúci funkcionalitu entite histogram, ktorá umožňuje zmenu atribútov entity.

**Component**: histogram-control

| Param | Type | Description |
| --- | --- | --- |
| histogram | <code>Object</code> | Objekt histogramu z ktorého je potrebné spraviť projekciu |
| projectionAxes | <code>Array</code> | Pole obsahuje názvy osí, podľa ktorých sa má vytvoriť projekcia |
| projections | <code>Object</code> | Objekt obsahujúci súborovú štruktúru s projekciami |
| projIndex | <code>Number</code> | Index projekcie v poli, ktorá sa má zobraziť |


## labelHandlerAframeComponent

---
A-Frame komponent zabezpečí oneskorené aktualizovanie označenia na entite.

**Component**: label-handler

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | Hodnota označenia, na ktorú sa má aktuálne označenie zmeniť |
| delay | <code>number</code> | Hodnota oneskorenia aktualizácie označenia na entite |

## leftOculusAframeComponent

---
A-Frame interakčný komponent zabezpečí možnosť používateľa interagovať s ľavým ovládačom zariadenia Oculus.

**Component**: left-controller-logging

## rightOculusAframeComponent

---
A-Frame interakčný komponent zabezpečí možnosť používateľa interagovať s pravým ovládačom zariadenia Oculus.

**Component**: right-controller-logging

## th2AframeComponent

---
A-Frame interakčný komponent pridávajúci funkcionalitu entite binu TH2 histogram, ktorá umožňuje zmenu atribútov entity.

**Component**: binth2

| Param | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | Identifikátor binu |
| content | <code>number</code> | Hodnota optimalizovaného obsahu binu (predvolená 0) |
| absoluteContent | <code>number</code> | Hodnota neoptimalizovaného obsahu binu (predvolená 0) |
| binName | <code>string</code> | Meno binu |
| xTitle | <code>string</code> | Titulok na osi x |
| yTitle | <code>string</code> | Titulok na osi y |
| xMin | <code>number</code> | Súradnica min binu na osi x |
| yMin | <code>number</code> | Súradnica min binu na osi y |
| xMax | <code>number</code> | Súradnica max binu na osi x |
| yMax | <code>number</code> | Súradnica max binu na osi y |
| xCenter | <code>number</code> | Súradnica stredu binu na osi x |
| yCenter | <code>number</code> | Súradnica stredu binu na osi y |
| xWidth | <code>number</code> | Šírka binu na osi x |
| yWidth | <code>number</code> | Šírka binu na osi y |
| color | <code>string</code> | Farba binu |
| axisX | <code>string</code> | Farba označení na osi x |
| axisY | <code>string</code> | Farba označení na osi y |
| axisZ | <code>string</code> | Farba označení na osi z |
| selectColor | <code>string</code> | Farba určená za zvolenie binu |
| markedColor | <code>string</code> | Farba určená pre označenie binu (v poli označených binov) |

## th2AframeComponent

---
A-Frame interakčný komponent pridávajúci funkcionalitu entite binu TH3 histogram, ktorá umožňuje zmenu atribútov entity.

**Component**: binth3

| Param | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | Identifikátor binu |
| content | <code>number</code> | Hodnota optimalizovaného obsahu binu (predvolená 0) |
| absoluteContent | <code>number</code> | Hodnota neoptimalizovaného obsahu binu (predvolená 0) |
| binName | <code>string</code> | Meno binu |
| xTitle | <code>string</code> | Titulok na osi x |
| yTitle | <code>string</code> | Titulok na osi y |
| xMin | <code>number</code> | Súradnica min binu na osi x |
| yMin | <code>number</code> | Súradnica min binu na osi y |
| zMin | <code>number</code> | Súradnica min binu na osi z |
| xMax | <code>number</code> | Súradnica max binu na osi x |
| yMax | <code>number</code> | Súradnica max binu na osi y |
| zMax | <code>number</code> | Súradnica max binu na osi z |
| xCenter | <code>number</code> | Súradnica stredu binu na osi x |
| yCenter | <code>number</code> | Súradnica stredu binu na osi y |
| zCenter | <code>number</code> | Súradnica stredu binu na osi z |
| xWidth | <code>number</code> | Šírka binu na osi x |
| yWidth | <code>number</code> | Šírka binu na osi y |
| zWidth | <code>number</code> | Šírka binu na osi z |
| color | <code>string</code> | Farba binu |
| axisX | <code>string</code> | Farba označení na osi x |
| axisY | <code>string</code> | Farba označení na osi y |
| axisZ | <code>string</code> | Farba označení na osi z |
| selectColor | <code>string</code> | Farba určená za zvolenie binu |
| markedColor | <code>string</code> | Farba určená pre označenie binu (v poli označených binov) |
