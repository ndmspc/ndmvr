---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Adresár ***components***

Adresár obsahuje implementované React komponenty pre menežment stavu histogramu a obmieňanie entít.

---
## JsrootHistogram(histogram, projectionAxes, projections, projIndex)

---
React komponent pre zobrazenie bočného panelu s JSROOT projekciami.
Zobrazenie SVG elementu s histogramom a potrebným formulárom pre nastavenie atribútov.

| Param | Type | Description |
| --- | --- | --- |
| histogram | <code>Object</code> | Objekt histogramu, z ktorého je potrebné spraviť projekciu |
| projectionAxes | <code>Array</code> | Pole obsahuje názvy osí, podľa ktorých sa má vytvoriť projekcia |
| projections | <code>Object</code> | Objekt obsahujúci súborovú štruktúru s projekciami |
| projIndex | <code>Number</code> | Index projekcie v poli, ktorá sa má zobraziť |

**Example**
```js
const histogram = {
  gitlab: '',// vytvorený alebo prečítany z root súboru JSROOT-tom
  projections: ''// prečítaná z root súboru JSROOT-tom
}
return (
  <JSrootHistogram
     histogram={histogram.gitlab}
     projectionAxes={['X','Y']}
     projections={histogram.projections}
     projIndex={0}/>
)
```

## NdmVrHelp()

---
React komponent pre vytvorenie kamery a jej menežment.

**Example**
```js
return (
 <NdmVrHelp />
)
```
<a name="NdmVrHistogram"></a>

## NdmVrHistogram(name, histogram, histoSection, projections, range, theme)

---
React komponent pre vytvorenie histogramu.


| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | Názov pre histogram |
| histogram | <code>Object</code> | Objekt histogramu, ktorý sa bude v komponente vizualizovať |
| histoSection | <code>Object</code> | Objekt obsahujúci údaje o sekcii a rozsahu, ktorý bude zobrazený |
| projections | <code>Object</code> | Objekt obsahujúci súborovú štruktúru s projekciami |
| range | <code>number</code> | Definuje rozsah, v prípade ak sa vizualizuje bez určených ofsetov iba určitý rozsah |
| theme | <code>string</code> | Definuje tému pre vizualizáciu histogramu |

**Example**
```js
return (
  <NdmVrHistogram
   name={info.name}
   histogram={data.histogram}
   histoSection={data.section}
   projections={data.projections}
   range={data.range}
   theme={info.theme}/>
)
```
<a name="NdmVrScene"></a>

## NdmVrScene(data, info)

---
React komponent pre vytvorenie VR scény so všetkými potrebnými komponentami.

| Param | Type | Description |
| --- | --- | --- |
| data | <code>Object</code> | Objekt obsahuje objekty histogramu, sekcie poprípade projekcie |
| info | <code>Object</code> | Objekt obsahuje sekundárne údaje (meno, témy, atribúty pozadia v scéne) |

**Example**
```js
data = {
   histogram: histograms.physics,
   projections: histograms.projections,
   section: section
}
info = {
   name: 'Physics',
   theme: theme,
   background: {
       url: './ndmvr/backgrounds/background1.jpg',
       radius: '3000',
       height: '2048',
       width: '2048'
    }
 }
 return (
 <NdmVrScene data={data} info={info} />
)
```
