# ndmvr

[![NPM](https://img.shields.io/npm/v/@ndmspc/ndmvr.svg)](https://www.npmjs.com/package/@ndmspc/ndmvr)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

> NDimensional VR library

**NDMVR** is a package of the components that are used in the visualization of the histograms of the type such as TH in virtual reality.

Library for displaying TH histograms in virtual reality.

> ### Library provides:
>
> - ndmVrHistogramScene
> - ndmVrStorage
> - jsrootHistogram
>

---

## Install

```bash
npm install --save @ndmspc/ndmvr
```

After installation, you have 3 components which you can import and use.

<code>import { ndmVrStorage, NdmVrScene, JSrootHistogram } from '@ndmspc/ndmvr'</code>


To display the controls and background, you need to provide a file that contains the necessary resources, such as images. The user can then add their own backgrounds to the scene to the file structure and then use them to integrate the component. It is also possible to customize the controls according to the nature of the client application by editing textures.

Otherwise, the user will have functional components that do not contain the appropriate textures.

File structure:

``` sh
ndmvr
├── backgrounds
│   └── background1.png
│   └── background2.jpg
│   └── ...
├── keybordControls
│   └── keyboard.png
│   └── keuboard1.png
│   └── keyboard2.png
│   └── keuboard3.png
├── oculusControls
│   └── oculus.png
│   └── oculus1.png
│   └── oculus2.png
│   └── oculus3.png
└── ...
```

The file must be placed in the `public` directory in the client application to ensure access to resources in the project.

To load all the default textures and resources for control and interaction, you must obtain the default file by downloading project resources:

>
>- [zip](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.zip)
>- [tar.gz](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar.gz)
>- [tar.bz2](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar.bz2)
>- [tar](https://gitlab.com/ndmspc/ndmvr/-/archive/v0.1.0/ndmvr-v0.1.0.tar)
>

After obtaining the resources, it is necessary to move the `etc / ndmvr` directory to the` public` directory in the client application.

Subsequently, everything is ready to use the package in the client application.


### Prepare environment

For the use of this technology, it is necessary to have an appropriate environment for development, which depends on [Node >= 10.16](https://nodejs.org/en/) and the package manager such as [npm](https://www.npmjs.com/package/@ndmspc/ndmvr). The components have been developed using library [React](https://reactjs.org/docs/getting-started.html#try-react). From that reason, it is possible to integrate only in applications with the react.

---

## Components

---

### JsrootHistogram

The component displays JSROOT projections.

The example shows a projection from the root structure. In this case,`projIndex` is important and determines projection in the array.
```jsx
<JSrootHistogram histogram={histogram} projectionAxes={['X','Y']} projections={projections} projIndex={0}/>
```

The example shows a creating projection on axes X and Y. In this case, `projIndex` is useless.

```jsx
<JSrootHistogram histogram={histogram} projectionAxes={['X','Y']} projections={null} projIndex={0}
```

#### attribute ***histogram***

This object defines main histogram TH.
We get this object from ROOT file.
More info about th histograms and objects are [here]()

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **histogram** | Object | Main object defines histogram for displaying | `null` | `False` |

#### attribute ***projections***

File structure contains organised directories and projections for each bin of the main histogram.

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **projections** | Object | File structure with projection | `null` | `True` |

Structure of the files can look like that:

``` sh
.
├── 1
│   └── 1
│   │   └── projectionArray
│   └── 2
│   └── 3
│   └── 4
│   └── ...
├── 2
│   └── 1
│   └── 2
│   └── ...
└── ...
```

Displayed file structure describes the template where each bin has her own projection array addressed in directories with path, which determines the bin's coordinates.
As we can see, the bin with x coordinate 1 and y coordinate 1 has a projection stored in a directory with path `root/1/1/projArray[0]`.

#### attribute ***projectionAxes***

To create 2-dimensional projection we define attribute as an array with names of the axes.

This attribute is optional.

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **projectionAxes** | Array | Array of projection objects | `null` | `True` |

#### attribute ***projIndex***

Choose the right projection from the projection array of the root directory.

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **projIndex** | number | index of the projection in array | `0` | `True` |


### NdmVRHistogramScene

The component is primary maintained for displaying VR scene with TH histogram. It contains 2 properties such as data and info.

```jsx
<NdmVrScene data={data} info={info} />
```

Properties are defined as an object with the necessary data.

- **data** - primary data such as histogram object, projections...
- **info** - secondary data for define schemes, background attributes...

#### attribute ***data***

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **histogram** | Object | Main object defines histogram for displaying | `null` | `False` |
| **projections** | Object | File structure with projection | `null` | `True` |
| **section** |  Object | Data about offsets and histogram type | -- | `True` |

##### object ***histogram***

This object defines main histogram TH.
We get this object from ROOT file.
More info about th histograms and objects are [here]()

##### object ***projections***

File structure contains organised directories and projections for each bin of the main histogram.
Structure of the files can look like that:

``` sh
.
├── 1
│   └── 1
│   │   └── projectionArray
│   └── 2
│   └── 3
│   └── 4
│   └── ...
├── 2
│   └── 1
│   └── 2
│   └── ...
└── ...
```

Displayed file structure describes the template where each bin has her own projection array addressed in directories with path, which determines the bin's coordinates.
As we can see, the bin with x coordinate 1 and y coordinate 1 has a projection stored in a directory with path `root/1/1/projArray[0]`.

##### object ***section***

The object defines data for displaying the right amount of data. The face of the object represents attributes for offset throughout axes, range and the typename of the histogram. The parameter is optional.

Range defines how many bins will be displayed on an axis. In overall in Th3 histogram will be displayed (range+1 * range+1 * range+1) bins.

The object should look like that:

``` js
{
  name: 'TH3',
  xOffset: 1,
  yOffset: 1,
  zOffset: 1,
  range: 20
}
```

#### attribute ***info***

This parameter is used for defining general information about histograms and themes.

The object defines the scale and url of the image background, a user could use his or her own background image so that the user could save these images in the `public` directory of the client application.

Main properties of the object info:

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **name** | String | Unique name of the histogram | `null` | `True` |
| **theme** | String | String defines theme | `def` | `False` |
| **background** | Object | Object defines background properties | `null` | `True` |

Main properties of the object background:

| Property | Type | Description | Default | Optional |
| ------ | ------ | ------ | ------ | ------ |
| **url** | String | defines url of the background image | `./ndmvr/backgrounds/background1.jpg` | `False` |
| **radius** | number | background radius | `3000` | `False` |
| **height** | number | height of the background image | `2048` | `False` |
| **width** | number | width of the background image | `2048` | `False` |

``` js
{
  name: 'histogram unique name',
  theme: 'string characterizing the theme',
  background: {
      url: 'image url',
      radius: 'radius of the background in VR',
      height: 'height',
      width: 'height'
  }
}
```

### NdmVRStorage

Service manages offsets and ranges. Data are saved in localstorage for manipulation in next time.

- Service provide function for storing offsets:

```js
const section = {
  name: 'TH3',
  xOffset: 1,
  yOffset: 1,
  zOffset: 1,
  range: 8
}
storeOffsets(section)
```
- Service provide function for loading offsets:

```js
loadOffsets('TH3')
```

If the localstorage has stored data, we get returned value represented an object which determines the section.

---

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from '@ndmspc/ndmvr'
import '@ndmspc/ndmvr/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

---

## Example

The first step is to get the necessary objects that will need to be visualized using the **ndmVrHistogramScene** component. For this purpose, we can use the JSROOT object to create a histogram but also to obtain objects from **ROOT files**.

### get object from ***ROOT file***


In this case, the url address of the root file is required. We should get all the objects needed for visualization from the file.

To load an object, assume that we have a reference to the JSROOT object stored in the jsroot variable, which contains all the necessary functions.

```js
// url root suboru
let filename = 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root'
// JSROOT funkcia pre otvorenie súboru, ako návratovú hodnotu ziskavame Promise.
JSROOT.openFile(filename)
// ak funkcia otvorí súbor následne dôjde k prečítaniu objektu "main" a vracia sa prečítaný objekt
.then(file => file.readObject('main'))
.then(obj => {
/*
* V tomto bloku spracujeme samotný objekt
* Objekt získame ako parameter obj a vieme s ním pracovať
* Vhodné je uloženie objektu do premennej a nastavenie stavu indikujúceho prístupnosť objektu
*/
})
.catch(error => {
/*
* V prípade ak dôjde k chybe vykoná sa tento blok
*/
});
```


### Create histogram


The JSROOT object also contains functions for creating histograms, this procedure is less recommended due to the fact that creating complex histograms can be difficult for the browser on the client side.

Therefore, it is most recommended to create statistics in the ROOT application on a special device and distribute to the client already finished objects in the ROOT file, which are then only loaded and visualized by the client's browser.

```js
// Fragment, ktorý vytvorí histogram TH2I s rozmermi 20x20 a vyplní obsahy binov
const histo = jsroot.createHistogram('TH2I', 20, 20)
// Počítadlo
let cnt = 0;
// Cykly na zabezpečenie prechodu všetkých binov vytvoreného histogramu
for (var iy=1;iy<=20;iy++)
  for (var ix=1;ix<=20;ix++) {
    // Získanie konkrétneho objektu binu na základe jeho súradnic v histograme
    var bin = histo.getBin(ix, iy), val = 0;
    val=cnt;
    // Nastavenie hodnoty binu
    histo.setBinContent(bin, val);
    cnt+=0.1;
  }
// Nastavenie globálnych údajov histogramu
histo.fXaxis.fTitle = 'x Axis'
histo.fYaxis.fTitle = 'y Axis'
histo.fName = "You don't have a valid file path"
histo.fTitle = "This is a TH2 histogram demo"
histo.fMaximum = cnt
```

#### Create visualization


Ak už sú načítané potrebné objekty v aplikácii, tak je možné histogramy vizualizovať pomocou komponentov **NDMVR**.

If the required objects in the application have already been loaded, the histograms can be visualized using the **NDMVR** components.

```jsx
<NdmVrScene data={data} info={info} />

...

<JSrootHistogram histogram={histograms.physics} projectionAxes={['X','Y']} projections={histograms.projections} projIndex={0}/>
```

Components create everything you need. The parameters must be provided to the components as they were in the NDMVR documentation.

---

## NDMVR UI Menu

-  The provided documentation for the UI menu used in the solution is available at this address: https://gitlab.com/ndmspc/ndmvr/-/blob/main/docs/enVersion/menu.md?ref_type=heads.

---

## VR Headset Specific Topics

### Oculus console log on the VR headset
source: https://developer.oculus.com/documentation/web/browser-remote-debugging/
The guide is for the Chrome browser.


#### Software needed
**The easiest way is to Download Meta Quest Developer Hub**
- link for the windows: https://developer.oculus.com/downloads/package/oculus-developer-hub-win/
- link for the mac: https://developer.oculus.com/downloads/package/oculus-developer-hub-mac/

#### Setting up the console

The first steps are done in MQDH:

  1. Open the MQDH app
  2. Choose the right path to ADB (Android Debug Bridge)
  3. Sing in with the developer account (Ndm Spc)
  4. Make sure your account is verified (Developer mode)

Then we continue on the Headset:

  5. Connect the headset to the computer using a USB-C cable
  6. Click Allow when prompted to allow access to data.

And now back in MQDH:

  7. Add a new device in the MQDH 
  8. Choose your device (Ndm Spc)
  9. Make sure developer mode is on 

Next, we have two options to continue:
 - via USB connection 
 - via WiFi connection 
 

#### ADB over USB (Faster):
  10. In chrome browser: Open on your computer: chrome://inspect/#devices
  11. In chrome browser: Choose: Discover USB devices
  12. In chrome browser: Choose the default option

#### ADB over Wifi:
  10. Turn on ADB over Wifi in the MQDH 
  11. Add new ADB command (adb shell ip route)in the MQDH or cmd
  12. In chrome browser: Open: chrome://inspect/#devices
  13. In chrome browser: Choose: Discover network targets
  14. In chrome browser: Type the IP address from the created ADB command, a default port: 5555 


#### Note
To open web developer tools for the opened browser windows in the VR headset on the address chrome://inspect/#devices, click on the browser window you want to see and click inspect

---

## License

MIT © [NDMSPC](https://gitlab.com/ndmspc/ndmvr)
